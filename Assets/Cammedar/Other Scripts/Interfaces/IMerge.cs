﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMerge
{
    void Merge(IMerge merger);
}
