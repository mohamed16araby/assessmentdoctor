﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IConverter
{
    object ReadJson(JToken jT, JsonSerializer serializer);
}
