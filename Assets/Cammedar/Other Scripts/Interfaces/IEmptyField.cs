﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEmptyField
{
    bool IsEmpty();
}
