﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Linq;

public class DictionaryConverter : JsonConverter
{
    EntryFieldJSONConverter fieldJSONConverter;

    public DictionaryConverter()
    {
        fieldJSONConverter = new EntryFieldJSONConverter();
    }

    public override bool CanConvert(Type objectType)
    {
        return true;
    }

    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
        JObject jo = JObject.Load(reader);

        IDictionary instance = (IDictionary)Activator.CreateInstance(objectType);

        foreach (JProperty jp in jo.Properties())
        {
            Type genericType = objectType.GetGenericArguments()[0];

            if (genericType == typeof(EntryField))
            {
                instance[jp.Name.ToString()] = fieldJSONConverter.ReadJson(jp, serializer);
            }
            else
            {
                instance[jp.Name.ToString()] = jp.Value.ToObject(genericType, serializer);
            }
        }

        if (existingValue != null)
        {
            ((IMerge)instance).Merge((IMerge)existingValue);
        }

        return instance;
    }

    public override bool CanWrite => false;

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
        throw new NotImplementedException();
    }
}
