﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using Beebyte.Obfuscator;

public abstract class EntryField : IMerge, IEmptyField, ITransit
{
    [JsonProperty]
    public string title;
    [JsonProperty]
    public bool hideTitle;
    [JsonProperty, IgnoreForPatient]
    public bool hideTitleInCustomizationPanel;

    [System.Reflection.Obfuscation]
    [JsonProperty, SerializeField]
    public string linkingTag;

    public EntryField() {
        linkingTag = "";
        hideTitle = false;
        hideTitleInCustomizationPanel = false;
    }
    [SkipRename]
    public virtual string GetLinkingTag() { return linkingTag; }
    public abstract void LoadSavedData(string _title, EntryField entryField);

    public virtual bool IsEmpty() { return false; }

    public virtual void Merge(IMerge merger) { }

    public virtual object GetDataHolder() { return null; }

    [OnSerializing]
    internal void OnSerializingMethod(StreamingContext context)
    {
        linkingTag = GetLinkingTag();
    }

    public object Clone()
    {
        return this.MemberwiseClone();
    }
}