﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class Side : SerializableField<EntryField>, ITransit
{
    [JsonProperty]
    public int displayOrder;

    [JsonIgnore]
    public string name;
    
    [JsonIgnore]
    public SortedDictionary<ITransit> Tests { set; get; }

    [JsonProperty("tests")]
    public override MergeableDictionary<EntryField> childrenDictionary { get; protected set; }

    public Side() : base() { }

    public Side(MergeableDictionary<EntryField> childrenDictionary) : base(childrenDictionary)
    {
    }

    public object Clone()
    {
        return new Side()
        {
            name = name,
            displayOrder = displayOrder,
            childrenDictionary = childrenDictionary,
            Tests = Tests.Clone()
        };
    }

    public override void Merge(IMerge merger)
    {
        Side side = (Side)merger;
        //displayOrder = side.displayOrder;

        base.Merge(merger);
    }
}
