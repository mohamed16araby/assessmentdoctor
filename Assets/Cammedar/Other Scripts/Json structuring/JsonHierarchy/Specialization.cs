﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Specialization : SerializableField<Category>
{
    [JsonProperty("categories")]
    public override MergeableDictionary<Category> childrenDictionary { get; protected set; }

    public Specialization() : base() { }

    public Specialization(MergeableDictionary<Category> childrenDictionary) : base(childrenDictionary)
    {
    }
}
