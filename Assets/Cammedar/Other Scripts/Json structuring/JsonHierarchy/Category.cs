﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class Category : SerializableField<Group>
{
    [JsonProperty("groups")]
    public override MergeableDictionary<Group> childrenDictionary { get; protected set; }

    public Category() : base() { }

    public Category(MergeableDictionary<Group> childrenDictionary) : base(childrenDictionary)
    {
    }
}
