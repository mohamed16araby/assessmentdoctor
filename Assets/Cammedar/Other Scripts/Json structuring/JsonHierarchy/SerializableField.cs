﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SerializableField<T> : IMerge, IEmptyField
    where T : IMerge, IEmptyField
{
    [JsonProperty, JsonConverter(typeof(DictionaryConverter))]
    public virtual MergeableDictionary<T> childrenDictionary { get; protected set; }

    public SerializableField()
    {
        childrenDictionary = new MergeableDictionary<T>();
        this.childrenDictionary = new MergeableDictionary<T>();
    }

    public SerializableField(MergeableDictionary<T> childrenDictionary)
    {
        this.childrenDictionary = childrenDictionary;
    }

    public virtual bool IsEmpty()
    {
        bool empty = true;

        foreach (IEmptyField child in childrenDictionary.Values)
            empty &= child.IsEmpty();

        return empty;
    }

    public virtual IDictionary<string, T> GetChildDict()
    {
        return childrenDictionary;
    }

    public virtual void Merge(IMerge merger)
    {
        SerializableField<T> serializableField = (SerializableField<T>)merger;
        childrenDictionary.Merge(serializableField.childrenDictionary);
    }
}