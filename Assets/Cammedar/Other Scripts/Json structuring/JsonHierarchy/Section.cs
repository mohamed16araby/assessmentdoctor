﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class Section : SerializableField<Side>, ITransit
{
    [JsonProperty]
    public int displayOrder;

    [JsonIgnore]
    public string name;
    [JsonProperty]
    public bool hideTitle = true;
    [JsonProperty, IgnoreForPatient]
    public bool hideTitleInCustomizationPanel = true;
    [JsonIgnore]
    public int lastSideVisited_index = 0;
    [JsonIgnore]
    public SortedDictionary<ITransit> Sides { set; get; }

    [JsonProperty("sides")]
    public override MergeableDictionary<Side> childrenDictionary { get; protected set; }

    public Section() : base() { }

    public Section(MergeableDictionary<Side> childrenDictionary) : base(childrenDictionary)
    {
    }

    public bool CanGoNextInSides(int currentSidesIndex)
    {
        return Sides.SortedView.Count - 1 != currentSidesIndex;
    }

    public bool CanGoBackInSides(int currentSidesIndex)
    {
        return currentSidesIndex != 0;
    }

    public object Clone()
    {
        return new Section()
        {
            name = name,
            hideTitle = hideTitle,
            hideTitleInCustomizationPanel = hideTitleInCustomizationPanel,
            displayOrder = displayOrder,
            childrenDictionary = childrenDictionary,
            Sides = Sides.Clone()
        };
    }

    public override void Merge(IMerge merger)
    {
        Section section = (Section)merger;
        //displayOrder = section.displayOrder;
        //hideTitle = section.hideTitle;
        hideTitleInCustomizationPanel = section.hideTitleInCustomizationPanel;

        base.Merge(merger);
    }
}
