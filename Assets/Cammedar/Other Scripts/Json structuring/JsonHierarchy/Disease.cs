﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class Disease : SerializableField<Specialization>
{
    [JsonProperty("specializations")]
    public override MergeableDictionary<Specialization> childrenDictionary { get; protected set; }

    public Disease() : base() { }

    public Disease(MergeableDictionary<Specialization> childrenDictionary) : base(childrenDictionary) { }
}
