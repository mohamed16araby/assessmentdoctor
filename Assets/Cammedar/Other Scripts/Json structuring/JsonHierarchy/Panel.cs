﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.Runtime.Serialization;

public class Panel : SerializableField<Section>, ITransit
{
    [JsonProperty]
    public int displayOrder;
    [JsonProperty]
    public bool hideTitle;

    //[JsonIgnore]
    //public int scoreTotal;
    [JsonProperty, IgnoreForPatient]
    public int sectionScoreTotal;

    //[JsonIgnore]
    //public int scoreResult;
    [JsonProperty, IgnoreForDisease]
    public int sectionScoreResult;

    

    [JsonIgnore]
    public string name;
    [JsonIgnore]
    public int lastSectionVisited_index = 0;
    [JsonIgnore]
    public SortedDictionary<ITransit> Sections { set; get; }

    [JsonProperty("sections")]
    public override MergeableDictionary<Section> childrenDictionary { get; protected set; }

    public Panel() : base() { }

    public Panel(MergeableDictionary<Section> childrenDictionary) : base(childrenDictionary)
    {
    }

    public bool CanGoNextInSections(int currentSectionsIndex)
    {
        return Sections.SortedView.Count - 1 != currentSectionsIndex;
    }

    public bool CanGoBackInSections(int currentSectionsIndex)
    {
        return currentSectionsIndex != 0;
    }

    public object Clone()
    {
        return new Panel()
        {
            name = name,
            hideTitle = hideTitle,
            displayOrder = displayOrder,
            sectionScoreTotal = sectionScoreTotal,
            sectionScoreResult = sectionScoreResult,
            lastSectionVisited_index = lastSectionVisited_index,
            childrenDictionary = childrenDictionary,
            Sections = Sections.Clone()
        };
    }

    public override void Merge(IMerge merger)
    {
        Panel panel = (Panel)merger;
        //displayOrder = panel.displayOrder;
        //hideTitle = panel.hideTitle;
        sectionScoreTotal = panel.sectionScoreTotal;

        base.Merge(merger);
    }
}
