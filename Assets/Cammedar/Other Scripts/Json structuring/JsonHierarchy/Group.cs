﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Linq;
using System;

public interface ITransit : ICloneable
{

}
public class Group : SerializableField<Panel>, ITransit
{
    [JsonProperty]
    public int displayOrder;
    [JsonProperty, IgnoreForPatient]
    public bool scoreTracking = false;

    [JsonProperty, IgnoreForPatient]
    public int scoreTotal;
    [JsonProperty, IgnoreForDisease]
    public int scoreResult;

    [JsonIgnore]
    public string name;
    [JsonIgnore]
    public int lastPanelVisited_index= 0;
    [JsonIgnore]
    public SortedDictionary<ITransit> Panels { set; get; }

    [JsonProperty("panels")]
    public override MergeableDictionary<Panel> childrenDictionary { get; protected set; }

    public Group() : base() { }

    public Group(MergeableDictionary<Panel> childrenDictionary) : base(childrenDictionary)
    {
    }

    public bool CanGoNextInPanels(int currentPanelsIndex)
    {
        return Panels.SortedView.Count-1 != currentPanelsIndex;
    }

    public bool CanGoBackInPanels(int currentPanelsIndex)
    {
        return currentPanelsIndex != 0;
    }

    public object Clone()
    {
        return new Group()
        {
            name = name,
            displayOrder = displayOrder,
            scoreResult = scoreResult,
            scoreTotal = scoreTotal,
            scoreTracking = scoreTracking,
            lastPanelVisited_index = lastPanelVisited_index,
            Panels = Panels.Clone(),
            childrenDictionary = childrenDictionary
        };
    }

    public override void Merge(IMerge merger)
    {
        Group group = (Group)merger;
        //displayOrder = group.displayOrder;
        scoreTracking = group.scoreTracking;
        scoreTotal = group.scoreTotal;

        base.Merge(merger);
    }
}