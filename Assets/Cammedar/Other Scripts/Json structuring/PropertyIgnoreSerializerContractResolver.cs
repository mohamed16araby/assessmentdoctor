﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System.Reflection;
using Type = System.Type;
using Attribute = System.Attribute;
using UnityEngine;

public class PropertyIgnoreSerializerContractResolver<T> : DefaultContractResolver where T : IgnoredAttribute
{
    private readonly List<IgnoredAttribute> ignoredAttributes;
    private readonly bool ignoreEmpty;

    public PropertyIgnoreSerializerContractResolver(bool ignoreEmpty)
    {
        ignoredAttributes = new List<IgnoredAttribute>();
        this.ignoreEmpty = ignoreEmpty;
    }

    public void AddIgnoreAttributes(params IgnoredAttribute[] ignoredAttributes)
    {
        this.ignoredAttributes.AddRange(ignoredAttributes);
    }

    protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
    {
        var property = base.CreateProperty(member, memberSerialization);

        bool ignoredField = IgnoredField(member);

        property.ShouldSerialize = p =>
        {
            bool emptyField = ignoreEmpty && EmptyField(p);

            return !(ignoredField || emptyField);
        };

        return property;
    }

    bool IgnoredField(MemberInfo member)
    {
        var diseaseAttribute = member.GetCustomAttribute(typeof(IgnoredAttribute));

        return diseaseAttribute is T;
    }

    bool EmptyField(object field)
    {
        IEmptyField emptyField = field as IEmptyField;

        return emptyField != null && emptyField.IsEmpty();
    }
}