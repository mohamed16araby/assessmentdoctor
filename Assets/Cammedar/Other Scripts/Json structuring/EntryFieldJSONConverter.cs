﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System;
using System.Reflection;
using System.Linq;
using Newtonsoft.Json.Linq;

public class EntryFieldJSONConverter : IConverter
{
    Dictionary<string, Type> tagTypeDictionary = new Dictionary<string, Type>();

    public EntryFieldJSONConverter()
    {
        IEnumerable<Type> fieldTypes = Assembly.GetAssembly(typeof(EntryField)).GetTypes()
            .Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(EntryField)));
        foreach (Type type in fieldTypes)
        {
            EntryField field = Activator.CreateInstance(type) as EntryField;
            tagTypeDictionary.Add(field.GetLinkingTag(), type);
        }
    }

    public object ReadJson(JToken jt, JsonSerializer serializer)
    {
        JObject jo = jt.Values<JObject>().First();
        Type type = tagTypeDictionary[jo["linkingTag"].ToString()];

        object instance = Activator.CreateInstance(type);

        FieldInfo[] fields = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

        foreach (JProperty jp in jo.Properties())
        {
            FieldInfo field = fields.FirstOrDefault(n => jp.Name == n.Name);
            object obj = jp.Value.ToObject(field.FieldType, serializer);
            field.SetValue(instance, obj);
        }

        return instance;
    }
}
