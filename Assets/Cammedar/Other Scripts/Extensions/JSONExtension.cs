﻿using System.IO;

public class JSONExtension
{
     public static string DecryptJSON(string filePath, string key)
    {
        string EncryptedJSON = File.ReadAllText(filePath);
        string json = AES.DecryptString_Aes(EncryptedJSON, key, key, true);
        return json;
    }

}
