﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;

public class LocalFileSystem
{
    public static void PrepareDirectoryForNewFile(string fileDirectory, string fileName)
    {
        if (!Directory.Exists(fileDirectory))
            Directory.CreateDirectory(fileDirectory);

        if (File.Exists(fileDirectory + fileName))
            File.Delete(fileDirectory + fileName);
    }

    /// <summary>
    /// Read file of that name and return its content in string.
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public static string ReadFile(string fileName, string Path)
    {
        using (var fileStream = new FileStream(Path + fileName, FileMode.Open))
        {
            using (var streamReader = new StreamReader(fileStream))
            {
                return streamReader.ReadToEnd();
            }
        }
    }

    public static void Save(string fileName, string saveDataString, string path)
    {
        if (!Directory.Exists(path))
            Directory.CreateDirectory(path);

        File.WriteAllText(path + "/" + fileName + ".txt", saveDataString);
        Debug.Log("Assessment tests saved");
    }

    public static string Load(string filename, string path)
    {
        if (File.Exists(path + "/" + filename))
        {
            string saveString = File.ReadAllText(path + "/" + filename);
            return saveString;
        }
        else
            return null;
    }

    public static void Delete(string filename, string path)
    {
        if (File.Exists(path + "/" + filename))
            File.Delete(path + "/" + filename);
    }

    public static List<string> GettingSavedFiles(string path)
    {
        List<string> nameList = new List<string>();

        if (Directory.Exists(path))
        {
            string[] filesPaths = Directory.GetFiles(path);

            nameList = filesPaths.ToList().FindAll(s => s.Contains(".meta") == false);

            for (int i = 0; i < nameList.Count; i++)
            {
                string[] pathArr = nameList[i].Split('\\');
                nameList[i] = pathArr[pathArr.Length - 1];
            }
        }
        return nameList;
    }
}