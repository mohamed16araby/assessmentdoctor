﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MergeableDictionary<T> : Dictionary<string, T>, IMerge
    where T : IMerge, IEmptyField
{
    public void Merge(IMerge merger)
    {
        Dictionary<string, T> mergerDic = (Dictionary<string, T>)merger;

        foreach (var newPairs in mergerDic)
        {
            if (ContainsKey(newPairs.Key))
                this[newPairs.Key].Merge(newPairs.Value);
            else
                this[newPairs.Key] = newPairs.Value;
        }
    }
}
