﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class Dict_Extensions
{
    public static Dictionary<string, T> CloneValues<T>(this Dictionary<string, T> original)
        where T : ICloneable, IMerge
    {
        Dictionary<string, T> ret = new Dictionary<string, T>(original.Count, original.Comparer);

        foreach (KeyValuePair<string, T> entry in original)
        {
            ret.Add(entry.Key, (T)entry.Value.Clone());
        }
        return ret;
    }

    public static IDictionary MergeTo<T>(this IDictionary _new, IDictionary existing) where T : IMerge
    {
        IDictionary<string, T> newDic = CastDict(_new)
            .ToDictionary(e => (string)e.Key, e => (T)e.Value);

        IDictionary<string, T> exDic = CastDict(existing)
            .ToDictionary(e => (string)e.Key, e => (T)e.Value);

        Dictionary<string, T> mergingDic = new Dictionary<string, T>();

        foreach (var newPairs in newDic)
        {
            if (exDic.ContainsKey(newPairs.Key))
            {
                //Debug.Log("Type:" + typeof(T));
                //Debug.Log("NewType:" + exDic[newPairs.Key].Merge(newPairs.Value).GetType());
                ////Debug.Log("Value:"+exDic[newPairs.Key].Merge(newPairs.Value));

                //mergingDic[newPairs.Key] = (T)exDic[newPairs.Key].Merge(newPairs.Value);
                //exDic.Remove(newPairs.Key);
            }
            else
                mergingDic[newPairs.Key] = newPairs.Value;
        }
        foreach (var exPairs in exDic)
        {
            mergingDic[exPairs.Key] = exPairs.Value;
        }

        return mergingDic;
    }

    static IEnumerable<DictionaryEntry> CastDict(IDictionary dictionary)
    {
        foreach (DictionaryEntry entry in dictionary)
        {
            yield return entry;
        }
    }

    public static Dictionary<TKey, TValue> CloneDictionaryCloningValues<TKey, TValue>
   (Dictionary<TKey, TValue> original) where TValue : ICloneable
    {
        Dictionary<TKey, TValue> ret = new Dictionary<TKey, TValue>(original.Count,
                                                                original.Comparer);
        foreach (KeyValuePair<TKey, TValue> entry in original)
        {
            ret.Add(entry.Key, (TValue)entry.Value.Clone());
        }
        return ret;
    }
}
