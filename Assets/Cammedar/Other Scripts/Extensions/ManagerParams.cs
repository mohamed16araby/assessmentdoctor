﻿

public struct Options {
    public static bool SpecializationsList = true;
    public static bool Registration = true; // To detect for choosing SubSpecialization at registration or at After selecting patient
    public static bool YouTubeMode = false;
    public static bool ChangeSubSpecialityAtMenu = false;
}
