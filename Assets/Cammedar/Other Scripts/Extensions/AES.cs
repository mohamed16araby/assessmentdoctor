﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//namespace Aes_Example
//{
// [System.Serializable]
public class AES
    {
  
    // ====================   Encrypt   ===============================


    public static string EncryptString_Aes(string plainText, string KeyStr, string IVStr, bool CanBeNull)
        {

        byte[] Key = GetHash(KeyStr);
        byte[] IV = GetHash(IVStr);//ConvertToBytes(IVStr);
                                   // Check arguments.
        if ((plainText == null || plainText.Length <= 0) && CanBeNull)
            return "";
        if ((plainText == null || plainText.Length <= 0) && !CanBeNull)
            throw new ArgumentNullException("plainText");
        if (Key == null || Key.Length <= 0)
            throw new ArgumentNullException("Key");
        if (IV == null || IV.Length <= -1)
            throw new ArgumentNullException("IV");
        byte[] encrypted;

            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
            aesAlg.KeySize = 256;

            aesAlg.Key = Key;
           //aesAlg.GenerateIV(); 
           aesAlg.IV = IV;


           // aesAlg.BlockSize = 256;
                // Create an encryptor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

        //foreach (byte b in encrypted)
        //{
        //    Debug.Log("ivE=" + b);
        //}
        // Return the encrypted bytes from the memory stream.
         return ConvertToString(encrypted);
        // return encrypted;

    }

        //========================================================================================================




        // ====================   Decrypt   ========================================================


    public  static string DecryptString_Aes(string text, string KeyStr, string IVStr, bool CanBeNull)
    {

        byte[] cipherText = ConvertToBytes(text);
        byte[] Key = GetHash(KeyStr);
        byte[] IV = GetHash(IVStr);//ConvertToBytes(IVStr);


        if ((cipherText == null || cipherText.Length <= 0) && CanBeNull)
            return "";
        if ((cipherText == null || cipherText.Length <= 0) && !CanBeNull)
            throw new ArgumentNullException("cipherText");

        if (Key == null || Key.Length <= 0)
            throw new ArgumentNullException("Key");
        //if (IV == null || IV.Length <= 0)
            //throw new ArgumentNullException("IV");

        // Declare the string used to hold
        // the decrypted text.
        string plaintext = null;

        // Create an Aes object
        // with the specified key and IV.
        using (Aes aesAlg = Aes.Create())
        {
        aesAlg.KeySize = 256;

        aesAlg.Key = Key;
            aesAlg.IV = IV;
        //foreach (byte b in cipherText)
        //{
        //    Debug.Log("ivD=" + b);
        //}

        //   aesAlg.GenerateIV();
        //aesAlg.BlockSize = 256;

        // Create a decryptor to perform the stream transform.
        ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

            // Create the streams used for decryption.
            using (MemoryStream msDecrypt = new MemoryStream(cipherText))
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                    {

                        // Read the decrypted bytes from the decrypting stream
                        // and place them in a string.
                        plaintext = srDecrypt.ReadToEnd();
                    }
                }
            }

        }

        return plaintext;

    }



        // ========================================================================================



        //========================Convert To String ===============================================

      
        static string ConvertToString(byte[] result)
        {

           // return System.Text.Encoding.UTF8.GetString(result);
           return Convert.ToBase64String(result);
    }

        // ========================================================================================





        //========================Convert To Byte[] ===============================================

        public static byte[] ConvertToBytes(string result)
        {

           System.Text.Encoding encoding = System.Text.Encoding.UTF8;

        //  return encoding.GetBytes(result);
        return Convert.FromBase64String(result);

       // return SHA256.ComputeHash(Encoding.UTF8.GetBytes(result)) ;



    }

    public static byte[] GetHash(string inputString)
    {
        HashAlgorithm algorithm = MD5.Create();  //or use SHA256.Create();
        return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
    }

    // ========================================================================================



}


//}