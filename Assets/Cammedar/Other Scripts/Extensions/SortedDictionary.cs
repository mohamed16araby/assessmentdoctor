﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

/// <summary>
///  This dictionary gets sorted with every new entry and supports the foreach construct
///  This dictionary has custom indexing operator, which will add the key you are querying when it doesn't exist instead
///  of throwing an exception
/// </summary>
/// <typeparam name="T"></typeparam>
public class SortedDictionary<T> : IEnumerable<KeyValuePair<int, List<T>>> where T : ICloneable
{
    private readonly Dictionary<int, List<T>> _currentDictionary;
    private bool _sortedViewUpToDate;

    /// <summary>
    /// This is a list of KeyValue pairs sorted by the key
    /// this list is cached which means it will only get updated whenver you update the keys/values of the
    /// dictionary itself
    /// The list will be lazily updated
    /// </summary>
    private List<KeyValuePair<int, List<T>>> _sortedView;

    public List<KeyValuePair<int, List<T>>> SortedView
    {
        get
        {
            if (!_sortedViewUpToDate)
            {
                _sortedView = _currentDictionary.ToList();
                _sortedView = _sortedView.OrderBy(kv => kv.Key).ToList<KeyValuePair<int, List<T>>>(); //Descending Order
                _sortedViewUpToDate = true;
            }

            return _sortedView;
        }
    }

    /// <summary>
    /// A simple constructor, initalized the dictionary, and signals that the sorted view is out of date
    /// </summary>
    public SortedDictionary()
    {
        _currentDictionary = new Dictionary<int, List<T>>();
        _sortedViewUpToDate = false;
    }


    /// <summary>
    ///   The overloaded indexing operator
    ///   if the key exists then it will either return / set it according to what you call, if it doesn't it adds a new key
    ///   the returns/sets it
    ///   when setting a new key it also signals that the sorted view is out of data
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public List<T> this[int key]
    {
        get
        {
            if (!_currentDictionary.ContainsKey(key))
            {
                _currentDictionary.Add(key, new List<T>());
                _sortedViewUpToDate = false;
            }

            return _currentDictionary[key];
        }
        set
        {
            if (!_currentDictionary.ContainsKey(key))
            {
                _currentDictionary.Add(key, new List<T>());
            }

            _sortedViewUpToDate = false;
            _currentDictionary[key] = value;
        }
    }

    /// <summary>
    /// This function is used to implement the foreach functionality, it'a direct implementation of the needed
    /// IENumerbale<> interface
    /// </summary>
    /// <returns></returns>
    public IEnumerator<KeyValuePair<int, List<T>>> GetEnumerator()
    {
        return _currentDictionary.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public SortedDictionary<T> Clone() 
    {
        SortedDictionary<T> ret = new SortedDictionary<T>();
        foreach (KeyValuePair<int, List<T>> entry in _currentDictionary)
        {
            List<T> listValue = new List<T>();
            foreach (T item in entry.Value)
                listValue.Add((T)item.Clone());

            ret[entry.Key] = listValue;
        }
        return ret;
    }
}
