﻿using System.Collections.Generic;
using UnityEngine;
using Cammedar.Network;

public enum SessionFirebaseRequests {
    None,
    AddSessionData,
    EditSessionData,
    DownloadAssessmentFileFromStorage,
    
    DownloadPatientTreatGoals_ShortTerm_File,
    DownloadPatientTreatGoals_LongTerm_File,
}

struct Debugging
{
    public static bool encryptionEnabled = true;
}

struct PatientData
{
    public static string patient_UDID = "";
    public static string patient_name = "";
    public static string patient_phone = "";
}

struct ProgramGeneralData
{
    public static string currentSessionId = "";

    public static List<ManualTherapy_Algorithm> manualTherapyAlgorithms = new List<ManualTherapy_Algorithm>();
    public static List<ElectroTherapy_Algorithm> electroTherapyAlgorithms = new List<ElectroTherapy_Algorithm>();
}

struct UserData  // Use to store data at signup
{
    public static string tokenID = "";
    public static string user_UDID = "";

    public static string user_email = "";
    public static string user_password = "";

    public static bool authorized = false;
    public static bool approved = false;
    public static string first_name = "";
    public static string last_name = "";
    public static string phone_number = "";
    public static SessionsCount sessionsCount = new SessionsCount();

    public static string doctor_hospital_name = "";
    public static string doctor_hospital_id = "";
    public static string doctor_hospital_branch_name = "";
    public static string doctor_hospital_branch_id = "";
    public static string doctor_Specialization = "";
    public static string doctor_SubSpecialization = "";
    public static string doctor_SubSubSpecialization = "";

    public static string doctor_SubSpecialization_logIn = "";
    public static string doctor_SubSubSpecialization_logIn = "";

    public static string country_id = "";
    public static string city_id = "";

    public static string country_name = "";
    public static string city_name = "";
    public static string state_name = "";
    public static string state_id = "";
    public static Dictionary<string, string> states = new Dictionary<string, string>();
    public static bool has_state = false;

    public static string[] Doctor_Speciality_Sub_Subsub_Path
    {
        get
        {
            List<string> list = new List<string>();
            if (string.IsNullOrEmpty(doctor_Specialization)) return null;

            list.Add(doctor_Specialization);
            if(!string.IsNullOrEmpty(doctor_SubSpecialization_logIn))
                list.Add(doctor_SubSpecialization_logIn);
            if (!string.IsNullOrEmpty(doctor_SubSubSpecialization_logIn))
                list.Add(doctor_SubSubSpecialization_logIn);

            return list.ToArray();
        }
    }

    public static string GetLastSpeciality
    {
        get
        {
            if(string.IsNullOrEmpty(doctor_SubSubSpecialization_logIn))
            {
                if (string.IsNullOrEmpty(doctor_SubSpecialization_logIn))
                    return doctor_Specialization;
                return doctor_SubSpecialization_logIn;
            }
            return doctor_SubSubSpecialization_logIn;
        }
    }
}

struct FirebaseStoragePaths
{
    public static string StorageDirectory
    {
        get
        {
            return string.IsNullOrEmpty(UserData.doctor_SubSubSpecialization_logIn) ?
                UserData.doctor_Specialization + "/" + UserData.doctor_SubSpecialization_logIn + "/" :
                UserData.doctor_Specialization + "/" + UserData.doctor_SubSpecialization_logIn + "/" + UserData.doctor_SubSubSpecialization_logIn + "/";
        }
    }

    public static string StorageFollowupDirectory
    {
        get => UserData.doctor_Specialization + "/" + UserData.doctor_SubSpecialization_logIn + "/" + "FollowUp/";
    }
}

public static class Operations
{
    public static string[] SplitBy(string str, char splitingChar)
    {
        return str.Split(splitingChar);
    }

    public static Dictionary<string, T> Merging2Dictionaries<T>(Dictionary<string, T> first, Dictionary<string, T> second)
    {
        Dictionary<string, T> formedDictionary = second;

        foreach (string key in first.Keys)
        {
            if (!formedDictionary.ContainsKey(key))
                formedDictionary.Add(key, first[key]);
            else
                Debug.Log(string.Format("{0} already exist in voice keys", key));
        }

        return formedDictionary;
    }
}