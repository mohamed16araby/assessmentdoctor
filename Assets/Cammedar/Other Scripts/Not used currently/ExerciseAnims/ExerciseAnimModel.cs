﻿using System.Collections.Generic;
using Cammedar.Network;

[System.Serializable]
public class AssignExercise : EncryptedSerialization<AssignExercise>
{
    // From Firebase Realtime database
    public string id = "";
    public string exercise_name = "";
    public string daysPerWeek = "";
    public string notes = "";
    public string grade = "";
    public string repetitionsPerSet = "";
    public string setsPerDay = "";

    private AssignExercise()
    {

    }

    //public AssignExercise(string id, string exerciseGroupID, string exerciseCategoryID, string exercise_name, string repetitionsPerSet, string setsPerDay, string daysPerWeek, string notes)
    public AssignExercise(string id, string exercise_name, string repetitionsPerSet, string setsPerDay, string daysPerWeek, string notes, string grade)
    {
        this.id = id;
        this.exercise_name = exercise_name;
        this.daysPerWeek = daysPerWeek;
        this.notes = notes;
        this.grade = grade;
        this.repetitionsPerSet = repetitionsPerSet;
        this.setsPerDay = setsPerDay;
    }

    public override AssignExercise Decrypt(string id)
    {
        if (!Debugging.encryptionEnabled) return this;

        this.id = Decryption(this.id, id, true);
        exercise_name = Decryption(exercise_name, id, true);
        daysPerWeek = Decryption(daysPerWeek, id, true);
        notes = Decryption(notes, id, true);
        grade = Decryption(grade, id, true);
        repetitionsPerSet = Decryption(repetitionsPerSet, id, true);
        setsPerDay = Decryption(setsPerDay, id, true);

        return this;
    }

    public override AssignExercise Encrypt(string id)
    {
        if (!Debugging.encryptionEnabled) return this;

        this.id = Encryption(this.id, id, true);
        exercise_name = Encryption(exercise_name, id, true);
        daysPerWeek = Encryption(daysPerWeek, id, true);
        notes = Encryption(notes, id, true);
        grade = Encryption(grade, id, true);
        repetitionsPerSet = Encryption(repetitionsPerSet, id, true);
        setsPerDay = Encryption(setsPerDay, id, true);

        return this;
    }

    protected override string Decryption(string text, string key, bool CanBeNull)
    {
        return DecryptString_Aes(text, key, key, CanBeNull);
    }

    protected override string Encryption(string text, string key, bool CanBeNull)
    {
        return EncryptString_Aes(text, key, key, CanBeNull);
    }
}