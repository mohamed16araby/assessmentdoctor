﻿using UnityEngine;
using Beebyte.Obfuscator;

public class AnimationsManager : MonoBehaviour
{
    // Start is called before the first frame update

    private string youtubeURL = "";
    public GameObject SelectSubSpecializationPrefab;
    void Start()
    {
        Options.SpecializationsList = false;
        Options.Registration = false;
        SelectSubSpecializationPrefab.SetActive(true);
    }
    

    // Setter & getters 

    [SkipRename]
     public string YoutubeURL
    {
         get { return youtubeURL; }
         set { youtubeURL = value; }
    }

    [SkipRename]
    public string Test(){
        return "Islam";
    }

}