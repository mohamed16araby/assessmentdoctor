﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptables/Fields/GameObject Field")]
public class GameObjectField : AbstractField<GameObject>
{
    [HideInInspector]
    public bool instantiated;

    public override void OnAfterDeserialize()
    {
        base.OnAfterDeserialize();

        instantiated = false;
    }
}
