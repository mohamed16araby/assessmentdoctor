﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptables/Fields/String List Field")]
public class StringListField : AbstractField<List<string>>
{
    public override List<string> Value
    {
        get
        {
            if (runtimeValue == null)
                runtimeValue = new List<string>();
            return runtimeValue;
        }
        set
        {
            if (HasValueChanged(value))
            {
                runtimeValue = value;
                ValueHasChanged(value);
            }
        }
    }

    public override void OnAfterDeserialize()
    {
        runtimeValue = new List<string>();
    }
}
