﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FirebaseOperationStatus
{
    Idle, Inprocess, Completed, Failed
}

public class FirebaseOperation
{
    public string operationID = "";
    public FirebaseOperationStatus operationStatus;
    public string content = "";
    public string errorMssg = "";

    public FirebaseOperation()
    {
        operationID = "";
        operationStatus = FirebaseOperationStatus.Idle;
        content = "";
        errorMssg = "";
    }
    public bool IsIdle()
    {
        return operationStatus == FirebaseOperationStatus.Idle;
    }

    public bool IsInProcess()
    {
        return operationStatus == FirebaseOperationStatus.Inprocess;
    }

    public bool IsCompleted()
    {
        return operationStatus == FirebaseOperationStatus.Completed;
    }

    public bool IsFailed()
    {
        return operationStatus == FirebaseOperationStatus.Failed;
    }

    public bool ProcessFinished()
    {
        return operationStatus == FirebaseOperationStatus.Completed || operationStatus == FirebaseOperationStatus.Failed;
    }
}
