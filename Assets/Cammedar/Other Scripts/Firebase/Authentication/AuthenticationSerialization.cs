﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cammedar.Network
{

    internal class Authentication
    {
        public string email;
        public string password;
        public bool returnSecureToken = true;

        public Authentication(string email, string password)
        {
            this.email = email;
            this.password = password;
        }
    }

    internal class ResetPassword
    {
        public string email;
        public string requestType = "PASSWORD_RESET";

        public ResetPassword(string email)
        {
            this.email = email;
        }
    }

    internal class RefreshToken
    {
        public string requestType;
        public string grant_type = "refresh_token";

        public RefreshToken(string requestType)
        {
            this.requestType = requestType;
        }
    }

    internal class UserApproved
    {
        public string approved;
    }
}