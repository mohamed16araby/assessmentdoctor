﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cammedar.Network
{
    [System.Serializable]
    internal class Errors
    {
        public string domain;
        public string message;
        public string reason;
    }

    [System.Serializable]
    internal class Error
    {
        public string code;
        public string message;
        public Errors[] errors;
    }

    [System.Serializable]
    internal class AuthError
    {
        public Error error;
    }
}