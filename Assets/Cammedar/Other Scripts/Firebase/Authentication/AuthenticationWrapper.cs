﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cammedar.Network;

namespace Cammedar
{
    public static class AuthenticationWrapper
    {
        public static Cammedar.Network.Firebase Firebase = Cammedar.Network.Firebase.Instance;
        public static Doctor doctorRetrieved;

        public static void SignIn(string email, string password, System.Action<string> onSuccess, System.Action<string> onFail)
        {
            URI signInURI = Firebase.authenticationPath.Child("identitytoolkit/v3/relyingparty/verifyPassword")
                .APIKeyQuery();

            System.Action<AuthInfo> onSuccessCallback = (returnedInfo) =>
            {
                Firebase.authInfo = returnedInfo;
                RetrieveUserData(onSuccess, onFail);
            };

            Firebase.Post(signInURI, new Authentication(email, password), onSuccessCallback, onFail);
        }

        public static void RetrieveUserData(System.Action<string> onSuccess, System.Action<string> onFail)
        {
            URI uRI = Firebase.databasePath.Child("doctors").Child(Firebase.authInfo.localId).Child("doctor_Data").JSON().AuthQuery(Firebase.authInfo.idToken);

            System.Action<Doctor> onSuccessCallback = (doctor) =>
            {
                doctorRetrieved = null;
                if (doctor != null && !string.IsNullOrEmpty(doctor.email))
                {
                    doctorRetrieved = doctor;
                    onSuccess("Signed in successfully!");
                }
                else
                    onFail("User not registered, check your email or password");

            };

            Firebase.Get(uRI, onSuccessCallback, (res) => onFail("This is taking too long, try again later!"));
        }

        public static void SignUp(string email, string password, System.Action<string> onSuccess, System.Action<string> onFail)
        {
            URI signUpURI = Firebase.authenticationPath.Child("identitytoolkit/v3/relyingparty/signupNewUser")
                .APIKeyQuery();

            System.Action<AuthInfo> onSuccessCallback = (returnedInfo) =>
            {
                Firebase.authInfo = returnedInfo;
                onSuccess("");
            };

            Firebase.Post(signUpURI, new Authentication(email, password), onSuccessCallback, onFail);
        }

        public static void SendResetPasswordLink(string email, System.Action<string> onSuccess, System.Action<string> onFail)
        {
            URI resetPasswordURI = Firebase.authenticationPath.Child("identitytoolkit/v3/relyingparty/getOobConfirmationCode")
                .APIKeyQuery();


            Firebase.Post(resetPasswordURI, new ResetPassword(email), (res) => onSuccess("Reset link is sent"), onFail);
        }

        public static void RefreshToken(System.Action onSuccess, System.Action<string> onFail)
        {
            URI refreshTokenURI = Firebase.authenticationPath.Child("v1/token")
                .APIKeyQuery();

            Firebase.Post(refreshTokenURI, new RefreshToken(Firebase.authInfo.refreshToken), (res) => onSuccess(), onFail);
        }
    }
}