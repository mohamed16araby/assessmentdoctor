﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cammedar.Network
{
    [System.Serializable]
    public class AuthInfo
    {
        public string localId;
        public string email;
        public string displayName;

        //Unique key for each user session to talk to the database
        //valid for 60 minutes
        public string idToken;
        public bool registered;

        //Unique key for each user to get an another id token valid for 24 hours
        public string refreshToken;
        public string expiresIn;

        public override string ToString()
        {
            return UnityEngine.JsonUtility.ToJson(this, true);
        }
    }
}
