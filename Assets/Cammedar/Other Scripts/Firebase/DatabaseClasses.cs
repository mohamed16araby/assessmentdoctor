﻿using System.Collections.Generic;
using UnityEngine;
namespace Cammedar.Network
{
    [System.Serializable]
    public class SelectedParts : EncryptedSerialization<SelectedParts>
    {
        public Dictionary<string, List<string>> items = new Dictionary<string, List<string>>();

        public SelectedParts() { }
        public SelectedParts(SelectedParts selectedParts)
        {
            items = new Dictionary<string, List<string>>();
            foreach (KeyValuePair<string, List<string>> item in selectedParts.items)
            {
                items.Add(item.Key, new List<string>());
                foreach (string str in item.Value)
                    items[item.Key].Add(str);
            }
        }

        public override SelectedParts Decrypt(string id)
        {
            if (!Debugging.encryptionEnabled) return this;

            Dictionary<string, List<string>> items_Temp = new Dictionary<string, List<string>>();
            foreach (KeyValuePair<string, List<string>> item in items)
            {
                string keyDecryp = item.Key;
                items_Temp.Add(keyDecryp, new List<string>());
                foreach (string str in item.Value)
                    items_Temp[keyDecryp].Add(Decryption(str, id, true));
            }
            items = items_Temp;
            return this;
        }

        public override SelectedParts Encrypt(string id)
        {
            if (!Debugging.encryptionEnabled) return this;

            Dictionary<string, List<string>> items_Temp = new Dictionary<string, List<string>>();
            foreach (KeyValuePair<string, List<string>> item in items)
            {
                string keyDecryp = item.Key;
                items_Temp.Add(keyDecryp, new List<string>());
                foreach (string str in item.Value)
                {
                    items_Temp[keyDecryp].Add(Encryption(str, id, true));
                    Debug.Log(item.Key + " " + str);
                }
            }
            items = items_Temp;
            return this;
        }

        protected override string Decryption(string text, string key, bool CanBeNull)
        {
            return DecryptString_Aes(text, key, key, CanBeNull);
        }

        protected override string Encryption(string text, string key, bool CanBeNull)
        {
            return EncryptString_Aes(text, key, key, CanBeNull);
        }
    }

    [System.Serializable]
    public class StatesParent
    {
        public string has_state = "";
        public Dictionary<string, string> list = new Dictionary<string, string>();
    }

    [System.Serializable]
    public class Hospital
    {
        public string hospital_name = "";
        public Dictionary<string, string> branches = new Dictionary<string, string>();
    }

    [System.Serializable]
    public class SessionsCount : EncryptedSerialization<SessionsCount>
    {
        public string followUps;
        public string examinations;

        public SessionsCount()
        {
            followUps = "0";
            examinations = "0";
        }

        public override SessionsCount Decrypt(string id)
        {
            if (!Debugging.encryptionEnabled) return this;

            followUps = Decryption(followUps, id, true);
            examinations = Decryption(examinations, id, true);

            return this;
        }

        public override SessionsCount Encrypt(string id)
        {
            if (!Debugging.encryptionEnabled) return this;

            followUps = Encryption(followUps, id, true);
            examinations = Encryption(examinations, id, true);

            return this;
        }

        protected override string Decryption(string text, string key, bool CanBeNull)
        {
            return DecryptString_Aes(text, key, key, CanBeNull);
        }

        protected override string Encryption(string text, string key, bool CanBeNull)
        {
            return EncryptString_Aes(text, key, key, CanBeNull);
        }
    }

    [System.Serializable]
    public class SessionPreview
    {
        public string date;
        public bool isFollowup = false;
    }

    [System.Serializable]
    public class SubspecialityContainer
    {
        public bool has_subspecialities;
        public Dictionary<string, bool> subsubSpecialities = new Dictionary<string, bool>();
    }

    [System.Serializable]
    public class Patient_GeneralData : EncryptedSerialization<Patient_GeneralData>
    {
        public bool authorized = false;
        public string serial_number = "";
        public string email = "";
        public string first_name = "";
        public string middle_name = "";
        public string last_name = "";
        public string nationalID = "";
        public string gender = "";
        public string dateOfBirth = "";
        public string phoneNumber = "";
        public string country_id = "";
        public string country_name = "";

        public string state_id = "";
        public string state_name = "";
        public string city_id = "";
        public string city_name = "";

        public string address = "";
        public string zipCode = "";
        public string residence = "";

        public string maritalStatus = "";
        public string Handedness = "";
        public string occupation = "";

        public override Patient_GeneralData Decrypt(string id)
        {
            if (!Debugging.encryptionEnabled) return this;

            email = Decryption(email, id, true);
            serial_number = Decryption(serial_number, id, true);
            first_name = Decryption(first_name, id, true);
            middle_name = Decryption(middle_name, id, true);
            last_name = Decryption(last_name, id, true);
            nationalID = Decryption(nationalID, id, true);
            gender = Decryption(gender, id, true);
            dateOfBirth = Decryption(dateOfBirth, id, true);
            phoneNumber = Decryption(phoneNumber, id, true);

            country_id = Decryption(country_id, id, true);
            state_id = Decryption(state_id, id, true);
            city_id = Decryption(city_id, id, true);
            
            country_name = Decryption(country_name, id, true);
            state_name = Decryption(state_name, id, true);
            city_name = Decryption(city_name, id, true);

            address = Decryption(address, id, true);
            zipCode = Decryption(zipCode, id, true);
            residence = Decryption(residence, id, true);
            maritalStatus = Decryption(maritalStatus, id, true);
            Handedness = Decryption(Handedness, id, true);
            occupation = Decryption(occupation, id, true);

            return this;
        }

        public override Patient_GeneralData Encrypt(string id)
        {
            if (!Debugging.encryptionEnabled) return this;

            email = Encryption(email, id, true);
            serial_number = Encryption(serial_number, id, true);
            first_name = Encryption(first_name, id, true);
            middle_name = Encryption(middle_name, id, true);
            last_name = Encryption(last_name, id, true);
            nationalID = Encryption(nationalID, id, true);
            gender = Encryption(gender, id, true);
            dateOfBirth = Encryption(dateOfBirth, id, true);
            phoneNumber = Encryption(phoneNumber, id, true);

            country_id = Encryption(country_id, id, true);
            state_id = Encryption(state_id, id, true);
            city_id = Encryption(city_id, id, true);
            
            country_name = Encryption(country_name, id, true);
            state_name = Encryption(state_name, id, true);
            city_name = Encryption(city_name, id, true);

            address = Encryption(address, id, true);
            zipCode = Encryption(zipCode, id, true);
            residence = Encryption(residence, id, true);
            maritalStatus = Encryption(maritalStatus, id, true);
            Handedness = Encryption(Handedness, id, true);
            occupation = Encryption(occupation, id, true);

            return this;
        }

        protected override string Decryption(string text, string key, bool CanBeNull)
        {
            return DecryptString_Aes(text, key, key, CanBeNull);
        }

        protected override string Encryption(string text, string key, bool CanBeNull)
        {
            return EncryptString_Aes(text, key, key, CanBeNull);
        }
    }

    [System.Serializable]
    public class Patient
    {
        public Patient_GeneralData Patient_Data = new Patient_GeneralData();
    }

    [System.Serializable]
    public class Doctor : EncryptedSerialization<Doctor>
    {
        public bool approved = false;
        public bool authorized = false;
        public string email = "";
        public string first_name = "";
        public string last_name = "";
        public string hospital_name = "";
        public string hospital_id = "";
        public string hospital_branch_name = "";
        public string hospital_branch_id = "";
        public string phone_number = "";
        public string speciality = "";
        public string sub_speciality = "";
        public string sub_sub_speciality = "";
        public string country_id = "";
        public string city_id = "";
        public SessionsCount sessionsCount = new SessionsCount();

        public Doctor()
        {
        }

        public Doctor(string email, bool authorized)
        {
            this.email = email;
            this.authorized = authorized;
        }

        public override Doctor Decrypt(string id)
        {
            if (!Debugging.encryptionEnabled) return this;

            email = Decryption(email, id, true);
            first_name = Decryption(first_name, id, true);
            last_name = Decryption(last_name, id, true);
            sessionsCount.Decrypt(id);
            hospital_name = Decryption(hospital_name, id, true);
            hospital_id = Decryption(hospital_id, id, true);
            hospital_branch_name = Decryption(hospital_branch_name, id, true);
            hospital_branch_id = Decryption(hospital_branch_id, id, true);
            phone_number = Decryption(phone_number, id, true);
            speciality = Decryption(speciality, id, true);
            sub_speciality = Decryption(sub_speciality, id, true);
            sub_sub_speciality = Decryption(sub_sub_speciality, id, true);
            country_id = Decryption(country_id, id, true);
            city_id = Decryption(city_id, id, true);

            return this;
        }

        public override Doctor Encrypt(string id)
        {
            if (!Debugging.encryptionEnabled) return this;

            email = Encryption(email, id, true);
            first_name = Encryption(first_name, id, true);
            last_name = Encryption(last_name, id, true);
            sessionsCount.Encrypt(id);
            hospital_name = Encryption(hospital_name, id, true);
            hospital_id = Encryption(hospital_id, id, true);
            hospital_branch_name = Encryption(hospital_branch_name, id, true);
            hospital_branch_id = Encryption(hospital_branch_id, id, true);
            phone_number = Encryption(phone_number, id, true);
            speciality = Encryption(speciality, id, true);
            sub_speciality = Encryption(sub_speciality, id, true);
            sub_sub_speciality = Encryption(sub_sub_speciality, id, true);
            country_id = Encryption(country_id, id, true);
            city_id = Encryption(city_id, id, true);

            return this;
        }

        protected override string Decryption(string text, string key, bool CanBeNull)
        {
            return DecryptString_Aes(text, key, key, CanBeNull);
        }

        protected override string Encryption(string text, string key, bool CanBeNull)
        {
            return EncryptString_Aes(text, key, key, CanBeNull);
        }
    }

    public class EncryptString : EncryptedSerialization<string>
    {
        public string data;

        public override string Decrypt(string id)
        {
            if (!Debugging.encryptionEnabled) return data;
            data = Decryption(data, id, true);
            return data;
        }

        public override string Encrypt(string id)
        {
            if (!Debugging.encryptionEnabled) return data;
            data = Encryption(data, id, true);
            return data;
        }

        protected override string Decryption(string text, string key, bool CanBeNull)
        {
            return DecryptString_Aes(text, key, key, CanBeNull);
        }

        protected override string Encryption(string text, string key, bool CanBeNull)
        {
            return EncryptString_Aes(text, key, key, CanBeNull);
        }
    }

    [System.Serializable]
    public abstract class EncryptedSerialization<T> : AES
    {
        public abstract T Encrypt(string id);
        public abstract T Decrypt(string id);

        protected abstract string Encryption(string text, string key, bool CanBeNull);

        protected abstract string Decryption(string text, string key, bool CanBeNull);
    }

    [System.Serializable]
    public class History : EncryptedSerialization<History>
    {
        public string History_Info = "";
        public string dateOfLastEntry = "";
        public string side = "";

        public void UpdateHistory(string History_Info, string date = "")
        {
            this.History_Info = History_Info;
            dateOfLastEntry = date;
        }

        public void Clear()
        {
            History_Info = "";
            dateOfLastEntry = "";
            side = "";
        }

        public override History Decrypt(string id)
        {
            if (!Debugging.encryptionEnabled) return this;
            History_Info = Decryption(History_Info, id, true);
            dateOfLastEntry = Decryption(dateOfLastEntry, id, true);
            //side = Decryption(side, id, true);
            return this;
        }

        public override History Encrypt(string id)
        {
            if (!Debugging.encryptionEnabled) return this;
            History_Info = Encryption(History_Info, id, true);
            dateOfLastEntry = Encryption(dateOfLastEntry, id, true);
            //side = Encryption(side, id, true);
            return this;
        }

        protected override string Decryption(string text, string key, bool CanBeNull)
        {
            return DecryptString_Aes(text, key, key, CanBeNull);
        }

        protected override string Encryption(string text, string key, bool CanBeNull)
        {
            return EncryptString_Aes(text, key, key, CanBeNull);
        }
    }
}