﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cammedar.Network
{
    [System.Serializable]
    internal static class APIConfig
    {
        public static string apiKey = "AIzaSyCX_SYgDkxqDv0d4YoD5fuHsBJBp-puZF4";
        public static string authDomain = "cammedar-dynamic.firebaseapp.com";
        public static string databaseURL = "https://cammedar-dynamic.firebaseio.com";
        public static string projectId = "cammedar-dynamic";
        public static string storageBucket = "cammedar-dynamic.appspot.com";
        public static string messagingSenderId = "691878263654";
    }
}
