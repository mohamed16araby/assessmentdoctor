﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Proyecto26;
using Newtonsoft.Json.Linq;
using System.Linq;
using Newtonsoft.Json;

namespace Cammedar.Network
{
    public class Firebase
    {
        public AuthInfo authInfo;

        public URI authenticationPath { get; private set; }

        public URI databasePath { get; private set; }

        private static Firebase instance = null;

        public static Firebase Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Firebase();
                }
                return instance;
            }
        }

        private Firebase()
        {
            authenticationPath = new URI("https://www.googleapis.com");
            databasePath = new URI(APIConfig.databaseURL);
        }

        public void Get<T>(URI uri, System.Action<T> onSuccess, System.Action<string> onFail)
        {
            RequestHelper currentRequest = new RequestHelper
            {
                Uri = uri.Path,
                IgnoreHttpException = true
            };
            RestClient.Get(currentRequest, (exception, res) => ResolveResponse(exception, res, onSuccess, onFail));
        }

        public void Get(URI uri, System.Action<string> onSuccess, System.Action<string> onFail)
        {
            Get<string>(uri, onSuccess, onFail);
        }

        //public void Get(URI uri, System.Action<EncryptedSerialization> onSuccess, System.Action<string> onFail)
        //{
        //    //System.Action<EncryptedSerialization> onRecieveSuccess = (es) =>
        //    //{
        //    //    es.Decrypt(authInfo.localId);
        //    //    onSuccess(es);
        //    //};
        //    Get<EncryptedSerialization>(uri, onSuccess, onFail);
        //}



        public void Post<T, K>(URI uri, T body, System.Action<K> onSuccess, System.Action<string> onFail)
        {
            RequestHelper currentRequest = new RequestHelper
            {
                Uri = uri.Path,
                BodyString = JsonConvert.SerializeObject(body),

                IgnoreHttpException = true
            };
            Debug.Log("BODY_post: " + currentRequest.BodyString);
            RestClient.Post(currentRequest, (exception, res) => ResolveResponse(exception, res, onSuccess, onFail));
        }

        public void Post<T>(URI uri, T body, System.Action<string> onSuccess, System.Action<string> onFail)
        {

            Post<T, string>(uri, body, onSuccess, onFail);
        }



        public void Put<T, K>(URI uri, T body, System.Action<K>  onSuccess, System.Action<string> onFail)
        {
         
            RequestHelper currentRequest = new RequestHelper
            {
                Uri = uri.Path,
                BodyString = JsonConvert.SerializeObject(body),

                // Prevent http exceptions catch to view a meaningful custom message
                IgnoreHttpException = true
            };

            Debug.Log("BODY_put: " + currentRequest.BodyString);
       
            RestClient.Put(currentRequest, (exception, res) => ResolveResponse(exception, res, onSuccess, onFail));
        }



        public void Put<T>(URI uri, T body, System.Action<string> onSuccess, System.Action<string> onFail)
        {
            Put<T, string>(uri, body, onSuccess, onFail);
        }



        void ResolveResponse<T>(RequestException exception, ResponseHelper res, System.Action<T> onSuccess, System.Action<string> onFail)
        {
            string returnedText = res.Text;
            AuthError authError = null;

            try
            {
                authError = JsonConvert.DeserializeObject<AuthError>(returnedText);
            }
            catch (System.Exception ex)
            {
                Debug.Log(ex);
            }
            finally
            {
                if (authError != null && authError.error != null && authError.error.message != null)
                {
                    onFail(BeautifyMessage(authError.error.message));
                }
                else if (exception != null && (exception.IsHttpError || exception.IsNetworkError))
                {
                    onFail(BeautifyMessage(exception.Message));
                }
                else if (typeof(T) == typeof(string))
                {
                    onSuccess((T)(object)returnedText);
                }
                else
                {
                    onSuccess(JsonConvert.DeserializeObject<T>(returnedText));
                }
            }
        }

        string BeautifyMessage(string message)
        {
            return message[0] + message.Replace('_', ' ').ToLower().Substring(1);
        }

        public bool ValidateJSON(string s)
        {
            try
            {
                JToken.Parse(s);
                return true;
            }
            catch (JsonReaderException ex)
            {
                Debug.Log(ex);
                return false;
            }
        }
    }
}