﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cammedar.Network
{
    public class URI
    {
        public string Path { get; private set; }

        public URI(string path)
        {
            this.Path = path;
        }

        public URI(URI uri)
        {
            this.Path = uri.Path;
        }

        public URI Child(string child)
        {
            string path = Path + "/" + child;
            return new URI(path);
        }

        public URI JSON()
        {
            string path = Path + ".json";
            return new URI(path);
        }

        public URI Query(string query)
        {
            string path = Path;
            if (!Path.Contains("?"))
                path += "?" + query;
            else
                path += "&" + query;
            return new URI(path);
        }

        public URI APIKeyQuery()
        {
            return Query("key=" + APIConfig.apiKey);
        }

        public URI AuthQuery(string idToken)
        {
            return Query("auth=" + idToken);
        }

        public static URI operator +(URI first, string second)
        {
            return new URI(first.Path + second);
        }

        public static URI operator +(URI first, URI second)
        {
            return new URI(first.Path + second.Path);
        }
    }
}
