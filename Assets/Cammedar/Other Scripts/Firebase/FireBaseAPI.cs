﻿using UnityEngine;
using Firebase.Unity.Editor;
using Firebase.Database;
using Firebase;
using Firebase.Auth;
using System;

public class FirebaseAPI : MonoBehaviour
{
    // Start is called before the first frame update
    protected Firebase.Auth.FirebaseAuth auth;
    protected Firebase.Auth.FirebaseUser user;

    public static DatabaseReference reference;
    // 0 : still request status in progress , 1: Error response from request , 2: Sucess resoinse from request
    // We use this status flag to check finishing status of the request to update UI in other scripts
    // IMPORTANT : Reset this flag to (0) after each request
    private int RequestStatus = 0;
    public int requestStatus
    {
        get { return RequestStatus; }
        set { RequestStatus = value; }
    }

    private void Awake()
    {
        // Set up the Editor before calling into the realtime database.
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(FirebaseDatabaseData.DataBaseURL);
        // Get the root reference location of the database.
        reference = FirebaseDatabase.DefaultInstance.RootReference;
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
    }

    // Hospitals Refrences
    public static DatabaseReference Hospitals()
    {
        return FirebaseDatabase.DefaultInstance.GetReference("hospitals");
    }
    public static DatabaseReference City_hospitals(string countryID, string cityID)
    {
        return FirebaseDatabase.DefaultInstance.GetReference("hospital_branches").Child(countryID).Child(cityID);
    }
    public static DatabaseReference City_hospital_branches(string countryID, string cityID, string hospitalID)
    {
        return City_hospitals(countryID, cityID).Child(hospitalID).Child("branches");
    }
    public static DatabaseReference Hospital(string id)
    {
        return Hospitals().Child(id);
    }
    public static DatabaseReference HospitalOwners(string HospitalID)
    {
        return Hospital(HospitalID).Child("owners");
    }
    public static DatabaseReference HospitalSuperAdmins(string HospitalID)
    {
        return Hospital(HospitalID).Child("super_admins");
    }
    public static DatabaseReference HospitalBranches(string HospitalID)
    {
        return Hospital(HospitalID).Child("branches");
    }
    public static DatabaseReference HospitalBranch(string HospitalID, string HospitalBranchID)
    {
        return HospitalBranches(HospitalID).Child(HospitalBranchID);
    }
    public static DatabaseReference HospitalBranchSpecializations(string hospitalID, string hospitalBranchID)
    {
        return HospitalBranch(hospitalID, hospitalBranchID).Child("specializations");
    }
    public static DatabaseReference HospitalBranchSubSpecializations(string hospitalID, string hospitalBranchID, string specialization)
    {
        return HospitalBranchSpecializations(hospitalID,hospitalBranchID).Child(specialization);
    }
    public static DatabaseReference HospitalBranchSubSubSpecializations(string hospitalID, string hospitalBranchID, string specialization, string subSpectialization)
    {
        return HospitalBranchSubSpecializations(hospitalID,hospitalBranchID, specialization).Child(subSpectialization);
    }
    public static DatabaseReference HospitalBranchAdmins(string HospitalID, string HospitalBranchID)
    {
        return HospitalBranch(HospitalID, HospitalBranchID).Child("admins");
    }
    public static DatabaseReference HospitalBranchMiniAdmins(string HospitalID, string HospitalBranchID)
    {
        return HospitalBranch(HospitalID, HospitalBranchID).Child("mini_admins");
    }
    public static DatabaseReference HospitalBranchDoctors(string HospitalID, string HospitalBranchID)
    {
        return HospitalBranch(HospitalID, HospitalBranchID).Child("doctors");
    }
    public static DatabaseReference HospitalBranchPatients(string HospitalID, string HospitalBranchID, string specialization)
    {
        return HospitalBranch(HospitalID, HospitalBranchID).Child(specialization).Child("patients");
    }



    //////////////////////////////////////////////////////////////////////////////////////



    // Patient References
    public static DatabaseReference Patients()
    {
        return FirebaseDatabase.DefaultInstance.GetReference("patients");
    }
    public static DatabaseReference PatientSessions(string patientID, string hospitalID, string hospitalBranchID, string specialization, string subspecilization, string subsubspecilization)
    {
        return Patients().Child(patientID).Child("sessions").Child(hospitalID).Child(hospitalBranchID).Child(specialization).Child(subspecilization).Child(subsubspecilization).Child("sessions_Dates");
    }
    public static DatabaseReference PatientSession(string sessionID)
    {
        return FirebaseDatabase.DefaultInstance.GetReference("sessions").Child(sessionID);
    }
    public static DatabaseReference PatientExercisesData(string sessionID)
    {
        return PatientSession(sessionID).Child("exercises");
    }
    public static DatabaseReference PatientElectroTherapyData(string sessionID)
    {
        return PatientSession(sessionID).Child("ElectroTherapy");
    }
    public static DatabaseReference PatientProblemList(string sessionID)
    {
        return PatientSession(sessionID).Child("problemList");
    }
    public static DatabaseReference PatientTreatGoals_ShortTermList(string sessionID)
    {
        return PatientSession(sessionID).Child("TreatmentGoals_ShortTerm");
    }
    public static DatabaseReference PatientTreatGoals_LongTermList(string sessionID)
    {
        return PatientSession(sessionID).Child("TreatmentGoals_LongTerm");
    }

    //////////////////////////////////////////////////////////////////////////////////////




    // Doctor References
    public static DatabaseReference Doctors()
    {
        return FirebaseDatabase.DefaultInstance.GetReference("doctors");
    }

    public static DatabaseReference Doctor(string id)
    {
        return Doctors().Child(id);
    }

    public static DatabaseReference Doctor_Data(string id)
    {
        return Doctors().Child(id).Child("doctor_Data");
    }

    public static DatabaseReference Doctor_ManualTherapy_Algorithms(string id, params string [] paths)
    {
        DatabaseReference reference = Doctor(id).Child("algorithms_ManualTherapy");
        foreach (string path in paths)
            reference = reference.Child(path);

        return reference;
    }
    public static DatabaseReference Doctor_ElectroTherapy_Algorithms(string id, params string [] paths)
    {
        DatabaseReference reference = Doctor(id).Child("algorithms_ElectroTherapy");
        foreach (string path in paths)
            reference = reference.Child(path);

        return reference;
    }

    
    //////////////////////////////////////////////////////////////////////////////////////


    // Countries Refrences
    public static DatabaseReference Countries()
    {
        return FirebaseDatabase.DefaultInstance.GetReference("countries");
    }

    // Cities Refrences 
    public static DatabaseReference State(string countryID)
    {
        return FirebaseDatabase.DefaultInstance.GetReference("states").Child(countryID);
    }
    public static DatabaseReference Cities(string countryID)
    {
        return FirebaseDatabase.DefaultInstance.GetReference("cities").Child(countryID);
    }
    public static DatabaseReference State_Cities(string countryID, string stateID)
    {
        return FirebaseDatabase.DefaultInstance.GetReference("cities").Child(countryID).Child(stateID);
    }

    //public static string GetErrorMessage(Exception exception)
    //{
    //    Debug.Log(exception.ToString());
    //    Firebase.FirebaseException firebaseEx = exception as Firebase.FirebaseException;
    //    if (firebaseEx != null)
    //    {
    //        var errorCode = (AuthError)firebaseEx.ErrorCode;
    //        return GetErrorMessage(errorCode);
    //    }

    //    return exception.ToString();
    //}

    //private static string GetErrorMessage(AuthError errorCode)
    //{
    //    var message = "";
    //    switch (errorCode)
    //    {
    //        case AuthError.AccountExistsWithDifferentCredentials:
    //            message = "Ya existe la cuenta con credenciales diferentes";
    //            break;
    //        case AuthError.MissingPassword:
    //            message = "Hace falta el Password";
    //            break;
    //        case AuthError.WeakPassword:
    //            message = "El password es debil";
    //            break;
    //        case AuthError.WrongPassword:
    //            message = "El password es Incorrecto";
    //            break;
    //        case AuthError.EmailAlreadyInUse:
    //            message = "Ya existe la cuenta con ese correo electrónico";
    //            break;
    //        case AuthError.InvalidEmail:
    //            message = "Correo electronico invalido";
    //            break;
    //        case AuthError.MissingEmail:
    //            message = "Hace falta el correo electrónico";
    //            break;
    //        default:
    //            message = "Ocurrió un error";
    //            break;
    //    }
    //    return message;
    //}

}

struct FirebaseDatabaseData
{
    public static string DataBaseURL = "https://cammedar-dynamic.firebaseio.com/";
    //"https://cammedar-website-816de.firebaseio.com/";

}