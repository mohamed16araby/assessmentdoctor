using UnityEngine;
using System.Threading.Tasks;
using Firebase.Storage;
using System;
using System.Threading.Tasks;
using System.Threading;

public class FBStorage
{
    static Firebase.Storage.FirebaseStorage storage;
    static Firebase.Storage.StorageReference root_storage_ref;
    public float Progressvalue;
    public  bool flag;
    private static FBStorage instance = null;

    public static FBStorage Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new FBStorage();
            }
            return instance;
        }
    }

    private FBStorage()
    {
        storage = Firebase.Storage.FirebaseStorage.DefaultInstance;
        root_storage_ref = storage.GetReferenceFromUrl("gs://cammedar-dynamic.appspot.com/AllSheets");
    }

    public void UploadFile(string upload2_ref, byte[] LocalFileBytes)
    {
        root_storage_ref.Child(upload2_ref).PutBytesAsync(LocalFileBytes).ContinueWith((task) =>
        {
            if (task.IsFaulted || task.IsCanceled)
            {
                Debug.Log(task.Exception.ToString());
                // Uh-oh, an error occurred!
                Debug.Log("Not Uploaded");
                // Uh-oh, an error occurred!
            }
            else
            {
                Debug.Log("Finished uploading...");
            }
        });
    }

    public void UploadFile(string upload2_ref, string LocalFilePath)
    {
        root_storage_ref.Child(upload2_ref).PutFileAsync(LocalFilePath).ContinueWith((task) =>
        {
            if (task.IsFaulted || task.IsCanceled)
            {
                Debug.Log(task.Exception.ToString());
                Debug.Log("Not Uploaded");
                // Uh-oh, an error occurred!
            }
            else
            {
                Debug.Log("Finished uploading...");
            }
        });
    }

    public void DownloadFile(string downloadFrom_ref, string downloadTo_path, System.Action OnSuccess, System.Action OnFail)
    {
        root_storage_ref.Child(downloadFrom_ref).GetFileAsync(downloadTo_path).ContinueWith(task =>
        {
            if (!task.IsFaulted && !task.IsCanceled)
                OnSuccess();

            else
                OnFail();
        });
    }

    const long maxAllowedSize = 10 * 1024 * 1024;

    public void Download_To_ByteArray(string downloadFrom_ref, System.Action<string> OnSuccess, System.Action<string> OnFail)
    {
        root_storage_ref.Child(downloadFrom_ref).GetFileAsync(downloadFrom_ref,
            new StorageProgress<DownloadState>(state => {

            Debug.Log(String.Format(
                        "Progress: {0} of {1} bytes transferred.",
                        state.BytesTransferred,
                        state.TotalByteCount,
                        (state.BytesTransferred / state.TotalByteCount) * 100));
            AddEdit_MedicalRecord_Panel_Manager.Instance.slider.value = ((float)state.BytesTransferred / state.TotalByteCount) * 100;

        

            }),default);

        // Download in memory with a maximum allowed size of 1MB(1 * 1024 * 1024 bytes)
        root_storage_ref.Child(downloadFrom_ref).GetBytesAsync(maxAllowedSize).ContinueWith(task =>
        {
            if (task.IsFaulted || task.IsCanceled)
                OnFail(task.Exception.ToString());

            else
            {
                
                byte[] fileContentsInByte = task.Result;
                string fileContent = System.Text.Encoding.Default.GetString(fileContentsInByte);
                OnSuccess(fileContent);
                flag = true;
               
            }
        });
    }



    /// <summary>
    /// Function to check existing of file
    /// </summary>
    /// <param name="downloadFrom_ref"></param>
    /// <param name="OnSuccess"></param>
    /// <param name="OnFail"></param>
    public void FileExist_ImplementAccordingly(string downloadFrom_ref, System.Action OnSuccess, System.Action OnFail)
    {
        root_storage_ref.Child(downloadFrom_ref).GetDownloadUrlAsync().ContinueWith(task =>
        {
            if (!task.IsFaulted && !task.IsCanceled)
                OnSuccess();

            else
                OnFail();
        });

        //function onResolve(foundURL)
        //{
        //    //stuff
        //}

        //function onReject(error)
        //{
        //    console.log(error.code);
        //}
    }

    public void DeleteFile(string deleteFrom_ref, string delete_localPath)
    {
        root_storage_ref.Child(deleteFrom_ref).DeleteAsync().ContinueWith(task =>
        {
            if (!task.IsFaulted && !task.IsCanceled)
            {
                Debug.Log("File deleted.");
            }
            else
            {
                Debug.Log("File Not Found");
            }
        });
    }
}