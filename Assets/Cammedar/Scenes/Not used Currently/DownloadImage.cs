﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DownloadImage : MonoBehaviour
{
    public Image profileImage;

    public void yourMethod()
    {
        StartCoroutine(setImage("https://image.shutterstock.com/image-photo/white-transparent-leaf-on-mirror-260nw-1029171697.jpg")); //balanced parens CAS
    }

    IEnumerator setImage(string url)
    {
        Texture2D texture = profileImage.canvasRenderer.GetMaterial().mainTexture as Texture2D;

        WWW www = new WWW(url);
        yield return www;

        // calling this function with StartCoroutine solves the problem
        Debug.Log("Why on earh is this never called?");

        www.LoadImageIntoTexture(texture);
        www.Dispose();
        www = null;
    }


    // Start is called before the first frame update
    void Start()
    {
        yourMethod();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
