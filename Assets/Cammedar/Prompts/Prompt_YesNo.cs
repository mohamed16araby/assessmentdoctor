﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;
using Ricimi;

public class Prompt_YesNo : MonoBehaviour
{
    [SerializeField]
    BasicButton first_Btn;
    [SerializeField]
    BasicButton second_Btn;
    [SerializeField]
    BasicButton close_Btn;

    [SerializeField]
    Text first_Btn_Txt;
    [SerializeField]
    Text second_Btn_Txt;

    [SerializeField]
    Text headerQuestion;
    [SerializeField]
    Popup popUp;

    Action First_Btn_Action;
    Action Second_Btn_Action;
    Action Close_Btn_Action;

    private void Start()
    {
        first_Btn.onClicked.AddListener(First_Btn_Pressed);
        second_Btn.onClicked.AddListener(Second_Btn_Pressed);
        close_Btn.onClicked.AddListener(Close_Btn_Pressed);
    }
    public void Init(string headerQuestion, Action First_Btn_Action, Action Second_Btn_Action, Action Close_Btn_Action, string first_Btn_Txt, string second_Btn_Txt)
    {
        this.headerQuestion.text = headerQuestion;

        this.First_Btn_Action = First_Btn_Action;
        this.Second_Btn_Action = Second_Btn_Action;
        this.Close_Btn_Action = Close_Btn_Action;

        this.first_Btn_Txt.text = first_Btn_Txt;
        this.second_Btn_Txt.text = second_Btn_Txt;

        first_Btn.gameObject.SetActive(First_Btn_Action != null);
        second_Btn.gameObject.SetActive(Second_Btn_Action != null);
        close_Btn.gameObject.SetActive(Close_Btn_Action != null);
    }

    public void First_Btn_Pressed()
    {
        First_Btn_Action?.Invoke();
        popUp.Close();
    }

    public void Second_Btn_Pressed()
    {
        Second_Btn_Action?.Invoke();
        popUp.Close();
    }

    public void Close_Btn_Pressed()
    {
        Close_Btn_Action?.Invoke();
        popUp.Close();
    }
}