﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;
using Ricimi;
using Beebyte.Obfuscator;

public class Prompt_View : MonoBehaviour
{
    [SerializeField]
    BasicButton save_Btn;
    [SerializeField]
    BasicButton confirmBack_Btn;
    [SerializeField]
    Popup popUp;
    [SerializeField]
    Text feedbackTxt;

    [SerializeField]
    Color successColor;
    [SerializeField]
    Color failingColor;

    Action saveAction;
    Action backAction;

    public void AssignButtonsFunctions(Action saveAction, Action backAction)
    {
        this.saveAction = saveAction;
        this.backAction = backAction;
    }

    [SkipRename]
    public void Save()
    {
        saveAction();
    }

    [SkipRename]
    public void ConfirmGoingBack()
    {
        popUp.Close();
        backAction();
        CustomizePanels_Manager.Instance.ActionBack();
    }

    [SkipRename]
    public void AssessmentIsSaved()
    {
        feedbackTxt.gameObject.SetActive(true);
        feedbackTxt.text = "Data is saved successfully.";
        feedbackTxt.color = successColor;
    }

    [SkipRename]
    public void SavingAssessmentFailed()
    {
        feedbackTxt.gameObject.SetActive(true);
        feedbackTxt.text = "Something went wrong try again later!";
        feedbackTxt.color = failingColor;
    }
}
