﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;
using Ricimi;
using Beebyte.Obfuscator;

public class Prompt_PerformAnotherSession_View : MonoBehaviour
{
    [SerializeField]
    BasicButton yes_Btn;
    [SerializeField]
    BasicButton no_Btn;
    [SerializeField]
    Popup popUp;

    Action YesAction;
    Action NoAction;

    private void Start()
    {
        yes_Btn.onClicked.AddListener(Yes_PerformAnotherSession);
        no_Btn.onClicked.AddListener(No_IamDone);
    }
    public void AssignButtonsFunctions(Action yesAction, Action noAction)
    {
        this.YesAction = yesAction;
        this.NoAction = noAction;
    }

    [SkipRename]
    public void Yes_PerformAnotherSession()
    {
        popUp.Close();
        YesAction();
    }

    [SkipRename]
    public void No_IamDone()
    {
        popUp.Close();
        NoAction();
    }
}
