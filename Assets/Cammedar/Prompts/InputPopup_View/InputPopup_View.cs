﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Ricimi;

public class InputPopup_View : MonoBehaviour
{
    [SerializeField]
    Text title;
    [SerializeField]
    TMP_InputField inputField;

    [SerializeField]
    BasicButton closeButton;
    [SerializeField]
    BasicButton button;
    [SerializeField]
    Text buttonLabel;
    [SerializeField]
    TMP_Text feedback_Txt;

    [SerializeField]
    Color successColor;
    [SerializeField]
    Color failColor;

    [SerializeField]
    Popup Popup;

    public string ValueEntered
    {
        get => inputField.text;
    }

    public void OnSuccess(string message)
    {
        feedback_Txt.text = message;
        feedback_Txt.color = successColor;
    }

    public void OnFail(string message)
    {
        feedback_Txt.text = message;
        feedback_Txt.color = failColor;
    }

    public void Init(string _title, string _buttonLabel, System.Action OnButtonPressed_Action, System.Action Closing_Action)
    {
        title.text = _title;
        buttonLabel.text = _buttonLabel;
        button.onClicked.AddListener(()=> OnButtonPressed_Action());
        closeButton.onClicked.AddListener(delegate { Closing_Action(); ClosePopup(); });
    }

    void ClosePopup()
    {
        Popup.Close();
    }
}