﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Action = UnityEngine.Events.UnityAction;

public class AssessButton : MonoBehaviour
{
    [SerializeField]
    Button button;

    [Header("Arrow")]
    public Image arrow;
    public Color active;
    public Color inactive;

    public Button.ButtonClickedEvent OnClick { get => button.onClick; }

    public void UpdateInteractability(bool _Interact)
    {
        if (_Interact) EnableButton();
        else DisableButton();
    }
    private void DisableButton()
    {
        button.interactable = false;
        arrow.color = inactive;
    }
    private void EnableButton()
    {
        button.interactable = true;
        arrow.color = active;
    }
    public void AddListener(Action method)
    {
        button.onClick.AddListener(method);
    }
}