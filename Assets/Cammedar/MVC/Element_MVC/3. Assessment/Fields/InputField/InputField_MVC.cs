﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputField_MVC : Element_MVC
{
    [System.Reflection.Obfuscation]
    public InputField_Controller controller { get; private set; }
    [System.Reflection.Obfuscation]
    public InputField_Model model { get; private set; }
    public InputField_View view { get; private set; }

    public InputField_MVC() { }

    public override void Initialize(Transform mainViewCanvas)
    {
        GameObject instance = GameObject.Instantiate<GameObject>(prefab_View, mainViewCanvas);

        this.model = new InputField_Model();
        this.view = instance.GetComponent<InputField_View>();
        this.controller = new InputField_Controller(model, view);
    }
}