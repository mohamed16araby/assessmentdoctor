﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Toggles_Grid_MVC : Element_MVC
{
    [System.Reflection.Obfuscation]
    public Toggles_Controller controller { get; private set; }
    [System.Reflection.Obfuscation]
    public Toggles_Grid_Model model { get; private set; }
    public Toggles_View view { get; private set; }

    public Toggles_Grid_MVC() { }

    public override void Initialize(Transform mainViewCanvas)
    {
        GameObject instance = GameObject.Instantiate<GameObject>(prefab_View, mainViewCanvas);

        this.model = new Toggles_Grid_Model();
        this.view = instance.GetComponent<Toggles_View>();
        this.controller = new Toggles_Controller(model, view);
    }
}
