﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Toggles_GridImages_MVC : Element_MVC
{
    [System.Reflection.Obfuscation]
    public Toggles_Controller controller { get; private set; }
    [System.Reflection.Obfuscation]
    public Toggles_GridImages_Model model { get; private set; }
    public Toggles_GridDynamic_View view { get; private set; }

    public Toggles_GridImages_MVC() { }

    public override void Initialize(Transform mainViewCanvas)
    {
        GameObject instance = GameObject.Instantiate<GameObject>(prefab_View, mainViewCanvas);

        this.model = new Toggles_GridImages_Model();
        this.view = instance.GetComponent<Toggles_GridDynamic_View>();
        this.controller = new Toggles_Controller(model, view);
    }
}
