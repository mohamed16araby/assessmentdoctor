﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageField_MVC : Element_MVC
{
    [System.Reflection.Obfuscation]
    public ImageField_Controller controller { get; private set; }
    [System.Reflection.Obfuscation]
    public ImageField_Model model { get; private set; }
    public ImageField_View view { get; private set; }

    public ImageField_MVC() { }

    public override void Initialize(Transform mainViewCanvas)
    {
        GameObject instance = GameObject.Instantiate<GameObject>(prefab_View, mainViewCanvas);

        this.model = new ImageField_Model();
        this.view = instance.GetComponent<ImageField_View>();
        this.controller = new ImageField_Controller(model, view);
    }
}
