﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextField_MVC : Element_MVC
{
    [System.Reflection.Obfuscation]
    public TextField_Controller controller { get; private set; }
    [System.Reflection.Obfuscation]
    public TextField_Model model { get; private set; }
    public TextField_View view { get; private set; }

    public TextField_MVC() { }

    public override void Initialize(Transform mainViewCanvas)
    {
        GameObject instance = GameObject.Instantiate<GameObject>(prefab_View, mainViewCanvas);

        this.model = new TextField_Model();
        this.view = instance.GetComponent<TextField_View>();
        this.controller = new TextField_Controller(model, view);
    }
}
