﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SidePanel_MVC : Panel_MVC<SidePanel_View>
{
    public SidePanel_MVC() { }

    /// <summary>
    /// Instantiate model, view and controller.
    /// </summary>
    /// <param name="Content"></param>
    public override void Initialize(Transform Content)
    {
        GameObject instance = GameObject.Instantiate<GameObject>(prefab_View, Content);

        this.model = new SidePanel_Model();
        this.view = instance.GetComponent<SidePanel_View>();
        this.controller = new SidePanel_Controller(model, view);
    }
}