﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChoosingSides_Panel_MVC : Panel_MVC<ChoosingSides_Panel_View>
{
    public ChoosingSides_Panel_MVC() { }

    /// <summary>
    /// Instantiate model, view and controller.
    /// </summary>
    /// <param name="Content"></param>
    public override void Initialize(Transform Content)
    {
        GameObject instance = GameObject.Instantiate<GameObject>(prefab_View, Content);

        this.model = new ChoosingSides_Panel_Model();
        this.view = instance.GetComponent<ChoosingSides_Panel_View>();
        this.controller = new ChoosingSides_Panel_Controller(model, view);
    }
}
