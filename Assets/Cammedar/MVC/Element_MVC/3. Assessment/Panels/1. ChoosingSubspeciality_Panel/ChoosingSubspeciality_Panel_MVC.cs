﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChoosingSubspeciality_Panel_MVC : Panel_MVC<ChoosingSubspeciality_Panel_View>
{
    public ChoosingSubspeciality_Panel_MVC() { }

    /// <summary>
    /// Instantiate model, view and controller.
    /// </summary>
    /// <param name="Content"></param>
    public override void Initialize(Transform Content)
    {
        GameObject instance = GameObject.Instantiate<GameObject>(prefab_View, Content);

        this.model = new ChoosingSubspeciality_Panel_Model();
        this.view = instance.GetComponent<ChoosingSubspeciality_Panel_View>();
        this.controller = new ChoosingSubspeciality_Panel_Controller(model, view);
    }
}