﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomizePanel_MVC : Panel_MVC<CustomizePanel_View>
{
    public CustomizePanel_MVC() { }

    /// <summary>
    /// Instantiate model, view and controller.
    /// </summary>
    /// <param name="Content"></param>
    public override void Initialize(Transform Content)
    {
        GameObject instance = GameObject.Instantiate<GameObject>(prefab_View, Content);

        this.model = new CustomizePanel_Model();
        this.view = instance.GetComponent<CustomizePanel_View>();
        this.controller = new CustomizePanel_Controller(model, view);
    }
}
