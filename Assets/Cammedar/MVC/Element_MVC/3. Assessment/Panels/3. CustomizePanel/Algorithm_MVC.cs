﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Algorithm_MVC : Element_MVC
{
    public Algorithm_Controller controller { get; private set; }
    public Algorithm_Model model { get; private set; }
    public Algorithm_View view { get; private set; }

    public Algorithm_MVC() { }

    public override void Initialize(Transform _externalTransform)
    {
        GameObject instance = GameObject.Instantiate<GameObject>(prefab_View, _externalTransform);

        this.model = new Algorithm_Model();
        this.view = instance.GetComponent<Algorithm_View>();
        this.controller = new Algorithm_Controller(model, view);
    }
}
