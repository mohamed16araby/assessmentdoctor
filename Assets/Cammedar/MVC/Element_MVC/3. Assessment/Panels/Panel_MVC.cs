﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panel_MVC<T> : Element_MVC where T: Panel_2MainHeaders_View
{
    public Panel_Controller<T> controller { get; protected set; }
    public Panel_2MainHeaders_Model model { get; protected set; }
    public T view { get; protected set; }

    public Panel_MVC() { }

    /// <summary>
    /// Instantiate model, view and controller.
    /// </summary>
    /// <param name="Content"></param>
    public override void Initialize(Transform Content)
    {
    }
}
