﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainPanel_MVC : Panel_MVC<MainPanel_View>
{
    public MainPanel_MVC() { }

    /// <summary>
    /// Instantiate model, view and controller.
    /// </summary>
    /// <param name="Content"></param>
    public override void Initialize(Transform Content)
    {
        GameObject instance = GameObject.Instantiate<GameObject>(prefab_View, Content);

        this.model = new MainPanel_Model();
        this.view = instance.GetComponent<MainPanel_View>();
        this.controller = new MainPanel_Controller(model, view);
    }
}
