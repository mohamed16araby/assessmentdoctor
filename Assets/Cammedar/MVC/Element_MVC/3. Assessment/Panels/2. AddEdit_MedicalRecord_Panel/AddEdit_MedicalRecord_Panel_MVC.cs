﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddEdit_MedicalRecord_Panel_MVC : Panel_MVC<AddEdit_MedicalRecord_Panel_View>
{
    public AddEdit_MedicalRecord_Panel_MVC() { }

    /// <summary>
    /// Instantiate model, view and controller.
    /// </summary>
    /// <param name="Content"></param>
    public override void Initialize(Transform Content)
    {
        GameObject instance = GameObject.Instantiate<GameObject>(prefab_View, Content);

        this.model = new AddEdit_MedicalRecord_Panel_Model();
        this.view = instance.GetComponent<AddEdit_MedicalRecord_Panel_View>();
        this.controller = new AddEdit_MedicalRecord_Panel_Controller(model, view);
    }
}
