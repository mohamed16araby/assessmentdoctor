﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Abstract class holding view prefab of that element and abstract load function to ins
/// </summary>
public abstract class Element_MVC : MonoBehaviour
{
    /// <summary>
    /// Element view prefab
    /// </summary>
    [SerializeField]
    protected GameObject prefab_View;

    /// <summary>
    /// To instantiate model, view and controller.
    /// View is instantiated as a child to viewParent. 
    /// </summary>
    /// <param name="viewParent"></param>
    public abstract void Initialize(Transform viewParent);
}
