﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Beebyte.Obfuscator;

public enum Sides
{
    [SkipRename]
    None,
    [SkipRename]
    Right,
    [SkipRename]
    Left,
    [SkipRename]
    UpperRight,
    [SkipRename]
    UpperLeft,
    [SkipRename]
    LowerRight,
    [SkipRename]
    LowerLeft,
    [SkipRename]
    Bilateral,
}

public class ChoosingSides_Panel_Model : Panel_2MainHeaders_Model
{
    public List<string> sides_Assessment = new List<string>();
    public Sides sideToAssess;

    public override event PanelDataEvent OnPanelDataChanged;

    public ChoosingSides_Panel_Model() : base() { }
    public override void SetData(Panel _model, MonoBehaviour mono = null)
    {
        base.SetData(_model);

        ChoosingSides_Panel_Model choosingSides_Panel_Model = _model as ChoosingSides_Panel_Model;
        sides_Assessment = choosingSides_Panel_Model.sides_Assessment;

        if (sides_Assessment.Contains(Sides.None.ToString()))
            sides_Assessment.Remove(Sides.None.ToString());

        if (UserData.doctor_Specialization != "Dentistry")
        {
            if (sides_Assessment.Contains(Sides.Right.ToString()) && sides_Assessment.Contains(Sides.Left.ToString()))
                if (!sides_Assessment.Contains(Sides.Bilateral.ToString()))
                    sides_Assessment.Add(Sides.Bilateral.ToString());
        }

        if (sides_Assessment.Count > 0)
        {
            if (Enum.TryParse(sides_Assessment[0], out sideToAssess)) ;
        }
        else
            sideToAssess = Sides.None;

        OnPanelDataChanged();
    }
}
