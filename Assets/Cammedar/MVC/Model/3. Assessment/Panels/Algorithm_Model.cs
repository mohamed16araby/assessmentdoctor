﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Firebase.Database;
using fb = Cammedar.Network.Firebase;
using URI = Cammedar.Network.URI;

public enum OperationState { SUCCESS, FAILED }
enum AgorithmFirebaseRequests { Save, Delete }

public class Algorithm_Model
{
    public List<CustomizeItem> currentSelectedItems;
    private DatabaseReference FB_RootReference;

    public Dictionary<string, List<CustomizeItemSerializable>> algorithmFilesNames = new Dictionary<string, List<CustomizeItemSerializable>>();
    public Dictionary<string, List<CustomizeItemSerializable>> patientLastVisit = new Dictionary<string, List<CustomizeItemSerializable>>();

    public delegate void OperationCallBack(OperationState _state, string _message);
    public event OperationCallBack On_Save_OperationCallBackEvent;

    public delegate void AlgorithmLoaded();
    public event AlgorithmLoaded On_AlgorithmsLoadedEvent;

    public delegate void ModelInitializationEvent();
    public event ModelInitializationEvent On_LastVisitLoadedEvent;

    MonoBehaviour _mono;

    Dictionary<AgorithmFirebaseRequests, FirebaseOperation> firebaseResponses = new Dictionary<AgorithmFirebaseRequests, FirebaseOperation>
    {
        {AgorithmFirebaseRequests.Save, new FirebaseOperation(){ operationStatus = FirebaseOperationStatus.Idle}},
        { AgorithmFirebaseRequests.Delete, new FirebaseOperation(){ operationStatus = FirebaseOperationStatus.Idle}}
    };

    public void Init(List<CustomizeItem> customizeItems, Dictionary<string, List<CustomizeItemSerializable>> patientLastVisit)
    {
        _mono = CustomizePanels_Manager.Instance;

        currentSelectedItems = customizeItems;
        this.patientLastVisit = patientLastVisit;

        FB_RootReference = FirebaseDatabase.DefaultInstance.RootReference;

        LoadAlgorithmsFromDatabase();
        On_LastVisitLoadedEvent();
    }

    public void LoadAlgorithmsFromDatabase()
    {
        URI uri = fb.Instance.databasePath.Child("doctors").Child(UserData.user_UDID).Child("algorithms").Child(UserData.doctor_Specialization)
           .Child(UserData.doctor_SubSpecialization_logIn).Child(UserData.doctor_SubSubSpecialization_logIn).JSON().AuthQuery(UserData.tokenID);

        System.Action<Dictionary<string, string>> onSuccess = (algorithm) =>
        {
            Dictionary<string, List<CustomizeItemSerializable>> temp = new Dictionary<string, List<CustomizeItemSerializable>>();

            if (algorithm != null)
                foreach (KeyValuePair<string, string> keyValue in algorithm)
                {
                    //EncryptString encryptString = new EncryptString() { data = keyValue.Value };
                    List<CustomizeItemSerializable> customizeItems = 
                    //JsonConvert.DeserializeObject<List<CustomizeItemSerializable>>(encryptString.Decrypt(UserData.user_UDID));
                    JsonConvert.DeserializeObject<List<CustomizeItemSerializable>>(keyValue.Value.ToString());

                    temp.Add(keyValue.Key, customizeItems);
                }

            algorithmFilesNames = temp;

            On_AlgorithmsLoadedEvent();
        };

        fb.Instance.Get(uri, onSuccess, delegate { });
    }

    public void SaveAlgorithm_OnClick(string fileName, List<CustomizeItemSerializable> groupItems, bool _patientLastVisit)
    {
        string json = JsonConvert.SerializeObject(groupItems);
  
        string[] savePath = new string[] { };

        if (_patientLastVisit)
        {
            Session.Instance.patientVisitAlgorithm = json;
            savePath = new string[] { "sessions", ProgramGeneralData.currentSessionId, fileName };
        }
        else
            savePath = new string[] { "doctors", UserData.user_UDID, "algorithms", UserData.doctor_Specialization, UserData.doctor_SubSpecialization_logIn, UserData.doctor_SubSubSpecialization_logIn, fileName };
        AgorithmFirebaseRequests agorithmFirebaseRequests = AgorithmFirebaseRequests.Save;

        firebaseResponses[agorithmFirebaseRequests].operationStatus = FirebaseOperationStatus.Inprocess;
        _mono.StartCoroutine(CheckFirebaseProcessStatus(agorithmFirebaseRequests));

        //EncryptString encryptString = new EncryptString() { data = json };
        //SaveAlgorithmToDatabase(encryptString.Encrypt(UserData.user_UDID), agorithmFirebaseRequests, savePath); //save to cloud
        SaveAlgorithmToDatabase(json, agorithmFirebaseRequests, savePath); //save to cloud
    }

    public void DeleteAlgorithm_OnClick(string fileName)
    {
        string[] path = new string[] { "doctors", UserData.user_UDID, "algorithms", UserData.doctor_Specialization, UserData.doctor_SubSpecialization_logIn, UserData.doctor_SubSubSpecialization_logIn, fileName };

        firebaseResponses[AgorithmFirebaseRequests.Delete].operationStatus = FirebaseOperationStatus.Inprocess;
        _mono.StartCoroutine(CheckFirebaseProcessStatus(AgorithmFirebaseRequests.Delete));
        DeleteAlgorithmFromDatabase(AgorithmFirebaseRequests.Delete, path); //save to cloud
    }

    public List<CustomizeItemSerializable> LoadAlgorithm_OnClick(string _filename, bool _patientLastVisit)
    {
        if (!_patientLastVisit)
        {
            if (algorithmFilesNames.ContainsKey(_filename))
                return algorithmFilesNames[_filename];
        }
        else
           if (patientLastVisit.ContainsKey(_filename))
            return patientLastVisit[_filename];

        return new List<CustomizeItemSerializable>();
    }

    void SaveAlgorithmToDatabase(string jsonFileData, AgorithmFirebaseRequests agorithmFirebaseRequests, params string[] path)
    {
        DatabaseReference reference = FB_RootReference;
        for (int i = 0; i < path.Length; i++)
        {
            reference = reference.Child(path[i]);
        }
        reference.SetValueAsync(jsonFileData).ContinueWith(task =>
        {
            if (task.IsFaulted || task.IsCanceled)
            {
       
                if (task.IsFaulted)
                    firebaseResponses[agorithmFirebaseRequests] = new FirebaseOperation()
                    {
                        operationStatus = FirebaseOperationStatus.Failed,
                        errorMssg = task.Exception.InnerExceptions[0].InnerException.Message
                    };
                else
                    firebaseResponses[agorithmFirebaseRequests] = new FirebaseOperation() { operationStatus = FirebaseOperationStatus.Failed };
            }
            else if (task.IsCompleted)
            {
             
                firebaseResponses[agorithmFirebaseRequests] = new FirebaseOperation() { operationStatus = FirebaseOperationStatus.Completed };
            }
        });
    }

    void DeleteAlgorithmFromDatabase(AgorithmFirebaseRequests agorithmFirebaseRequests, params string[] path)
    {
        DatabaseReference reference = FB_RootReference;
        for (int i = 0; i < path.Length; i++)
            reference = reference.Child(path[i]);

        reference.RemoveValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted || task.IsCanceled)
            {
                if (task.IsFaulted)
                    firebaseResponses[agorithmFirebaseRequests] = new FirebaseOperation()
                    {
                        operationStatus = FirebaseOperationStatus.Failed,
                        errorMssg = task.Exception.InnerExceptions[0].InnerException.Message
                    };
                else
                    firebaseResponses[agorithmFirebaseRequests] = new FirebaseOperation() { operationStatus = FirebaseOperationStatus.Failed };
            }
            else if (task.IsCompleted)
            {
                firebaseResponses[agorithmFirebaseRequests] = new FirebaseOperation() { operationStatus = FirebaseOperationStatus.Completed };

            }
        });
    }

    IEnumerator CheckFirebaseProcessStatus(AgorithmFirebaseRequests agorithmFirebaseRequests)
    {

        while (!firebaseResponses[agorithmFirebaseRequests].ProcessFinished())
            yield return null;

        switch (agorithmFirebaseRequests)
        {
            case AgorithmFirebaseRequests.Save:
                if (firebaseResponses[agorithmFirebaseRequests].IsCompleted())
                {
                    firebaseResponses[agorithmFirebaseRequests].operationStatus = FirebaseOperationStatus.Idle;
                    On_Save_OperationCallBackEvent(OperationState.SUCCESS, "Algorithm is saved!");
                    LoadAlgorithmsFromDatabase();
                }
                else
                    On_Save_OperationCallBackEvent(OperationState.FAILED, firebaseResponses[agorithmFirebaseRequests].errorMssg);

                firebaseResponses[agorithmFirebaseRequests] = new FirebaseOperation();
                break;
                
            case AgorithmFirebaseRequests.Delete:
                if (firebaseResponses[agorithmFirebaseRequests].IsCompleted())
                {
                    firebaseResponses[agorithmFirebaseRequests].operationStatus = FirebaseOperationStatus.Idle;
                    LoadAlgorithmsFromDatabase();
                }

                firebaseResponses[agorithmFirebaseRequests] = new FirebaseOperation();
                break;
        }
    }
}