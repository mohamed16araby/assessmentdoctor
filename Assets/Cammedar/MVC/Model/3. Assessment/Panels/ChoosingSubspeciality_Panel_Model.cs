﻿using System.Collections.Generic;
using UnityEngine;
using fb = Cammedar.Network.Firebase;
using URI = Cammedar.Network.URI;
using Cammedar.Network;
using System.Linq;

public class ChoosingSubspeciality_Panel_Model : Panel_2MainHeaders_Model
{
    public Dictionary<string, List<string>> subspecialitiesList = new Dictionary<string, List<string>>();
    public KeyValuePair<string, string> keyValueOfSelectedChoiceUnderneath = new KeyValuePair<string, string>();

    public override event PanelDataEvent OnPanelDataChanged;

    public ChoosingSubspeciality_Panel_Model() : base() { }
    public override void SetData(Panel _model, MonoBehaviour mono = null)
    {
        base.SetData(_model);

        GetSpecialitiesFromFirebase(mono);
    }

    public void GetSpecialitiesFromFirebase(MonoBehaviour mono)
    {
        URI uri = fb.Instance.databasePath.Child("hospitals").Child(UserData.doctor_hospital_id).Child("branches").Child(UserData.doctor_hospital_branch_id)
                    .Child("specializations").Child(UserData.doctor_Specialization).JSON().AuthQuery(UserData.tokenID);

        System.Action<Dictionary<string, SubspecialityContainer>> onSuccess = (subspeciality) =>
        {
            if (subspeciality == null)
            {
                Debug.Log("There is no specialities inside:\n{0} branch\nwith{1} as id of this branch");
            }
            else
            {
                foreach (KeyValuePair<string, SubspecialityContainer> keyValue in subspeciality)
                {
                    subspecialitiesList.Add(keyValue.Key, new List<string>());
                    if (keyValue.Value.has_subspecialities)
                        subspecialitiesList[keyValue.Key] = keyValue.Value.subsubSpecialities.Keys.ToList();
                }
                OnPanelDataChanged();
            }
        };

        fb.Instance.Get(uri, onSuccess, delegate { });
    }
}