﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SidePanel_Model : Panel_4MainHeaders_Model
{
    public override event PanelDataEvent OnPanelDataChanged;

    public SidePanel_Model() : base() { }
    public override void SetData(Panel _model, MonoBehaviour mono = null)
    {
        base.SetData(_model);

        OnPanelDataChanged();
    }
}