﻿using System.Collections.Generic;
using UnityEngine;
using System;
using Beebyte.Obfuscator;
using fb = Cammedar.Network.Firebase;
using URI = Cammedar.Network.URI;
using Cammedar.Network;

public enum PerformAnotherAssessmentChoices
{
    [SkipRename]
    None,
    [SkipRename]
    NewAssessmentFromScratch,
    [SkipRename]
    Followup,
    [SkipRename]
    ReevaluateOldAssessment,
    [SkipRename]
    EditOldAssessment


}

public enum FollowupChoices
{
    [SkipRename]
    EditLastFollowup,
    [SkipRename]
    CreateFollowupFromScratch
}

public class AddEdit_MedicalRecord_Panel_Model : Panel_2MainHeaders_Model
{
    public List<PerformAnotherAssessmentChoices> performAnotherAssessmentChoices = new List<PerformAnotherAssessmentChoices>();
    public Dictionary<string, string> sessionIDwithDate = new Dictionary<string, string>();
    public KeyValuePair<string, string> lastFollowupDate = new KeyValuePair<string, string>();

    public override event PanelDataEvent OnPanelDataChanged;

    public PerformAnotherAssessmentChoices performAnotherAssessmentChoiceChoosen = PerformAnotherAssessmentChoices.None;
    public KeyValuePair<string, string> keyValueOfSelectedChoiceUnderneath = new KeyValuePair<string, string>();

    public string LastSession_Id { get; set; }

    public AddEdit_MedicalRecord_Panel_Model() : base() { }
    public override void SetData(Panel _model, MonoBehaviour mono = null)
    {
        base.SetData(_model);

        foreach (PerformAnotherAssessmentChoices performAnotherAssessment in (PerformAnotherAssessmentChoices[])Enum.GetValues(typeof(PerformAnotherAssessmentChoices)))
        {
            if (performAnotherAssessment != PerformAnotherAssessmentChoices.None)
            {
                Debug.Log(performAnotherAssessment.ToString());
                performAnotherAssessmentChoices.Add(performAnotherAssessment);
            }
        }

        GetSessionsDatesFromFirebase();
    }

    public void GetSessionsDatesFromFirebase()
    {
        string doctor_SubSpecialization_logIn = string.IsNullOrEmpty(UserData.doctor_SubSpecialization_logIn) ? UserData.doctor_SubSpecialization :
                                                        UserData.doctor_SubSpecialization_logIn;

        URI uri = fb.Instance.databasePath.Child("patients").Child(PatientData.patient_UDID).Child("sessions").Child(UserData.doctor_hospital_id).Child(UserData.doctor_hospital_branch_id)
                    .Child(UserData.doctor_Specialization).Child(doctor_SubSpecialization_logIn).Child(UserData.doctor_SubSubSpecialization_logIn)
                    .Child("sessions_Dates").JSON().AuthQuery(UserData.tokenID);

        System.Action<Dictionary<string, SessionPreview>> onSuccess = (sessionsPreview) =>
        {
            sessionIDwithDate = new Dictionary<string, string>();
            LastSession_Id = "";

            if (sessionsPreview != null)
                foreach (KeyValuePair<string, SessionPreview> session in sessionsPreview)
                {
                    if (!session.Value.isFollowup)
                        sessionIDwithDate.Add(session.Key, session.Value.date);
                    else
                        lastFollowupDate = new KeyValuePair<string, string>(session.Key, session.Value.date);

                    LastSession_Id = session.Key;
                }

            OnPanelDataChanged();
        };

        fb.Instance.Get(uri, onSuccess, delegate { });
    }
}