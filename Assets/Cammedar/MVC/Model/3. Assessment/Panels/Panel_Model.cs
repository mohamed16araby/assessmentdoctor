﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panel_2MainHeaders_Model : Panel
{
    public string groupName;
    public string panelName;

    public MonoBehaviour mono;

    public delegate void PanelDataEvent();
    public virtual event PanelDataEvent OnPanelDataChanged;

    public Panel_2MainHeaders_Model() { }

    public virtual void SetData(Panel _model, MonoBehaviour mono = null)
    {
        Panel_2MainHeaders_Model panel_Model = _model as Panel_2MainHeaders_Model;

        groupName = panel_Model.groupName;
        panelName = panel_Model.panelName;
        displayOrder = panel_Model.displayOrder;
        hideTitle = panel_Model.hideTitle;

        this.mono = mono;
    }
}

public class Panel_3MainHeaders_Model : Panel_2MainHeaders_Model
{
    public string sectionName;
    public bool hideSectionTitle;

    public Panel_3MainHeaders_Model() : base() { }
    public override void SetData(Panel _model, MonoBehaviour mono = null)
    {
        base.SetData(_model);
        Panel_3MainHeaders_Model panel_Model = _model as Panel_3MainHeaders_Model;
        sectionName = panel_Model.sectionName;
        hideSectionTitle = panel_Model.hideSectionTitle;
    }
}

public class Panel_4MainHeaders_Model : Panel_3MainHeaders_Model
{
    public string sideName;
    public SortedDictionary<ITransit> Questions { set; get; }

    public Panel_4MainHeaders_Model() : base() { }
    public override void SetData(Panel _model, MonoBehaviour mono = null)
    {
        base.SetData(_model);
        Panel_4MainHeaders_Model panel_Model = _model as Panel_4MainHeaders_Model;
        sideName = panel_Model.sideName;
        Questions = panel_Model.Questions;
    }
}
