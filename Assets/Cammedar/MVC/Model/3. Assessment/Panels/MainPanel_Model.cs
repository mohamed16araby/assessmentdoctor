﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainPanel_Model : Panel_4MainHeaders_Model
{
    public Group currentGroup;
    public Panel currentPanel;

    public bool scoreTracking = false;

    public override event PanelDataEvent OnPanelDataChanged;

    public MainPanel_Model() : base() { }
    public override void SetData(Panel _model, MonoBehaviour mono = null)
    {
        base.SetData(_model);
        MainPanel_Model panel_Model = _model as MainPanel_Model;

        scoreTracking = panel_Model.scoreTracking;
        currentGroup = panel_Model.currentGroup;
        currentPanel = panel_Model.currentPanel;
        sectionScoreTotal = currentPanel.sectionScoreTotal;
        sectionScoreResult = currentPanel.sectionScoreResult;

        OnPanelDataChanged();
    }

    public void UpdateScoresResults(int sectionScoreResult, int scoreResult)
    {
        currentPanel.sectionScoreResult = sectionScoreResult;
        currentGroup.scoreResult = scoreResult;
    }
}
