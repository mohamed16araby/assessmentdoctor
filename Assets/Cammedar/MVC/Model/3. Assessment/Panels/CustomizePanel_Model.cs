﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CustomizePanel_Model : Panel_2MainHeaders_Model
{
    public SortedDictionary<ITransit> CurrentGroups { set; get; }
    public Dictionary<CustomizeItem, ITransit> CustomizeGroup{ set; get; }

    public override event PanelDataEvent OnPanelDataChanged;

    public CustomizePanel_Model() : base() { }
    public override void SetData(Panel _model, MonoBehaviour mono = null)
    {
        base.SetData(_model);
        CustomizePanel_Model panel_Model = _model as CustomizePanel_Model;
        CurrentGroups = panel_Model.CurrentGroups;

        OnPanelDataChanged();
    }
}
