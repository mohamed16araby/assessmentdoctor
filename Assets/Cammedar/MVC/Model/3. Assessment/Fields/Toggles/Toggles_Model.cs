﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class Toggles_Model : EntryField
{
    [JsonProperty, IgnoreForPatient]
    public string descriptionToTitle;
    /// <summary>
    /// path of this model if it has been selected.
    /// </summary>
    [JsonProperty]
    public string path;

    [JsonProperty, IgnoreForPatient]
    public int number;

    [JsonProperty, IgnoreForPatient]
    public int gridColumnsCount;

    [JsonProperty, IgnoreForPatient]
    public float gridXSpace;

    [JsonProperty, IgnoreForPatient]
    public bool scoreTracking = false;

    [JsonProperty, IgnoreForPatient]
    public int scoreWeight;

    [JsonProperty, IgnoreForPatient]
    public string imageName = "";
    
    [JsonProperty, IgnoreForPatient]
    public string toggleImageName = "";

    [JsonProperty, IgnoreForPatient]
    public List<string> viewDependingOn = new List<string>();
    [JsonProperty, IgnoreForPatient]
    public bool toggleShouldBeOn_SectionsViews = true;

    [JsonProperty, IgnoreForPatient]
    public List<string> trackToggleChoice = new List<string>();

    //[JsonIgnore]
    //public string imagesPath;

    [JsonProperty, IgnoreForPatient]
    public bool singleSelection = false;

    [JsonProperty, IgnoreForPatient]
    public List<Toggles_Model> values = new List<Toggles_Model>();

    [JsonProperty, IgnoreForDisease]
    public List<Toggles_Model> selectedValues = new List<Toggles_Model>();

    public delegate void ToggleDataEvent();
    public event ToggleDataEvent OnToggleDataChanged;

    //---Set path of model and its nested values---//
    public void SettingModelPath(Toggles_Model model)
    {
        if (model.values.Count == 0)
            return;

        foreach (Toggles_Model child in model.values)
        {
            child.path = model.path + "/" + child.title;
            SettingModelPath(child);
        }
    }

    //---Intiating---//
    public override void LoadSavedData(string title, EntryField _model) //Gets Called once at the beginning of app
    {
        Toggles_Model toggle_model = _model as Toggles_Model;
        this.title = title;
        descriptionToTitle = toggle_model.descriptionToTitle;
        scoreTracking = toggle_model.scoreTracking;
        imageName = toggle_model.imageName;

        toggleImageName = toggle_model.toggleImageName;

        hideTitle = toggle_model.hideTitle;
        singleSelection = toggle_model.singleSelection;
        gridColumnsCount = toggle_model.gridColumnsCount;
        gridXSpace = toggle_model.gridXSpace;
        values = toggle_model.values;
        viewDependingOn = toggle_model.viewDependingOn;
        toggleShouldBeOn_SectionsViews = toggle_model.toggleShouldBeOn_SectionsViews;
        trackToggleChoice = toggle_model.trackToggleChoice;
        values = toggle_model.values;
        selectedValues = toggle_model.selectedValues;
        path = toggle_model.path;
        SettingModelPath(toggle_model);
        OnToggleDataChanged(); //Fire event to set the view
    }


    //---Returns parent that contains child in its selected Values and start its search from parentModel---//
    public Toggles_Model FindModelParent(Toggles_Model parentModel, Toggles_Model child, int i = 1)
    {
        int childPathLength = child.path.Split('/').Length;
        if (i >= childPathLength)
            return null;

        if (i == childPathLength - 1)
            return parentModel;

        Toggles_Model model = parentModel.selectedValues.Find(m => m.title == child.path.Split('/')[i]);
        return (model != null) ? FindModelParent(model, child, ++i) : null;
    }


    //---Adding and removing values from Selected Values---//
    public void AddValue(Toggles_Model val)
    {
        Toggles_Model returnedParent = FindModelParent(this, val);

        if (returnedParent != null && (returnedParent.selectedValues.Find(m => m.title == val.title) == null))
            returnedParent.selectedValues.Add(val);
    }
    public void RemoveValue(Toggles_Model val)
    {
        Toggles_Model returnedParent = FindModelParent(this, val);

        if (returnedParent != null && (returnedParent.selectedValues.Find(m => m.title == val.title) != null))
            returnedParent.selectedValues.RemoveAll(m => m.title == val.title);
    }

    public void ResetChoices()
    {
        selectedValues.Clear();
    }

    public override bool IsEmpty()
    {
        return selectedValues != null && selectedValues.Count == 0 && string.IsNullOrEmpty(path);
    }

    public override void Merge(IMerge merger)
    {
        Toggles_Model toggleField = (Toggles_Model)merger;
        hideTitle = toggleField.hideTitle;
        hideTitleInCustomizationPanel = toggleField.hideTitleInCustomizationPanel;
        descriptionToTitle = toggleField.descriptionToTitle;
        scoreTracking = toggleField.scoreTracking;
        values = toggleField.values;
        number = toggleField.number;
        imageName = toggleField.imageName;
        toggleImageName = toggleField.toggleImageName;
        scoreWeight = toggleField.scoreWeight;
        singleSelection = toggleField.singleSelection;
        gridColumnsCount = toggleField.gridColumnsCount;
        gridXSpace = toggleField.gridXSpace;
        viewDependingOn = toggleField.viewDependingOn;
        toggleShouldBeOn_SectionsViews = toggleField.toggleShouldBeOn_SectionsViews;
        trackToggleChoice = toggleField.trackToggleChoice;
    }

    public static bool IsTogglesModel(string linkingTag)
    {
        switch (linkingTag)
        {
            case "Toggles_Horizontal":
            case "Toggles_Horizontal_TitleInLine":
            case "Toggles_Vertical":
            case "Toggles_Grid":
            case "Toggles_Grid_Horizontal":
            case "Toggles_GridDynamic":
            case "Toggles_GridImages":
                return true;
        }

        return false;
    }
}