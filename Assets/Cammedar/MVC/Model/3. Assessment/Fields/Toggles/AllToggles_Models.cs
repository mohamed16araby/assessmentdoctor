﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Toggles_Horizontal_Model : Toggles_Model
{
    public Toggles_Horizontal_Model()
    {
        linkingTag = "Toggles_Horizontal";
    }
}

public class Toggles_Horizontal_TitleInline_Model : Toggles_Model
{
    public Toggles_Horizontal_TitleInline_Model()
    {
        linkingTag = "Toggles_Horizontal_TitleInLine";
    }
}

public class Toggles_Vertical_Model : Toggles_Model
{
    public Toggles_Vertical_Model()
    {
        linkingTag = "Toggles_Vertical";
    }
}

public class Toggles_Grid_Model : Toggles_Model
{
    public Toggles_Grid_Model()
    {
        linkingTag = "Toggles_Grid";
    }
}
public class Toggles_Grid_Horizontal_Model : Toggles_Model
{
    public Toggles_Grid_Horizontal_Model()
    {
        linkingTag = "Toggles_Grid_Horizontal";
    }
}

public class Toggles_GridDynamic_Model : Toggles_Model
{
    public Toggles_GridDynamic_Model()
    {
        linkingTag = "Toggles_GridDynamic";
    }
}

public class Toggles_GridImages_Model : Toggles_Model
{
    public Toggles_GridImages_Model()
    {
        linkingTag = "Toggles_GridImages";
    }
}