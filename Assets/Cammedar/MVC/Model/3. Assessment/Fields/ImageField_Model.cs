﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class ImageField_Model : EntryField
{
    [JsonProperty, IgnoreForPatient]
    public string imageName = "";

    public delegate void ImageFieldDataEvent();
    public event ImageFieldDataEvent ImageFieldDataChanged;

    public ImageField_Model() { linkingTag = "Image"; }

    public override bool IsEmpty()
    {
        return true;
    }

    public override void LoadSavedData(string _title, EntryField _model)
    {
        ImageField_Model textField_model = _model as ImageField_Model;

        this.title = _title;
        hideTitle = textField_model.hideTitle;
        imageName = textField_model.imageName;
        ImageFieldDataChanged();
    }

    public string GetImageName()
    {
        return imageName;
    }

    ///--to store the values that exist in one json file--//
    public override void Merge(IMerge merger)
    {
        ImageField_Model imageField = (ImageField_Model)merger;
        imageName = imageField.imageName;
        hideTitle = imageField.hideTitle;
        hideTitleInCustomizationPanel = imageField.hideTitleInCustomizationPanel;
    }
}
