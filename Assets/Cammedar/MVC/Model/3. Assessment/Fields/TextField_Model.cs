﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class TextField_Model : EntryField
{
    [JsonProperty, IgnoreForPatient]
    public string text = "";

    public delegate void InputFieldDataEvent();
    public event InputFieldDataEvent TextFieldDataChanged;

    public TextField_Model() { linkingTag = "Text"; }

    public override void LoadSavedData(string _title, EntryField _model)
    {
        TextField_Model textField_model = _model as TextField_Model;

        this.title = _title;
        hideTitle = textField_model.hideTitle;
        text = textField_model.text;
        TextFieldDataChanged();
    }

    public string GetText()
    {
        return text;
    }

    public override bool IsEmpty()
    {
        return true;
    }

    ///--to store the values that exist in one json file--//
    public override void Merge(IMerge merger)
    {
        TextField_Model textField = (TextField_Model)merger;
        text = textField.text;
        hideTitle = textField.hideTitle;
        hideTitleInCustomizationPanel = textField.hideTitleInCustomizationPanel;
    }
}