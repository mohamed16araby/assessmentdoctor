﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class InputField_Model : EntryField
{
    [JsonProperty, IgnoreForDisease]
    public List<string> value = new List<string>();
    //public string value = "";

    [JsonProperty, IgnoreForPatient]
    public string hintValue = "";

    public delegate void InputFieldDataEvent();
    public event InputFieldDataEvent InputFieldDataChanged;

    public InputField_Model() { linkingTag = "InputField_DecimalNumber"; }

    public override void LoadSavedData(string title, EntryField _model)
    {
        InputField_Model inputField_model = _model as InputField_Model;

        this.title = title;
        this.hideTitle = inputField_model.hideTitle;
        this.value = inputField_model.value;
        this.hintValue = inputField_model.hintValue;

        InputFieldDataChanged();
    }

    public override bool IsEmpty()
    {
        return value.Count == 0 || string.IsNullOrEmpty(value[0]);
    }

    public void AddValue(string val)
    {
        value.Add(val);
    }

    public void RemoveValue()
    {
        value.Clear();
    }

    public string GetHintValue()
    {
        return hintValue;
    }

    public override void Merge(IMerge merger)
    {
        InputField_Model textField = (InputField_Model)merger;
        hintValue = textField.hintValue;
        hideTitle = textField.hideTitle;
        hideTitleInCustomizationPanel = textField.hideTitleInCustomizationPanel;
    }
}