﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cammedar.Network;
using fb = Cammedar.Network.Firebase;
using URI = Cammedar.Network.URI;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Linq;
using Cammedar.Network;

public class SessionReport : EncryptedSerialization<SessionReport>
{
    public string doctor_ID = "";
    public string patient_ID = "";
    public string hospital_branch_name = "";
    public string sideOfAssessment = "";
    public string Assessment_Info = "";
    public string patientVisitAlgorithm = "";
    public string followUp = "";

    public Dictionary<string, AssignExercise> exercises = new Dictionary<string, AssignExercise>();
    public Dictionary<string, AssignExercise> ElectroTherapy = new Dictionary<string, AssignExercise>();

    public List<string> problemList = new List<string>();
    public List<string> diagnosisList = new List<string>();

    public List<string> TreatmentGoals_ShortTerm = new List<string>();
    public List<string> TreatmentGoals_LongTerm = new List<string>();

    public bool IsEmpty()
    {
        return (Assessment_Info == "{}") &&
               (followUp == "") &&
               (exercises.Count == 0) &&
               (ElectroTherapy.Count == 0) &&
               (problemList.Count == 0) &&
               (diagnosisList.Count == 0) &&
               (TreatmentGoals_ShortTerm.Count == 0) &&
               (TreatmentGoals_LongTerm.Count == 0);
    }

    public override SessionReport Decrypt(string id)
    {
        if (!Debugging.encryptionEnabled) return this;

        doctor_ID = Decryption(doctor_ID, id, true);
        patient_ID = Decryption(patient_ID, id, true);
        hospital_branch_name = Decryption(hospital_branch_name, id, true);
        sideOfAssessment = Decryption(sideOfAssessment, id, true);
        Assessment_Info = Decryption(Assessment_Info, id, true);
        followUp = Decryption(followUp, id, true);

        foreach (AssignExercise assignExercise in exercises.Values)
            assignExercise.Decrypt(id);

        foreach (AssignExercise assignExercise in ElectroTherapy.Values)
            assignExercise.Decrypt(id);

        for (int i = 0; i < problemList.Count; i++)
            problemList[i] = Decryption(problemList[i], id, true);

        for (int i = 0; i < diagnosisList.Count; i++)
            diagnosisList[i] = Decryption(diagnosisList[i], id, true);

        for (int i = 0; i < TreatmentGoals_ShortTerm.Count; i++)
            TreatmentGoals_ShortTerm[i] = Decryption(TreatmentGoals_ShortTerm[i], id, true);

        for (int i = 0; i < TreatmentGoals_LongTerm.Count; i++)
        {
            Debug.Log("before: "+ TreatmentGoals_LongTerm[i]);
            TreatmentGoals_LongTerm[i] = Decryption(TreatmentGoals_LongTerm[i], id, true);
            Debug.Log("after: " + TreatmentGoals_LongTerm[i]);

        }

        return this;
    }

    public override SessionReport Encrypt(string id)
    {
        if (!Debugging.encryptionEnabled) return this;

        doctor_ID = Encryption(doctor_ID, id, true);
        patient_ID = Encryption(patient_ID, id, true);
        hospital_branch_name = Encryption(hospital_branch_name, id, true);
        sideOfAssessment = Encryption(sideOfAssessment, id, true);
        Assessment_Info = Encryption(Assessment_Info, id, true);
        followUp = Encryption(followUp, id, true);

        foreach (AssignExercise assignExercise in exercises.Values)
            assignExercise.Encrypt(id);

        foreach (AssignExercise assignExercise in ElectroTherapy.Values)
            assignExercise.Encrypt(id);

        for (int i = 0; i < problemList.Count; i++)
            problemList[i] = Encryption(problemList[i], id, true);

        for (int i = 0; i < diagnosisList.Count; i++)
            diagnosisList[i] = Encryption(diagnosisList[i], id, true);

        for (int i = 0; i < TreatmentGoals_ShortTerm.Count; i++)
            TreatmentGoals_ShortTerm[i] = Encryption(TreatmentGoals_ShortTerm[i], id, true);

        for (int i = 0; i < TreatmentGoals_LongTerm.Count; i++)
            TreatmentGoals_LongTerm[i] = Encryption(TreatmentGoals_LongTerm[i], id, true);

        return this;
    }

    protected override string Decryption(string text, string key, bool CanBeNull)
    {
        return DecryptString_Aes(text, key, key, CanBeNull);
    }

    protected override string Encryption(string text, string key, bool CanBeNull)
    {
        return EncryptString_Aes(text, key, key, CanBeNull);
    }
}

public class HistoryData
{
    public List<string> specialitiesHolders = new List<string>();
    public History historyAnswers = new History();
    public Disease disease = null;
    public SortedDictionary<ITransit> groupDictionary = new SortedDictionary<ITransit>();

    public string fileName = "";
    public bool iscommon = false;
}

public class SessionData
{
    public string sessionID = "";
    public SessionPreview sessionPreview;
    public SessionReport sessionReport;
    public SortedDictionary<ITransit> groupDictionary = new SortedDictionary<ITransit>();
}

public class Report_Model
{
    /// <summary>
    /// key contains specialities parents of common history that is added as a value.
    /// </summary>
    private Dictionary<List<string>, History> specialities_CommonHistories = new Dictionary<List<string>, History>();
    /// <summary>
    /// key contains specialities parents of specific history that is added as a value.
    /// </summary>
    private Dictionary<List<string>, List<History>> specialities_SpecificHistories = new Dictionary<List<string>, List<History>>();

    public List<HistoryData> historyDatas = new List<HistoryData>();
    public Patient_GeneralData patientData_Retrieved;
    public SelectedParts bodyPartsComplaints = new SelectedParts();
    public Dictionary<List<string>, List<SessionData>> specialityPath_SessionData = new Dictionary<List<string>, List<SessionData>>();

    public delegate void ViewReportEvent();
    public event ViewReportEvent ViewReport;

    MonoBehaviour mono;

    public void GetPatientData(MonoBehaviour mono)
    {
        this.mono = mono;

        historyDatas = new List<HistoryData>();
        GetPatientGeneralData();
    }

    private void GetPatientGeneralData()
    {
        patientData_Retrieved = new Patient_GeneralData();
        URI patient_Data_URI = fb.Instance.databasePath.Child("patients").Child(PatientData.patient_UDID).Child("Patient_Data").JSON().AuthQuery(UserData.tokenID);

        System.Action<Patient_GeneralData> onSuccess = (patient) =>
        {
            patientData_Retrieved = patient.Decrypt(PatientData.patient_UDID);
            GetPatientHistoryData();
        };

        fb.Instance.Get(patient_Data_URI, onSuccess, delegate { });
    }

    private void GetPatientHistoryData()
    {
        URI patient_Data_URI = fb.Instance.databasePath.Child("patients").Child(PatientData.patient_UDID).Child("History_Data").JSON().AuthQuery(UserData.tokenID);

        System.Action<Dictionary<string, JObject>> onSuccess = (history) =>
        {
            List<string> path = new List<string>();

            specialities_CommonHistories = new Dictionary<List<string>, History>();
            specialities_SpecificHistories = new Dictionary<List<string>, List<History>>();

            FillCommon_and_SpecificHistories(history, path);

            foreach (KeyValuePair<List<string>, History> keyValue in specialities_CommonHistories)
                FormHistoryAnswers_Groups(new List<History> { keyValue.Value }, keyValue.Key, true);

            foreach (KeyValuePair<List<string>, List<History>> keyValue in specialities_SpecificHistories)
                FormHistoryAnswers_Groups(keyValue.Value, keyValue.Key, false);

            GetBodyPartsComplaints();
        };

        fb.Instance.Get(patient_Data_URI, onSuccess, delegate { });
    }

    void FillCommon_and_SpecificHistories(Dictionary<string, JObject> parent, List<string> path)
    {
        if (parent == null) return;

        foreach (KeyValuePair<string, JObject> keyValue in parent)
        {
           // Debug.Log(keyValue.Key);
            Dictionary<string, JObject> dict = new Dictionary<string, JObject>();
            switch (keyValue.Key)
            {
                case "CommonHistory":
                    specialities_CommonHistories.Add(path,
                        new History
                        {
                            History_Info = keyValue.Value["History_Info"].ToString(),
                            dateOfLastEntry = keyValue.Value["dateOfLastEntry"].ToString()
                        }.Decrypt(PatientData.patient_UDID));
                    break;

                case "SpecificHistory":
                    dict = JsonConvert.DeserializeObject<Dictionary<string, JObject>>(keyValue.Value.ToString());
                    foreach (KeyValuePair<string, JObject> keyValueChild in dict)
                    {
                        History history = new History
                        {
                            History_Info = keyValueChild.Value["History_Info"].ToString(),
                            dateOfLastEntry = keyValueChild.Value["dateOfLastEntry"].ToString(),
                        };

                        history.Decrypt(PatientData.patient_UDID);

                        history.side = keyValueChild.Key.ToString();

                        if (specialities_SpecificHistories.ContainsKey(path))
                            specialities_SpecificHistories[path].Add(history);
                        else
                            specialities_SpecificHistories.Add(path, new List<History> { history });
                    }
                    break;

                default:
                    dict = JsonConvert.DeserializeObject<Dictionary<string, JObject>>(keyValue.Value.ToString());
                    List<string> list = new List<string>(path);
                    list.Add(keyValue.Key);
                    FillCommon_and_SpecificHistories(dict, list);
                    break;
            }
        }
    }

    void FormHistoryAnswers_Groups(List<History> historys, List<string> spec_list, bool isCommonFile)
    {
        string fileName = isCommonFile? "CommonHistory.txt" : spec_list[spec_list.Count - 1] + " History.txt";
        
        foreach (History history in historys)
        {
            HistoryData historyData = new HistoryData
            {
                fileName = fileName,
                specialitiesHolders = spec_list,
                historyAnswers = history,
                iscommon = isCommonFile
            };
            historyData.disease = JsonConvert.DeserializeObject<Disease>(historyData.historyAnswers.History_Info); //Deserialize disease json file to disease variable in type Disease

            foreach (Specialization specialization in historyData.disease.GetChildDict().Values)
                foreach (Category category in specialization.GetChildDict().Values)
                    historyData.groupDictionary = Manager.LinkItemswithSortedDictionaryChildren(category.GetChildDict());

            historyDatas.Add(historyData);
        }
    }

    void FormAssessmentsAnswers_Groups(SessionData sessionData)
    {
        Disease assessment_Info_Structured = JsonConvert.DeserializeObject<Disease>(sessionData.sessionReport.Assessment_Info); //Deserialize disease json file to disease variable in type Disease

        foreach (Specialization specialization in assessment_Info_Structured.GetChildDict().Values)
            foreach (Category category in specialization.GetChildDict().Values)
                sessionData.groupDictionary = Manager.LinkItemswithSortedDictionaryChildren(category.GetChildDict());

    }

    void GetBodyPartsComplaints()
    {
        bodyPartsComplaints = new SelectedParts();
        URI patient_Complaints_URI = fb.Instance.databasePath.Child("patients").Child(PatientData.patient_UDID).Child("Body_Parts_Complaints").JSON().AuthQuery(UserData.tokenID);

        System.Action<SelectedParts> onSuccess = (items) =>
        {
            if (items != null)
            {
                items.Decrypt(PatientData.patient_UDID);
                bodyPartsComplaints = items;
            }
            GetSessionsPreviews_withSpecialityPath();
        };

        fb.Instance.Get(patient_Complaints_URI, onSuccess, delegate { });
    }

    void GetSessionsPreviews_withSpecialityPath()
    {
        specialityPath_SessionData = new Dictionary<List<string>, List<SessionData>>();
        URI patient_Complaints_URI = fb.Instance.databasePath.Child("patients").Child(PatientData.patient_UDID).Child("sessions")
                                                           .Child(UserData.doctor_hospital_id).JSON().AuthQuery(UserData.tokenID);
        Debug.Log("patient_Complaints_URI  " + patient_Complaints_URI);
        Debug.Log("atientData.patient_UDID  " + PatientData.patient_UDID);
        Debug.Log("UserData.doctor_hospital_id  " + UserData.doctor_hospital_id);
        Debug.Log("UserData.tokenID  " + UserData.tokenID);
        System.Action<Dictionary<string, JObject>> onSuccess = (branchID_withSpeciaities) =>
        {
            List<string> path = new List<string>();
            Debug.Log("the path is  "+"test");
            if (branchID_withSpeciaities == null)
            {
                GetSessionsInfo();
            }
            else
            {


                foreach (KeyValuePair<string, JObject> keyValue in branchID_withSpeciaities)
                {
                    Dictionary<string, JObject> dict = JsonConvert.DeserializeObject<Dictionary<string, JObject>>(keyValue.Value.ToString());
                    GetSessions_InAllBranches(dict, path);
                }

                GetSessionsInfo();
            }
        };

        fb.Instance.Get(patient_Complaints_URI, onSuccess, delegate { });
    }

    void GetSessions_InAllBranches(Dictionary<string, JObject> parent, List<string> path)
    {
        if (parent == null) return;

        foreach (KeyValuePair<string, JObject> keyValue in parent)
        {
            Dictionary<string, JObject> dict = JsonConvert.DeserializeObject<Dictionary<string, JObject>>(keyValue.Value.ToString());
            switch (keyValue.Key)
            {
                case "sessions_Dates":
                    foreach (KeyValuePair<string, JObject> keyValueChild in dict)
                    {
                        if (specialityPath_SessionData.ContainsKey(path))
                        {
                            specialityPath_SessionData[path].Add(
                                new SessionData() { sessionPreview = JsonConvert.DeserializeObject<SessionPreview>(keyValueChild.Value.ToString()), sessionID = keyValueChild.Key });
                        }
                        else
                        {
                            specialityPath_SessionData.Add(path, new List<SessionData> {
                                    new SessionData() { sessionPreview = JsonConvert.DeserializeObject<SessionPreview>(keyValueChild.Value.ToString()), sessionID = keyValueChild.Key} });
                        }
                    }
                    break;

                default:
                    List<string> list = new List<string>(path);
                    list.Add(keyValue.Key);
                    GetSessions_InAllBranches(dict, list);
                    break;
            }
        }
    }

    int noOfSessionsRetrieved = 0;
    int noOfSessionsToBeRetrieved = 0;
    void GetSessionsInfo()
    {
        noOfSessionsToBeRetrieved = specialityPath_SessionData.Sum(dict => dict.Value.Count);
        noOfSessionsRetrieved = 0;

        mono.StartCoroutine(WaitUntillSessionsGetRetrieved());
        foreach (KeyValuePair<List<string>, List<SessionData>> keyValue in specialityPath_SessionData)
            foreach (SessionData sessionData in keyValue.Value)
                GetSessionInfo_FromDatabase(keyValue.Key, sessionData);
    }

    void GetSessionInfo_FromDatabase(List<string> specialityPath, SessionData sessionData)
    {
        URI session_URI = fb.Instance.databasePath.Child("sessions").Child(sessionData.sessionID).JSON().AuthQuery(UserData.tokenID);

        System.Action<SessionReport> onSuccess = (session) =>
        {
            if (session != null)
            {
              //  Debug.Log("sessionID: " + sessionData.sessionID);
                session.Decrypt(PatientData.patient_UDID);
                sessionData.sessionReport = session;
                FormAssessmentsAnswers_Groups(sessionData);
            }
            noOfSessionsRetrieved++;
        };

        fb.Instance.Get(session_URI, onSuccess, delegate { });
    }

    IEnumerator WaitUntillSessionsGetRetrieved()
    {
        yield return new WaitUntil(() => noOfSessionsToBeRetrieved == noOfSessionsRetrieved);
        ViewReport();
    }
}