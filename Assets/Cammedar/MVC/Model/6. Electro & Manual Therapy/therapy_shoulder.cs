﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Beebyte.Obfuscator;
using Cammedar.Network;

[SkipRename]
public struct ExerciseAnimControllerStruct
{
    public static int selectedGroupIndex = 0;
    public static int selectedExerciseIndex = 0;
    public static string selectedGroupId = "";
    public static string selectedExerciseID = "";
    public static string selectedSubExerciseID = "";

}

[SkipRename]
public struct AssignedExerciseData
{
    public static string exercise_id = "", exercise_name = "", repetitionsPerSet = "", setsPerDay = "", daysPerWeek = "", notes = "", grade = "";
    public static bool hasGrades = false;
}

[SkipRename]
public struct FileData
{
    public static Therapy currentExercises = new Therapy();
}

[System.Serializable]
public class Therapy
{
    public List<therapy_shoulder_exercises> therapy = new List<therapy_shoulder_exercises>();

    public void OrderList()
    {
        therapy = therapy.OrderBy(t => t.name).ToList();
        foreach (therapy_shoulder_exercises therapy_Shoulder_Exercise in therapy)
            therapy_Shoulder_Exercise.OrderList();
    }
}

[System.Serializable]
public class therapy_shoulder_exercises
{
    public string id = "";
    public string name = "";
    public bool hasSelectedChild = false;
    public List<therapy_shoulder_exercise> exercises = new List<therapy_shoulder_exercise>();

    public void OrderList()
    {
        exercises = exercises.OrderBy(t => t.name).ToList();
        foreach (therapy_shoulder_exercise therapy_Shoulder_Exercise in exercises)
            therapy_Shoulder_Exercise.OrderList();
    }
}

[System.Serializable]
public class therapy_shoulder_exercise
{
    public string id = "";
    public string name = "";
    public string link = "";
    public bool hasGrades = false;
    public string info = "";

    public string daysPerWeek = "";
    public string setsPerDay = "";
    public string repetitionsPerSet = "";
    public string notes = "";
    public string grade = "";

    public List<therapy_shoulder_exercise> sub_exs = new List<therapy_shoulder_exercise>();

    [System.NonSerialized]
    public bool hasSelectedChild = false;
    [System.NonSerialized]
    public bool isAssigned = false;

    public void OrderList()
    {
        sub_exs = sub_exs.OrderBy(t => t.name).ToList();
        foreach (therapy_shoulder_exercise therapy_Shoulder_Exercise in sub_exs)
            therapy_Shoulder_Exercise.OrderList();
    }

    public void ResetData()
    {
        daysPerWeek = "";
        setsPerDay = "";
        repetitionsPerSet = "";
        notes = "";
        grade = "";

        isAssigned = false;
    }
}