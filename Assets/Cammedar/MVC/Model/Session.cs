﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cammedar.Network;

[System.Serializable]
public class Session : EncryptedSerialization<Session>
{
    private static Session instance = null;
    public static Session Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new Session();
            }
            return instance;
        }
    }

    public string doctor_ID = "";
    public string patient_ID = "";
    public string hospital_branch_name = "";

    public string sideOfAssessment = "";
    public string Assessment_Info = "";
    public string patientVisitAlgorithm = "";

    public Dictionary<string, AssignExercise> exercises = new Dictionary<string, AssignExercise>();
    public Dictionary<string, AssignExercise> ElectroTherapy = new Dictionary<string, AssignExercise>();

    public string followUp = "";

    public List<string> diagnosisList = new List<string>();
    public List<string> problemList = new List<string>();

    public List<string> TreatmentGoals_ShortTerm = new List<string>();
    public List<string> TreatmentGoals_LongTerm = new List<string>();

    private Session()
    {
        Clear();
    }

    public void SetSessionData(Session session)
    {
        doctor_ID = session.doctor_ID;
        patient_ID = session.patient_ID;
        sideOfAssessment = session.sideOfAssessment;
        hospital_branch_name = session.hospital_branch_name;
        Assessment_Info = session.Assessment_Info;
        patientVisitAlgorithm = session.patientVisitAlgorithm;

        exercises = session.exercises;
        ElectroTherapy = session.ElectroTherapy;

        followUp = session.followUp;

        problemList = session.problemList;
        diagnosisList = session.diagnosisList;
        TreatmentGoals_ShortTerm = session.TreatmentGoals_ShortTerm;
        TreatmentGoals_LongTerm = session.TreatmentGoals_LongTerm;
    }

    /// <summary>
    /// Clear assessmentInfo, patientVisitAlgorithm data and sideOfAssessment,
    /// Data that are uncommon between normal session and followup session
    /// </summary>
    public void ClearUncommonData()
    {
        sideOfAssessment = "";
        Assessment_Info = "";
        patientVisitAlgorithm = "";

        problemList = new List<string>();
        diagnosisList = new List<string>();
        TreatmentGoals_ShortTerm = new List<string>();
        TreatmentGoals_LongTerm = new List<string>();
    }

    public void Clear()
    {
        doctor_ID = "";
        patient_ID = "";
        sideOfAssessment = "";
        hospital_branch_name = "";
        Assessment_Info = "";
        patientVisitAlgorithm = "";

        exercises = new Dictionary<string, AssignExercise>();
        ElectroTherapy = new Dictionary<string, AssignExercise>();
        followUp = "";

        problemList = new List<string>();
        diagnosisList = new List<string>();
        TreatmentGoals_ShortTerm = new List<string>();
        TreatmentGoals_LongTerm = new List<string>();
    }

    public override Session Decrypt(string id)
    {
        if (!Debugging.encryptionEnabled) return this;

        doctor_ID = Decryption(doctor_ID, id, true);
        patient_ID = Decryption(patient_ID, id, true);
        hospital_branch_name = Decryption(hospital_branch_name, id, true);
        sideOfAssessment = Decryption(sideOfAssessment, id, true);
        Assessment_Info = Decryption(Assessment_Info, id, true);
        followUp = Decryption(followUp, id, true);
        //patientVisitAlgorithm = Decryption(patientVisitAlgorithm, id, true);

        foreach (AssignExercise assignExercise in exercises.Values)
            assignExercise.Decrypt(id);

        foreach (AssignExercise assignExercise in ElectroTherapy.Values)
            assignExercise.Decrypt(id);

        for (int i = 0; i < diagnosisList.Count; i++)
            diagnosisList[i] = Decryption(diagnosisList[i], id, true);

        for (int i = 0; i < problemList.Count; i++)
            problemList[i] = Decryption(problemList[i], id, true);

        for (int i = 0; i < TreatmentGoals_ShortTerm.Count; i++)
            TreatmentGoals_ShortTerm[i] = Decryption(TreatmentGoals_ShortTerm[i], id, true);

        for (int i = 0; i < TreatmentGoals_LongTerm.Count; i++)
            TreatmentGoals_LongTerm[i] = Decryption(TreatmentGoals_LongTerm[i], id, true);

        return this;
    }

    public override Session Encrypt(string id)
    {
        if (!Debugging.encryptionEnabled) return this;

        doctor_ID = Encryption(doctor_ID, id, true);
        patient_ID = Encryption(patient_ID, id, true);
        hospital_branch_name = Encryption(hospital_branch_name, id, true);
        sideOfAssessment = Encryption(sideOfAssessment, id, true);
        Assessment_Info = Encryption(Assessment_Info, id, true);
        followUp = Encryption(followUp, id, true);
        //patientVisitAlgorithm = Encryption(patientVisitAlgorithm, id, true);

        foreach (AssignExercise assignExercise in exercises.Values)
            assignExercise.Encrypt(id);

        foreach (AssignExercise assignExercise in ElectroTherapy.Values)
            assignExercise.Encrypt(id);

        for (int i = 0; i < problemList.Count; i++)
            problemList[i] = Encryption(problemList[i], id, true);

        for (int i = 0; i < diagnosisList.Count; i++)
            diagnosisList[i] = Encryption(diagnosisList[i], id, true);

        for (int i = 0; i < TreatmentGoals_ShortTerm.Count; i++)
            TreatmentGoals_ShortTerm[i] = Encryption(TreatmentGoals_ShortTerm[i], id, true);

        for (int i = 0; i < TreatmentGoals_LongTerm.Count; i++)
            TreatmentGoals_LongTerm[i] = Encryption(TreatmentGoals_LongTerm[i], id, true);

        return this;
    }

    public static Dictionary<string, AssignExercise> EncryptDictOfExercises(Dictionary<string, AssignExercise> original_Dict)
    {
        Dictionary<string, AssignExercise> exercises = new Dictionary<string, AssignExercise>();

        foreach (KeyValuePair<string, AssignExercise> exercise in original_Dict)
        {
            AssignExercise assignExercise = new AssignExercise(exercise.Value.id, exercise.Value.exercise_name, 
                                                               exercise.Value.repetitionsPerSet, exercise.Value.setsPerDay, 
                                                               exercise.Value.daysPerWeek, exercise.Value.notes, 
                                                               exercise.Value.grade);
            assignExercise.Encrypt(PatientData.patient_UDID);
            exercises.Add(exercise.Key, assignExercise);
        }

        return exercises;
    }

    public static Dictionary<string, AssignExercise> DecryptDictOfExercises(Dictionary<string, AssignExercise> original_Dict)
    {
        Dictionary<string, AssignExercise> exercises = new Dictionary<string, AssignExercise>();

        foreach (KeyValuePair<string, AssignExercise> exercise in original_Dict)
        {
            AssignExercise assignExercise = new AssignExercise(exercise.Value.id, exercise.Value.exercise_name, 
                                                               exercise.Value.repetitionsPerSet, exercise.Value.setsPerDay, 
                                                               exercise.Value.daysPerWeek, exercise.Value.notes, 
                                                               exercise.Value.grade);
            assignExercise.Decrypt(PatientData.patient_UDID);
            exercises.Add(exercise.Key, assignExercise);
        }

        return exercises;
    }

    protected override string Decryption(string text, string key, bool CanBeNull)
    {
        return DecryptString_Aes(text, key, key, CanBeNull);
    }

    protected override string Encryption(string text, string key, bool CanBeNull)
    {
        return EncryptString_Aes(text, key, key, CanBeNull);
    }
}