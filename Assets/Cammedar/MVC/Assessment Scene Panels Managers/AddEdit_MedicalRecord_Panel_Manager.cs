﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Beebyte.Obfuscator;
using UnityEngine.UI;
public class AddEdit_MedicalRecord_Panel_Manager : MonoBehaviour
{
    private static AddEdit_MedicalRecord_Panel_Manager instance;
    public Slider slider;
    public GameObject SliderObject;
    public static AddEdit_MedicalRecord_Panel_Manager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<AddEdit_MedicalRecord_Panel_Manager>();
            return instance;
        }
    }
    public AddEdit_MedicalRecord_Panel_MVC addEdit_MedicalRecord_Panel_MVC;

    [SerializeField]
    Canvas addEdit_MedicalRecordHolder;

    public bool isFollowupSession = false;
    public GameObject gRP_ManagerBase;
    public GameVoiceControl gameVoiceControl;
    public GameObject gameVoiceControlSubSub;
    public GameObject gameVoiceControlAddedit;
    public GameObject VoiceCustomization;
    public Sprite Audioon;
    public Sprite AudioOff;
    public Button AudioButton;
    public static bool flag = false;
    private void Start()
    {
        AddEdit_MedicalRecord_Panel_Visibility(false);
    }
    public void DisactiveSlider()
    {

        SliderObject.SetActive(false);
    }
    void LoadAddEdit_MedicalRecord_Panel()
    {

        addEdit_MedicalRecord_Panel_MVC.Initialize(addEdit_MedicalRecordHolder.transform);

        AddEdit_MedicalRecord_Panel_Model panel_Model = new AddEdit_MedicalRecord_Panel_Model();
        panel_Model.groupName = "Patient Assessments";
        panel_Model.panelName = "Add/Edit assessment?";

        AddEdit_MedicalRecord_Panel_Controller addEdit_MedicalRecord_Panel_Controller = addEdit_MedicalRecord_Panel_MVC.controller as AddEdit_MedicalRecord_Panel_Controller;
        addEdit_MedicalRecord_Panel_Controller.SetData(panel_Model, this);

    }
    [SkipRename]
    public void FVoiceController()
    {

        if (AudioButton.GetComponent<Image>().sprite == Audioon)
        {
            gameVoiceControl.onStopListening();
            AudioButton.GetComponent<Image>().sprite = AudioOff;
        }
        else
        {
            gameVoiceControl.onStartListening();
            AudioButton.GetComponent<Image>().sprite = Audioon;
        }
    }

    KeyValuePair<PerformAnotherAssessmentChoices, KeyValuePair<string, string>> GetPerformAnotherAssessmentChoiceChoosen()
    {
        AddEdit_MedicalRecord_Panel_Controller addEdit_MedicalRecord_Panel_Controller = addEdit_MedicalRecord_Panel_MVC.controller as AddEdit_MedicalRecord_Panel_Controller;
        return addEdit_MedicalRecord_Panel_Controller.GetAddEdit_MedicalRecordChoice();
    }

    [SkipRename]
    public void DisablePanel()
    {
        AddEdit_MedicalRecord_Panel_Visibility(false);
    }

    public void PrepareAssessment(KeyValuePair<PerformAnotherAssessmentChoices, KeyValuePair<string, string>> AssessmentChoiceChoosen)
    {
        switch (AssessmentChoiceChoosen.Key)
        {
            case PerformAnotherAssessmentChoices.NewAssessmentFromScratch:
                Manager.Instance.PrepareAssessment(AssessmentChoiceChoosen.Key);
                break;

            case PerformAnotherAssessmentChoices.Followup:
                if (System.Enum.TryParse(AssessmentChoiceChoosen.Value.Key, out FollowupChoices followupChoice))
                    Manager.Instance.PrepareFollowupQuestions(followupChoice, AssessmentChoiceChoosen.Value.Value);
                break;

            case PerformAnotherAssessmentChoices.EditOldAssessment:
            case PerformAnotherAssessmentChoices.ReevaluateOldAssessment:
                Manager.Instance.PrepareAssessment(AssessmentChoiceChoosen.Key, AssessmentChoiceChoosen.Value.Key);
                break;
        }

        CustomizePanels_Manager.Instance.PrepareCustomizationPanel(AssessmentChoiceChoosen.Key == PerformAnotherAssessmentChoices.Followup);
    }

    public string GetLastSession_Id()
    {
        return (addEdit_MedicalRecord_Panel_MVC.controller as AddEdit_MedicalRecord_Panel_Controller).GetLastSession_Id();
    }
    public string ToUpperEveryWord(string s)
    {
        var words = s.Split(' ');

        var t = "";
        foreach (var word in words)
        {
            t += char.ToUpper(word[0]) + word.Substring(1) + ' ';
        }
        return (t.Trim());
    }
    public void onReceiveRecognitionResult(string result)
    {
        Debug.Log("Add edit caled "+result);
        result = ToUpperEveryWord(result);
        if (result == "Back Group") 
            GoBack();
        else if (result == "Previous")
            GoBack();
        else if (result == "Back")
            GoBack();
        else if (result == "Previous Group")
            GoBack();
        else if (result == "Next") 
            GoNext();
        else if (result == "Forward")
            GoNext();
        else if (result == "Next Group")
            GoNext();
        else if (result == "Help") 
            helpButtonfunction();
        else if (result == "Close")
            helpPromptClose();
   
    }

    public void GoNext()
    {
        KeyValuePair<PerformAnotherAssessmentChoices, KeyValuePair<string, string>> AssessmentChoiceChoosen = GetPerformAnotherAssessmentChoiceChoosen();
        if (AssessmentChoiceChoosen.Key != PerformAnotherAssessmentChoices.None)
        {
            if (AssessmentChoiceChoosen.Key != PerformAnotherAssessmentChoices.NewAssessmentFromScratch && string.IsNullOrEmpty(AssessmentChoiceChoosen.Value.Key)) return; //didn't select date
            
            switch(AssessmentChoiceChoosen.Key)
            {
                case PerformAnotherAssessmentChoices.Followup:
                    isFollowupSession = true;
                    break;

                default:
                    isFollowupSession = false;
                    break;
            }

            AssessPanels_Manager.Instance.RemoveButtonsListeners();
            PrepareAssessment(AssessmentChoiceChoosen);

            ChoosingSubspeciality_Panel_Manager.Instance.helpButton.OnClick.RemoveAllListeners();
            ChoosingSubspeciality_Panel_Manager.Instance.helpButton.AddListener(CustomizePanels_Manager.Instance.helpButtonfunction);
            AudioButton.onClick.RemoveListener(FVoiceController);
            try
            {
                var speechRecognizier = gameVoiceControlAddedit.GetComponent("AndroidSpeechRecognizer");
                if (speechRecognizier != null)
                {
                    Destroy(speechRecognizier);
                }
            }
            catch (System.Exception e)
            {

            }
            gameVoiceControl.onStopListening();
            gameVoiceControl.RecognitionResult.RemoveListener(onReceiveRecognitionResult);
            gameVoiceControlAddedit.SetActive(false);
            VoiceCustomization.SetActive(true);
        }

    }

    public void GoBack()
    {
        gameVoiceControl.RecognitionResult.RemoveListener(onReceiveRecognitionResult);
        try
        {
            var speechRecognizier = gameVoiceControlAddedit.GetComponent("AndroidSpeechRecognizer");
            if (speechRecognizier != null)
            {
                Destroy(speechRecognizier);
            }
        }
        catch(System.Exception e)
        {

        }
        gameVoiceControl.onStopListening();
        gameVoiceControlAddedit.SetActive(false);
        gameVoiceControlSubSub.SetActive(true);
        AudioButton.onClick.RemoveListener(FVoiceController);

        AddEdit_MedicalRecord_Panel_Visibility(false);
        AssessPanels_Manager.Instance.RemoveButtonsListeners();
        ChoosingSubspeciality_Panel_Manager.Instance.Enter_ChoosingSubspeciality_Panel();
        

    }

    private void AddEdit_MedicalRecord_Panel_Visibility(bool _show)
    {
        addEdit_MedicalRecordHolder.gameObject.SetActive(_show);
        //addEdit_MedicalRecordHolder.enabled = _show;
        //addEdit_MedicalRecordHolder.gameObject.SetActive(_show);
    }

    [SkipRename]
    public void AdjustButtons()
    {
        AssessPanels_Manager.Instance.AddButtonsListeners(GoBack, GoBack, GoNext, GoNext);
        AddEdit_MedicalRecord_Panel_Visibility(true);

        gameVoiceControlAddedit.SetActive(true);
        gameVoiceControlSubSub.SetActive(false);
        gameVoiceControl.reinitializeVoice();
        gameVoiceControl.RecognitionResult.AddListener(onReceiveRecognitionResult);
        AudioButton.onClick.AddListener(FVoiceController);
        AudioButton.GetComponent<Image>().sprite = Audioon;

    }

    public void Enter_AddEdit_MedicalRecord_Panel()
    {
        RefreshContents();
        ProgramGeneralData.currentSessionId = "";
    }
    public void helpButtonfunction()
    {
        gRP_ManagerBase.SetActive(true);
    }
    public void RemoveListnerFromAddedit()
    {
       // gameVoiceControl.RecognitionResult.RemoveListener(onReceiveRecognitionResult);
    }
    public void helpPromptClose()
    {
        //todo
        gRP_ManagerBase.SetActive(false);
    }
    public void RefreshContents()
    {
        while (addEdit_MedicalRecordHolder.transform.childCount > 0)
            DestroyImmediate(addEdit_MedicalRecordHolder.transform.GetChild(0).gameObject);

        LoadAddEdit_MedicalRecord_Panel();
    }

    //[ObfuscateLiterals]
    void SetVoiceControl()
    {
        //Dictionary<string, System.Action> phraseAction = new Dictionary<string, System.Action>()
        //{
        //    {"Next", GoNext},
        //    {"Next Group", GoNext},
        //    {"Back", GoBack},
        //    {"Back Group", GoBack},
        //    {"help", helpButtonfunction},
        //    {"close", helpPromptClose},
        //};

        //PanelVoiceController.Instance.AddSetOfPhrases(phraseAction);
    }
}