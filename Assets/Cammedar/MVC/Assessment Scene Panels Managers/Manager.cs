﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Newtonsoft.Json;
using System;
using System.Reflection;
using Firebase.Database;
using UnityEngine.SceneManagement;
using Beebyte.Obfuscator;
using Ricimi;
using fb = Cammedar.Network.Firebase;
using URI = Cammedar.Network.URI;
using Cammedar.Network;

/// <summary>
/// Class to load the groups and their panel and sections, from json files.
/// Also, to save the patient data to patient json file.
/// </summary>
public class Manager : MonoBehaviour
{
    private static Manager _instance;
    public static Manager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<Manager>();
            return _instance;
        }
    }

    /// <summary>
    /// MVC Elements of all the questions type that exist (inputField, text, toggles_horizontal and toggles_Vertical)
    /// </summary>
    [Header("MVC Elements of all the questions type that exist")]
    [SerializeField]
    List<Element_MVC> mvc_Elements;

    [Header("Json Files Data")]
    public string jsonFilesPath;
    public string questionsFileName;
    public string patientFileName;

    [SerializeField]
    [Header("Submit")]
    Button submit_Btn;

    [Header("Events")]
    [SerializeField]
    GameEvent savingAssessmentIsDoneEvent;
    [SerializeField]
    GameEvent savingAssessmentFailedEvent;

    /// <summary>
    /// After session is done save patient last visit algorithm
    /// </summary>
    [SerializeField]
    GameEvent sessionIsDoneEvent;

    [Header("Popup")]
    [SerializeField]
    PopupOpener popupOpener;
    [SerializeField]
    GameObject savingPromptView;
    GameObject getitemCustomization;
    public int CustomeIndex;
    [SerializeField]
    GameObject assessmentView;
    public ArrayList ItemsScrollList = new ArrayList();
    private DatabaseReference FB_RootReference;
    private ScrollRect scrollRect;

    /// <summary>
    ///hold all data questions and selected values, will serialize/deserialize to/from it when loading and saving.
    /// </summary>
    private Disease disease;

    /// <summary>
    /// Flag to detect that loading groups and their panels and section has been completed.
    /// </summary>

    private bool _questionLoaded;

    [HideInInspector]
    public bool QuestionLoaded
    {
        get => _questionLoaded;
        set
        {
            _questionLoaded = value;
            if (_questionLoaded)
                CustomizePanels_Manager.Instance.LoadGroupsWhenReady_questionLoaded();
        }
    }

    /// <summary>
    /// Dictionary contains mvc elements with a key of their linking tag.
    /// </summary>
    public Dictionary<string, Element_MVC> taggedMVCDict = new Dictionary<string, Element_MVC>();

    /// <summary>
    /// Questions grouped based on their panel number
    /// </summary>
    public static SortedDictionary<ITransit> groupDictionary = new SortedDictionary<ITransit>();

    string local_url;
    PerformAnotherAssessmentChoices performAnotherAssessmentChoice = PerformAnotherAssessmentChoices.None;
    FollowupChoices followupChoice;

    string assessmentId;
    bool loadDiagnosisScene = false;
    public Dictionary<string, List<CustomizeItemSerializable>> patientVisitAlgorithm = new Dictionary<string, List<CustomizeItemSerializable>>();

    #region Properties
    string AssessmentFileName
    {
        get
        {
            return (string.IsNullOrEmpty(UserData.doctor_SubSubSpecialization_logIn)) ? UserData.doctor_SubSpecialization_logIn + ".txt" : UserData.doctor_SubSubSpecialization_logIn + ".txt";
        }
    }

    #endregion

    /// Making a dictionary that has all the linking tags from mvc elements that has been referenced
    /// Later when we need to access specific mvc to reach to its controller will do that through that dictionary with knowing the linking tag
    private void Awake()
    {
        scrollRect = GameObject.FindWithTag("ScrollRect").GetComponent<ScrollRect>();

        FB_RootReference = FirebaseDatabase.DefaultInstance.RootReference;

        jsonFilesPath = Application.persistentDataPath + jsonFilesPath;
            
        Debug.Log("jsonFilesPath= " + jsonFilesPath);
        submit_Btn.onClick.AddListener(delegate { AddSessionToCloud(); });

        foreach (Element_MVC element_MVC in mvc_Elements)
        {
            Type mvcType = element_MVC.GetType();
            PropertyInfo model_propertyInfo = mvcType.GetProperty("model");
            Type modelType = model_propertyInfo.PropertyType;            //Get model type

            ConstructorInfo modelConstructor = modelType.GetConstructor(Type.EmptyTypes);        //Get constructor of that model type
            object modelClassObject = modelConstructor.Invoke(new object[] { });                //Calling that constructor

            MethodInfo linkingTagMethod = modelType.GetMethod("GetLinkingTag");                  //From model type get method of name GetLinkingTag
            object linkeingTag = linkingTagMethod.Invoke(modelClassObject, new object[] { });    //Call GetLinkingTag method which will return a string 
                                                                                                 //with linking tag of that model

            taggedMVCDict.Add(linkeingTag.ToString(), element_MVC);         //Add that linking tag to the dictionary with its mvc element
        }
    }

    public void GetQuestionsAndSavedAssessment(string _fileContent, bool getOldAssessment = false)
    {
        //string txt = LocalFileSystem.ReadFile(fileName, LocalDownloadDirectory);
        //string decrypText = AES.DecryptString_Aes(txt, "Cammedar", "Cammedar", true);
        string decrypText = _fileContent;
        disease = JsonConvert.DeserializeObject<Disease>(decrypText); //Deserialize disease json file to disease variable in type Disease

        if (getOldAssessment) //If patient file exist populate or add values that has been stored in patient file to disease object
            JsonConvert.PopulateObject(Session.Instance.Assessment_Info, disease); //Load patient data to disease variable

        sides = new List<string>();
        PopulateControllers(); //then loop through disease variable to link groups with their panels, and panels with their sections and store the groups in dictionary
    }

    /// <summary>
    /// Populate all the groups and storing them in dictionary, also to add panels to each group, and then sections to each panel.
    /// groupDictionary will hold groups with their display order, it's of type sortedDictionary.
    /// SortedDictionary type, has a sortedView property to return the sorted dictionary based on key which is here (diplayOrder), it is used with groups. panels and sections, all of them has a diplayOrder.
    /// groupDictionary will be used when transiting between groups as we can sort this dictionary, 
    /// and with having each group holding its panels in a dictionary (that we can sort), and each panel has its sections that can be sorted also,
    /// we can go through not just groups but also panels and sections when going next and back.
    /// </summary>
    void PopulateControllers()
    {
        foreach (Specialization specialization in disease.GetChildDict().Values)
        {
            foreach (Category category in specialization.GetChildDict().Values)
                groupDictionary = LinkItemswithSortedDictionaryChildren(category.GetChildDict(), AddToSides);
        }
        QuestionLoaded = true;
    }

    public List<string> sides = new List<string>();

    void AddToSides(string side)
    {
        if (!sides.Contains(side))
            sides.Add(side);
    }

    /// <summary>
    /// To link panels children to each group, and sections children to each panel and then return the dictionary containing all the groups.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="dictionary"></param>
    /// <returns></returns>
    public static SortedDictionary<ITransit> LinkItemswithSortedDictionaryChildren<T>(IDictionary<string, T> dictionary, System.Action<string> AddToSides = null)
    {
        SortedDictionary<ITransit> returnedDictionary = new SortedDictionary<ITransit>();

        foreach (KeyValuePair<string, T> keyValuePair in dictionary)
        {
            int displayOrder = 0;
            object item = null;
            if (typeof(T) == typeof(Group))
            {
                Group group = keyValuePair.Value as Group;
                group.name = keyValuePair.Key;
                group.Panels = LinkItemswithSortedDictionaryChildren(group.GetChildDict(), AddToSides);
                item = group;
                displayOrder = group.displayOrder;
            }
            else if (typeof(T) == typeof(Panel))
            {
                Panel panel = keyValuePair.Value as Panel;
                panel.name = keyValuePair.Key;
                panel.Sections = LinkItemswithSortedDictionaryChildren(panel.GetChildDict(), AddToSides);
                item = panel;
                displayOrder = panel.displayOrder;
            }
            else if (typeof(T) == typeof(Section))
            {
                Section section = keyValuePair.Value as Section;
                section.name = keyValuePair.Key;
                section.Sides = LinkItemswithSortedDictionaryChildren(section.GetChildDict(), AddToSides);
                item = section;
                displayOrder = section.displayOrder;
            }
            else if (typeof(T) == typeof(Side))
            {
                Side side = keyValuePair.Value as Side;
                side.name = keyValuePair.Key;
                side.Tests = LinkItemswithSortedDictionaryChildren(side.GetChildDict(), AddToSides);
                AddToSides?.Invoke(side.name);
                item = side;
                displayOrder = side.displayOrder;
            }
            else if (typeof(T) == typeof(EntryField))
            {
                EntryField entry = keyValuePair.Value as EntryField;
                entry.title = keyValuePair.Key;
                item = entry;
                displayOrder = 1;
            }

            if (item == null)
                Debug.LogError("Class has a wrong type. Not Group, Panel or Section");

            else
                returnedDictionary[displayOrder].Add((ITransit)item);
        }
        return returnedDictionary;
    }

    Dictionary<SessionFirebaseRequests, FirebaseOperation> firebaseResponses = new Dictionary<SessionFirebaseRequests, FirebaseOperation>
    {
        {SessionFirebaseRequests.AddSessionData, new FirebaseOperation(){ operationStatus = FirebaseOperationStatus.Idle} },
        {SessionFirebaseRequests.EditSessionData, new FirebaseOperation(){ operationStatus = FirebaseOperationStatus.Idle} },
        {SessionFirebaseRequests.DownloadAssessmentFileFromStorage, new FirebaseOperation(){ operationStatus = FirebaseOperationStatus.Idle} },
    };

    public void AddSessionToCloud()
    {
        string assessmentData = GetPatientAssessmentSerialized();

        string sideOfAssessment = ChoosingSides_Panel_Manager.Instance.IsThereSidesToChooseFrom() ?
            ChoosingSides_Panel_Manager.Instance.GetSideToAssess().ToString() : Sides.None.ToString();

        Session.Instance.patient_ID = PatientData.patient_UDID;
        Session.Instance.doctor_ID = UserData.user_UDID;
        Session.Instance.hospital_branch_name = UserData.doctor_hospital_branch_name;
        Session.Instance.Assessment_Info = assessmentData;
        Session.Instance.sideOfAssessment = sideOfAssessment;

        if (performAnotherAssessmentChoice == PerformAnotherAssessmentChoices.NewAssessmentFromScratch ||
            performAnotherAssessmentChoice == PerformAnotherAssessmentChoices.EditOldAssessment ||
            performAnotherAssessmentChoice == PerformAnotherAssessmentChoices.ReevaluateOldAssessment ||
            performAnotherAssessmentChoice == PerformAnotherAssessmentChoices.Followup
            )
        {
            SessionFirebaseRequests sessionFirebaseRequests = SessionFirebaseRequests.None;

            switch (performAnotherAssessmentChoice)
            {
                case PerformAnotherAssessmentChoices.NewAssessmentFromScratch:
                case PerformAnotherAssessmentChoices.ReevaluateOldAssessment:
                    sessionFirebaseRequests = SessionFirebaseRequests.AddSessionData;

                    break;
                case PerformAnotherAssessmentChoices.EditOldAssessment:
                    sessionFirebaseRequests = SessionFirebaseRequests.EditSessionData;
                    break;

                case PerformAnotherAssessmentChoices.Followup:
                    sessionFirebaseRequests = followupChoice == FollowupChoices.EditLastFollowup ? SessionFirebaseRequests.EditSessionData : SessionFirebaseRequests.AddSessionData;
                    break;
            }

            SaveSessionData_ToCloud(Session.Instance, sessionFirebaseRequests);
        }
    }

    public void LinkPatientWithHisSession(bool addingNewRecord)
    {
        SessionPreview sessionPreview = new SessionPreview()
        {
            date = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt", System.Globalization.CultureInfo.InvariantCulture),
            isFollowup = AddEdit_MedicalRecord_Panel_Manager.Instance.isFollowupSession
        };

        URI uri = fb.Instance.databasePath.Child("patients").Child(PatientData.patient_UDID).Child("sessions").Child(UserData.doctor_hospital_id)
            .Child(UserData.doctor_hospital_branch_id).Child(UserData.doctor_Specialization).Child(UserData.doctor_SubSpecialization_logIn)
            .Child(UserData.doctor_SubSubSpecialization_logIn).Child("sessions_Dates").Child(ProgramGeneralData.currentSessionId).JSON().AuthQuery(UserData.tokenID);

        fb.Instance.Put(uri, sessionPreview, delegate
        {
            if (addingNewRecord)
                UpdateDoctor_SessionsCount(sessionPreview.isFollowup);
            else
                savingAssessmentIsDoneEvent.Raise();
        }, delegate { });
    }

    /// <summary>
    /// Increase sessions count under doctor node by 1.
    /// </summary>
    /// <param name="_isFollowUp">Detecting which session is going to be increased, follow up or assessment.</param>
    public void UpdateDoctor_SessionsCount(bool _isFollowUp)
    {
        URI uri = fb.Instance.databasePath.Child("doctors").Child(UserData.user_UDID).Child("doctor_Data").JSON().AuthQuery(UserData.tokenID);

        SessionsCount sessionsCount = UserData.sessionsCount;
        if (sessionsCount == null)
            sessionsCount = new SessionsCount();

        if (_isFollowUp)
        {
            if (int.TryParse(sessionsCount.followUps, out int count))
                sessionsCount.followUps = (count + 1).ToString();
        }
        else if (int.TryParse(sessionsCount.examinations, out int count))
            sessionsCount.examinations = (count + 1).ToString();

        UserData.sessionsCount = sessionsCount;

        Doctor doctor = new Doctor()
        {
            authorized = UserData.authorized,
            approved = UserData.approved,
            email = UserData.user_email,
            first_name = UserData.first_name,
            last_name = UserData.last_name,
            sessionsCount = new SessionsCount() { examinations = UserData.sessionsCount.examinations, followUps = UserData.sessionsCount.followUps },
            hospital_name = UserData.doctor_hospital_name,
            hospital_id = UserData.doctor_hospital_id,
            hospital_branch_name = UserData.doctor_hospital_branch_name,
            hospital_branch_id = UserData.doctor_hospital_branch_id,
            phone_number = UserData.phone_number,
            speciality = UserData.doctor_Specialization,
            sub_speciality = UserData.doctor_SubSpecialization,
            country_id = UserData.country_id,
            city_id = UserData.city_id,
        };

        doctor.Encrypt(UserData.user_UDID);
        fb.Instance.Put(uri, doctor, delegate { GetHospitalBranch_SessionsCount(_isFollowUp); }, delegate(string mssg) { });
    }

    /// <summary>
    /// Retrieving assessment/follow up count according to _isFollowUp boolean value.
    /// </summary>
    /// <param name="_isFollowUp">Detecting which session is going to be retrieved, follow up or assessment.</param>
    public void GetHospitalBranch_SessionsCount(bool _isFollowUp)
    {
        URI uri = fb.Instance.databasePath.Child("hospitals").Child(UserData.doctor_hospital_id)
            .Child("branches").Child(UserData.doctor_hospital_branch_id).Child("sessionsCount").JSON().AuthQuery(UserData.tokenID);

        System.Action<SessionsCount> onSuccess = (_sessionsCountRetrieved) =>
        {
            SessionsCount sessions_Count = new SessionsCount();

            if (_sessionsCountRetrieved != null)
                sessions_Count = _sessionsCountRetrieved;

            UpdateHospitalBranch_SessionsCount(sessions_Count, _isFollowUp);
        };

        fb.Instance.Get(uri, onSuccess, delegate (string mssg) { });
    }

    /// <summary>
    /// Increase sessions count under current hospital branch node by 1.
    /// </summary>
    /// <param name="sessions_Count"></param>
    /// <param name="_isFollowUp">Detecting which session is going to be Updated, follow up or assessment.</param>
    public void UpdateHospitalBranch_SessionsCount(SessionsCount sessions_Count, bool _isFollowUp)
    {
        URI uri = fb.Instance.databasePath.Child("hospitals").Child(UserData.doctor_hospital_id)
            .Child("branches").Child(UserData.doctor_hospital_branch_id).Child("sessionsCount").JSON().AuthQuery(UserData.tokenID);

        if (_isFollowUp)
        {
            if (int.TryParse(sessions_Count.followUps, out int count))
                sessions_Count.followUps = (count + 1).ToString();
        }
        else if (int.TryParse(sessions_Count.examinations, out int count))
            sessions_Count.examinations = (count + 1).ToString();

        fb.Instance.Put(uri, sessions_Count, delegate { savingAssessmentIsDoneEvent.Raise(); }, delegate (string mssg) { });
    }

    public void PrepareAssessment(PerformAnotherAssessmentChoices performAnotherAssessmentChoice, string assessmentId = "")
    {
        groupDictionary = new SortedDictionary<ITransit>();
        QuestionLoaded = false;

        this.performAnotherAssessmentChoice = performAnotherAssessmentChoice;
        this.assessmentId = assessmentId;

        patientVisitAlgorithm = new Dictionary<string, List<CustomizeItemSerializable>>();
        Session.Instance.Clear();

        if (!string.IsNullOrEmpty(assessmentId))
            RetrieveSessionsData_FromCloud(assessmentId, delegate
            {
                GetQuestionsFromFB_Storage(FirebaseStoragePaths.StorageDirectory, AssessmentFileName);
            });
        else
            GetQuestionsFromFB_Storage(FirebaseStoragePaths.StorageDirectory, AssessmentFileName);
    }

    public void PrepareFollowupQuestions(FollowupChoices followupChoice, string sessionId = "")
    {
        groupDictionary = new SortedDictionary<ITransit>();
        QuestionLoaded = false;

        this.performAnotherAssessmentChoice = PerformAnotherAssessmentChoices.Followup;
        this.followupChoice = followupChoice;
        this.assessmentId = string.IsNullOrEmpty(sessionId) ? AddEdit_MedicalRecord_Panel_Manager.Instance.GetLastSession_Id() : sessionId;

        patientVisitAlgorithm = new Dictionary<string, List<CustomizeItemSerializable>>();

        Session.Instance.Clear();

        RetrieveSessionsData_FromCloud(assessmentId, delegate
        {
            GetQuestionsFromFB_Storage(FirebaseStoragePaths.StorageDirectory, "FollowUp.txt");
            if (followupChoice == FollowupChoices.CreateFollowupFromScratch)
            {
                Session.Instance.ClearUncommonData();
                patientVisitAlgorithm = new Dictionary<string, List<CustomizeItemSerializable>>();
            }
        });
    }

    public void GetQuestionsFromFB_Storage(string StorageDirectory, string fileName)
    {
        firebaseResponses[SessionFirebaseRequests.DownloadAssessmentFileFromStorage].operationStatus = FirebaseOperationStatus.Inprocess;
        StartCoroutine(CheckFirebaseProcessStatus(SessionFirebaseRequests.DownloadAssessmentFileFromStorage));
        GetFromFB_Storage(StorageDirectory + fileName);
    }

    public void GetFromFB_Storage(string FirebaseFilePath)
    {
        FBStorage.Instance.Download_To_ByteArray(FirebaseFilePath,
            delegate (string fileContent)
            {
                firebaseResponses[SessionFirebaseRequests.DownloadAssessmentFileFromStorage] = new FirebaseOperation()
                {
                    operationStatus = FirebaseOperationStatus.Completed,
                    content = fileContent
                };
            }, delegate { });
    }

    public void RetrieveSessionsData_FromCloud(string assessmentId, System.Action AfterRetrievingAction) //to retrieve assessment
    {
        URI uri = fb.Instance.databasePath.Child("sessions").Child(assessmentId).JSON().AuthQuery(UserData.tokenID);

        System.Action<Session> onSuccess = (session) =>
        {
            if (session == null) return;

            session.Decrypt(PatientData.patient_UDID);
            Session.Instance.SetSessionData(session);

            patientVisitAlgorithm.Add("patientVisitAlgorithm",
            JsonConvert.DeserializeObject<List<CustomizeItemSerializable>>(Session.Instance.patientVisitAlgorithm));
            AfterRetrievingAction();
        };

        fb.Instance.Get(uri, onSuccess, delegate { });
    }

    public void SaveSessionData_ToCloud(Session session, SessionFirebaseRequests sessionFirebaseRequests)
    {
        Debug.Log("save button");
        ChoosingSubspeciality_Panel_Manager.Instance.helpButton.OnClick.RemoveAllListeners();
        if (session.Assessment_Info == "null" || session.Assessment_Info == null) return;

        bool addingNewRecord = false;
        if (string.IsNullOrEmpty(ProgramGeneralData.currentSessionId))
        {
            if (sessionFirebaseRequests == SessionFirebaseRequests.AddSessionData)
            {
                ProgramGeneralData.currentSessionId = FB_RootReference.Child("sessions").Push().Key;
                addingNewRecord = true;
            }
            else
                ProgramGeneralData.currentSessionId = assessmentId;
        }

        URI uri = fb.Instance.databasePath.Child("sessions").Child(ProgramGeneralData.currentSessionId).JSON().AuthQuery(UserData.tokenID);

        System.Action<string> onDone = (message) =>
        {
            session.Decrypt(PatientData.patient_UDID);

            if (sessionFirebaseRequests == SessionFirebaseRequests.AddSessionData)
                LinkPatientWithHisSession(addingNewRecord);

            else if(sessionFirebaseRequests == SessionFirebaseRequests.EditSessionData)
                savingAssessmentIsDoneEvent.Raise();

            sessionIsDoneEvent.Raise();
        };

        session.Encrypt(PatientData.patient_UDID);
        fb.Instance.Put(uri, session, onDone, delegate { savingAssessmentFailedEvent.Raise(); });
    }

    IEnumerator CheckFirebaseProcessStatus(SessionFirebaseRequests assessmentFirebaseRequests)
    {
        while (!firebaseResponses[assessmentFirebaseRequests].ProcessFinished())
        {
            yield return null;
        }

        if (firebaseResponses[SessionFirebaseRequests.DownloadAssessmentFileFromStorage].IsCompleted())
        {
            string fileContent = firebaseResponses[SessionFirebaseRequests.DownloadAssessmentFileFromStorage].content;
            switch (performAnotherAssessmentChoice)
            {
                case PerformAnotherAssessmentChoices.EditOldAssessment:
                case PerformAnotherAssessmentChoices.ReevaluateOldAssessment:
                case PerformAnotherAssessmentChoices.NewAssessmentFromScratch:
                    GetQuestionsAndSavedAssessment(fileContent, performAnotherAssessmentChoice != PerformAnotherAssessmentChoices.NewAssessmentFromScratch);
                    break;

                case PerformAnotherAssessmentChoices.Followup:
                    GetQuestionsAndSavedAssessment(fileContent, followupChoice == FollowupChoices.EditLastFollowup);
                    break;
            }
            
        }

        firebaseResponses[SessionFirebaseRequests.DownloadAssessmentFileFromStorage] = new FirebaseOperation();
    }

    /// <summary>
    /// Returns patient assessment answers serialized.
    /// </summary>
    public string GetPatientAssessmentSerialized()
    {
        JsonSerializer settings = new JsonSerializer();
        var jsonResolver = new PropertyIgnoreSerializerContractResolver<IgnoreForPatientAttribute>(true);
        settings.ContractResolver = jsonResolver;
        settings.NullValueHandling = NullValueHandling.Ignore;
        settings.DefaultValueHandling = DefaultValueHandling.Ignore;

        return JsonHelper.SerializeToMinimalJson(disease, settings);
    }
    public void StoreData()
    {
        getitemCustomization = GameObject.Find("Toggles_transform");
        Text[] newText;

        newText = getitemCustomization.GetComponentsInChildren<Text>();
        ItemsScrollList.Clear();
        for (int i = 0; i < newText.Length; i++)
        {
          //  Debug.Log(newText[i].text.ToString());
            ItemsScrollList.Add(newText[i].text.ToString());
        }

    }
    public void ResetScroll()
    {
        scrollRect.GetComponent<RectTransform>().offsetMax = new Vector2(0, (-30));
    }
    public void scrollToSpecificElement(int specificIndex)
    {
        getitemCustomization = GameObject.Find("Toggles_transform");
        GameObject MainToggleObject;
        MainToggleObject = getitemCustomization.transform.GetChild(specificIndex).gameObject;
        scrollRect.GetComponent<RectTransform>().offsetMax = new Vector2(0, -(-250 + MainToggleObject.GetComponent<RectTransform>().anchoredPosition.y));
        //  MainToggleObject.transform.SetSiblingIndex(0);

    }
    //public void scrollToSpecificElement(int specificIndex)
    //{
    //    var siblingIndex = specificIndex;

    //    float pos = 1f - (float)siblingIndex / ItemsScrollList.Count;

    //    if (pos < 0.4)
    //    {
    //        float correction = 1f / ItemsScrollList.Count;
    //        pos -= correction;
    //    }

    //    scrollRect.verticalNormalizedPosition = pos;
    //}
    public void SnapUp()
    {
        Canvas.ForceUpdateCanvases();
        float scrollRectval = PlayerPrefs.GetFloat("scrollRect");
        if (scrollRectval < 1)
        {   
            scrollRectval += 0.3f;
            scrollRect.verticalNormalizedPosition = scrollRectval;
        }
        PlayerPrefs.SetFloat("scrollRect", scrollRectval);

    }
    public void SnapMiddle()
    {
        Canvas.ForceUpdateCanvases();
        scrollRect.verticalNormalizedPosition = 0.5f;
        PlayerPrefs.SetFloat("scrollRect", 0.5f);
    }
    public void SnapDown()
    {
        Debug.Log("lol");
        Canvas.ForceUpdateCanvases();
        float scrollRectval = PlayerPrefs.GetFloat("scrollRect");
        if (scrollRectval > 0)
        {
            scrollRectval -= 0.3f;
            scrollRect.verticalNormalizedPosition = scrollRectval;
        }
        PlayerPrefs.SetFloat("scrollRect", scrollRectval);
    }
    public void SnapBottom()
    {
        Canvas.ForceUpdateCanvases();
        scrollRect.verticalNormalizedPosition = 0;
        PlayerPrefs.SetFloat("scrollRect", 0);
    }
    public void SnapTop()
    {
        ResetScroll();
        Canvas.ForceUpdateCanvases();
        scrollRect.verticalNormalizedPosition = 1f;
        PlayerPrefs.SetFloat("scrollRect", 1);

    }

    [SkipRename]
    public void AssessmentSceneFinished()
    {
        loadDiagnosisScene = true;
        AddSessionToCloud();
        OpenPrompt(savingPromptView);
    }

    [SkipRename]
    public void LoadBackScene()
    {
        SceneManager.LoadScene("PDFViewer_Asset");
    }

    [SkipRename]
    public void LoadNextScene()
    {
        if (loadDiagnosisScene&&(UserData.doctor_Specialization.ToString() != "Dentistry"))
        {
            ClosePrompt();
            HideSceneContent();
            StartCoroutine(LoadYourAsyncScene("Diagnosis_List"));
            loadDiagnosisScene = false;
        }
        else
        {
            PlayerPrefs.SetString("PDFWriter", "AssessmentScene");
            SceneManager.LoadScene("PDFWriter");
        }
    }

    public void HideSceneContent()
    {
        GameObject viewGO = GameObject.FindGameObjectWithTag("View_Canvas");
        if (viewGO)
            viewGO.GetComponent<Canvas>().enabled = false;

        GameObject[] cameras = GameObject.FindGameObjectsWithTag("MainCamera");
        for (int i = 0; i < cameras.Length; i++)
            cameras[i].GetComponent<AudioListener>().enabled = false;
    }
    public void ViewAssessmentSceneViewContent()
    {
        GameObject viewGO = GameObject.FindGameObjectWithTag("View_Canvas");
        if (viewGO)
            viewGO.GetComponent<Canvas>().enabled = true;

        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
        camera.GetComponent<AudioListener>().enabled = true;
    }

    IEnumerator LoadYourAsyncScene(string sceneName)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));
    }

    public GameObject OpenPrompt(GameObject prefabView)
    {
        popupOpener.popupPrefab = prefabView;
        popupOpener.OpenPopup();

        return popupOpener.popupInstantiated;
    }

    public void ClosePrompt()
    {
        popupOpener.ClosePopup();
    }
}