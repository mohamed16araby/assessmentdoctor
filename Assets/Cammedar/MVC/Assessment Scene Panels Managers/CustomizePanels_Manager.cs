﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Ricimi;
using Beebyte.Obfuscator;
using UnityEngine.UI;
public class CustomizePanels_Manager : MonoBehaviour
{
    private static CustomizePanels_Manager instance;
    public static CustomizePanels_Manager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<CustomizePanels_Manager>();
            return instance;
        }
    }

    [Header("Panel Canvas")]
    [SerializeField]
    Canvas customizePanelContent;

    [Header("Customization Part Info")]
    public CustomizePanel_MVC customizePanel_MVC;
    [SerializeField]
    Transform customizePanelHolder;

    [Header("Algorithms Part Info")]
    public Algorithm_MVC algorithm_MVC;
    [SerializeField]
    Transform algorithmViewHolder;

    [Header("Extra-s")]
    [SerializeField]
    GameObject promptPrefab;
    List<GameObject> unityGameObjects = new List<GameObject>();
    public GameObject[] respawns;
    private SortedDictionary<ITransit> _allGroupsDictionary = new SortedDictionary<ITransit>();
    public bool CurrentPanelIsCustomizePanel { get; set; }
    public GameObject getitemCustomization;
    public GameObject getitemCustomizationTreatment;
    public GameObject getVoicecontrollerObject;
    public GameVoiceControl gameVoiceControl;
    public GameObject VoiceCustomization;
    public GameObject VoiceSupAddedit;
    public GameObject VoiceSides;

    public Sprite Audioon;
    public Sprite AudioOff;
    public Button AudioButton;
    GameObject AudioController;
    public GameObject gRP_ManagerBase;
    public GameObject SliderObject;
    //public string[] Ankel = new string[] { "Examination", "Active range of motion", "Passive range of motion", "Muscle power assessment", "Stability tests", "Fracture screen test", "Test for Morton neuroma", "Test for vascular assessment", "DVT screen test", "Tibial torsion assessment", "Neurological assessment", "Gait" };
    //public string[] Shoulder = new string[] { "Examination", "Active range of motion", "Passive range of motion", "Examination of muscle power", "Rotator cuff muscles examination", "Shoulder muscles examination", "Labral tests", "Stability tests", "Impingement test", "Scapular tests", "Joint special tests" };
    //public string[] Lumbar = new string[] { "Examination", "Active range of motion", "Examination of muscle power", "Special tests", "Hip tests", "Sacroiliac joint tests", "Neurological assessment", "Gait" };
    //public string[] Dorsal = new string[] { "Examination", "Active range of motion", "Examination of muscle power", "Special Tests"};
    //public string[] Knee = new string[] { "Examination", "Active range of motion", "Passive range of motion", "Muscle power assessment", "Stability tests", "Effusion tests", "Meniscal tests", "The patella tests", "Gait" };
    //public string[] Hip = new string[] { "Examination", "Active range of motion", "Passive range of motion", "Muscle power assessment", "Hip tests", "Radiculopathy tests", "Muscles tests", "Sacroiliac joint tests", "Neurological examination", "Congenital hip dislocation tests", "Gait" };
    //public string[] Elbow = new string[] { "Examination", "Active range of motion", "Passive range of motion", "Muscle power assessment", "Stability tests",  "Neurological assessment", "Provocative tests for tendinitis" };
    //public string[] Cervical = new string[] { "Examination", "Active range of motion", "Passive range of motion", "Examination of muscle power", "Special tests", "Neurological assessment", "Tempromandibular joint tests" };
    //public string[] wrist = new string[] { "Examination", "Deformities", "Swelling", "Active wrist range of motion", "Passive wrist range of motion", "Active fingers range of motion", "Passive fingers range of motion", "Wrist muscle power", "Fingers muscle power", "Stability tests", "Tendons tests", "Neurological assessment","" };
    void LoadCustomizationPanel()
    {
        customizePanel_MVC.Initialize(customizePanelHolder);

        CustomizePanel_Model panel_Model = new CustomizePanel_Model();
        panel_Model.groupName = "Customize Panel";
        panel_Model.panelName = "Customize Panel";
        panel_Model.hideTitle = true;
        panel_Model.CurrentGroups = _allGroupsDictionary;

        CustomizePanel_Controller customizePanel_Controller = customizePanel_MVC.controller as CustomizePanel_Controller;

        customizePanel_Controller.OnCustomizationItemsInstantiated = OnCustomizationItemsInstantiated;
        customizePanel_Controller.SetData(panel_Model);
    }

    [SkipRename]
    public void VoiceControllerF()
    {


        if (AudioButton.GetComponent<Image>().sprite == Audioon)
        {
            gameVoiceControl.onStopListening();
            AudioButton.GetComponent<Image>().sprite = AudioOff;
        }
        else
        {
            gameVoiceControl.onStartListening();
            AudioButton.GetComponent<Image>().sprite = Audioon;
        }
    }
    private void Start()
    {
       // AudioController = GameObject.Find("VoiceControl");
    }
    private void Update()
    {
        if (FBStorage.Instance.flag)
        {
            SliderObject.SetActive(false);
            FBStorage.Instance.flag = false;
        }
    }
    public void SDisactive()
    {
        Destroy(SliderObject.gameObject);
    }

    IEnumerator DisActiveSliderObject()
    {
        while (!SliderObject.activeSelf)
        {
            Debug.Log("test");
            SliderObject.SetActive(false);
            yield return null;
        }
    }

    public void OnCustomizationItemsInstantiated(Dictionary<CustomizeItem, ITransit> _CustomGroupDictionary)
    {
        AssessPanels_Manager.Instance._allGroupsDictionary_Before_Customization = _CustomGroupDictionary;

        algorithm_MVC.Initialize(algorithmViewHolder);
        algorithm_MVC.controller.SetData(_CustomGroupDictionary.Keys.ToList(), Manager.Instance.patientVisitAlgorithm, hideAlgorithmView);
        if(UserData.doctor_Specialization=="Dentistry")
        DisActiveAllToothFromCustomizePanel();
    }

    bool hideAlgorithmView = false;

    public void PrepareCustomizationPanel(bool hideAlgorithmView)
    {
        this.hideAlgorithmView = hideAlgorithmView;
        RemovePanelsContents();
    }

    public void LoadGroupsWhenReady_questionLoaded()
    {
        _allGroupsDictionary = Manager.groupDictionary;
        LoadCustomizationPanel();
    }

    public void RemovePanelsContents()
    {
        RemovePanelContent(customizePanelHolder);
        RemovePanelContent(algorithmViewHolder);
    }

    public void RemovePanelContent(Transform holderTransform)
    {
        while (holderTransform.childCount > 0)
            DestroyImmediate(holderTransform.GetChild(0).gameObject);
    }

    [SkipRename]
    public void DisablePanel()
    {
        CustomizePanelVisibility(false);
    }

    public void GoNext()
    {
        gameVoiceControl.RecognitionResult.RemoveListener(onReceiveRecognitionResult);
        try
        {
            var speechRecognizier = VoiceCustomization.GetComponent("AndroidSpeechRecognizer");
            if (speechRecognizier != null)
            {
                Destroy(speechRecognizier);
            }
        }
        catch (System.Exception e)
        {

        }
        gameVoiceControl.onStopListening();
        AudioButton.onClick.RemoveAllListeners();
        VoiceCustomization.SetActive(false);
        VoiceSides.SetActive(true);

        if (ChoosingSides_Panel_Manager.Instance.IsThereSidesToChooseFrom())
        {

            AssessPanels_Manager.Instance.RemoveButtonsListeners();
            ChoosingSides_Panel_Manager.Instance.Enter_ChoosingSides_Panel();
        }
        else
        {
            AssessPanels_Manager.Instance.SetAssessment();
            if (AssessPanels_Manager.Instance.IsThereTestsSelected)
            {
                AssessPanels_Manager.Instance.RemoveButtonsListeners();
                DisablePanel();
                AssessPanels_Manager.Instance.StartAssessment();
            }
        }
        ChoosingSubspeciality_Panel_Manager.Instance.helpButton.OnClick.RemoveAllListeners();
        ChoosingSubspeciality_Panel_Manager.Instance.helpButton.AddListener(ChoosingSides_Panel_Manager.Instance.helpButtonfunction);
}

    public void GoBack()
    {
        ApproveGoingBackPrompt();
        ChoosingSubspeciality_Panel_Manager.Instance.helpButton.OnClick.RemoveAllListeners();
        ChoosingSubspeciality_Panel_Manager.Instance.helpButton.AddListener(AddEdit_MedicalRecord_Panel_Manager.Instance.helpButtonfunction);
    }
    public void ActionBack()
    {
        AudioButton.onClick.RemoveAllListeners();
        gameVoiceControl.RecognitionResult.RemoveListener(onReceiveRecognitionResult);
        try
        {
            var speechRecognizier = VoiceCustomization.GetComponent("AndroidSpeechRecognizer");
            if (speechRecognizier != null)
            {
                Destroy(speechRecognizier);
            }
        }
        catch (System.Exception e)
        {

        }
        gameVoiceControl.onStopListening();
        VoiceCustomization.SetActive(false);
        VoiceSupAddedit.SetActive(true);
    }

    [SkipRename]
    public void SavePatientVisitAlgorithm()
    {
        algorithm_MVC.controller.SaveAlgorithm("patientVisitAlgorithm", true);
    }

    public void EnterBackPanel()
    {
        CustomizePanelVisibility(false);
        AssessPanels_Manager.Instance.RemoveButtonsListeners();
        AddEdit_MedicalRecord_Panel_Manager.Instance.Enter_AddEdit_MedicalRecord_Panel();
    }
    public void ApproveGoingBackPrompt()
    {
        Manager.Instance.ClosePrompt();
        GameObject popupInstantiated= Manager.Instance.OpenPrompt(promptPrefab);

        Prompt_View prompt_View = popupInstantiated.GetComponent<Prompt_View>();
        if (prompt_View) prompt_View.AssignButtonsFunctions(Manager.Instance.AddSessionToCloud, EnterBackPanel);
    }

    private void CustomizePanelVisibility(bool _show)
    {
        customizePanelHolder.gameObject.SetActive(_show);
        algorithmViewHolder.gameObject.SetActive(_show);
        CurrentPanelIsCustomizePanel = _show;
    }

    [SkipRename]
    public void EnterCustomizePanel()
    {

        CustomizePanelVisibility(true);
        AssessPanels_Manager.Instance.AddButtonsListeners(GoBack, GoBack, GoNext, GoNext);
        VoiceCustomization.SetActive(true);
        VoiceSupAddedit.SetActive(false);
        AudioButton.onClick.AddListener(VoiceControllerF);
        gameVoiceControl.reinitializeVoice();
        gameVoiceControl.RecognitionResult.AddListener(onReceiveRecognitionResult);
        AudioButton.GetComponent<Image>().sprite = Audioon;

    }
    public void WriteDebugger()
    {
#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
        GameObject MainObject = GameObject.Find(VoiceControl.word);
        MainObject.GetComponent<CustomizeItem>().OnCheckboxChange();
#elif UNITY_ANDROID

        Debug.Log("android");
#endif


    }
    
    IEnumerator WaitForObkectToinstanciated()
    {
        while (getitemCustomization==null)
        {
            getitemCustomization = GameObject.Find("Choose Your Tooth Number");
            getitemCustomizationTreatment = GameObject.Find("Tooth Number");
            yield return null;
        }
        getitemCustomization.SetActive(false);
        getitemCustomizationTreatment.SetActive(false);
    }
    IEnumerator WaitForfindVoiveController()
    {
        while (getVoicecontrollerObject == null)
        {
            getVoicecontrollerObject = GameObject.Find("Choose Your Tooth Number");

            yield return null;
        }
        getVoicecontrollerObject.SetActive(false);

    }


    public void DisActiveAllToothFromCustomizePanel()
    {
      StartCoroutine(WaitForObkectToinstanciated());
    }
   
    public void ActiveSingleTooth(int index)
    {
        unityGameObjects[index].GetComponent<CustomizeItem>().OnCheckboxChange();
    }
    public void ActiveAllTooth()
    {
        try
        {
            getitemCustomization.SetActive(true);
            getitemCustomization.GetComponent<CustomizeItem>().OnCheckboxChange();
            getitemCustomization.SetActive(false);
        }
        catch(System.Exception e)
        {

        }
    }
    public void ActiveToothNumersTreatment()
    {
        getitemCustomizationTreatment.SetActive(true);
        getitemCustomizationTreatment.GetComponent<CustomizeItem>().OnCheckboxChange();
        getitemCustomizationTreatment.SetActive(false);
    }
    public void helpButtonfunction()
    {
        gRP_ManagerBase.SetActive(true);
    }
    public void helpPromptClose()
    {

        gRP_ManagerBase.SetActive(false);
    }

    public void AndroidCallItemCustomization(string word)
    {
        try
        {
            GameObject MainObject = GameObject.Find(word);
            MainObject.GetComponent<CustomizeItem>().OnCheckboxChange();

        }
        catch (System.Exception e)
        {
        }
    }
    public string ToUpperEveryWord(string s)
    {
        var words = s.Split(' ');

        var t = "";
        foreach (var word in words)
        {
            t += char.ToUpper(word[0]) + word.Substring(1) + ' ';
        }
        return (t.Trim());
    }
    [SkipRename]
    public void onReceiveRecognitionResult(string result)
    {
        Debug.Log("lololo  in the customization");
        var words = result.Split(' ');

        var t = "";
        foreach (var word in words)
        {
            t += char.ToUpper(word[0]) + word.Substring(1) + ' ';
        }
        result = t.Trim();
        result = result.ToString();
        switch (result)
        {
            case "Previous":
                    GoBack();                
                break;
            case "Back Group":
                GoBack();
                break;
            case "Back":
                    GoBack();               
                break;
            case "Previous Group":
                    GoBack();
                break;
            case "Next":
                GoNext();
                break;
            case "Forward":
                GoNext();
                break;
            case "Next Group":
                GoNext();
                break;
            case "Help":
                helpButtonfunction();
                break;
            case "Close":
                helpPromptClose();                  
                break;
            case "Up":
                Manager.Instance.SnapUp();
                break;
            case "Middle":
                Manager.Instance.SnapMiddle();
                break;
            case "Down":
                Manager.Instance.SnapDown();
                break;
            case "Top":
                Manager.Instance.SnapTop();
                break;
            case "Bottom":
                Manager.Instance.SnapBottom();
                break;

            case "Diagnosis":
                Debug.Log("the switchcase enter here 1" + result);
                AndroidCallItemCustomization("Diagnosis");
                break;
            case "Treatment":
                Debug.Log("the switchcase enter here 2" + result);
                AndroidCallItemCustomization("Treatment");
                break;
            case "Soft Tissue Diagnosis":
                Debug.Log("the switchcase enter here 3" + result);
                AndroidCallItemCustomization("Soft Tissue Diagnosis");
                break;
            case "Soft Tissue Treatment":
                Debug.Log("the switchcase enter here 4" + result);
                AndroidCallItemCustomization("Soft Tissue Treatment");
                break;
            default:
                Debug.Log("the switchcase enter here " + result);
                break;


        }
        ChoosingSides_Panel_Manager.flag = false;

    }
    //[ObfuscateLiterals]
    void SetVoiceControl()
    {
      

        //Dictionary<string, System.Action> phraseAction = new Dictionary<string, System.Action>()
        //{
        //    {"Next", GoNext},
        //    {"Forward", GoNext},
        //    {"Next Group", GoNext},
        //    {"Back", GoBack},
        //    {"Back Group", GoBack},
        //    {"help", helpButtonfunction},
        //    {"close", helpPromptClose},
        //    //{"Confirm going back", EnterBackPanel},
        //};
        //for (int i = 0; i < CustomizePanel_Controller.Data.Count; i++)
        //{
        //    phraseAction.Add(CustomizePanel_Controller.Data[i].ToString(), WriteDebugger);

        //}
        //PanelVoiceController.Instance.AddSetOfPhrases(phraseAction);
    }
}