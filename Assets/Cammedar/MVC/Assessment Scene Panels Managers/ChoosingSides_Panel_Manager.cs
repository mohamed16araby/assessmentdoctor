﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Beebyte.Obfuscator;
using UnityEngine.UI;
public class ChoosingSides_Panel_Manager : MonoBehaviour
{
    private static ChoosingSides_Panel_Manager instance;
    public static ChoosingSides_Panel_Manager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<ChoosingSides_Panel_Manager>();
            return instance;
        }
    }
    public ChoosingSides_Panel_MVC choosingSides_Panel_MVC;
    public GameVoiceControl gameVoiceControl;
    [SerializeField]
    Transform choosingSides_PanelHolder;

   public static Dictionary<string, System.Action> ChoicesVoicePhrases = new Dictionary<string, System.Action>();

    public GameObject gRP_ManagerBase;
    public GameObject VoiceCustomization;
    public GameObject VoiceSides;
    public GameObject VoiceAssessment;
    public Sprite Audioon;
    public Sprite AudioOff;
    public Button AudioButton;
    public static bool flag = false;
    void LoadChoosingSides_Panel()
    { 
        
        choosingSides_Panel_MVC.Initialize(choosingSides_PanelHolder);

        ChoosingSides_Panel_Model panel_Model = new ChoosingSides_Panel_Model();
        panel_Model.groupName = "Assessment Sides";
        panel_Model.panelName = "Which side do you want to assess?";
        panel_Model.sides_Assessment = Manager.Instance.sides;

        ChoosingSides_Panel_Controller choosingSides_Panel_Controller = choosingSides_Panel_MVC.controller as ChoosingSides_Panel_Controller;
        choosingSides_Panel_Controller.SetData(panel_Model);

       AudioButton.onClick.AddListener(FVoiceController);
    }
    [SkipRename]
    public void FVoiceController()
    {
        if (AudioButton.GetComponent<Image>().sprite == Audioon)
        {
            gameVoiceControl.onStopListening();
            AudioButton.GetComponent<Image>().sprite = AudioOff;
        }
        else
        {
            gameVoiceControl.onStartListening();

            AudioButton.GetComponent<Image>().sprite = Audioon;
        }
    }
    //public void test()
    //{
    //    gameVoiceControl.RecognitionResult.AddListener(onReceiveRecognitionResult);
    //}

    public bool IsThereSidesToChooseFrom()
    {
        if (Manager.Instance.sides.Count > 1) return true;

        if (Manager.Instance.sides.Count == 1)
            if (System.Enum.TryParse(Manager.Instance.sides[0], out Sides side))
            {
                if (side == Sides.None) return false;
                if (side == Sides.Right || side == Sides.Left) return true;
            }
        return false;
    }

    public bool CanGoToAssessmentPanels()
    {
       return AssessPanels_Manager.Instance.IsThereTestsSelected;
    }

    public Sides GetSideToAssess()
    {
        ChoosingSides_Panel_Controller choosingSides_Panel_Controller = choosingSides_Panel_MVC.controller as ChoosingSides_Panel_Controller;
        return choosingSides_Panel_Controller.GetSideToAssess();
    }

    void StartAssessment()
    {
        ChoosingSides_Panel_Visibility(false);
        AssessPanels_Manager.Instance.RemoveButtonsListeners();
        AssessPanels_Manager.Instance.StartAssessment();
    }

    public void GoNext               ()
    {


        AssessPanels_Manager.Instance.SetAssessment(GetSideToAssess());
        if (CanGoToAssessmentPanels())
        {
            gameVoiceControl.RecognitionResult.RemoveListener(onReceiveRecognitionResult);
            try
            {
                var speechRecognizier = VoiceSides.GetComponent("AndroidSpeechRecognizer");
                if (speechRecognizier != null)
                {
                    Destroy(speechRecognizier);
                }
            }
            catch (System.Exception e)
            {

            }
            gameVoiceControl.onStopListening();
            AudioButton.onClick.RemoveAllListeners();
            VoiceSides.SetActive(false);
            VoiceAssessment.SetActive(true);
           AudioButton.onClick.RemoveAllListeners();
            ChoosingSubspeciality_Panel_Manager.Instance.helpButton.OnClick.RemoveAllListeners();
            ChoosingSubspeciality_Panel_Manager.Instance.helpButton.AddListener(AssessPanels_Manager.Instance.helpButtonfunction);
            StartAssessment();
            Debug.Log("test");
        }
    }

    public void GoBack()
    {
        AudioButton.onClick.RemoveAllListeners();

        gameVoiceControl.RecognitionResult.RemoveListener(onReceiveRecognitionResult);
        try
        {
            var speechRecognizier = VoiceSides.GetComponent("AndroidSpeechRecognizer");
            if (speechRecognizier != null)
            {
                Destroy(speechRecognizier);
            }
        }
        catch (System.Exception e)
        {

        }
        gameVoiceControl.onStopListening();
        VoiceSides.SetActive(false);
        VoiceCustomization.SetActive(true);
        RemovePanelContent(choosingSides_PanelHolder);
        AssessPanels_Manager.Instance.RemoveButtonsListeners();
        CustomizePanels_Manager.Instance.EnterCustomizePanel();
        ChoosingSubspeciality_Panel_Manager.Instance.helpButton.OnClick.RemoveAllListeners();
        ChoosingSubspeciality_Panel_Manager.Instance.helpButton.AddListener(CustomizePanels_Manager.Instance.helpButtonfunction);


    }

    private void RemovePanelContent(Transform holderTransform)
    {
        while (holderTransform.childCount > 0)
            DestroyImmediate(holderTransform.GetChild(0).gameObject);
    }
    private void ChoosingSides_Panel_Visibility(bool _show)
    {
        choosingSides_PanelHolder.gameObject.SetActive(_show);
    }
    [SkipRename]
    public void EnableChoosingSides_Panel_View()
    {
        AssessPanels_Manager.Instance.AddButtonsListeners(GoBack, GoBack, GoNext, GoNext);
        ChoosingSides_Panel_Visibility(true);
    }

    [SkipRename]
    public void AdjustButtons()
    {
        AssessPanels_Manager.Instance.AddButtonsListeners(GoBack, GoBack, GoNext, GoNext);
        ChoosingSides_Panel_Visibility(true);
        StartCoroutine(ViewIsReadySetVoice());

    }
    [SkipRename]
    public void AudioConvert()
    {
        AssessPanels_Manager.Instance.AddButtonsListeners(GoBack, GoBack, GoNext, GoNext);
        ChoosingSides_Panel_Visibility(true);
        StartCoroutine(ViewIsReadySetVoice());
    }
    public void Enter_ChoosingSides_Panel()
    {
        LoadChoosingSides_Panel();
    }
    public void helpButtonfunction()
    {
        gRP_ManagerBase.SetActive(true);
    }
    public void helpPromptClose()
    {
        //todo
        gRP_ManagerBase.SetActive(false);
    }
    IEnumerator ViewIsReadySetVoice()
    {
        ChoosingSides_Panel_Controller choosingSides_Panel_Controller = choosingSides_Panel_MVC.controller as ChoosingSides_Panel_Controller;
        yield return new WaitUntil(() => choosingSides_Panel_Controller.phraseAction.Count > 0);

          ChoicesVoicePhrases = choosingSides_Panel_Controller.phraseAction;
        //VoiceCustomization.SetActive(false);
        VoiceSides.SetActive(true);
        gameVoiceControl.reinitializeVoice();
        gameVoiceControl.RecognitionResult.AddListener(onReceiveRecognitionResult);
        AudioButton.GetComponent<Image>().sprite = Audioon;
    }
    public string ToUpperEveryWord(string s)
    {
        var words = s.Split(' ');

        var t = "";
        foreach (var word in words)
        {
            t += char.ToUpper(word[0]) + word.Substring(1) + ' ';
        }
        return (t.Trim());
    }
    public void onReceiveRecognitionResult(string result)
    {
        Debug.Log("popos  in the sides" +result);
        var words = result.Split(' ');

        var t = "";
        foreach (var word in words)
        {
            t += char.ToUpper(word[0]) + word.Substring(1) + ' ';
        }
        result= (t.Trim());
        result = result.ToString();
        switch (result)
        {
            case "Previous":
                GoBack();
                break;
            case "Previous Group":
                GoBack();
                break;
            case "Back Group":
                GoBack();
                break;
            case "Back":
                GoBack();
                break;
            case "Next":
                GoNext();
                break;
            case "Forward":
                GoNext();
                break;
            case "Next Group":
                GoNext();
                break;
            case "Help":
                helpButtonfunction();
                break;
            case "Close":
                helpPromptClose();
                break;
            case "Upper Right":
                Debug.Log("the switchcase enter here 1" + result);
                ChoicesVoicePhrases["UpperRight"]();
                break;
            case "Upper Left":
                Debug.Log("the switchcase enter here 2" + result);
                ChoicesVoicePhrases["UpperLeft"]();
                break;
            case "Lower Right":
                Debug.Log("the switchcase enter here 3" + result);
                ChoicesVoicePhrases["LowerRight"]();
                break;
            case "Lower Left":
                Debug.Log("the switchcase enter here 4" + result);
                ChoicesVoicePhrases["LowerLeft"]();
                break;
            default:
                Debug.Log("the switchcase enter here " + result);
                break;


        }
    }

   
    void SetVoiceControl()
    {
    //    Dictionary<string, System.Action> allphraseAction = Dict_Extensions.CloneDictionaryCloningValues(ChoicesVoicePhrases);

    //    allphraseAction.Add("Next Group", GoNext);
    //    allphraseAction.Add("Next", GoNext);
    //    allphraseAction.Add("Forward", GoNext);
    //    allphraseAction.Add("Back Group", GoBack);
    //    allphraseAction.Add("Back", GoBack);
    //    allphraseAction.Add("help", helpButtonfunction);
    //    allphraseAction.Add("close", helpPromptClose);
    //    PanelVoiceController.Instance.AddSetOfPhrases(allphraseAction);
    }
}