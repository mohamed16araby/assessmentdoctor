﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;
using System;
using System.Reflection;
using UnityAction = UnityEngine.Events.UnityAction;
using Beebyte.Obfuscator;

public class AssessPanels_Manager : MonoBehaviour
{
    [Header("Main Panel with its Holder")]
    [SerializeField]
    MainPanel_MVC mainPanel_MVC;
    [SerializeField]
    public Transform assessPanelHolder;

    [Header("Side Panel with its Holder")]
    [SerializeField]
    SidePanel_MVC sidePanel_MVC;
    [SerializeField]
    Transform sidePanelHolder;

    [Header("Panel transition buttons")]
    [SerializeField]
    public AssessButton previousGroup;
    [SerializeField]
    public AssessButton previousPanel;
    [SerializeField]
    public AssessButton nextPanel;
    [SerializeField]
    public AssessButton nextGroup;
    public static  Dictionary<string, System.Action> ChoicesVoicePhrases = new Dictionary<string, System.Action>();
    [Header("Events")]
    [SerializeField]
    GameEvent loadNextSceneEvent;

    List<string> sidesToAssess;
    bool ableToGONextScene = true;
    public static int countDiagnosis;
    public static int countlistTreatment;

    public string[] number = new string[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "25", "30", "35", "40", "45", "50", "55", "60", "65", "70", "75", "80", "85", "90", "95", "100", "105", "110", "115", "120", "125", "130", "135", "140", "145", "150", "155", "160", "165", "170", "175", "180" };
    public string[] numberresult = new string[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen", "twenty", "twenty five", "thirty", "thirty five", "forty", "forty five", "fifty", "fifty five", "sixty", "sixty five", "seventy", "seventy five", "eighty", "eighty five", "ninety", "ninety five", "hundred", "hundred five", "hundred ten", "hundred fifteen", "hundred twenty", "hundred twenty five", "hundred thirty", "hundred thirty five", "hundred forty", "hundred forty five", "hundred fifty", "hundred fifty five", "hundred sixty", "hundred sixty five", "hundred seventy", "hundred seventy five", "hundred eighty" };

    public Sprite Audioon;
    public Sprite AudioOff;
    public Button AudioButton;
    public GameObject gR_ManagerBase;
    private static AssessPanels_Manager instance;
    public GameVoiceControl gameVoiceControl;
    public GameObject VoiceSides;
    public GameObject VoiceAssessment;
    public static int flag=0;
    public static AssessPanels_Manager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<AssessPanels_Manager>();
            return instance;
        }
    }

    public Dictionary<CustomizeItem, ITransit> _allGroupsDictionary_Before_Customization = new Dictionary<CustomizeItem, ITransit>();
    public SortedDictionary<ITransit> _groupsDictionary_After_Customization = new SortedDictionary<ITransit>();
    Dictionary<Group, Group> groupsAfter_BeforeCustomization = new Dictionary<Group, Group>();

    ///---Indices---///
    int currentGroupsIndex = 0;
    int CurrentPanelsIndex
    {
        get => CurrentGroup_toDisplay.lastPanelVisited_index;
        set => CurrentGroup_toDisplay.lastPanelVisited_index = value;
    }


    int CurrentSectionsIndex
    {
        get => CurrentPanel_toDisplay.lastSectionVisited_index;
        set => CurrentPanel_toDisplay.lastSectionVisited_index = value;
    }
    int CurrentSidesIndex
    {
        get => CurrentSection_toDisplay.lastSideVisited_index;
        set => CurrentSection_toDisplay.lastSideVisited_index = value;
    }

    /// <summary>
    /// First group with currentGroupsIndex in _groupsDictionary_After_Customization.
    /// </summary>
    public Group CurrentGroup_toDisplay { get => _groupsDictionary_After_Customization.SortedView[currentGroupsIndex].Value[0] as Group; }

    /// <summary>
    /// First panel with CurrentPanelsIndex in CurrentGroup_toDisplay.
    /// </summary>
    public Panel CurrentPanel_toDisplay { get => CurrentGroup_toDisplay.Panels.SortedView[CurrentPanelsIndex].Value[0] as Panel; }

    /// <summary>
    /// First section with CurrentSectionsIndex in CurrentPanel_toDisplay.
    /// </summary>
    public Section CurrentSection_toDisplay { get => CurrentPanel_toDisplay.Sections.SortedView[CurrentSectionsIndex].Value[0] as Section; }

    /// <summary>
    /// First side with CurrentSectionsIndex in CurrentSection_toDisplay.
    /// </summary>
    public Side CurrentSide_toDisplay { get => CurrentSection_toDisplay.Sides.SortedView[CurrentSidesIndex].Value[0] as Side; }

    public bool IsThereTestsSelected
    {
        get
        {
            return (_groupsDictionary_After_Customization.Count() != 0);
        }
    }

    #region Start
    public void SetAssessment(Sides sideToAssess = Sides.None)
    {
        if (_allGroupsDictionary_Before_Customization == null || _allGroupsDictionary_Before_Customization.Count == 0)
            return;

        switch (sideToAssess)
        {
            case Sides.None:
                this.sidesToAssess = new List<string>() { sideToAssess.ToString() };
                break;

            case Sides.Left:
            case Sides.Right:
                this.sidesToAssess = new List<string>() { Sides.None.ToString(), sideToAssess.ToString() };
                break;
            case Sides.UpperLeft:
            case Sides.UpperRight:
            case Sides.LowerLeft:
            case Sides.LowerRight:
                this.sidesToAssess = new List<string>() { Sides.None.ToString(), sideToAssess.ToString() };
                break;

            case Sides.Bilateral:
                this.sidesToAssess = new List<string>() { Sides.None.ToString(), Sides.Left.ToString(), Sides.Right.ToString() };
                break;


        }

        _groupsDictionary_After_Customization = GetSelectedChildren<Group>(_allGroupsDictionary_Before_Customization);
        _groupsDictionary_After_Customization = GetAssessmentsAccordingToSides<Group>(_groupsDictionary_After_Customization);
    }
    /// <summary>
    /// First instantiation to panel content.
    /// </summary>
    public void StartAssessment()
    {
       // VoiceSides.SetActive(false);
        VoiceAssessment.SetActive(true);
        AudioButton.onClick.AddListener(FVoiceController);
        gameVoiceControl.reinitializeVoice();
        gameVoiceControl.RecognitionResult.AddListener(onReceiveRecognitionResult);
        AudioButton.GetComponent<Image>().sprite = Audioon;
        for (int i = 0; i < numberresult.Length; i++)
        {
            Debug.Log("the number is " + numberresult[i].ToString());
        }
            AddAssessmentButtonsListeners();
        UpdatePanels();
    }
    [SkipRename]
    public void FVoiceController()
    {
        if (AudioButton.GetComponent<Image>().sprite == Audioon)
        {
            gameVoiceControl.onStopListening();
            AudioButton.GetComponent<Image>().sprite = AudioOff;
        }
        else
        {
            gameVoiceControl.onStartListening();
            AudioButton.GetComponent<Image>().sprite = Audioon;
        }
    }
    SortedDictionary<ITransit> GetAssessmentsAccordingToSides<T>(SortedDictionary<ITransit> groupsDictionary_After_Customization)
    {
        SortedDictionary<ITransit> returnedDictionary = new SortedDictionary<ITransit>();

        foreach (KeyValuePair<int, List<ITransit>> keyValuePair in groupsDictionary_After_Customization.SortedView)
        {
            if (typeof(T) == typeof(Group))
            {
                Group group = keyValuePair.Value[0] as Group;

                group.Panels = GetAssessmentsAccordingToSides<Panel>(group.Panels);
                if (group.Panels.Count() != 0)
                    returnedDictionary[group.displayOrder].Add(group);
            }

            else if (typeof(T) == typeof(Panel))
            {
                Panel panel = keyValuePair.Value[0] as Panel;
                panel.Sections = GetAssessmentsAccordingToSides<Section>(panel.Sections);
                if (panel.Sections.Count() != 0)
                    returnedDictionary[panel.displayOrder].Add(panel);
            }

            else if (typeof(T) == typeof(Section))
            {
                Section section = keyValuePair.Value[0] as Section;
                section.Sides = GetAssessmentsAccordingToSides<Side>(section.Sides);
                if (section.Sides.Count() != 0)
                    returnedDictionary[section.displayOrder].Add(section);
            }

            else if (typeof(T) == typeof(Side))
            {
                Side side = keyValuePair.Value[0] as Side;
                if (sidesToAssess.Contains(side.name))
                    //if (sidesToAssess.Contains(side.name) || sidesToAssess[0] == Sides.None.ToString())
                    returnedDictionary[side.displayOrder].Add(side);
            }
        }

        return returnedDictionary;
    }

    /// <summary>
    /// Returns children that got selected from customize panel.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="childrenDictionary"></param>
    /// <returns></returns>
    SortedDictionary<ITransit> GetSelectedChildren<T>(Dictionary<CustomizeItem, ITransit> childrenDictionaryOrig) where T : ITransit
    {
        Dictionary<CustomizeItem, ITransit> childrenDictionary = childrenDictionaryOrig;

        SortedDictionary<ITransit> childrenList = new SortedDictionary<ITransit>();
        foreach (CustomizeItem item in childrenDictionary.Keys.ToList())
        {
            int displayOrder = 0;
            ITransit item2 = null;

            if (item.CheckType == TristateCheck.FullChecked || item.CheckType == TristateCheck.SemiChecked)
                if (typeof(T) == typeof(Group))
                {
                    Group group = (Group)(childrenDictionary[item] as Group).Clone();
                    displayOrder = group.displayOrder;

                    if (item.CheckType == TristateCheck.SemiChecked)
                        group.Panels = (GetSelectedChildren<Panel>(item.childrenDictionary));
                    item2 = group;

                    groupsAfter_BeforeCustomization.Add(group, childrenDictionary[item] as Group);

                }
                else if (typeof(T) == typeof(Panel))
                {
                    Panel panel = (Panel)(childrenDictionary[item] as Panel).Clone();
                    displayOrder = panel.displayOrder;

                    if (item.CheckType == TristateCheck.SemiChecked)
                        panel.Sections = GetSelectedChildren<Section>(item.childrenDictionary);

                    item2 = panel;
                }
                else if (typeof(T) == typeof(Section))
                {
                    Section section = (Section)(childrenDictionary[item] as Section).Clone();
                    displayOrder = section.displayOrder;

                    if (item.CheckType == TristateCheck.SemiChecked)
                    {
                        EqualizeCheckTypeForSides(item);
                        section.Sides = GetSelectedChildren<Side>(item.childrenDictionary);
                    }

                    item2 = section;
                }
                else if (typeof(T) == typeof(Side))
                {
                    Side side = (Side)(childrenDictionary[item] as Side).Clone();
                    displayOrder = side.displayOrder;

                    if (item.CheckType == TristateCheck.SemiChecked)
                        side.Tests = GetSelectedChildren<EntryField>(item.childrenDictionary);

                    item2 = side;
                }
                else if (typeof(T) == typeof(EntryField))
                {
                    EntryField entry = (EntryField)(childrenDictionary[item] as EntryField).Clone();
                    displayOrder = 1;

                    item2 = entry;
                }

            if (item2 != null)
                childrenList[displayOrder].Add(item2);
        }
        return childrenList;
    }

    void EqualizeCheckTypeForSides(CustomizeItem sides)
    {
        for (int i = 1; i < sides.childrenDictionary.Keys.Count; i++)
            if (sides.childrenDictionary.Count > 1)
                SameCheckTypeForSides(sides.childrenDictionary.Keys.ToList()[i], sides.childrenDictionary.Keys.ToList()[0]);
    }


    void SameCheckTypeForSides(CustomizeItem itemToBeUpdated, CustomizeItem itemReference)
    {
        itemToBeUpdated.ChangeCheckType(itemReference.CheckType);

        for (int i = 0; i < itemToBeUpdated.childrenDictionary.Keys.Count; i++)
            SameCheckTypeForSides(itemToBeUpdated.childrenDictionary.Keys.ToList()[i], itemReference.childrenDictionary.Keys.ToList()[i]);
    }
    #endregion

    #region Update Panels Contents
    #region Update
    private void UpdatePanels()
    {

        RemovePanelsContents();
        SetPanels();
        StartToListen(mainPanel_MVC.controller as MainPanel_Controller);
        UpdateButtonsInteractability(AssessButtonsConstraints());
        Manager.Instance.StoreData();
        Manager.Instance.ResetScroll();

    }
    #endregion

    #region Set
    public void SetPanels()
    {
        SetPanel(mainPanel_MVC, assessPanelHolder);
        SetPanel(sidePanel_MVC, sidePanelHolder);
    }
    
    public void resetAudio()
    {

    }
    public void SetPanel<T>(Panel_MVC<T> mvcPanel, Transform mvcPanel_Holder) where T : Panel_4MainHeaders_View
    {
        mvcPanel.Initialize(mvcPanel_Holder);
        //VoiceAssessment.SetActive(true);
        //VoiceSides.SetActive(false);

   //  AudioButton.onClick.AddListener(FVoiceController);
        Panel_4MainHeaders_Model panel_Model = typeof(T) == typeof(MainPanel_View) ? new MainPanel_Model() : new Panel_4MainHeaders_Model();
        panel_Model.groupName = CurrentGroup_toDisplay.name;
        panel_Model.panelName = CurrentPanel_toDisplay.name;
        panel_Model.sectionName = CurrentSection_toDisplay.name;
        panel_Model.sideName = CurrentSide_toDisplay.name;
        panel_Model.hideSectionTitle = CurrentSection_toDisplay.hideTitle;
        panel_Model.hideTitle = CurrentPanel_toDisplay.hideTitle;
        panel_Model.Questions = CurrentSide_toDisplay.Tests;
        if (typeof(T) == typeof(MainPanel_View))
            panel_Model = SetExtraDataForMainPanel(panel_Model);

        mvcPanel.controller.SetData(panel_Model);
    }

    Panel_4MainHeaders_Model SetExtraDataForMainPanel(Panel_4MainHeaders_Model panel_Model)
    {
        MainPanel_Model mainPanel_Model = (MainPanel_Model)panel_Model;

        mainPanel_Model.scoreTracking = CurrentGroup_toDisplay.scoreTracking;
        mainPanel_Model.currentGroup = groupsAfter_BeforeCustomization[CurrentGroup_toDisplay];
        mainPanel_Model.currentPanel = groupsAfter_BeforeCustomization[CurrentGroup_toDisplay].Panels.SortedView
                                    .Where(d => d.Key == CurrentPanel_toDisplay.displayOrder).ToList()[0].Value[0] as Panel;
        return mainPanel_Model;
    }

    void StartToListen(MainPanel_Controller mainPanel_Controller)
    {
      // ChoicesVoicePhrases = mainPanel_Controller.panelPhrasesActions_Voice;
        
        StartCoroutine(StartToListen_WhenReady(mainPanel_Controller));
    }


    IEnumerator StartToListen_WhenReady(MainPanel_Controller mainPanel_Controller)
    {
        yield return new WaitUntil(() => mainPanel_Controller.PanelIsDone_VoiceReady == true);
       ChoicesVoicePhrases = mainPanel_Controller.panelPhrasesActions_Voice;
       
       // SetVoice(mainPanel_Controller.panelPhrasesActions_Voice);
    }
    public void helpButtonfunction()
    {
        gR_ManagerBase.SetActive(true);
    }
    public void helpPromptClose()
    {
        //todo
        gR_ManagerBase.SetActive(false);
    }
    public void onReceiveRecognitionResult(string result)
    {     
        var words = result.Split(' ');

        var t = "";
        foreach (var word in words)
        {
            t += char.ToUpper(word[0]) + word.Substring(1) + ' ';
        }
        result = t.Trim();
        if (result == "Previous") GoPrevious_Panel();
        else if (result == "Previous Group") GoPrevious_Group();
        else if (result == "Next") GoNext_Panel();
        else if (result == "Forward") GoNext_Panel();
        else if (result == "Next Group") GoNext_Group();
        else if (result == "Help") helpButtonfunction();
        else if (result == "Close") helpPromptClose();
        else if (result == "Main Sides") SideBack();
        else if (result == "Home") SideBack();
        else
        {
            for (int i = 0; i < numberresult.Length; i++)
            {
                if (result == numberresult[i].ToString())
                {
                    ChoicesVoicePhrases[number[i]]();
                    flag = 1;
                    return;
                }
                else
                {
                    flag = 0;
                }
            }
            if (flag == 0)
            {
                ChoicesVoicePhrases[result]();
            }
        }
    }
    

    //[ObfuscateLiterals]
    void SetVoice(Dictionary<string, Action> phrasesOfPanel)
    {
        //Dictionary<string, Action> finalPhrases = phrasesOfPanel;

        //finalPhrases.Add("Next Group", GoNext_Group);
        //finalPhrases.Add("Next", GoNext_Panel);
        //finalPhrases.Add("Forward", GoNext_Panel);
        //finalPhrases.Add("Back Group", GoPrevious_Group);
        //finalPhrases.Add("Back", GoPrevious_Panel);
        //finalPhrases.Add("Main Sides", SideBack);
        //finalPhrases.Add("home", HomeCustomization);
        //finalPhrases.Add("quadrant", SideBack);
        //finalPhrases.Add("help", helpButtonfunction);
        //finalPhrases.Add("close", helpPromptClose);
        //PanelVoiceController.Instance.AddSetOfPhrases(finalPhrases);
    }
    #endregion

    #region Remove
    public void RemovePanelsContents()
    {
        RemoveContents(assessPanelHolder);
        RemoveContents(sidePanelHolder);
    }
    void RemoveContents(Transform holderTransform)
    {
        while (holderTransform.childCount > 0)
            DestroyImmediate(holderTransform.GetChild(0).gameObject);
    }
    #endregion

    #region Enable/Disable
    public void DisablePanelsContents()
    {
        assessPanelHolder.gameObject.SetActive(false);
        sidePanelHolder.gameObject.SetActive(false);
    }

    public void EnablePanelsContents()
    {
        assessPanelHolder.gameObject.SetActive(true);
        sidePanelHolder.gameObject.SetActive(true);
    }

    #endregion
    #endregion

    #region Validate
    #region Next
    #region Group
    bool CanGoNext_Group()
    {
        bool canGoNextGroup = _groupsDictionary_After_Customization.SortedView.Count - 1 != currentGroupsIndex;
        return canGoNextGroup;
    }
    #endregion
    #region Panel
    bool CanGoNext_Panel()
    {
        return CurrentGroup_toDisplay.CanGoNextInPanels(CurrentPanelsIndex);
    }
    #endregion
    #region Section
    bool CanGoNext_Section()
    {
        return CurrentPanel_toDisplay.CanGoNextInSections(CurrentSectionsIndex/*, sidesToAssess*/);
    }
    #endregion
    #region Side
    bool CanGoNext_Side()
    {
        return CurrentSection_toDisplay.CanGoNextInSides(CurrentSidesIndex/*, sidesToAssess*/);
    }
    #endregion
    #endregion

    #region Back
    #region Group
    bool CanGoBack_Group()
    {
        return (currentGroupsIndex > 0) | (!CustomizePanels_Manager.Instance.CurrentPanelIsCustomizePanel);
    }
    #endregion
    #region Panel
    bool CanGoBack_Panel()
    {
        return CurrentGroup_toDisplay.CanGoBackInPanels(CurrentPanelsIndex) & (!CustomizePanels_Manager.Instance.CurrentPanelIsCustomizePanel);
    }
    #endregion
    #region Section
    bool CanGoBack_Section()
    {
        return CurrentPanel_toDisplay.CanGoBackInSections(CurrentSectionsIndex/*, sidesToAssess*/);
    }
    #endregion
    #region Side
    bool CanGoBack_Side()
    {
        return CurrentSection_toDisplay.CanGoBackInSides(CurrentSidesIndex/*, sidesToAssess*/);
    }
    #endregion
    #endregion
    #endregion

    #region Next
    #region Group
    public void GoNext_Group()
    {

        if (CanGoNext_Group())
        {
            EnterGroup_Next();

        }
        else
        {
            ChoosingSubspeciality_Panel_Manager.Instance.helpButton.OnClick.RemoveAllListeners();
            Debug.Log("test");
            loadNextSceneEvent.Raise();

           
        }  
        }
    #endregion
    #region Panel
    public void GoNext_Panel()
    {
        if (CanGoNext_Side())
            EnterSide_Next();

        else if (CanGoNext_Section())
            EnterSection_Next();

        else if (CanGoNext_Panel())
                if (CurrentGroup_toDisplay.name == "Diagnosis" || CurrentGroup_toDisplay.name == "Treatment")
                    if (CanGoNext_Panel())
                        EnterPanel_Next();
                    else
                        loadNextSceneEvent.Raise();
                else
                    EnterPanel_Next();

        else if (CanGoNext_Group())
            EnterGroup_Next();
        else
            loadNextSceneEvent.Raise();
    }
    #endregion
    #endregion

    #region Previous
    #region Group
    public void GoPrevious_Group()
    {
        if (CanGoBack_Group())
            EnterGroup_Back();
    }
    #endregion
    #region Panel
    public void GoPrevious_Panel()
    {
        if (CanGoBack_Side())
            EnterSide_Back();

        else if (CanGoBack_Section())
            EnterSection_Back();

        else if (CanGoBack_Panel())
            if (CurrentGroup_toDisplay.name == "Diagnosis" || CurrentGroup_toDisplay.name == "Treatment")
                // EnterGroup_Back();
                EnterPanel_Back();
            else
                EnterPanel_Back();

        else if (CanGoBack_Group())
            EnterGroup_Back();
    }
    #endregion
    #endregion

    #region Enter
    #region Group
    public void EnterGroup_Next()
    {
        string str = CurrentGroup_toDisplay.name;
        currentGroupsIndex++;
        if ((str == "Choose Your Tooth Number"))
        {
            countDiagnosis = 0;
            CurrentPanelsIndex = (int)ToggleAssess_Number.ToothNumberDiagnsis[countDiagnosis] - 1;
                countDiagnosis++;
        }
        if ((str == "Tooth Number"))
        {
            countlistTreatment = 0;
                CurrentPanelsIndex = (int)ToggleAssess_Number.ToothNumberTreatment[countlistTreatment] - 1;
                countlistTreatment++;
        }
        UpdatePanels();
    }

    public void EnterGroup_Back()
    {
        if (currentGroupsIndex > 0)
        {
            currentGroupsIndex--;
            UpdatePanels();
        }
        else
        {
            ChoosingSubspeciality_Panel_Manager.Instance.helpButton.OnClick.RemoveAllListeners();
            ChoosingSubspeciality_Panel_Manager.Instance.helpButton.AddListener(ChoosingSides_Panel_Manager.Instance.helpButtonfunction);
            gameVoiceControl.RecognitionResult.RemoveListener(onReceiveRecognitionResult);
            try
            {
                var speechRecognizier = VoiceAssessment.GetComponent("AndroidSpeechRecognizer");
                if (speechRecognizier != null)
                {
                    Destroy(speechRecognizier);
                }
            }
            catch (System.Exception e)
            {

            }
            gameVoiceControl.onStopListening();
            VoiceSides.SetActive(true);
            VoiceAssessment.SetActive(false);
            AudioButton.onClick.RemoveAllListeners();
            //AudioButton.onClick.AddListener(ChoosingSides_Panel_Manager.Instance.FVoiceController);
            RemovePanelsContents();
            RemoveButtonsListeners();
            if (ChoosingSides_Panel_Manager.Instance.IsThereSidesToChooseFrom())
            
                ChoosingSides_Panel_Manager.Instance.EnableChoosingSides_Panel_View();
            else
                CustomizePanels_Manager.Instance.EnterCustomizePanel();
        }
    }

    #endregion
    #region Panel
    public void EnterPanel_Next()
    {
        if (CurrentGroup_toDisplay.name == "Diagnosis")
        {
            if (ToggleAssess_Number.ToothNumberDiagnsis.Count > countDiagnosis)
            {
                CurrentPanelsIndex = (int)ToggleAssess_Number.ToothNumberDiagnsis[countDiagnosis] - 1;
                countDiagnosis++;
            }
            else
            {
                if (CanGoNext_Group())
                    EnterGroup_Next();
                else
                {
                    loadNextSceneEvent.Raise();
                   
                }
            }
        }
        else if (CurrentGroup_toDisplay.name == "Treatment")
        {
            if (ToggleAssess_Number.ToothNumberTreatment.Count > countlistTreatment)
            {
                CurrentPanelsIndex = (int)ToggleAssess_Number.ToothNumberTreatment[countlistTreatment] - 1;
                countlistTreatment++;
            }
            else
            {
                if (CanGoNext_Group())
                    EnterGroup_Next();
                else
                    loadNextSceneEvent.Raise();
            }
        }
        else
        CurrentPanelsIndex++;
        UpdatePanels();
    }
    public void EnterPanel_Back()
    {
        if (CurrentGroup_toDisplay.name == "Diagnosis")
        {
            countDiagnosis--;
            if (countDiagnosis > 0)
            {
                CurrentPanelsIndex = (int)ToggleAssess_Number.ToothNumberDiagnsis[countDiagnosis-1] - 1;          
            }
            else
            {
                if (CanGoBack_Group())
                {
                    EnterGroup_Back();
                }
            }
        }
        else if (CurrentGroup_toDisplay.name == "Treatment")
        {
            countlistTreatment--;
            if (countlistTreatment > 0)
            {
                CurrentPanelsIndex = (int)ToggleAssess_Number.ToothNumberTreatment[countlistTreatment - 1] - 1;
            }
            else
            {
                if (CanGoBack_Group())
                {
                    EnterGroup_Back();
                }
            }
        }
        else
            CurrentPanelsIndex--;
        UpdatePanels();
    }
    #endregion
    #region Section
    public void EnterSection_Next()
    {
        CurrentSectionsIndex++;
        UpdatePanels();
    }
    public void EnterSection_Back()
    {
        CurrentSectionsIndex--;
        UpdatePanels();
    }
    #endregion
    #region Side
    public void EnterSide_Next()
    {

        CurrentSidesIndex++;
        UpdatePanels();
    }
    public void SideBack()
    {
        currentGroupsIndex = 0;
        RemovePanelsContents();
        RemoveButtonsListeners();
        if (ChoosingSides_Panel_Manager.Instance.IsThereSidesToChooseFrom())
            ChoosingSides_Panel_Manager.Instance.EnableChoosingSides_Panel_View();
        else
            CustomizePanels_Manager.Instance.EnterCustomizePanel();
    }
    public void HomeCustomization()
    {
        currentGroupsIndex = 0;
        RemovePanelsContents();
        RemoveButtonsListeners();
        if (ChoosingSides_Panel_Manager.Instance.IsThereSidesToChooseFrom())
            ChoosingSides_Panel_Manager.Instance.EnableChoosingSides_Panel_View();
        else
            CustomizePanels_Manager.Instance.EnterCustomizePanel();
        ChoosingSides_Panel_Manager.Instance.GoBack();
    }
    public void EnterSide_Back()
    {
        CurrentSidesIndex--;
        UpdatePanels();
    }
    #endregion
    #endregion

    #region Buttons
    public void AddAssessmentButtonsListeners()
    {
        //helpButton.OnClick.RemoveAllListeners();
        //helpButton.AddListener(HelpButtonFunction);
        AddButtonsListeners(GoPrevious_Group, GoPrevious_Panel, GoNext_Panel, GoNext_Group);
    }
    //public void HelpButtonFunction()
    //{
    //    gRP_ManagerBase.Create(true);
    //}

    /// <summary>
    /// Add new functions to listen to assessment buttons clicks to transit between panels.
    /// </summary>
    public void AddButtonsListeners(params UnityAction[] buttonsActions)
    {
        previousGroup.AddListener(buttonsActions[0]);
        previousPanel.AddListener(buttonsActions[1]);
        nextPanel.AddListener(buttonsActions[2]);
        nextGroup.AddListener(buttonsActions[3]);

        UpdateButtonsInteractability(buttonsActions[0] != null, buttonsActions[1] != null, buttonsActions[2] != null, buttonsActions[3] != null);
    }
    public void RemoveButtonsListeners()
    {
        previousGroup.OnClick.RemoveAllListeners();
        previousPanel.OnClick.RemoveAllListeners();
        nextPanel.OnClick.RemoveAllListeners();
        nextGroup.OnClick.RemoveAllListeners();

        UpdateButtonsInteractability(false, false, false, false);
    }

    public bool[] AssessButtonsConstraints()
    {
        bool prevGroupBtn_Interact = CanGoBack_Group();
        bool prevPanelBtn_Interact = CanGoBack_Side() | CanGoBack_Section() | CanGoBack_Panel() | CanGoBack_Group();
        bool nextPanelBtn_Interact = CanGoNext_Side() | CanGoNext_Section() | CanGoNext_Panel() | CanGoNext_Group() | ableToGONextScene;
        bool nextGroupBtn_Interact = CanGoNext_Group() | ableToGONextScene;

        return new bool[] { prevGroupBtn_Interact, prevPanelBtn_Interact, nextPanelBtn_Interact, nextGroupBtn_Interact };
    }

    private void UpdateButtonsInteractability(params bool[] buttonsConditions)
    {
        previousGroup.UpdateInteractability(buttonsConditions[0]);
        previousPanel.UpdateInteractability(buttonsConditions[1]);
        nextPanel.UpdateInteractability(buttonsConditions[2]);
        nextGroup.UpdateInteractability(buttonsConditions[3]);
    }
    #endregion
}