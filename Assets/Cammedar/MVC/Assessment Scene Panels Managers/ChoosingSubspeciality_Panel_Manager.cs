﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Beebyte.Obfuscator;
using UnityEngine.UI;

public class ChoosingSubspeciality_Panel_Manager : MonoBehaviour
{
    private static ChoosingSubspeciality_Panel_Manager instance;
    public static ChoosingSubspeciality_Panel_Manager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<ChoosingSubspeciality_Panel_Manager>();
            return instance;
        }
    }
    public ChoosingSubspeciality_Panel_MVC choosingSubspeciality_Panel_MVC;

    [SerializeField]
    Transform choosingSubspecialityHolder;
    [SerializeField]
    Text subspeciality_Chosen_Txt;

    [Header("Events")]
    [SerializeField]
    GameEvent LoadBackSceneEvent;
    public AssessButton helpButton;
    public GameObject gRP_ManagerBase;
    public GameVoiceControl gameVoiceControl;
    public GameObject AddeditObject;
    public GameObject SubSubObject;
    public Sprite Audioon;
    public Sprite AudioOff;
    public Button AudioButton;
    bool flag = true;
    private void Start()
    {
        //helpButton.OnClick.RemoveAllListeners();
        //helpButton.AddListener(helpButtonfunction);

        AssessPanels_Manager.Instance.RemoveButtonsListeners();
        LoadChoosingSubspeciality_Panel();

    }
    [SkipRename]
    public void FVoiceController()
    {
        if (AudioButton.GetComponent<Image>().sprite == Audioon)
        {
            gameVoiceControl.onStopListening();
            AudioButton.GetComponent<Image>().sprite = AudioOff;
        }
        else
        {
            gameVoiceControl.onStartListening();
            AudioButton.GetComponent<Image>().sprite = Audioon;
        }

    }
    public void helpButtonfunction()
    {
        gRP_ManagerBase.SetActive(true);
    }
    public void helpPromptClose()
    {
        gRP_ManagerBase.SetActive(false);
    }

    void LoadChoosingSubspeciality_Panel()
    {
        choosingSubspeciality_Panel_MVC.Initialize(choosingSubspecialityHolder);

        ChoosingSubspeciality_Panel_Model panel_Model = new ChoosingSubspeciality_Panel_Model();
        panel_Model.groupName = "Choose Subspeciality";
        panel_Model.panelName = "";
        panel_Model.hideTitle = true;

        ChoosingSubspeciality_Panel_Controller choosingSubspeciality_Panel_Controller = choosingSubspeciality_Panel_MVC.controller as ChoosingSubspeciality_Panel_Controller;
        choosingSubspeciality_Panel_Controller.SetData(panel_Model, this);
    }

    KeyValuePair<string, string> GetSubspecialityChoiceChoiceChoosen()
    {
        ChoosingSubspeciality_Panel_Controller choosingSubspeciality_Panel_Controller = choosingSubspeciality_Panel_MVC.controller as ChoosingSubspeciality_Panel_Controller;
        return choosingSubspeciality_Panel_Controller.GetSubspecialityChoice();
    }

    [SkipRename]
    public void DisablePanel()
    {
        ChoosingSubspeciality_Panel_Visibility(false);
    }

    public void Enter_AddEdit_MedicalRecord_Panel()
    {
        AssessPanels_Manager.Instance.RemoveButtonsListeners();
        AddEdit_MedicalRecord_Panel_Manager.Instance.Enter_AddEdit_MedicalRecord_Panel();
    }

    public void GoNext()
    {
        if (string.IsNullOrEmpty(GetSubspecialityChoiceChoiceChoosen().Key)) return; 
        
        try
        {
            var speechRecognizier = SubSubObject.GetComponent("AndroidSpeechRecognizer");
            if (speechRecognizier != null)
            {
                Destroy(speechRecognizier);
            }
        }
        catch(System.Exception e)
        {
            Debug.Log("Enter ");
        }
        gameVoiceControl.onStopListening();
        AudioButton.onClick.RemoveListener(FVoiceController);
        SubSubObject.SetActive(false);
        AddeditObject.SetActive(true);
        Enter_AddEdit_MedicalRecord_Panel();
    }


    public void UpdateSubspecialities()
    {
        KeyValuePair<string, string> subspecialityChoiceChoosen = GetSubspecialityChoiceChoiceChoosen();

        UserData.doctor_SubSpecialization_logIn = subspecialityChoiceChoosen.Key;
        UserData.doctor_SubSubSpecialization_logIn = subspecialityChoiceChoosen.Value;

        subspeciality_Chosen_Txt.text = string.IsNullOrEmpty(UserData.doctor_SubSubSpecialization_logIn) ? UserData.doctor_SubSpecialization_logIn : UserData.doctor_SubSubSpecialization_logIn;

        if (!string.IsNullOrEmpty(subspeciality_Chosen_Txt.text)) subspeciality_Chosen_Txt.text += " Assessment";
    }

    public void GoBack()
    {
        Debug.Log("test");
        LoadBackSceneEvent.Raise();
    }

    private void ChoosingSubspeciality_Panel_Visibility(bool _show)
    {
        choosingSubspecialityHolder.gameObject.SetActive(_show);
        ChoosingSubspeciality_Panel_Controller choosingSubspeciality_Panel_Controller = choosingSubspeciality_Panel_MVC.controller as ChoosingSubspeciality_Panel_Controller;
        choosingSubspeciality_Panel_Controller.ViewVisibility(_show);
    }

    [SkipRename]
    public void Enter_ChoosingSubspeciality_Panel()
    {
        AudioButton.onClick.AddListener(FVoiceController);


        Debug.Log("Enter_ChoosingSubspeciality_Panel");
        AssessPanels_Manager.Instance.AddButtonsListeners(GoBack, GoBack, GoNext, GoNext);
        ChoosingSubspeciality_Panel_Visibility(true);
        gameVoiceControl.reinitializeVoice();
        AudioButton.GetComponent<Image>().sprite = Audioon;
    }
    public string ToUpperEveryWord(string s)
    {
        var words = s.Split(' ');

        var t = "";
        foreach (var word in words)
        {
            t += char.ToUpper(word[0]) + word.Substring(1) + ' ';
        }
        return (t.Trim());
    }
    [SkipRename]
    public void onReceiveRecognitionResultSubSub(string result)
    {
        result = ToUpperEveryWord(result);

        if (result == "Back Group")  GoBack();     
        else if (result == "Previous Group")  GoBack();     
        else if (result == "Next") GoNext();
        else if (result == "Forward") GoNext();
        else if (result == "Next Group") GoNext();
        else if (result == "Help") helpButtonfunction();
        else if (result == "Close") helpPromptClose();
        else if (result == "Previous") GoBack();                 
        else if (result == "Back") GoBack();     
    }

    // [ObfuscateLiterals]
    void SetVoiceControl()
    {
        //Dictionary<string, System.Action> phraseAction = new Dictionary<string, System.Action>()
        //{
        //    {"Next", GoNext},
        //    {"Next Group", GoNext},
        //    {"Back", GoBack},
        //    {"Back Group", GoBack},
        //    {"help", helpButtonfunction},
        //    {"close", helpPromptClose},
        //};

        //PanelVoiceController.Instance.AddSetOfPhrases(phraseAction);
    }
}