﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Beebyte.Obfuscator;
using UnityEngine.UI;

public class HistoryController : MonoBehaviour
{
    [SerializeField]
    Button nextBtn;
    [SerializeField]
    Button backBtn;

    private void Start()
    {
        nextBtn.onClick.AddListener(NextBtn_pressed);
        backBtn.onClick.AddListener(BackBtn_pressed);
        AdPhrasesToVoice();
    }

    public void NextBtn_pressed(){
       SceneManager.LoadScene("AssessmentScene");
       //SceneManager.LoadScene("AssessmentScene", LoadSceneMode.Single);
    }

    public void BackBtn_pressed(){
        SceneManager.LoadScene("SelectPatientScene");
        //SceneManager.LoadScene("SelectPatientScene", LoadSceneMode.Single);
    }

    public void AdPhrasesToVoice()
    {
        Dictionary<string, System.Action> phrasesOfPanel = new Dictionary<string, System.Action>()
        {
            {"Next", NextBtn_pressed},
            {"Back", BackBtn_pressed},
        };

        PanelVoiceController.Instance.AddSetOfPhrases(phrasesOfPanel);
    }
}
