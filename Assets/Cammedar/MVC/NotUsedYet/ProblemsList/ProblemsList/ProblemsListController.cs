﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;
using Beebyte.Obfuscator;
using fb = Cammedar.Network.Firebase;
using URI = Cammedar.Network.URI;
using Cammedar.Network;

public class ProblemList : EncryptedSerialization<ProblemList>
{
    public static List<string> selected_problems_Orig = new List<string>();
    public List<string> selected_problems_EnOrDecrpted = new List<string>();

    public override ProblemList Encrypt(string id)
    {
        if (!Debugging.encryptionEnabled) return this;
        selected_problems_EnOrDecrpted = new List<string>();

        for (int i = 0; i < selected_problems_Orig.Count; i++)
            selected_problems_EnOrDecrpted.Add(Encryption(selected_problems_Orig[i], id, true));

        return this;
    }

    public override ProblemList Decrypt(string id)
    {
        if (!Debugging.encryptionEnabled) return this;

        for (int i = 0; i < selected_problems_Orig.Count; i++)
            selected_problems_Orig[i] = Decryption(selected_problems_Orig[i], id, true);

        return this;
    }

    protected override string Decryption(string text, string key, bool CanBeNull)
    {
        return DecryptString_Aes(text, key, key, CanBeNull);
    }

    protected override string Encryption(string text, string key, bool CanBeNull)
    {
        return EncryptString_Aes(text, key, key, CanBeNull);
    }
}

[System.Serializable]
public class ProblemsListModel 
{
    public List<string> problems_list = new List<string>();

    public void OrderList()
    {
        problems_list.Sort();
    }
}

public class ProblemsListController : MonoBehaviour, IEnhancedScrollerDelegate
{
    public EnhancedScroller ProblemsScroller;
    public EnhancedScrollerCellView ProblemsViewPrefab;

    [SerializeField]
    AssessButton nextBtn;
    [SerializeField]
    AssessButton backBtn;
    [SerializeField]
    Text Subspeciality_Chosen_Txt;

    private bool FirebaseProcessFinish = false;
    private Dictionary<string, System.Action> itemsVoicePhrases = new Dictionary<string, System.Action>();
    private ProblemsListModel problems;
    bool goingBack = false;

    void Start()
    {
        Subspeciality_Chosen_Txt.text = UserData.GetLastSpeciality + " Assessment";

        ProblemsScroller.Delegate = this;

        StartCoroutine(CheckFirebaseProcessStatus());
        LoadProblemsListData(FirebaseStoragePaths.StorageDirectory + "Problems_list.json");
    }

    [SkipRename]
    public void LoadProblemsListData(string FirebaseFilePath)
    {
        FBStorage.Instance.Download_To_ByteArray(FirebaseFilePath, delegate (string fileContent)
        {
            FirebaseProcessFinish = true;
            problems = JsonUtility.FromJson<ProblemsListModel>(fileContent);
            problems.OrderList();
        }, delegate { });
    }

    IEnumerator CheckFirebaseProcessStatus()
    {
        yield return new WaitUntil(() => FirebaseProcessFinish == true);

        ProblemList.selected_problems_Orig = Session.Instance.problemList;

        itemsVoicePhrases = new Dictionary<string, System.Action>();
        ProblemsScroller.ReloadData();

        nextBtn.AddListener(NextBtn_pressed);
        backBtn.AddListener(BackBtn_pressed);
        AddPhrasesToVoice(itemsVoicePhrases);
    }

    void SaveProblemList()
    {
        URI uri = fb.Instance.databasePath.Child("sessions").Child(ProgramGeneralData.currentSessionId).Child("problemList").JSON().AuthQuery(UserData.tokenID);

        ProblemList problemList = new ProblemList().Encrypt(PatientData.patient_UDID);

        fb.Instance.Put(uri, problemList.selected_problems_EnOrDecrpted, delegate
        {
            if (goingBack)
                StartCoroutine(ScenesController.Instance.LoadYourAsyncScene("ProblemsList", "Diagnosis_List"));
            else
                StartCoroutine(ScenesController.Instance.LoadYourAsyncScene("ProblemsList", "TreatmentGoals"));

        }, delegate
        {
            EnableTransitionButtons(true);
        });
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return problems != null ? problems.problems_list.Count : 0;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 30f;
    }

    void AddToVoiceDictionary(string phrase, System.Action action)
    {
        if (itemsVoicePhrases.ContainsKey(phrase))
            itemsVoicePhrases[phrase] = action;
        else
            itemsVoicePhrases.Add(phrase, action);
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        ProblemItem cellView = scroller.GetCellView(ProblemsViewPrefab) as ProblemItem;
        cellView.name = "Cell " + dataIndex.ToString();
        cellView.SetData(problems.problems_list[dataIndex], dataIndex, AddToVoiceDictionary);
        return cellView;
    }

    public void NextBtn_pressed()
    {
        EnableTransitionButtons(false);
        goingBack = false;
        SaveProblemList();
    }

    public void BackBtn_pressed()
    {
        EnableTransitionButtons(false);
        goingBack = true;
        SaveProblemList();
    }

    public void EnableTransitionButtons(bool enable)
    {
        backBtn.UpdateInteractability(enable);
        nextBtn.UpdateInteractability(enable);
    }

    public void AddPhrasesToVoice(Dictionary<string, System.Action> phrasesOfPanel)
    {
        phrasesOfPanel.Add("Next", NextBtn_pressed);
        phrasesOfPanel.Add("Back", BackBtn_pressed);

        PanelVoiceController.Instance.AddSetOfPhrases(phrasesOfPanel);
    }
}