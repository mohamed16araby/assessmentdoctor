﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;
using Beebyte.Obfuscator;
using fb = Cammedar.Network.Firebase;
using URI = Cammedar.Network.URI;
using UnityEngine.SceneManagement;
using Cammedar.Network;

public class DiagnosisList : EncryptedSerialization<DiagnosisList>
{
    public static List<string> selected_diagnosis_Orig = new List<string>();
    public List<string> selected_diagnosis_EnOrDecrpted = new List<string>();

    public override DiagnosisList Encrypt(string id)
    {
        if (!Debugging.encryptionEnabled) return this;
        selected_diagnosis_EnOrDecrpted = new List<string>();

        for (int i = 0; i < selected_diagnosis_Orig.Count; i++)
            selected_diagnosis_EnOrDecrpted.Add(Encryption(selected_diagnosis_Orig[i], id, true));

        return this;
    }

    public override DiagnosisList Decrypt(string id)
    {
        if (!Debugging.encryptionEnabled) return this;

        for (int i = 0; i < selected_diagnosis_Orig.Count; i++)
            selected_diagnosis_Orig[i] = Decryption(selected_diagnosis_Orig[i], id, true);

        return this;
    }

    protected override string Decryption(string text, string key, bool CanBeNull)
    {
        return DecryptString_Aes(text, key, key, CanBeNull);
    }

    protected override string Encryption(string text, string key, bool CanBeNull)
    {
        return EncryptString_Aes(text, key, key, CanBeNull);
    }
}

[System.Serializable]
public class DiagnosisList_Model
{
    public List<string> diagnosis_list = new List<string>();

    public void OrderList()
    {
        diagnosis_list.Sort();
    }
}

public class DiagnosisList_Controller : MonoBehaviour, IEnhancedScrollerDelegate
{
    public EnhancedScroller DiagnosisScroller;
    public EnhancedScrollerCellView diagnosisViewPrefab;

    [SerializeField]
    AssessButton nextBtn;
    [SerializeField]
    AssessButton backBtn;
    [SerializeField]
    Text Subspeciality_Chosen_Txt;
    public Sprite Audioon;
    public Sprite AudioOff;
    [SerializeField]
    Button AudioButton;
    GameObject AudioController;
    private bool FirebaseProcessFinish = false;
    private Dictionary<string, System.Action> itemsVoicePhrases = new Dictionary<string, System.Action>();
    private DiagnosisList_Model diagnosis;
    bool goingBack = false;

    void Start()
    {
        Subspeciality_Chosen_Txt.text = UserData.GetLastSpeciality + " Assessment";

        DiagnosisScroller.Delegate = this;

        StartCoroutine(CheckFirebaseProcessStatus());
        LoadDiagnosisListData(FirebaseStoragePaths.StorageDirectory + "Diagnosis_list.json");
        AudioButton.onClick.AddListener(FVoiceController);
        AudioController = GameObject.Find("VoiceControl");
    }
    public void FVoiceController()
    {
        Debug.Log("log");
        if (AudioController.activeSelf)
        {
            AudioButton.GetComponent<Image>().sprite = AudioOff;
#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
            VoiceControl.Instance.Discard();
#elif UNITY_ANDROID

        Debug.Log("android");
#endif
            AudioController.SetActive(false);
        }
        else
        {
            AudioButton.GetComponent<Image>().sprite = Audioon;
            AudioController.SetActive(true);
#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
            VoiceControl.Instance.RestartRecognizer();
#elif UNITY_ANDROID

        Debug.Log("android");
#endif

        }
    }

    [SkipRename]
    public void LoadDiagnosisListData(string FirebaseFilePath)
    {
        FBStorage.Instance.Download_To_ByteArray(FirebaseFilePath,
            delegate (string fileContent)
            {
                FirebaseProcessFinish = true;
                diagnosis = JsonUtility.FromJson<DiagnosisList_Model>(fileContent);
                diagnosis.OrderList();
            }, delegate { });
    }

    IEnumerator CheckFirebaseProcessStatus()
    {
        yield return new WaitUntil(() => FirebaseProcessFinish == true);

        DiagnosisList.selected_diagnosis_Orig = Session.Instance.diagnosisList;

        itemsVoicePhrases = new Dictionary<string, System.Action>();
        DiagnosisScroller.ReloadData();

        nextBtn.AddListener(NextBtn_pressed);
        backBtn.AddListener(BackBtn_pressed);
        AddPhrasesToVoice(itemsVoicePhrases);
    }

    void SaveDiagnosisList()
    {
        URI uri = fb.Instance.databasePath.Child("sessions").Child(ProgramGeneralData.currentSessionId).Child("diagnosisList").JSON().AuthQuery(UserData.tokenID);

        DiagnosisList diagnosisList = new DiagnosisList().Encrypt(PatientData.patient_UDID);

        fb.Instance.Put(uri, diagnosisList.selected_diagnosis_EnOrDecrpted, delegate
        {
            if (goingBack)
                GoBack();
            else
                StartCoroutine(ScenesController.Instance.LoadYourAsyncScene("Diagnosis_List", "TreatmentGoals"));

        }, delegate
        {
            EnableTransitionButtons(true);
        });
    }

    public void GoBack() //Will be removed when diag&predictions are viewed
    {
        Manager.Instance.HideSceneContent();
        SceneManager.UnloadSceneAsync("Diagnosis_List");
        Manager.Instance.ViewAssessmentSceneViewContent();
        PanelVoiceController.Instance.ContinueListeningToAssessmentContent();
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return diagnosis != null ? diagnosis.diagnosis_list.Count : 0;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 30f;
    }

    void AddToVoiceDictionary(string phrase, System.Action action)
    {
        if (itemsVoicePhrases.ContainsKey(phrase))
            itemsVoicePhrases[phrase] = action;
        else
            itemsVoicePhrases.Add(phrase, action);
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        Diagnosis_Item cellView = scroller.GetCellView(diagnosisViewPrefab) as Diagnosis_Item;
        cellView.name = "Cell " + dataIndex.ToString();
        cellView.SetData(diagnosis.diagnosis_list[dataIndex], dataIndex, AddToVoiceDictionary);
        return cellView;
    }

    public void NextBtn_pressed()
    {
        EnableTransitionButtons(false);
        goingBack = false;
        SaveDiagnosisList();
    }

    public void BackBtn_pressed()
    {
        EnableTransitionButtons(false);
        goingBack = true;
        SaveDiagnosisList();
    }

    public void EnableTransitionButtons(bool enable)
    {
        backBtn.UpdateInteractability(enable);
        nextBtn.UpdateInteractability(enable);
    }
    public void AddPhrasesToVoice(Dictionary<string, System.Action> phrasesOfPanel)
    {
        phrasesOfPanel.Add("Next", NextBtn_pressed);
        phrasesOfPanel.Add("Back", BackBtn_pressed);

        PanelVoiceController.Instance.AddSetOfPhrases(phrasesOfPanel);
    }
}