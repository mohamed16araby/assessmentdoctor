﻿using UnityEngine;
using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;
using TMPro;
using Beebyte.Obfuscator;
using System.Collections.Generic;

public class ProblemItem : EnhancedScrollerCellView
{
    public TextMeshProUGUI ItemNameTxt;
    public Button mainBtn;
    private int number;

    [SkipRename]
    public void SetData(string problemItem, int numberItem, System.Action<string, System.Action> AddToVoiceDictionary)
    {
        // update the UI text with the cell data
        number = numberItem + 1;
        ItemNameTxt.text = number.ToString()+". "+ problemItem;

        AddToVoiceDictionary(problemItem, MainBtn_pressed);
        AddToVoiceDictionary(number.ToString(), MainBtn_pressed);
    }

    // Update is called once per frame
    void Update()
    {
        if (ProblemList.selected_problems_Orig.Contains(ItemNameTxt.text))
        {
            ItemNameTxt.color = Color.white;
            mainBtn.GetComponent<Image>().color = new Color32(3, 189, 91, 255);
        }
        else
        {
            ItemNameTxt.color = Color.black;
            mainBtn.GetComponent<Image>().color = new Color32(255, 255, 255, 100);
        }
    }


    [SkipRename]
    public void MainBtn_pressed()
    {

        if (ProblemList.selected_problems_Orig.Contains(ItemNameTxt.text))
        {
            ProblemList.selected_problems_Orig.Remove(ItemNameTxt.text);
        }
        else
        {
            ProblemList.selected_problems_Orig.Add(ItemNameTxt.text);
        }

    }
}
