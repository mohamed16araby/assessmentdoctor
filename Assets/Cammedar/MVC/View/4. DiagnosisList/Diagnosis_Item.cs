﻿using UnityEngine;
using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;
using TMPro;
using Beebyte.Obfuscator;
using System.Collections.Generic;

public class Diagnosis_Item : EnhancedScrollerCellView
{
    public TextMeshProUGUI ItemNameTxt;
    public Button mainBtn;
    private int number;

    [SkipRename]
    public void SetData(string Item, int numberItem, System.Action<string, System.Action> AddToVoiceDictionary)
    {
        // update the UI text with the cell data
        number = numberItem + 1;
        ItemNameTxt.text = number.ToString()+". "+ Item;

        AddToVoiceDictionary(Item, MainBtn_pressed);
        AddToVoiceDictionary(number.ToString(), MainBtn_pressed);
    }

    // Update is called once per frame
    void Update()
    {
        if (DiagnosisList.selected_diagnosis_Orig.Contains(ItemNameTxt.text))
        {
            ItemNameTxt.color = Color.white;
            mainBtn.GetComponent<Image>().color = new Color32(3, 189, 91, 255);
        }
        else
        {
            ItemNameTxt.color = Color.black;
            mainBtn.GetComponent<Image>().color = new Color32(255, 255, 255, 100);
        }
    }


    [SkipRename]
    public void MainBtn_pressed()
    {

        if (DiagnosisList.selected_diagnosis_Orig.Contains(ItemNameTxt.text))
        {
            DiagnosisList.selected_diagnosis_Orig.Remove(ItemNameTxt.text);
        }
        else
        {
            DiagnosisList.selected_diagnosis_Orig.Add(ItemNameTxt.text);
        }

    }
}
