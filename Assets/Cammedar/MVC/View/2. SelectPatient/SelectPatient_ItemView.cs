﻿using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using Beebyte.Obfuscator;
using Cammedar.Network;
using UnityEngine;

public class SelectPatient_ItemView : EnhancedScrollerCellView
{
    public TextMeshProUGUI PatientNameTxt;
    public Button mainBtn;

    string UDID = "" ;
    string patientName = "" ;
    string patient_phone = "" ;

    System.Action Reload_PatientScroller_Data;
    System.Action CheckNextButtonActivation;

    [SkipRename]
    public void SetData(Patient_GeneralData patient, string patient_UID, System.Action Reload_PatientScroller_Data, System.Action CheckNextButtonActivation)
    {
        patientName = string.Join(" ", new string[] { patient.first_name, patient.middle_name, patient.last_name });
        UDID = patient_UID;
        patient_phone = patient.phoneNumber;

        PatientNameTxt.text = string.Format("{0}\nID: {1} \t Tel: {2} \t Email: {3}", patientName, patient.nationalID, patient_phone, patient.email);

        this.Reload_PatientScroller_Data = Reload_PatientScroller_Data;
        this.CheckNextButtonActivation = CheckNextButtonActivation;

        ChangeSelectionColors(PatientNameTxt, mainBtn.GetComponent<Image>(), CheckIfSelected());
    }

    [SkipRename]
    public void MainBtn_pressed()
    {
        if (CheckIfSelected())
        {
            PatientData.patient_UDID = "";
            PatientData.patient_name = "";
            PatientData.patient_phone = "";
        }
        else
        {
            PatientData.patient_UDID = UDID;
            PatientData.patient_name = patientName;
            PatientData.patient_phone = patient_phone;
        }

        CheckNextButtonActivation();
        Reload_PatientScroller_Data(); //To apply color selection on the selected item only
    }

    private bool CheckIfSelected()
    {
        return PatientData.patient_UDID == UDID;
    }

    private void ChangeSelectionColors(TextMeshProUGUI text, Image image, bool changeColor_To_Selected)
    {
        if (changeColor_To_Selected)
        {
            text.color = Color.white;
            image.color = new Color32(3, 189, 91, 255);
        }
        else
        {
            text.color = Color.black;
            image.color = new Color32(255, 255, 255, 100);
        }
    }
}
