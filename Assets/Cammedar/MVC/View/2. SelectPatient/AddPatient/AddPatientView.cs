﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Ricimi;
using System.Linq;

public class AddPatientView : MonoBehaviour
{
    [SerializeField]
    Text headline;
    [SerializeField]
    ToggleGroup toggleGroup;
    
    [SerializeField]
    BasicButton addBtn;
    [SerializeField]
    private Text errorMsgTxt;

    public AddPatient_Toggle SelectedToggle
    {
        get
        {
            if (toggleGroup.ActiveToggles().ToList().Count > 0)
                return toggleGroup.ActiveToggles().ToList().First().GetComponent<AddPatient_Toggle>();

            return null;
        }
    }

    UnityEngine.Events.UnityAction AddBtnFunctionality_IfPassed;

    public void Init(UnityEngine.Events.UnityAction AddBtnFunctionality_IfPassed)
    {
        Debug.Log("init in add patient view");

        headline.text = string.Format("Add patient to {0} ", UserData.doctor_hospital_branch_name);
        this.AddBtnFunctionality_IfPassed = AddBtnFunctionality_IfPassed;

        addBtn.onClicked.AddListener(OnAddBtnClicked);
    }

    public bool PanelFilled_RequiredData()
    {
        if (SelectedToggle != null && SelectedToggle.AllFieldsAreFilled()) return true;
        return false;
    }

    private void OnAddBtnClicked()
    {
        if(SelectedToggle == null)
        {
            ViewError("Choose one way of the above!");
        }
        else if (!PanelFilled_RequiredData())
        {
            ViewError("Please fill patient info");
        }
        else
        {
            ViewError("");
            AddBtnFunctionality_IfPassed();
        }
    }

    public void ViewError(string text)
    {
        Debug.Log(text);
        errorMsgTxt.text = text;
        errorMsgTxt.color = new Color32(212, 49, 49, 255);
    }

    public void ViewSuccessFeedback(string text)
    {
        errorMsgTxt.text = text;
        errorMsgTxt.color = new Color32(67, 144, 57, 255);
    }
}
