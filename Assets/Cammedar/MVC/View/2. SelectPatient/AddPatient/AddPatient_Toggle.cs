﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using System.Linq;

public enum ToggleTag { None, NationalID, PhoneAndSerial, PhoneAndEmail}

public class AddPatient_Toggle : MonoBehaviour
{
    [SerializeField]
    Toggle toggle;
    [SerializeField]
    List<TMP_InputField> inputFields;
    [SerializeField]
    Image panel;
    [SerializeField]
    List<TMP_Text> labels;

    public ToggleTag choiceTag;

    public bool ToggleOn { get => toggle.isOn; }
    public List<string> FieldsEntries { get => inputFields.Select(i=>i.text).ToList(); }

    private void Start()
    {
        ActOnToggleChange(false);
        toggle.onValueChanged.AddListener(ActOnToggleChange);
        toggle.isOn = false;
    }

    void ActOnToggleChange(bool isOn)
    {
        panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, isOn ? 0f : 0.38f);

        for (int i = 0; i < inputFields.Count; i++)
        {
            inputFields[i].interactable = isOn;
            labels[i].color = new Color(panel.color.r, panel.color.g, panel.color.b, isOn ? 1f : 0.57f);
        }
    }

    public bool AllFieldsAreFilled()
    {
        foreach (TMP_InputField inputField in inputFields)
            if (string.IsNullOrEmpty(inputField.text))
                return false;

        return true;
    }
}
