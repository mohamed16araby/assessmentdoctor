﻿using UnityEngine;
using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;
using Beebyte.Obfuscator;

public class AnimsGroupNameView : EnhancedScrollerCellView
{
    public Text GroupNameTxt;
    public Button mainBtn;
    public Text SAVED;

    [Header("Flag Panel")]
    public Image AssignedFlagPanel;
    [SerializeField]
    Color emptyColor;
    [SerializeField]
    Color dirtyColor;

    private string group_id = "";
    private bool is_selected = false;
    private bool first_scroller = false;

    private void Start()
    {
        mainBtn.onClick.AddListener(delegate
        {

            if (is_selected)
            {
                if (group_id == ExerciseAnimControllerStruct.selectedGroupId)
                    ExerciseAnimControllerStruct.selectedGroupId = "";
                else if (group_id == ExerciseAnimControllerStruct.selectedExerciseID)
                    ExerciseAnimControllerStruct.selectedExerciseID = "";
            }

            AnimsGroupUpdateSelected(!is_selected);
        });
    }

    private void Update()
    {
        if (group_id == ExerciseAnimControllerStruct.selectedGroupId || group_id == ExerciseAnimControllerStruct.selectedExerciseID)
        {
            AnimsGroupUpdateSelected(true);
        }
        else
        {
            AnimsGroupUpdateSelected(false);
        }
    }

    public void AnimsGroupUpdateSelected(bool is_selected)
    {
        if (is_selected)
        {
            GroupNameTxt.color = Color.white;
            mainBtn.GetComponent<Image>().color = new Color32(0, 0, 0, 32);
        }
        else
        {
            GroupNameTxt.color = Color.black;
            mainBtn.GetComponent<Image>().color = Color.white;
        }

        this.is_selected = is_selected;
    }

    public void SetData(string name, string id, bool hasSelectedChild, bool scroller)
    {
        GroupNameTxt.text = name;
        group_id = id;
        first_scroller = scroller;
        UpdateFlag(hasSelectedChild);
    }

    [SkipRename]
    public void MainBtn_pressed()
    {
        if (first_scroller)
        {
            ExerciseAnimControllerStruct.selectedGroupId = group_id;
        }
        else
        {
            ExerciseAnimControllerStruct.selectedExerciseID = group_id;
        }
    }

    public void ShowHideAssignedText(bool show)
    {
        SAVED.gameObject.SetActive(show);
    }

    public void UpdateFlag(bool isDirty)
    {
        AssignedFlagPanel.color = isDirty ? dirtyColor : emptyColor;
    }
}