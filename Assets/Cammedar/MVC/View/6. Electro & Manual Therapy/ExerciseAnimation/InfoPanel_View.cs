﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class InfoPanel_View : MonoBehaviour
{
    [SerializeField]
    Button infoBtn;
    [SerializeField]
    TMP_Text text;
    [SerializeField]
    GameObject panel;

    private bool panelIsOpen = false;

    // Start is called before the first frame update
    void Start()
    {
        infoBtn.onClick.AddListener(delegate { UpdatePanelView(!panelIsOpen); });
    }

    void UpdatePanelView(bool _openPanel)
    {
        panel.SetActive(_openPanel);
        panelIsOpen = _openPanel;
    }

    public void SetView(string _text, string _title)
    {
        if (string.IsNullOrEmpty(_text))
        {
            infoBtn.gameObject.SetActive(false);
            UpdatePanelView(false);
            text.text = "";
        }

        else
        {
            infoBtn.gameObject.SetActive(true);
            UpdatePanelView(panelIsOpen);
            text.text = string.Format("<b>{0}</b>\n\n{1}", _title, _text);
        }
    }
}
