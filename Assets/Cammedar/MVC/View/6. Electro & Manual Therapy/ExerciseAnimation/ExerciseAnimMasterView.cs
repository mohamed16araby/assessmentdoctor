﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Beebyte.Obfuscator;
using EnhancedUI.EnhancedScroller;

public class ExerciseAnimMasterView : EnhancedScrollerCellView
{
    public GameObject AssignPanelObject;
    public GameObject ExerciseGroupNameObject;
    string id = "";

    [SkipRename]
    public void SetData(string name, string id, bool hasSelectedChild, bool scroller)
    {
        this.id = id;
        AssignPanelObject.gameObject.SetActive(false);
        ExerciseGroupNameObject.gameObject.SetActive(true);
        ExerciseGroupNameObject.GetComponent<AnimsGroupNameView>().SetData(name, id, hasSelectedChild, scroller);
    }

    [SkipRename]
    public void SetData(therapy_shoulder_exercise exercise)
    {
        Debug.Log("exercise name: " + exercise.name);
        this.id = exercise.id;
        AssignPanelObject.gameObject.SetActive(true);
        ExerciseGroupNameObject.gameObject.SetActive(false);
        AssignPanelObject.GetComponent<ExerciseAnimView>().SetData(exercise.name, exercise.id, exercise.hasGrades, 
                                                                   exercise.link, exercise.daysPerWeek, exercise.setsPerDay, 
                                                                   exercise.repetitionsPerSet, exercise.notes, exercise.grade, exercise.info);
    }

    [SkipRename]
    public void ChooseSettingData(therapy_shoulder_exercise exercise, bool scroller)
    {
        if (exercise.sub_exs.Count == 0)
        {
            SetData(exercise);
        }
        else
        {
            SetData(exercise.name, exercise.id, exercise.hasSelectedChild, scroller);
        }
    }
}
