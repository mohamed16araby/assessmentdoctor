﻿using UnityEngine;
using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Beebyte.Obfuscator;


/// <summary>
/// This is the view of our cell which handles how the cell looks.
/// </summary>
public class ExerciseAnimView : EnhancedScrollerCellView
{
    public Button playBtn;
    public Text ExerciseNameTxt;
    public GameObject DataPanel;
    public GameObject AssignPanel;
    public GameObject gradeGO;
    public InfoPanel_View infoPanel_View;
    private InfoPanel_View infoPanel_View_inst;

    public Text daysPerWeek;
    public Text setsPerDay;
    public Text repetitionsPerSet;
    public Text notes;
    public Text grade;


    GameEvent Remove_FROM_ExercisesSelected;

    StringField unassignedExerciseId;

    AnimationsManager animationsManager;
    string exercise_id = "";
    bool hasGrade = false;
    string url;

    /// This function just takes the Demo data and displays it
    [SkipRename]
    public void SetData(string _exerciseName, string _exerciseID, bool hasGrade, string _url, string _daysPerWeek, string _setsPerDay, string _repetitionsPerSet, string _notes, string _grade, string info)
    {
        ExerciseNameTxt.text = _exerciseName;
        exercise_id = _exerciseID;
        this.hasGrade = hasGrade;
        //AssignedExerciseData.info = info;

        url = _url;

        daysPerWeek.text = _daysPerWeek;
        setsPerDay.text = _setsPerDay;
        repetitionsPerSet.text = _repetitionsPerSet;
        grade.text = _grade;
        notes.text = _notes;

        gradeGO.SetActive(hasGrade);

        //if (infoPanel_View_inst == null)
        //    infoPanel_View_inst = Instantiate(infoPanel_View, transform);
        //infoPanel_View_inst.SetView(info, _exerciseName);
        infoPanel_View.SetView(info, _exerciseName);

        if (_repetitionsPerSet == "" && _setsPerDay == "" && _daysPerWeek == "" && _notes == "")
        {
            DataPanel.SetActive(false);
            AssignPanel.SetActive(true);
        }
        else
        {
            DataPanel.SetActive(true);
            AssignPanel.SetActive(false);
        }

        if (Options.YouTubeMode)
        {
            AssignPanel.SetActive(false);
        }
    }

    [SkipRename]
    public void PlayBtn_pressed()
    {
        animationsManager = GameObject.FindGameObjectWithTag("AnimationsManager").GetComponent<AnimationsManager>();
        animationsManager.YoutubeURL = url;

        GameObject.FindGameObjectWithTag("AnimationsCanvas").GetComponent<Canvas>().enabled = false;
        SceneManager.LoadScene("Youtube", LoadSceneMode.Additive);
    }

    [SkipRename]
    public void AssignBtn_pressed()
    {
        AssignedExerciseData.exercise_id = exercise_id;
        AssignedExerciseData.exercise_name = ExerciseNameTxt.text;
        AssignedExerciseData.hasGrades = hasGrade;
    }

    [SkipRename]
    public void UnassignBtn_pressed()
    {
        unassignedExerciseId.Value = exercise_id;
        Remove_FROM_ExercisesSelected.Raise();
    }
}
