﻿using UnityEngine;
using EnhancedUI.EnhancedScroller;
using TMPro;
using Beebyte.Obfuscator;
using System.Linq;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class ManualTherapy_Item_View : EnhancedScrollerCellView
{
    public TextMeshProUGUI title;
    public Button btn;
    Action<string> SetCurrentSelectedAlgorithm;

    private void Start()
    {
        btn.onClick.AddListener(MainBtn_pressed);
    }

    [SkipRename]
    public void SetData(string manualTherapy_title, Action<string> SetCurrentSelectedAlgorithm)
    {
        // update the UI text with the cell data
        title.text = manualTherapy_title;
        this.SetCurrentSelectedAlgorithm = SetCurrentSelectedAlgorithm;
    }

    public void MainBtn_pressed()
    {
        SetCurrentSelectedAlgorithm(title.text);
    }
}