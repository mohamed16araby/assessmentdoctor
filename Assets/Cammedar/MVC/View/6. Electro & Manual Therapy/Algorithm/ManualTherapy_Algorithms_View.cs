﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;
using System.IO;
using Beebyte.Obfuscator;
using Ricimi;

public class ManualTherapy_Algorithms_View : MonoBehaviour, IEnhancedScrollerDelegate
{
    public EnhancedScroller scroller;
    public EnhancedScrollerCellView manualTherapy_Item_ViewPrefab;
    public InputField SearchField;
    public StringField currentSelected_AlgorithmName;
    public GameEvent UpdateExercisesWithAlgorithmSelected;

    private List<ManualTherapy_Algorithm> algorithms = new List<ManualTherapy_Algorithm>();
    private List<ManualTherapy_Algorithm> searchedAlgorithms = new List<ManualTherapy_Algorithm>();
    private bool SearchActive = false;

    void Start()
    {
        scroller.Delegate = this;
        SearchField.onValueChanged.AddListener(delegate { ValueChangeCheck(); });

        algorithms = ProgramGeneralData.manualTherapyAlgorithms;
        scroller.ReloadData();
    }

    [SkipRename]
    public void ValueChangeCheck()
    {
        if (SearchField.text == "")
        {
            SearchActive = false;
            scroller.ReloadData();
        }
        else
        {
            searchedAlgorithms.Clear();

            foreach (ManualTherapy_Algorithm algorithm in algorithms)
                if (algorithm.algorithmName.ToLower().Contains(SearchField.text.ToLower()))
                    searchedAlgorithms.Add(algorithm);

            SearchActive = true;
            scroller.ReloadData();
        }
    }
    
    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        if (SearchActive)
            return searchedAlgorithms.Count;

        else
            return algorithms.Count;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 60f;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        ManualTherapy_Item_View cellView = scroller.GetCellView(manualTherapy_Item_ViewPrefab) as ManualTherapy_Item_View;
        cellView.name = "Cell " + dataIndex.ToString();

        if (SearchActive)
            cellView.SetData(searchedAlgorithms[dataIndex].algorithmName, SetCurrentSelectedAlgorithm);
        
        else
            cellView.SetData(algorithms[dataIndex].algorithmName, SetCurrentSelectedAlgorithm);
        
        return cellView;
    }

    private void SetCurrentSelectedAlgorithm(string currentAlgorithm)
    {
        currentSelected_AlgorithmName.Value = currentAlgorithm;
        UpdateExercisesWithAlgorithmSelected.Raise();
        ClosePopup();
    }

    void ClosePopup()
    {
        this.GetComponent<Popup>().Close();
    }
}