﻿using UnityEngine;
using EnhancedUI.EnhancedScroller;
using TMPro;
using Beebyte.Obfuscator;
using System.Linq;
using System.Collections.Generic;

public class ExerciseAnimView_search : EnhancedScrollerCellView
{
    public TextMeshProUGUI ExerciseNameTxt;
    string groupID = "";

    [SkipRename]
    public void SetData(therapy_shoulder_exercise exercise)
    {
        ExerciseNameTxt.text = exercise.name;
        groupID = exercise.id;
    }

    [SkipRename]
    public void MainBtn_pressed()
    {
        string[] id = Operations.SplitBy(groupID, '_');
        int idLength = id.Length;

        if (idLength >= 5)
        {
            ExerciseAnimControllerStruct.selectedGroupId = string.Join("_", new List<string>(id).GetRange(0, 4));
            ExerciseAnimControllerStruct.selectedExerciseID = string.Join("_", new List<string>(id).GetRange(0, 5));
            
            if (idLength == 6)
                ExerciseAnimControllerStruct.selectedSubExerciseID = string.Join("_", new List<string>(id).GetRange(0, 6));
        }

        GameObject.Destroy(GameObject.FindGameObjectsWithTag("SearchExercisesPanel")[0]);
        GameObject.Destroy(GameObject.Find("PopupBackground"));
    }
}