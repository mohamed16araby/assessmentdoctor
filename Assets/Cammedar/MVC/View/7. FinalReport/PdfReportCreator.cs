﻿using System.Collections;
using System.Collections.Generic;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using System;
using Cammedar.Network;
using System.Diagnostics;
using System.Linq;
using MonKey.Extensions;
using System.Web;
using UnityEngine.Networking;
using System.IO;


public class PdfReportCreator 
{
    static Chunk label = new Chunk();
    static Chunk body = new Chunk();
    static Phrase phrase = new Phrase();
    static Paragraph paragraph = new Paragraph();
    static BaseColor headerTitlesColor = new BaseColor(0, 49, 82);
    static BaseColor normalBoldColor = new BaseColor(12, 77, 105);

    static Font labelFont = FontFactory.GetFont(BaseFont.HELVETICA_BOLD, 13, BaseColor.BLACK);

    static Font normalBoldColoredFont = FontFactory.GetFont(BaseFont.HELVETICA_BOLD, 13, BaseColor.BLACK);
    static Font normalColoredFont = FontFactory.GetFont(BaseFont.HELVETICA_BOLD, 13, BaseColor.BLACK);

    static Font boldColoredFont = FontFactory.GetFont(BaseFont.HELVETICA_BOLD, 11, normalBoldColor);

    static Font labelcoloredFont = FontFactory.GetFont(BaseFont.HELVETICA_BOLD, 13, BaseColor.BLACK);
    static Font ContentFont = FontFactory.GetFont(BaseFont.HELVETICA, 13, BaseColor.BLACK);
    //static Font selectedChoicesFont = FontFactory.GetFont(BaseFont.HELVETICA, 14, new BaseColor(29, 41, 81));
    //static Font bulletFont = FontFactory.GetFont(BaseFont.HELVETICA, 17, new BaseColor(29, 41, 81));

    static Font selectedChoicesFont = FontFactory.GetFont(BaseFont.HELVETICA, 14, BaseColor.BLACK);
    static Font bulletFont = FontFactory.GetFont(BaseFont.HELVETICA, 17, BaseColor.BLACK);

    static Font headerFont = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, normalBoldColor);
    static Font midTitlesFont = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD, BaseColor.BLACK);
    static Font tableTitlesFont = new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD, BaseColor.BLACK);
    static Font pageMainTitleFont = new Font(Font.FontFamily.COURIER, 15, Font.BOLD | Font.UNDERLINE, BaseColor.BLACK);

    /// <summary>
    /// Adding Horizontal line
    /// </summary>
    static LineSeparator lineSeparatorGray = new LineSeparator(1.5f, 100f, BaseColor.LIGHT_GRAY, Element.ALIGN_LEFT, -4);
    static LineSeparator lineSeparatorColored = new LineSeparator(1.5f, 100f, normalBoldColor, Element.ALIGN_LEFT, -4);

    static Chunk glue = new Chunk(new VerticalPositionMark());

    static Font zapfdingbats = new Font(Font.FontFamily.ZAPFDINGBATS, 8);
    static Font font = new Font();
    static Chunk bullet = new Chunk("\u2022", zapfdingbats);

    static float leading = 15;
    public static void StyleReport(string doc_id, Patient_GeneralData patient_GeneralData, List<HistoryData> historyDatas, SelectedParts bodyPartsComplaints,
                                   Dictionary<List<string>, List<SessionData>> specialityPath_SessionData, byte[][] logo_bytes,
                                   ref PdfWriter pdfWriter, ref Document pdfReport)
    {
        if (pdfReport.IsOpen())
            pdfReport.Close();

        MyEvent myevent = new MyEvent(logo_bytes);
        pdfWriter.PageEvent = myevent;

        pdfReport.Open();

        //PdfContentByte canvas = pdfWriter.DirectContent;
        //ColumnText ct = new ColumnText(canvas);

        paragraph = new Paragraph(leading);
        phrase = new Phrase(patient_GeneralData.address + "\n", ContentFont);

        body = new Chunk(string.IsNullOrEmpty(patient_GeneralData.state_id) ?
            patient_GeneralData.city_name + ", " + patient_GeneralData.country_name :
            patient_GeneralData.state_name + ", " + patient_GeneralData.city_name + ", " + patient_GeneralData.country_name, ContentFont);
        phrase.Add(body + "\n");

        body = new Chunk(string.IsNullOrEmpty(patient_GeneralData.zipCode) ? patient_GeneralData.phoneNumber :
                                                                  string.Format("({0}){1}", patient_GeneralData.zipCode, patient_GeneralData.phoneNumber), ContentFont);
        phrase.Add("Phone: " + body);

        paragraph.Add(phrase);
        pdfReport.Add(paragraph);

        phrase = new Phrase(new Chunk("Patient Name: ", labelFont));
        phrase.Add(new Chunk(" " + PatientData.patient_name, ContentFont));
        phrase.Add(glue);
        phrase.Add(new Chunk("Email: ", labelFont));
        phrase.Add(new Chunk(patient_GeneralData.email + "\n", ContentFont));

        paragraph = new Paragraph(leading);
        paragraph.Add(phrase);
        paragraph.SpacingBefore = 10f;
        /////////

        phrase = new Phrase(new Chunk("Date of Birth: ", labelFont));
        phrase.Add(new Chunk(patient_GeneralData.dateOfBirth, ContentFont));
        phrase.Add(glue);
        phrase.Add(new Chunk("Occupation: ", labelFont));
        phrase.Add(new Chunk(patient_GeneralData.occupation + "\n", ContentFont));
        paragraph.Add(phrase);
        ///////////////

        phrase = new Phrase(new Chunk("Marital Status: ", labelFont));
        phrase.Add(new Chunk(patient_GeneralData.maritalStatus, ContentFont));
        phrase.Add(glue);
        phrase.Add(new Chunk("Handedness: ", labelFont));
        phrase.Add(new Chunk(patient_GeneralData.Handedness + "\n", ContentFont));
        paragraph.Add(phrase);
        ///////////////////
        phrase = new Phrase(new Chunk("Gender: ", labelFont));
        phrase.Add(new Chunk(patient_GeneralData.gender, ContentFont));
        phrase.Add(glue);
        phrase.Add(new Chunk("Residence: ", labelFont));
        phrase.Add(new Chunk(patient_GeneralData.residence + "\n", ContentFont));
        paragraph.Add(phrase);

        //
        pdfReport.Add(paragraph);

        paragraph = new Paragraph("MEDICAL REPORT", pageMainTitleFont) { Alignment = Element.ALIGN_CENTER, SpacingBefore = 20 };
        pdfReport.Add(paragraph);
        /////////////////////////////////////////////////////////////////
        //History Data

        
        //if (UserData.doctor_Specialization.ToString() != "Dentistry")
        //{
            PrintPatient_HistoryData(ref pdfReport, historyDatas);
           PrintPatient_BodyPartsComplaints(ref pdfReport, bodyPartsComplaints);
        //}

        PrintPatient_Sessions(ref pdfReport, specialityPath_SessionData);

        pdfReport.Close();
    }

    static void PrintPatient_HistoryData(ref Document pdfReport, List<HistoryData> historyDatas)
    {

        foreach (HistoryData historyData in historyDatas)
        {
            paragraph = new Paragraph() { SpacingBefore = 30 };

            body = historyData.iscommon ? new Chunk(string.Join(" - ", historyData.specialitiesHolders) + " - Common History" + "\n", headerFont) :
                                          new Chunk(string.Join(" - ", historyData.specialitiesHolders) + " History" + "\n", headerFont);

            if (UserData.doctor_Specialization.ToString() == "Dentistry")
            {
                if (body.ToString().Substring(0, 9) != "Dentistry")

                    continue;
            }
            if (UserData.doctor_Specialization.ToString() == "Physical Therapy")
            {
                if (body.ToString().Substring(0, 9) == "Dentistry")
                    continue;
            }
            paragraph.Add(body);
            pdfReport.Add(paragraph);
            pdfReport.Add(lineSeparatorColored);
            phrase = new Phrase();
            paragraph = new Paragraph() { SpacingAfter = 10 };

            phrase = new Phrase(new Chunk("Side: ", labelcoloredFont));
            phrase.Add(string.IsNullOrEmpty(historyData.historyAnswers.side) ?
                new Chunk("_") :
                new Chunk(historyData.historyAnswers.side/*, ContentFont*/));

            phrase.Add(glue);

            string dateLabel = historyData.iscommon ? "History date: " : "Specific history date: ";
            phrase.Add(new Chunk(dateLabel + historyData.historyAnswers.dateOfLastEntry, labelcoloredFont));

            paragraph.Add(phrase);
            pdfReport.Add(paragraph);

            PrintSelections_InSavedJson(ref pdfReport, historyData.groupDictionary);
            pdfReport.Add(lineSeparatorColored);
            phrase = new Phrase(glue);

            string txt = historyData.iscommon ? "End of " + historyData.specialitiesHolders[historyData.specialitiesHolders.Count - 1] + " common history" :
                                                "End of " + historyData.specialitiesHolders[historyData.specialitiesHolders.Count - 1] + " specific history";
            phrase.Add(new Chunk(txt + "\n", boldColoredFont));
            pdfReport.Add(phrase);
        }

    }
    static void PrintPatient_Sessions(ref Document pdfReport, Dictionary<List<string>, List<SessionData>> specialityPath_SessionData)
    {

        if (specialityPath_SessionData.Count == 0) return;

        pdfReport.NewPage();
        foreach (KeyValuePair<List<string>, List<SessionData>> keyValuePair in specialityPath_SessionData)
        {
            if (keyValuePair.Value == null)
                return;
            if (keyValuePair.Value.Any(s => s.sessionReport!=null)) //To noy print the header if all the reports are empty in that speciality path
            {
                paragraph = new Paragraph();
                paragraph.TabSettings = new TabSettings(100);

                body = new Chunk(string.Join(" - ", keyValuePair.Key) + "\n", headerFont);
                Debug.Write("rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr");

                if (UserData.doctor_Specialization.ToString() == "Dentistry")
                {
                    if (body.ToString().Substring(0, 9) != "Dentistry")

                        continue;
                }
                if (UserData.doctor_Specialization.ToString() == "Physical Therapy")
                {
                    if (body.ToString().Substring(0, 9) == "Dentistry")
                        continue;
                }
                paragraph.Add(body);
                pdfReport.Add(paragraph);

                foreach (SessionData sessionData in keyValuePair.Value)
                {
                    Debug.Print("rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr");

                    {
                        pdfReport.Add(lineSeparatorColored);
                        paragraph = new Paragraph() { SpacingAfter = 10 };

                        phrase = new Phrase(new Chunk("Branch name: ", labelcoloredFont));
                    try
                    {
                        phrase.Add(new Chunk(sessionData.sessionReport.hospital_branch_name));
                    }
                    catch(System.Exception e)
                    {}
                        phrase.Add(glue);

                        string dateLabel = sessionData.sessionPreview.isFollowup ? "Follow up" : "Examination";
                        phrase.Add(new Chunk(dateLabel + " date: ", labelcoloredFont));
                        phrase.Add(new Chunk(sessionData.sessionPreview.date + "\n"));
                        paragraph.Add(phrase);

                        phrase = new Phrase(new Chunk("Side of assessment: ", labelcoloredFont));
                    try
                    {
                        phrase.Add(new Chunk(sessionData.sessionReport.sideOfAssessment + "\n"));
                    }
                    catch (System.Exception e)
                    { }
                    paragraph.Add(phrase);
                        pdfReport.Add(paragraph);

                        PrintSelections_InSavedJson(ref pdfReport, sessionData.groupDictionary);
                    try
                    {
                        PrintList(ref pdfReport, sessionData.sessionReport.diagnosisList, "Diagnosis");
                    }
                    catch (System.Exception e)
                    { }
                    try
                    {
                        PrintList(ref pdfReport, sessionData.sessionReport.problemList, "Problem Lists");
                    }
                    catch (System.Exception e)
                    { }
                    try
                    {
                        PrintList(ref pdfReport, sessionData.sessionReport.TreatmentGoals_ShortTerm, "Short-term Treatment Goals");
                    }
                    catch (System.Exception e)
                    { }
                    try
                    {
                        PrintList(ref pdfReport, sessionData.sessionReport.TreatmentGoals_LongTerm, "Long-term Treatment Goals");
                    }
                    catch (System.Exception e)
                    { }
                    try
                    {
                        PrintExercises(ref pdfReport, sessionData.sessionReport.ElectroTherapy.Values.ToList(), "Electro-Therapy",
                                new List<string>() { "#", "Exercise name", "Rep. per set", "Sets per day", "Days per week", "Notes" });
                    }
                    catch (System.Exception e)
                    { }


                    try
                    {
                        PrintExercises(ref pdfReport, sessionData.sessionReport.exercises.Values.ToList(), "Manual Therapy",
                            new List<string>() { "#", "Exercise name", "Rep. per set", "Sets per day", "Days per week", "Notes" });
                    }
                    catch(System.Exception e)
                    {

                    }
                    try
                    {
                        string followup = sessionData.sessionReport.followUp;
                        if (!string.IsNullOrEmpty(followup))
                            if (int.TryParse(followup, out int followUpNo))
                                AddLabelInfo(ref pdfReport, "Manual therapy follow-up period", sessionData.sessionReport.followUp + (followUpNo < 2 ? " week\n" : " weeks\n"));
                    }
                    catch (System.Exception e)
                    {

                    }
                    if (keyValuePair.Value.IndexOf(sessionData) != keyValuePair.Value.Count - 1)
                            pdfReport.Add(new Paragraph("\n") { SpacingAfter = 2 });
                    }
                }
                pdfReport.Add(lineSeparatorColored);
                phrase = new Phrase(glue);

                string txt = keyValuePair.Key[keyValuePair.Key.Count - 1];
                phrase.Add(new Chunk("End of " + txt + " sessions\n", boldColoredFont));
                pdfReport.Add(phrase);
                pdfReport.Add(new Paragraph("\n") { SpacingAfter = 30 });
           }
        }
    }

    static void AddLabelInfo(ref Document pdfReport, string label, string info)
    {
        paragraph = new Paragraph();
        phrase = new Phrase(new Chunk(label + ": ", labelcoloredFont));
        phrase.Add(new Chunk(info, selectedChoicesFont));
        paragraph.Add(phrase);
        pdfReport.Add(paragraph);
    }
    static void PrintSelections_InSavedJson(ref Document pdfReport, SortedDictionary<ITransit> groupDict)
    {
        foreach (KeyValuePair<int, List<ITransit>> groups in groupDict.SortedView)
        {
            Group group = groups.Value[0] as Group;
            pdfReport.Add(new Phrase(group.name + "\n", normalBoldColoredFont));
            foreach (KeyValuePair<int, List<ITransit>> panels in group.Panels.SortedView)
            {
                Panel panel = panels.Value[0] as Panel;
                if (!panel.hideTitle) pdfReport.Add(new Phrase(panel.name + "\n", normalBoldColoredFont));
                foreach (KeyValuePair<int, List<ITransit>> sections in panel.Sections.SortedView)
                {
                    Section section = sections.Value[0] as Section;
                    if (!section.hideTitle) pdfReport.Add(new Phrase(section.name + "\n", normalBoldColoredFont));
                    foreach (KeyValuePair<int, List<ITransit>> sides in section.Sides.SortedView)
                    {
                        Side side = sides.Value[0] as Side;
                        if (side.name!= Sides.None.ToString()) pdfReport.Add(new Phrase(side.name + " side\n", normalBoldColoredFont));
                        foreach (KeyValuePair<int, List<ITransit>> questions in side.Tests)
                        {
                            foreach (ITransit question in questions.Value)
                            {
                                paragraph = new Paragraph();
                                paragraph.TabSettings = new TabSettings(200);

                                EntryField entryField = question as EntryField;
                                if (!entryField.hideTitle) pdfReport.Add(new Phrase(entryField.title/* + "\n"*/, normalBoldColoredFont));
                                if (entryField.linkingTag == "InputField_DecimalNumber")
                                {
                                    InputField_Model inputField = (entryField as InputField_Model);
                                    if (inputField.value.Count > 0)
                                    {
                                        phrase = new Phrase(bullet + " ", bulletFont);
                                        paragraph.Add(phrase);

                                        phrase = new Phrase(inputField.value[0], selectedChoicesFont);
                                        paragraph.Add(phrase);
                                    }
                                }
                                else if (Toggles_Model.IsTogglesModel(entryField.linkingTag))
                                {
                                    Toggles_Model toggle = (entryField as Toggles_Model);
                                    foreach (Toggles_Model child in toggle.selectedValues)
                                    {
                                        phrase = new Phrase(bullet + " ", bulletFont);
                                        paragraph.Add(phrase);

                                        phrase = new Phrase(PrintSelectedToggles_andCheckNested(child), selectedChoicesFont);
                                        paragraph.Add(phrase);
                                        paragraph.Add(Chunk.TABBING);
                                    }
                                }
                                pdfReport.Add(paragraph);
                            }
                        }
                    }
                }
            }

            pdfReport.Add(lineSeparatorGray);
        }
    }

    static void PrintPatient_BodyPartsComplaints(ref Document pdfReport, SelectedParts bodyPartsComplaints)
    {
        if (bodyPartsComplaints == null || bodyPartsComplaints.items.Count == 0) return;
        body = new Chunk("Selected body parts complaints:\n", midTitlesFont);
        paragraph = new Paragraph();
        paragraph.SpacingBefore = 30f;
        paragraph.SpacingAfter = 10f;
        paragraph.TabSettings = new TabSettings(200);
        paragraph.Add(body);

        foreach (KeyValuePair<string, List<string>> item in bodyPartsComplaints.items)
        {
            phrase = new Phrase();
            body = new Chunk(bullet + " ", bulletFont);
            phrase.Add(body);

            body = new Chunk(item.Key + ": ", labelFont);
            phrase.Add(body);

            phrase.Add(glue);

            body = new Chunk(string.Join(" - ", item.Value) + "\n", selectedChoicesFont);
            phrase.Add(body);
            paragraph.Add(phrase);
        }

        pdfReport.Add(paragraph);
        pdfReport.Add(lineSeparatorColored);
    }

    static void PrintList(ref Document pdfReport, List<string> list, string title)
    {
        if (list == null || list.Count == 0) return;

        paragraph = new Paragraph();
        paragraph.SpacingBefore = 10f;
        paragraph.SpacingAfter = 10f;

        PdfPTable table = new PdfPTable(3);
        table.HorizontalAlignment = Element.ALIGN_LEFT;
        table.DefaultCell.Border = Rectangle.NO_BORDER;
        table.TotalWidth = 540f;
        table.LockedWidth = true;
        float[] widths = new float[] { 180f, 180f, 180f };
        table.SetWidths(widths);
        body = new Chunk(title + "\n", midTitlesFont);
        phrase = new Phrase();
        phrase.Add(body);
        paragraph.Add(phrase);

        for (int i = 0; i < list.Count; i++)
        {
            UnityEngine.Debug.Log(list[i]);
            body = new Chunk(list[i].Split(new string[] { ". " }, StringSplitOptions.None)[1], selectedChoicesFont);
            phrase = new Phrase(body);
            PdfPCell cell = new PdfPCell(phrase) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE/*, Border = Rectangle.NO_BORDER */};
            table.AddCell(cell);
        }
        table.CompleteRow();

        paragraph.Add(table);
        pdfReport.Add(paragraph);
        pdfReport.Add(lineSeparatorGray);
    }

    static void PrintExercises(ref Document pdfReport, List<AssignExercise> list, string title, List<string> tableTitles)
    {
        if (list == null || list.Count == 0) return;

        paragraph = new Paragraph();
        paragraph.SpacingBefore = 10f;
        paragraph.SpacingAfter = 10f;

        body = new Chunk(title + "\n", midTitlesFont);
        phrase = new Phrase();
        phrase.Add(body);
        paragraph.Add(phrase);

        PdfPTable table = new PdfPTable(6);
        table.HorizontalAlignment = Element.ALIGN_LEFT;
        table.DefaultCell.Border = Rectangle.NO_BORDER;
        table.TotalWidth = 560f;
        table.LockedWidth = true;
        float[] widths = new float[] { 20f, 140f, 80f, 80f, 80f, 160f };
        table.SetWidths(widths);

        PdfPCell cell = new PdfPCell();

        for (int i = 0; i < tableTitles.Count; i++)
        {
            body = new Chunk(tableTitles[i], tableTitlesFont);
            phrase = new Phrase(body);
            cell = new PdfPCell(phrase) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE/*, Border = Rectangle.NO_BORDER */};

            table.AddCell(cell);
        }
        for (int i = 0; i < list.Count; i++)
        {
            phrase = new Phrase(new Chunk(i + ".", selectedChoicesFont));
            cell = new PdfPCell(phrase) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE/*, Border = Rectangle.NO_BORDER */};
            table.AddCell(cell);

            phrase = new Phrase(new Chunk(list[i].exercise_name, selectedChoicesFont));
            cell = new PdfPCell(phrase) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE/*, Border = Rectangle.NO_BORDER */};
            table.AddCell(cell);

            phrase = new Phrase(new Chunk(list[i].repetitionsPerSet, selectedChoicesFont));
            cell = new PdfPCell(phrase) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE/*, Border = Rectangle.NO_BORDER */};
            table.AddCell(cell);

            phrase = new Phrase(new Chunk(list[i].setsPerDay, selectedChoicesFont));
            cell = new PdfPCell(phrase) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE/*, Border = Rectangle.NO_BORDER */};
            table.AddCell(cell);

            phrase = new Phrase(new Chunk(list[i].daysPerWeek, selectedChoicesFont));
            cell = new PdfPCell(phrase) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE/*, Border = Rectangle.NO_BORDER */};
            table.AddCell(cell);

            phrase = new Phrase(new Chunk(list[i].notes, selectedChoicesFont));
            cell = new PdfPCell(phrase) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE/*, Border = Rectangle.NO_BORDER */};
            table.AddCell(cell);
        }
        table.CompleteRow();

        paragraph.Add(table);
        pdfReport.Add(paragraph);
        pdfReport.Add(lineSeparatorGray);
    }

    static string PrintSelectedToggles_andCheckNested(Toggles_Model toggles_Model)
    {
        string txtWillBeWritten = ReplaceNewLine_withSpace(toggles_Model.title);

        if (toggles_Model.selectedValues == null || toggles_Model.selectedValues.Count == 0) return txtWillBeWritten;

        foreach (Toggles_Model toggles_Model1 in toggles_Model.selectedValues)
        {
            txtWillBeWritten += " -> " + PrintSelectedToggles_andCheckNested(toggles_Model1);
            txtWillBeWritten += "\n";
        }

        return txtWillBeWritten;
    }

    static string ReplaceNewLine_withSpace(string txt)
    {
        return txt.ReplaceAll(new string[] { "\n", " \n", "\n " }, " ", true);
    }
}

public class MyEvent : PdfPageEventHelper
{
    Image[] images;
    byte[][] logo_bytes;
    Paragraph paragraph;

    public MyEvent() { }

    public MyEvent(byte[][] logo_bytes)
    {
        this.logo_bytes = logo_bytes;
    }

    public override void OnOpenDocument(PdfWriter writer, Document document)
    {
        images = new Image[] {
        AddLogo(logo_bytes[0], 15, 770, 40, 40),
        AddLogo(logo_bytes[1], 65, 770, 25, 100) };
    }

    public override void OnStartPage(PdfWriter writer, Document document)
    {
        for (int i = 0; i < images.Length; i++)
            writer.DirectContent.AddImage(images[i]);

        paragraph = new Paragraph();
        paragraph.SpacingAfter = 50;
        paragraph.Add(new Phrase("\n"));
        document.Add(paragraph);
    }
    Image AddLogo(byte[] image_byte, float xPosition, float yPosition, float height, float width)
    {
        MemoryStream ms = new MemoryStream(image_byte);
        var logo = Image.GetInstance(ms);

        logo.SetAbsolutePosition(xPosition, yPosition);
        logo.ScaleAbsoluteHeight(height);
        logo.ScaleAbsoluteWidth(width);
        return logo;
    }
}