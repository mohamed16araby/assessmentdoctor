﻿using EnhancedUI.EnhancedScroller;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using System;

public class AlgorithmNameOption_View : EnhancedScrollerCellView
{
    [SerializeField]
    TextMeshProUGUI algorithm_Option_Txt;
    public string NameWithoutColoring { get; set; }

    [SerializeField]
    Button algorithm_Option_Btn;
    [SerializeField]
    Button algorithm_Remove_Btn;

    Action<string, bool> LoadAlgorithm;
    Action<string> DeleteAlgorithm;

    private void Start()
    {
        algorithm_Option_Btn.onClick.AddListener(delegate { LoadAlgorithm(NameWithoutColoring, false); });
        algorithm_Remove_Btn.onClick.AddListener(delegate { DeleteAlgorithm(NameWithoutColoring); });
    }
    public void SetView(string _name, string _searchedText, Action<string, bool> _loadAlgorithm_Action, Action<string> _DeleteAlgorithm_Action)
    {
        NameWithoutColoring = _name;

        if (!string.IsNullOrEmpty(_searchedText))
        {
            algorithm_Option_Txt.text = _name.Replace(_searchedText, string.Format("<color=\"yellow\"><b>{0}</b></color>", _searchedText));
        }
        else
            algorithm_Option_Txt.text = _name;

        LoadAlgorithm = _loadAlgorithm_Action;
        DeleteAlgorithm = _DeleteAlgorithm_Action;
    }
}
