﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class CustomizePanel_View : Panel_2MainHeaders_View
{
    /// <summary>
    /// Question name text that is going to be instantiated depending on number of questions in that panel.
    /// </summary>
    [SerializeField]
    CustomizeItem item_prefab;

    public CustomizeItem AddItem<T>(string _title, T _item, CustomizeItem itemParent, bool _hide = false)
    {
        Transform parentTransform = itemParent == null ? contentHolder : itemParent.childrenHolder;
        CustomizeItem item = Instantiate(item_prefab, parentTransform);
        item.InstantiateItem(_title, _hide);

        if (itemParent != null)
        itemParent.AddChild(item, _item);

        return item; 
    }

    public void RaisePanel_isReadyTobeViewed()
    {
        panel_IsReady_TobeViewed.Raise();
    }
}