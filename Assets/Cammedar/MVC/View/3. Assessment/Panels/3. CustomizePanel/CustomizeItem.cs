﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

using System.Linq;

public class CustomizeItem : MonoBehaviour
{
    [SerializeField]
    public Text title;
    [SerializeField]
    public GameObject toggles;

    [SerializeField]
    ToggleExpandable expandable_Toggle;
    [SerializeField]
    Checkbox_Tristate checkbox_Tristate;
    [SerializeField]
    public Transform childrenHolder;

    CustomizeItem parent;
    public Dictionary<CustomizeItem, ITransit> childrenDictionary = new Dictionary<CustomizeItem, ITransit>();

    public TristateCheck CheckType { get => checkbox_Tristate.CheckType; }
    bool _isHiden = false;

    public void InstantiateItem(string _title, bool _hide= false)
    {

        this.gameObject.name = _title;
        title.text = _title;

        SetCheckToggle();
        SetExpandToggle();
        expandable_Toggle.SetInteractability(false);

        if (_hide)
        {
            toggles.SetActive(false);
            _isHiden = true;
        }
    }

    public void AddChild<T>(CustomizeItem item, T _data)
    {
        item.parent = this;
        item.gameObject.SetActive(false);
        childrenDictionary.Add(item, _data as ITransit);

        if (!item._isHiden)
        {
            CustomizeItem parentActive = Get_ExpandedToggle_ToVisibleParent(item.parent);
            if (parentActive != null && !parentActive.expandable_Toggle.IsInteractable)
                parentActive.expandable_Toggle.SetInteractability(true);
        }
    }

    private CustomizeItem Get_ExpandedToggle_ToVisibleParent(CustomizeItem item)
    {
        if (item == null) return null;
        if (!item._isHiden) return item;
        else return Get_ExpandedToggle_ToVisibleParent(item.parent);
    }

    public void SetExpandToggle()
    {
        expandable_Toggle.SetInteractability(true);
        expandable_Toggle.Init();
        expandable_Toggle.AddListener(delegate { OnToggleExpandChange(); });
    }

    public void ChangeCheckType(TristateCheck tristateCheck)
    {
        checkbox_Tristate.ChangeCheckType(tristateCheck);
    }

    public void SetCheckToggle()
    {
        checkbox_Tristate.AddListener(delegate { OnCheckboxChange(); });
    }

    void FullCheckToggleWithItsChildren()
    {
        checkbox_Tristate.FullCheckSprite();
        Expand();
        foreach (CustomizeItem item in childrenDictionary.Keys)
            item.FullCheckToggleWithItsChildren();
    }

    void UncheckToggleWithItsChildren()
    {
        checkbox_Tristate.UnCheckSprite();
        Collapse();
        foreach (CustomizeItem item in childrenDictionary.Keys)
            item.UncheckToggleWithItsChildren();
    }

    /// <summary>
    /// Respond to a change in checkbox, gets called after each click on checkbox
    /// </summary>
    public void OnCheckboxChange()
    {
        if (gameObject.name == "Diagnosis")
            CustomizePanels_Manager.Instance.ActiveAllTooth();
        if (gameObject.name == "Treatment")
            CustomizePanels_Manager.Instance.ActiveToothNumersTreatment();
        for (int i = 1; i <= 8; i++)
            if (gameObject.name == "Tooth " + i)
                return;
        switch (checkbox_Tristate.CheckType)
        {
            case TristateCheck.FullChecked:
            case TristateCheck.SemiChecked:
                UncheckToggleWithItsChildren();
                break;

            case TristateCheck.NoneChecked:
                FullCheckToggleWithItsChildren();
                break;
        }

        UpdateParentsCheckbox(parent);
    }

    /// <summary>
    /// Update all parents that this item is nested from.
    /// </summary>
    /// <param name="_parent"></param>
    public void UpdateParentsCheckbox(CustomizeItem _parent)
    {
        if (_parent == null) return; //Is _parent is a root parent which doesn't have a parent for it to be updated.

        switch (_parent.Get_Updated_CheckType()) //Get type of this parent item after its children change.
        {
            case TristateCheck.NoneChecked:
                _parent.checkbox_Tristate.UnCheckSprite();
                break;
            case TristateCheck.FullChecked:
                _parent.checkbox_Tristate.FullCheckSprite();
                break;
            case TristateCheck.SemiChecked:
                _parent.checkbox_Tristate.SemiCheckSprite();
                break;
        }
        _parent.UpdateParentsCheckbox(_parent.parent); //Recursive calls to update all the parents of an item.
    }

    /// <summary>
    /// Get type of checkbox. 
    /// </summary>
    /// <returns></returns>
    TristateCheck Get_Updated_CheckType()
    {
        int noOfFullCheckedChildren = 0;
        int noOfNoneCheckedChildren = 0;
        int noOfSemiCheckedChildren = 0;

        foreach (CustomizeItem item in childrenDictionary.Keys)
        {
            switch (item.checkbox_Tristate.CheckType)
            {
                case TristateCheck.NoneChecked:
                    noOfNoneCheckedChildren++;
                    break;
                case TristateCheck.FullChecked:
                    noOfFullCheckedChildren++;
                    break;
                case TristateCheck.SemiChecked:
                    noOfSemiCheckedChildren++;
                    break;
            }
        }

        if (noOfSemiCheckedChildren == 0)
            if (noOfFullCheckedChildren == childrenDictionary.Count)
                return TristateCheck.FullChecked;

            else if (noOfNoneCheckedChildren == childrenDictionary.Count)
                return TristateCheck.NoneChecked;

        return TristateCheck.SemiChecked;
    }

    ///---Toggle Expand Change functions---///
    void OnToggleExpandChange()
    {
        if (expandable_Toggle.Expanded) Collapse();
        else Expand();
    }

    public void Collapse()
    {
        expandable_Toggle.CollapseToggle();
        foreach (Transform child in childrenHolder)
            child.gameObject.SetActive(false);
    }

    public void Expand()
    {
        expandable_Toggle.ExpandToggle();

        foreach (Transform child in childrenHolder)
        {
            CustomizeItem itemChild = childrenDictionary.Keys.ToList().Find(c => c.title.text == child.name);
            child.gameObject.SetActive(true);
            if (itemChild._isHiden)
            {
                itemChild.GetComponent<VerticalLayoutGroup>().padding.top = 0;
                itemChild.childrenHolder.GetComponent<VerticalLayoutGroup>().padding.top = 0;
                itemChild.Expand();
            }
        }
    }
}