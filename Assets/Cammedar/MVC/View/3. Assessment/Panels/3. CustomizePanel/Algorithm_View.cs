﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using EnhancedUI.EnhancedScroller;

public class Algorithm_View : MonoBehaviour, IEnhancedScrollerDelegate
{
    [Header("Save View")]
    [SerializeField]
    Button save_OpenView_Btn;
    [SerializeField]
    TMP_InputField algorithmName_Save;
    [SerializeField]
    GameObject save_IF;
    [SerializeField]
    Button save_Btn;
    [SerializeField]
    TextMeshProUGUI save_Callback_Txt;

    [Header("Search View")]
    [SerializeField]
    Button search_OpenView_Btn;
    [SerializeField]
    TMP_InputField algorithmName_Search;
    [SerializeField]
    GameObject search_IF;  
    //[SerializeField]
    //Color matchingStr_Color;

    [SerializeField]
    EnhancedScrollerCellView algorithmNameOption;
    public EnhancedScroller algorithmScroller;
    public Transform optionsHolder;

    [Header("Feedback text colors")]
    public Color success_Color;
    public Color failing_Color;

    public delegate void SaveAlgorithmEvent(string _fileName, bool _patientLastVisit = false);
    public event SaveAlgorithmEvent OnSaveAlgorithmEvent;

    List<string> algorithmFilesNames = new List<string>();
    List<string> algorithms_matching = new List<string>();

    System.Action<string, bool> LoadAlgorithm;
    System.Action<string> DeleteAlgorithm;

    private void Start()
    {
        save_OpenView_Btn.onClick.AddListener(delegate { ChangeVisibility(save_IF); });
        save_Btn.onClick.AddListener(delegate { SaveAlgorithm(algorithmName_Save.text); });

        search_OpenView_Btn.onClick.AddListener(
            delegate {
                ChangeVisibility(search_IF, optionsHolder.gameObject);
                RefreshSearchOptions();
            });
        algorithmName_Search.onValueChanged.AddListener(SearchForAlgorithm);
        algorithmScroller.Delegate = this;
    }

    public void Init(System.Action<string, bool> LoadAlgorithm_Action, System.Action<string> DeleteAlgorithm_Action)
    {
        LoadAlgorithm = LoadAlgorithm_Action;
        DeleteAlgorithm = DeleteAlgorithm_Action;
    }

    public void HideView()
    {
        save_OpenView_Btn.gameObject.SetActive(false);
        search_OpenView_Btn.gameObject.SetActive(false);
    }

    public void SetView(List<string> _algorithmFilesNames)
    {
        algorithmFilesNames = _algorithmFilesNames;
        algorithms_matching.Clear();
        RefreshSearchOptions();
    }

    void RefreshSearchOptions()
    {
        if (!optionsHolder) return;
        if (optionsHolder.gameObject.activeSelf) algorithmScroller.ReloadData();
    }

    void ChangeVisibility(params GameObject[] gameObjects)
    {
        foreach (GameObject gameObject in gameObjects)
            gameObject.SetActive(!gameObject.activeSelf);
    }

    void SaveAlgorithm(string _fileName)
    {
        if (string.IsNullOrEmpty(_fileName))
        {
            ViewSaveCallBack(OperationState.FAILED, "Please, enter algorithm name!");
            return;
        }

        save_Callback_Txt.gameObject.SetActive(false);
        OnSaveAlgorithmEvent(_fileName);
    }

    public void ViewSaveCallBack(OperationState state, string _message)
    {
        if (!save_Callback_Txt) return;

        save_Callback_Txt.gameObject.SetActive(true);
        save_Callback_Txt.text = _message;
        save_Callback_Txt.color = state == OperationState.SUCCESS ? success_Color : failing_Color;
    }

    void SearchForAlgorithm(string _algorithmName)
    {
        algorithms_matching= algorithmFilesNames.FindAll(s => s.ToLower().Contains(_algorithmName.ToLower()));
        RefreshSearchOptions();
    }
    
    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return algorithms_matching.Count == 0 ? algorithmFilesNames.Count : algorithms_matching.Count;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 20f;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        AlgorithmNameOption_View cellView = scroller.GetCellView(algorithmNameOption) as AlgorithmNameOption_View;

        string algorithmName = algorithms_matching.Count == 0 ? algorithmFilesNames[dataIndex] : algorithms_matching[dataIndex];
        cellView.SetView(algorithmName, algorithmName_Search.text, LoadAlgorithm, DeleteAlgorithm);

        return cellView;
    }
}