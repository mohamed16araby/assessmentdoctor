﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SidePanel_View : Panel_4MainHeaders_View
{
    /// <summary>
    /// Question name text that is going to be instantiated depending on number of questions in that panel.
    /// </summary>
    [SerializeField]
    Text questionName;

    /// <summary>
    /// Set panel view based on group name, panel name and whether to show panel name or not.
    /// </summary>
    /// <param name="groupName"></param>
    /// <param name="panelName"></param>
    /// <param name="hidePanelTitle"></param>
    /// <param name="questionsHeaders_Appearence">Dictionary contains questions headers as keys, and their conditions of appearence as values</param>
    public void SetView(string groupName, string panelName, string sectionName, string sideName, bool hidePanelTitle, bool hideSectionTitle,
                        Dictionary<string, bool> questionsHeaders_Appearence)
    {
        base.SetView(groupName, panelName, sectionName, sideName, hidePanelTitle, hideSectionTitle);
        
        foreach(KeyValuePair<string, bool> questionHeader in questionsHeaders_Appearence)
        {
            Text question_txt = Instantiate(questionName, contentHolder);
            question_txt.text = questionHeader.Key;

            if (questionHeader.Value == true)
                question_txt.transform.gameObject.SetActive(false);
        }
    }
}
