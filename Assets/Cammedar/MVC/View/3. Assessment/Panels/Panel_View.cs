﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

/// <summary>
/// Contains group name, panel name texts and questions holder transform.
/// </summary>
//[System.Serializable]
public class Panel_2MainHeaders_View : MonoBehaviour
{
    [SerializeField]
    protected Text groupName;
    [SerializeField]
    protected Text panelName;
    [SerializeField]
    protected Transform contentHolder;

    [SerializeField]
    protected GameEvent panel_IsReady_TobeViewed;
    /// <summary>
    /// Set panel view based on group name, panel name and whether to show panel name or not.
    /// </summary>
    /// <param name="groupName"></param>
    /// <param name="panelName"></param>
    /// <param name="hidePanelTitle"></param>
    public virtual void SetView(string groupName, string panelName, bool hidePanelTitle)
    {
        try
        {
            this.groupName.text = groupName;

        this.panelName.text = panelName;
        }
        catch (System.Exception e)
        {

        }
        if (hidePanelTitle == true)
            this.panelName.transform.parent.gameObject.SetActive(false);
    }

    /// <summary>
    /// Get questions holder transform.
    /// </summary>
    /// <returns></returns>
    public Transform GetContentHolderTransform()
    {
        return contentHolder;
    }
}

public class Panel_3MainHeaders_View : Panel_2MainHeaders_View
{
    [SerializeField]
    protected Text sectionName;

    public void SetView(string groupName, string panelName, string sectionName, bool hidePanelTitle, bool hideSectionTitle)
    {
        base.SetView(groupName, panelName, hidePanelTitle);
        this.sectionName.text = sectionName;
        if (hideSectionTitle)
            this.sectionName.transform.parent.gameObject.SetActive(false);
    }
}

public class Panel_4MainHeaders_View : Panel_3MainHeaders_View
{
    [SerializeField]
    protected Text sideName;

    public void SetView(string groupName, string panelName, string sectionName, string sideName, bool hidePanelTitle, bool hideSectionTitle)
    {
        base.SetView(groupName, panelName, sectionName, hidePanelTitle, hideSectionTitle);
        this.sideName.text = sideName;

        if (Enum.TryParse(sideName, out Sides side))
            if (side == Sides.None) this.sideName.transform.parent.gameObject.SetActive(false);
    }
}