﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Scores : MonoBehaviour
{
    [Header("Total score")]
    [SerializeField]
    Text scoreTotal;
    [SerializeField]
    Text scoreResult;

    [Header("Section score")]
    [SerializeField]
    Text sectionScoreTotal;
    [SerializeField]
    Text sectionScoreResult;

    public int ScoreResult
    {
        get { return ConvertToInt(scoreResult.text); }
    }
    public int SectionScoreResult
    {
        get { return ConvertToInt(sectionScoreResult.text); }
    }

    public void Init(string scoreTotal, string scoreResult, string sectionScoreTotal, string sectionScoreResult)
    {
        this.scoreTotal.text = scoreTotal;
        this.scoreResult.text = scoreResult;
        this.sectionScoreTotal.text = sectionScoreTotal;
        this.sectionScoreResult.text = sectionScoreResult;
    }

    public void ModifyScores(int _amount, System.Action<int, int> updateGroupScoresResults)
    {
        int sectionScoreResultTemp = ModifyScore(sectionScoreResult, _amount);
        int scoreResultTemp = ModifyScore(scoreResult, _amount);


        updateGroupScoresResults(sectionScoreResultTemp, scoreResultTemp);
    }

    private int ModifyScore(Text text, int _amount)
    {
        int result = ConvertToInt(text.text) + _amount;
        text.text = result > 0 ? text.text = result.ToString() : "0";
        
        return result;
    }

    private int ConvertToInt(string str)
    {
        if(int.TryParse(str, out int result))
            return result;

        return 0;
    }
}
