﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

[Serializable]
public class MainPanel_View : Panel_4MainHeaders_View
{
    [SerializeField]
    public Transform scoreHolder;
    [SerializeField]
    public Scores scores;

    Action<int, int> ScoreResultsUpdated;

    public void SetView(string groupName, string panelName, string sectionName, string sideName, bool hidePanelTitle, bool hideSectionTitle,
                       string scoreTotal, string scoreResult, string sectionScoreTotal, string sectionScoreResult, Action<int, int> ScoreResultsUpdated, bool scoreTracking = false)
    {
        base.SetView(groupName, panelName, sectionName, sideName, hidePanelTitle, hideSectionTitle);
        this.ScoreResultsUpdated = ScoreResultsUpdated;

        scoreHolder.gameObject.SetActive(scoreTracking);

        if (scoreTracking)
            scores.Init(scoreTotal, scoreResult, sectionScoreTotal, sectionScoreResult);
    }
}
