﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BoolAction = UnityEngine.Events.UnityAction<bool>;
using System.Linq;

public class SessionSubspecialityChoice : MonoBehaviour
{
    [SerializeField]
    Toggle toggle;
    [SerializeField]
    Image background;

    [SerializeField]
    Color origColor;
    [SerializeField]
    Color selectedColor;

    [SerializeField]
    Text subspeciality_Label;

    [SerializeField]
    public Transform contentHolder;
    [SerializeField]
    public ToggleGroup toggleGroup;

    public List<SessionSubspecialityChoice> children = new List<SessionSubspecialityChoice>();
    public bool ToggleOn { get => toggle.isOn; }

    CanvasScript canvasScript;


    private void Awake() {
        canvasScript = GameObject.FindGameObjectWithTag("View_Canvas").GetComponent<CanvasScript>();    
    }


    public void AddListeners(params BoolAction[] methods)
    {
        for (int i = 0; i < methods.Length; i++)
            toggle.onValueChanged.AddListener(methods[i]);
    }

    public void Init(string subspeciality, ToggleGroup _toggleGroup/*, bool isChild = false*/)
    {
        AddListeners(ChangeSelectionColor, ResetChoices, HideViewDependencies);

        background.color = origColor;
        toggle.group = _toggleGroup;

        subspeciality_Label.text = subspeciality;

        //if (isChild)
        //{
        //    gameObject.SetActive(false);
        //}
    }

    public void AddChild(SessionSubspecialityChoice child)
    {
        //child.transform.SetParent(contentHolder);
        child.gameObject.SetActive(false);
        children.Add(child);
    }

    void ChangeSelectionColor(bool selected)
    {
        background.color = selected ? selectedColor : origColor;
    }

    void HideViewDependencies(bool appear)
    {
        // ListModel.items.Clear();

        // for(int i=0 ; i< children.Count; i++) {
        //     children[i].gameObject.SetActive(appear);
        // }
        
         //ListModel.items.Clear();

        //if (appear)
        if (children.Count > 0)
        {
            //for (int i = 0; i < children.Count; i++)
            //{
            //    ListModel.items.Add(children[i].subspeciality_Label.text);
            //    Debug.Log(children[i].subspeciality_Label.text);
            //}

            canvasScript.ListPrefab.SetActive(appear);
        }
    }

    void ResetChoices(bool appear)
    {
        if (!appear)
            if (toggleGroup.AnyTogglesOn())
            {
                foreach (Toggle toggle in toggleGroup.ActiveToggles().ToList())
                {
                    toggle.isOn = false;
                }
            }
    }
}
