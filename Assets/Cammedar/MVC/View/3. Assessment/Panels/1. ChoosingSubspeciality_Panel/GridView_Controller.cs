﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

using EnhancedScrollerDemos.GridSelection;

public class GridView_Controller : MonoBehaviour
{
    public Controller controller;
    [SerializeField]
    Text textHeadline;
    [SerializeField]
    Button close_Btn;

    Action ClosingAction;
    Action OnSelectedValueChangedAction;

    public void Init(string textHeadline, List<string> listData, StringField selectedField, Action ClosingAction, Action OnSelectedValueChangedAction)
    {
        this.textHeadline.text = textHeadline;
        this.ClosingAction = ClosingAction;

        if (this.ClosingAction == null)
            close_Btn.gameObject.SetActive(false);

        this.OnSelectedValueChangedAction = OnSelectedValueChangedAction;

        controller._ListModelItems = listData;
        controller.SelectedChoice = selectedField;
        controller.SelectedValueAction = OnSelectedValueChanged;
    }

    [Beebyte.Obfuscator.SkipRename]
    public void ClosePopUp()
    {
        ClosingAction();
    }

    public void OnSelectedValueChanged()
    {
        OnSelectedValueChangedAction();

        if (ClosingAction != null)
        {
           // ClosePopUp();
        }
    }
}
