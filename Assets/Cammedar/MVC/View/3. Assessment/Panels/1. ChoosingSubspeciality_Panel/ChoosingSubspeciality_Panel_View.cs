﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using System;


public class ChoosingSubspeciality_Panel_View : Panel_2MainHeaders_View
{
    [SerializeField]
    SessionSubspecialityChoice sessionSubspecialityChoice_Toggle;

    [SerializeField]
    GridView_Controller gridView_Controller_Org;

    [SerializeField]
    ToggleGroup toggleGroup;

    [SerializeField]
    StringField selected_Subsubspeciality;
    [SerializeField]
    StringField selected_Subspeciality;

    private Action<KeyValuePair<string, string>, bool> Set_AddEdit_UnderneathChoice;
    private Dictionary<string, List<string>> subspecialitiesList;
    private MonoBehaviour subspeciality_gridView_object;

    public void SetView(string groupName, string panelName, bool hidePanelTitle, Dictionary<string, List<string>> subspecialitiesList, Action<KeyValuePair<string, string>, bool> Set_AddEdit_UnderneathChoice)
    {
        base.SetView(groupName, panelName, hidePanelTitle);
        selected_Subsubspeciality.Value = "";
        this.Set_AddEdit_UnderneathChoice = Set_AddEdit_UnderneathChoice;
        this.subspecialitiesList = subspecialitiesList;

        CreateSubspecialitesView();

        panel_IsReady_TobeViewed.Raise();
    }

    public void CreateSubspecialitesView()
    {
        GridView_Controller subspeciality_gridView_Controller = Instantiate(gridView_Controller_Org, contentHolder);

        subspeciality_gridView_Controller.gameObject.SetActive(false);
        subspeciality_gridView_Controller.Init("Select Sub-speciality", subspecialitiesList.Keys.ToList(), selected_Subspeciality, null, OnSubspecialityChoiceChanged);
        subspeciality_gridView_Controller.gameObject.SetActive(true);

        subspeciality_gridView_object = subspeciality_gridView_Controller;
    }

    private void OnSubspecialityChoiceChanged()
    {
        if(subspecialitiesList[selected_Subspeciality.Value].Count == 0)
            Set_AddEdit_UnderneathChoice(new KeyValuePair<string, string>(selected_Subspeciality.Value, ""), true);

        else
        {
            Set_AddEdit_UnderneathChoice(new KeyValuePair<string, string>("", ""), false);
            ViewVisibility(false);
            GridView_Controller subsubspeciality_gridView_Controller = Instantiate(gridView_Controller_Org, contentHolder);

            subsubspeciality_gridView_Controller.gameObject.SetActive(false);

            subsubspeciality_gridView_Controller.Init("Select Subsub-speciality", subspecialitiesList[selected_Subspeciality.Value], selected_Subsubspeciality, 
                delegate { ClosingPopupAction(subsubspeciality_gridView_Controller.gameObject); ViewVisibility(true); },
                delegate { Set_AddEdit_UnderneathChoice(new KeyValuePair<string, string>(selected_Subspeciality.Value, selected_Subsubspeciality.Value), true); });

            subsubspeciality_gridView_Controller.gameObject.SetActive(true);
        }
    }

    public void ViewVisibility(bool _show)
    {
        subspeciality_gridView_object.gameObject.SetActive(_show);
    }

    void ClosingPopupAction(GameObject _object)
    {
        Destroy(_object.gameObject);
    }

}