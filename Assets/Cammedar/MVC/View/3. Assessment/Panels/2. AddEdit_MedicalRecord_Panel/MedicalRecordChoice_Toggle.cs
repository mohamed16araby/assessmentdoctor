﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BoolAction = UnityEngine.Events.UnityAction<bool>;
using System;
using System.Linq;

public class MedicalRecordChoice_Toggle : MonoBehaviour
{
    [SerializeField]
    Toggle toggle;
    [SerializeField]
    Transform contentHolder;
    [SerializeField]
    Text label;
    [SerializeField]
    GameObject labels;

    [SerializeField]
    ToggleGroup toggleGroup;

    [SerializeField]
    SessionDateChoice sessionDateChoice_Toggle;
    [SerializeField]
    SessionFollowupChoice sessionFollowupChoice_Toggle;

    [SerializeField]
    Image background;

    [SerializeField]
    Color origColor;

    [SerializeField]
    Color selectedColor;

    public bool ToggleOn { get => toggle.isOn; }
    public string Title { get; set; }

    public void AddListener(BoolAction method)
    {
        toggle.onValueChanged.AddListener(method);
    }

    /// <summary>
    /// For followup option
    /// </summary>
    /// <param name="_title"></param>
    /// <param name="_toggleGroup"></param>
    /// <param name="lastFollowupDate"></param>
    /// <param name="Set_AddEdit_UnderneathChoice"></param>
    public void Init(string _title, ToggleGroup _toggleGroup, KeyValuePair<string, string> lastFollowupDate, Action<KeyValuePair<string, string>, bool> Set_AddEdit_UnderneathChoice)
    {
        Init(_title, _toggleGroup);

        toggle.interactable = true;
        labels.SetActive(false);
        CreateFollowupChoices(lastFollowupDate, Set_AddEdit_UnderneathChoice);
        AddListener(val => HideViewDependencies(val, true));
    }
    public void Init(string _title, ToggleGroup _toggleGroup, Dictionary<string, string> sessionDatesList, Action<KeyValuePair<string, string>, bool> Set_AddEdit_UnderneathChoice)
    {
        Init(_title, _toggleGroup);

        //For the nested choices
        if (sessionDatesList.Count == 0) toggle.interactable = false;
        else
        {
            toggle.interactable = true;
            CreateSessionDateView(sessionDatesList, Set_AddEdit_UnderneathChoice);
        }

        AddListener(val => HideViewDependencies(val, false));
    }

    public void Init(string _title, ToggleGroup _toggleGroup)
    {
        toggle.group = _toggleGroup;
        Title = _title;
        label.text = FormReadablePhrase(_title);

        AddListener(ChangeSelectionColor);
    }

    void ChangeSelectionColor(bool selected)
    {
        background.color = selected ? selectedColor : origColor;
    }

    string FormReadablePhrase(string camelCase)
    {
        var s = camelCase;
        string result = string.Concat(s.Select(c => char.IsUpper(c) ? " " + c.ToString() : c.ToString()))
            .TrimStart();

        return result;
    }

    public void CreateSessionDateView(Dictionary<string, string> sessionDatesList, Action<KeyValuePair<string, string>, bool> Set_AddEdit_UnderneathChoice)
    {
        foreach(KeyValuePair<string, string> sessionId_Date in sessionDatesList)
        {
            SessionDateChoice sessionDateChoice = Instantiate(sessionDateChoice_Toggle, contentHolder);
            string sessionData = sessionId_Date.Value;
            string []parts = sessionData.Split(' ');
            string[] date = parts[0].Split('/');
            sessionDateChoice.Init(date[1], date[0], date[2], parts[1] +" "+ parts[2], toggleGroup);
            sessionDateChoice.AddListener(val=> Set_AddEdit_UnderneathChoice(new KeyValuePair<string, string>(sessionId_Date.Key, sessionData), val));
        }
    }
    public void CreateFollowupChoices(KeyValuePair<string, string> lastFollowupDate, Action<KeyValuePair<string, string>, bool> Set_AddEdit_UnderneathChoice)
    {
        if(!string.IsNullOrEmpty(lastFollowupDate.Key))
        {
            SessionFollowupChoice editFollowupChoice = Instantiate(sessionFollowupChoice_Toggle, contentHolder);
            string data = lastFollowupDate.Value;
            string []parts = data.Split(' ');
            string[] date = parts[0].Split('/');
            editFollowupChoice.Init(FormReadablePhrase(FollowupChoices.EditLastFollowup.ToString()) + " (" + date[1] + "/ " + date[0] + "/ "+ date[2] + "  "+ parts[1] +" "+ parts[2] + ")", toggleGroup);
            editFollowupChoice.AddListeners(val=> Set_AddEdit_UnderneathChoice(new KeyValuePair<string, string>(FollowupChoices.EditLastFollowup.ToString(), lastFollowupDate.Key), val));
        }
        SessionFollowupChoice newFollowupChoice = Instantiate(sessionFollowupChoice_Toggle, contentHolder);

        newFollowupChoice.Init(FormReadablePhrase(FollowupChoices.CreateFollowupFromScratch.ToString()), toggleGroup);
        newFollowupChoice.AddListeners(val => Set_AddEdit_UnderneathChoice(new KeyValuePair<string, string>(FollowupChoices.CreateFollowupFromScratch.ToString(), ""), val));
    }


    void HideViewDependencies(bool appear, bool hideLabels)
    {
        labels.SetActive(appear);
        if (hideLabels) labels.SetActive(false);

        if (!appear)
            if (toggleGroup.AnyTogglesOn())
            {
                foreach (Toggle toggle in toggleGroup.ActiveToggles().ToList())
                {
                    toggle.isOn = false;
                }
            }
        contentHolder.gameObject.SetActive(appear);
    }
}
