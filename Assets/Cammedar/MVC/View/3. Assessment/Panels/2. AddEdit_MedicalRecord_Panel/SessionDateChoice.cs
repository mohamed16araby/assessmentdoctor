﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BoolAction = UnityEngine.Events.UnityAction<bool>;

public class SessionDateChoice : MonoBehaviour
{
    [SerializeField]
    Toggle toggle;
    [SerializeField]
    Image background;

    [SerializeField]
    Color origColor;
    [SerializeField]
    Color selectedColor;

    [SerializeField]
    Text day_Label;
    [SerializeField]
    Text month_Label;
    [SerializeField]
    Text year_Label;
    [SerializeField]
    Text hour_Label;

    public bool ToggleOn { get => toggle.isOn; }

    public void AddListener(BoolAction method)
    {
        toggle.onValueChanged.AddListener(method);
    }

    public void Init(string day, string month, string year, string hour, ToggleGroup _toggleGroup)
    {
        AddListener(ChangeSelectionColor);

        background.color = origColor;
        toggle.group = _toggleGroup;

        day_Label.text = day;
        month_Label.text = month;
        year_Label.text = year;
        hour_Label.text = hour;
    }

    void ChangeSelectionColor(bool selected)
    {
        background.color = selected ? selectedColor : origColor;
    }
}
