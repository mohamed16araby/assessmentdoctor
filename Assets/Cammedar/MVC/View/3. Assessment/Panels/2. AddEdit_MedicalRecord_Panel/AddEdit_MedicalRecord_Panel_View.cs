﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using System;

public class AddEdit_MedicalRecord_Panel_View : Panel_2MainHeaders_View
{
    [SerializeField]
    MedicalRecordChoice_Toggle medicalRecordChoice_Toggle;

    [SerializeField]
    ToggleGroup toggleGroup;

    public delegate void AddEdit_MedicalRecordChoiceChanged(string performAnotherAssessmentChoice, bool isOn);
    public event AddEdit_MedicalRecordChoiceChanged AddEdit_MedicalRecordChoiceChangedEvent;

    public void SetView(string groupName, string panelName, bool hidePanelTitle, List<PerformAnotherAssessmentChoices> PerformAnotherAssessmentChoicesList, 
        Dictionary<string, string> sessionDatesList, KeyValuePair<string, string> lastFollowupDate, Action<KeyValuePair<string, string>, bool> Set_AddEdit_UnderneathChoice)
    {
        base.SetView(groupName, panelName, hidePanelTitle);

        for (int i = 0; i < PerformAnotherAssessmentChoicesList.Count; i++)
        {
            MedicalRecordChoice_Toggle medicalRecordChoice_Toggle_Inst = Instantiate(medicalRecordChoice_Toggle, contentHolder);

            switch (PerformAnotherAssessmentChoicesList[i])
            {
                case PerformAnotherAssessmentChoices.NewAssessmentFromScratch:
                    medicalRecordChoice_Toggle_Inst.Init(PerformAnotherAssessmentChoicesList[i].ToString(), toggleGroup);
                    break;

                case PerformAnotherAssessmentChoices.Followup:
                    medicalRecordChoice_Toggle_Inst.Init(PerformAnotherAssessmentChoicesList[i].ToString(), toggleGroup, lastFollowupDate, Set_AddEdit_UnderneathChoice);
                    break;

                default:
                    medicalRecordChoice_Toggle_Inst.Init(PerformAnotherAssessmentChoicesList[i].ToString(), toggleGroup, sessionDatesList, Set_AddEdit_UnderneathChoice);
                    break;
            }

            medicalRecordChoice_Toggle_Inst.AddListener(val => AddEdit_MedicalRecordChoiceChangedEvent(medicalRecordChoice_Toggle_Inst.Title, val));
        }

        panel_IsReady_TobeViewed.Raise();
    }
}