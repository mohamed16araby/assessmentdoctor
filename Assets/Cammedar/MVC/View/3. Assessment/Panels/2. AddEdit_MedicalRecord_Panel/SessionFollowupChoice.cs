﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BoolAction = UnityEngine.Events.UnityAction<bool>;
using System.Linq;

public class SessionFollowupChoice : MonoBehaviour
{
    [SerializeField]
    Toggle toggle;
    [SerializeField]
    Image background;

    [SerializeField]
    Color origColor;
    [SerializeField]
    Color selectedColor;

    [SerializeField]
    Text label;

    [SerializeField]
    public Transform contentHolder;
    [SerializeField]
    public ToggleGroup toggleGroup;

    public List<SessionFollowupChoice> children = new List<SessionFollowupChoice>();
    public bool ToggleOn { get => toggle.isOn; }

    public void AddListeners(params BoolAction[] methods)
    {
        for (int i = 0; i < methods.Length; i++)
            toggle.onValueChanged.AddListener(methods[i]);
    }

    public void Init(string title, ToggleGroup _toggleGroup)
    {
        AddListeners(ChangeSelectionColor, ResetChoices, HideViewDependencies);

        background.color = origColor;
        toggle.group = _toggleGroup;

        label.text = title;
    }

    public void AddChild(SessionFollowupChoice child)
    {
        child.gameObject.SetActive(false);
        children.Add(child);
    }

    void ChangeSelectionColor(bool selected)
    {
        background.color = selected ? selectedColor : origColor;
    }

    void HideViewDependencies(bool appear)
    {
        for (int i = 0; i < children.Count; i++)
        {
            children[i].gameObject.SetActive(appear);
        }
    }

    void ResetChoices(bool appear)
    {
        if (!appear)
            if (toggleGroup.AnyTogglesOn())
            {
                foreach (Toggle toggle in toggleGroup.ActiveToggles().ToList())
                {
                    toggle.isOn = false;
                }
            }
    }
}
