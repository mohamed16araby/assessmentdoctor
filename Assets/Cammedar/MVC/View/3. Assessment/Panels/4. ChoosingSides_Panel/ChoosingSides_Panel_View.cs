﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class ChoosingSides_Panel_View : Panel_2MainHeaders_View
{
    [SerializeField]
    ToggleCircled toggleCircled_Prefab;

    [SerializeField]
    ToggleGroup toggleGroup;

    public Dictionary<string, ToggleCircled> allTogglesSelections = new Dictionary<string, ToggleCircled>();

    public delegate void SideToAssessChanged(string side);
    public event SideToAssessChanged SideToAssessChangedEvent;

    public void SetView(string groupName, string panelName, bool hidePanelTitle, List<string> sides_Assessment)
    {
        base.SetView(groupName, panelName, hidePanelTitle);
        allTogglesSelections = new Dictionary<string, ToggleCircled>();

        for (int i = 0; i< sides_Assessment.Count; i++)
        {
            ToggleCircled toggleCircled = Instantiate(toggleCircled_Prefab, contentHolder);
            toggleCircled.Init(sides_Assessment[i], toggleGroup, i == 0);
            toggleCircled.AddListener(delegate {
                SideToAssessChangedEvent(toggleCircled.label.text);
            });
            allTogglesSelections.Add(sides_Assessment[i], toggleCircled);
        }

        panel_IsReady_TobeViewed.Raise();
    }

    //public void ChangeChoice(ToggleCircled toggle)
    //{
    //    toggle.ToggleOn = !toggle.ToggleOn;
    //}
}