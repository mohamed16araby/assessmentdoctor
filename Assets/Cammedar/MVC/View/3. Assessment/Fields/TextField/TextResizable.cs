﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class TextResizable : MonoBehaviour
{
    RectTransform rt;
    Text txt;

    void Awake()
    {
        rt = gameObject.GetComponent<RectTransform>(); // Acessing the RectTransform 
        txt = gameObject.GetComponent<Text>(); // Accessing the text component
    }

    public void Resize()
    {
        if (rt == null || txt == null) return;

        rt.sizeDelta = new Vector2(rt.rect.width, txt.preferredHeight); // Setting the height to equal the height of text
        rt.sizeDelta = new Vector2(txt.preferredWidth, txt.preferredHeight); // Setting the height to equal the height of text
    }
}
