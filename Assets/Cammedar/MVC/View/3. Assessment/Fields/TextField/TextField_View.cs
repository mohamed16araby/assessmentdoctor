﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TextField_View : MonoBehaviour
{
    [SerializeField]
    Text title;
    [SerializeField]
    Text text;

    public void SetView(string _title, bool hideTitle, string text)
    {
        this.title.text = _title;
        if (hideTitle == true)
            this.title.gameObject.SetActive(false);

        this.text.text = text;
    }
}
