﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class ToggleAssess_Image : ToggleAssess
{
    [Header("Toggle Image")]
    public Image iconImage;
    public Color orig_IconImage_Color;

   // public Color selected_Background_Color;

    public void Init(string _title, string _descriptionToTitle, Sprite _Icon, bool _isOn = false)
    {
        base.Init(_title, _descriptionToTitle, _isOn);
        iconImage.sprite = _Icon;
        iconImage.color = orig_IconImage_Color;
    }
    
}