﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class ToggleAssess : ToggleBasic
{
    public Text label;
    public Text descriptionToLabel;

    public Transform childrenTransform;

    [SerializeField]
    ToggleGroup toggleGroup;

    public ToggleGroup ToggleGroup { get => toggleGroup; }

    RectTransform title_rt;
    RectTransform description_rt;

    void Awake()
    {
        title_rt = label.gameObject.GetComponent<RectTransform>(); // Acessing the RectTransform 
        description_rt = descriptionToLabel.gameObject.GetComponent<RectTransform>(); // Acessing the RectTransform 
    }

    protected void Init(string _title, string _descriptionToTitle, bool _isOn, System.Action<bool> OnToggleChange = null)
    {
        base.Init();

        AddListener((val) => {  OnToggleChange?.Invoke(val); });

        label.text = _title;
        title_rt.sizeDelta = new Vector2(label.preferredWidth, label.preferredHeight); // Setting the height to equal the height of text

        if (string.IsNullOrEmpty(_descriptionToTitle)) descriptionToLabel.gameObject.SetActive(false);
        else
        {
            descriptionToLabel.gameObject.SetActive(true);
            descriptionToLabel.text = _descriptionToTitle;
            description_rt.sizeDelta = new Vector2(descriptionToLabel.preferredWidth, descriptionToLabel.preferredHeight); // Setting the height to equal the height of text
        }

        ToggleOn = _isOn;
    }

    public void ToggleChange()
    {
        ToggleOn = !ToggleOn;
    }
}