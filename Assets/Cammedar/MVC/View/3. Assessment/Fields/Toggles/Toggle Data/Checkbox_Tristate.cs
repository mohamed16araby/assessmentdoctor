﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Action = UnityEngine.Events.UnityAction;

public enum TristateCheck
{
    NoneChecked,
    FullChecked,
    SemiChecked
}

public class Checkbox_Tristate : MonoBehaviour
{
    [SerializeField]
    Button checkbox_Btn;

    [Header("Background")]
    [SerializeField]
    protected Image background;
    [SerializeField]
    protected Sprite background_Sprite;
    [SerializeField]
    protected Color background_Color;

    [Header("Checkmark")]
    [SerializeField]
    protected Image checkmark;
    [SerializeField]
    Sprite full_Checkmark_Sprite;
    [SerializeField]
    Color full_Checkmark_Color;

    [Header("Semi-Check")]
    [SerializeField]
    protected Sprite semi_Checkmark_Sprite;
    [SerializeField]
    protected Color semi_Checkmark_Color;

    [SerializeField]
    //[HideInInspector]
    TristateCheck checkType = TristateCheck.NoneChecked;

    //public delegate void CheckTypeChangedEvent();
    //public event CheckTypeChangedEvent CheckTypeChanged;

    public TristateCheck CheckType { get => checkType; set { checkType = value; /*CheckTypeChanged(); */} }

    private void Start()
    {
        background.sprite = background_Sprite;
        background.color = background_Color;
        checkmark.color = semi_Checkmark_Color;
       // checkmark.sprite = full_Checkmark_Sprite;
        checkmark.color = full_Checkmark_Color;
    }
    public void FullCheckSprite()
    {
        ChangeCheckType(TristateCheck.FullChecked);
    }
    public void SemiCheckSprite()
    {
        ChangeCheckType(TristateCheck.SemiChecked);
    }
    public void UnCheckSprite()
    {
        ChangeCheckType(TristateCheck.NoneChecked);
    }

    public void ChangeCheckType(TristateCheck tristateCheck)
    {
        switch (tristateCheck)
        {
            case TristateCheck.FullChecked:
                checkmark.sprite = full_Checkmark_Sprite;
                //background.enabled = false;
                checkmark.enabled = true;
                break;
            case TristateCheck.SemiChecked:
                checkmark.sprite = semi_Checkmark_Sprite;
                //background.enabled = false;
                checkmark.enabled = true;
                break;
            case TristateCheck.NoneChecked:
                //background.enabled = true;
                checkmark.enabled = false;
                break;
        }
        CheckType = tristateCheck;
    }

    public void AddListener(Action method)
    {
        checkbox_Btn.onClick.AddListener(method);
    }
}
