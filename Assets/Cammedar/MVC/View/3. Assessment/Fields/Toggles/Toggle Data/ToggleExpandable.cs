﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ToggleExpandable : ToggleBasic
{
    [SerializeField]
    Color disable_Color;

    [Header("Collapse/Expand")]
    [SerializeField]
    Sprite collapsed_Sprite;
    [SerializeField]
    Sprite expanded_Sprite;

    public bool Expanded { get; set; }

    public void CollapseToggle()
    {
        background.sprite = collapsed_Sprite;
        Expanded = false;
    }

    public void ExpandToggle()
    {
        background.sprite = expanded_Sprite;
        Expanded = true;
    }
}
