﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using Beebyte.Obfuscator;

public enum TogglesViewType
{
    [SkipRename]
    Horizontal,
    [SkipRename]
    Vertical,
    [SkipRename]
    Grid
}

public class ToggleAssess_Number : ToggleAssess
{
    [Header("Toggle Number")]
    public TextMeshProUGUI number;
    public Color origColor;
    public Color selectedColor;
    public static int Sumber;
    [Header("Children View")]
    public TogglesViewType childrenViewType;
    public static int SelectedItem;
    public static ArrayList ToothNumberDiagnsis = new ArrayList();
    public static ArrayList ToothNumberTreatment = new ArrayList();
    public int Number
    {
        get
        {
            int num = 0;
            if (int.TryParse(number.text, out num)) ;
            return num;
        }
    }

    public void Init(string _title, string _descriptionToTitle, string _number, bool _isOn = false)
    {
        base.Init(_title, _descriptionToTitle, _isOn, ChangeNumberColor);

        number.text = _number;

    }

    void ChangeNumberColor(bool _ToggleIsOn)
    {
        number.color = _ToggleIsOn ? selectedColor : origColor;
        try
        {
            string str = AssessPanels_Manager.Instance.CurrentGroup_toDisplay.name;
            if (str == "Choose Your Tooth Number")
            {
                SelectedItem = int.Parse(number.text);
                if (!ToothNumberDiagnsis.Contains(SelectedItem))
                    ToothNumberDiagnsis.Add(SelectedItem);
                else
                {
                    if(!_ToggleIsOn)
                    ToothNumberDiagnsis.Remove(SelectedItem);
                }
                Debug.Log("ToothNumberDiagnsis = " + ToothNumberDiagnsis.Count);
            }
            if (str == "Tooth Number")
            {
                SelectedItem = int.Parse(number.text);
                if (!ToothNumberTreatment.Contains(SelectedItem))
                    ToothNumberTreatment.Add(SelectedItem);
                else
                {
                    if (!_ToggleIsOn)
                        ToothNumberTreatment.Remove(SelectedItem);
                }
               
            }


        }
        catch (System.Exception e)
        {
            Debug.Log("Exception happen");
        }

    }
}