﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

using BoolAction = UnityEngine.Events.UnityAction<bool>;

public class ToggleBasic : MonoBehaviour
{
    [SerializeField]
    protected Toggle toggle;

    [Header("Background")]
    [SerializeField]
    protected Image background;
    [SerializeField]
    protected Sprite background_Sprite;
    [SerializeField]
    protected Color background_Color;

    [Header("Checkmark")]
    [SerializeField]
    protected Image checkmark;
    [SerializeField]
    Sprite checkmark_Sprite;
    [SerializeField]
    Color checkmark_Color;

    public RectTransform ToggleRect { get => toggle.transform.GetComponent<RectTransform>(); }
    public bool ToggleOn { get => toggle.isOn; set => toggle.isOn = value; }
    public Sprite Checkmark_Sprite { get => checkmark.sprite; set => checkmark.sprite = value; }
    public bool IsInteractable { get => toggle.IsInteractable(); private set => toggle.interactable = value; }
    public void AddListener(BoolAction method)
    {
        toggle.onValueChanged.AddListener(method);
    }

    public void Init()
    {
        background.sprite = background_Sprite;
        background.color = background_Color;
        if (checkmark != null)
        {
            checkmark.sprite = checkmark_Sprite;
            checkmark.color = checkmark_Color;
        }
    }

    /// <summary>
    /// Assigning to a toggle group
    /// </summary>
    /// <param name="toggleGroup">Toggle group to assing to</param>
    public void SetToggleGroup(ToggleGroup toggleGroup, bool mustHaveValue)
    {
        toggle.group = toggleGroup;
        toggle.group.allowSwitchOff = !mustHaveValue;
    }

    public void SwitchToggleSelection()
    {
        ToggleOn = !ToggleOn;
    }

    public void SetInteractability(bool interact)
    {
        IsInteractable = interact;
    }
}
