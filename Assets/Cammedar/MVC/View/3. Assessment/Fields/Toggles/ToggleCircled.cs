﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleCircled : ToggleBasic
{
    public Image border_Image;
    public Sprite border_Sprite;
    public Color border_Color;

    public Text label;

    public void Init(string _title, ToggleGroup _toggleGroup, bool defaultChoice)
    {
        base.Init();
        AddListener((val) => { ChangeBorderVisibility(val); });

        toggle.group = _toggleGroup;
        label.text = _title;
        border_Image.sprite = border_Sprite;
        border_Image.color = border_Color;

        if (defaultChoice) ToggleOn = true;
        ChangeBorderVisibility(ToggleOn);
    }

    public void ChangeBorderVisibility(bool _ToggleIsOn)
    {
        border_Image.enabled = _ToggleIsOn;
    }
}