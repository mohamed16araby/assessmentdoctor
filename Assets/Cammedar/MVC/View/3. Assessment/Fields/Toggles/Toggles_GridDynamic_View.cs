﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using System;

public class Toggles_GridDynamic_View : Toggles_View
{
    public override void SetView(Item item, Action AfterChildItemInstantiated = null)
    {
        base.SetView(item, SetTogglesAndTheirChildren);
        if (item.gridColumnsCount != 0)
        {
            gridLayoutGroup.constraintCount = item.gridColumnsCount;
        }
        if (item.gridXSpace != 0)
        {
            gridLayoutGroup.spacing = new Vector2(item.gridXSpace, gridLayoutGroup.spacing.y);
        }
    }

    /// <summary>
    /// Instantiate toggles and assign its selected values and do it recursivly for their children
    /// </summary>
    /// <param name="item"></param>
    /// <param name="childrenTransform"></param>
    void SetTogglesAndTheirChildren()
    {
            gridLayoutGroup = toggleTransform.GetComponent<GridLayoutGroup>();
            RectTransform currentRect = toggleAssess_instant.ToggleRect;
            gridLayoutGroup.cellSize = new Vector2(0, 0);
            StartCoroutine(SetGridWidth(currentRect));
    }

    GridLayoutGroup gridLayoutGroup;

    public IEnumerator SetGridWidth(RectTransform currentRect)
    {
        yield return new WaitForEndOfFrame();

        if (currentRect.sizeDelta.y > gridLayoutGroup.cellSize.y)
        {
            gridLayoutGroup.cellSize = new Vector2(gridLayoutGroup.cellSize.x, currentRect.sizeDelta.y);
        }
        if (currentRect.sizeDelta.x > gridLayoutGroup.cellSize.x)
        {
            gridLayoutGroup.cellSize = new Vector2(currentRect.sizeDelta.x, gridLayoutGroup.cellSize.y);
        }
    }
}