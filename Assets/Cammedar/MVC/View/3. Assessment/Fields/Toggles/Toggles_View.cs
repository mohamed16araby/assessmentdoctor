﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using System;

public class Toggles_View : MonoBehaviour
{
    [SerializeField]
    Text title;
    [SerializeField]
    Text descriptionToTitle;
    [SerializeField]
    Image image;
    //[SerializeField]
    //ToggleAssess_Number toggleAssess_Number;
    [SerializeField]
    ToggleAssess_Number toggleAssess_Number_Grid;
    [SerializeField]
    ToggleAssess_Number toggleAssess_Number_Vertical;
    [SerializeField]
    ToggleAssess_Number toggleAssess_Number_Horizontal;

    [SerializeField]
    ToggleAssess_Image toggleAssess_Image;
    [SerializeField]
    ToggleGroup toggleGroup;
    [SerializeField]
    protected Transform toggleTransform;

    //public ToggleAssess toggleDependingSection;

    public System.Action<int, Action<int, int>> modifyScores;
    public System.Action<int, int> updateGroupScoresResults;

    public delegate void ToggleSelectionEvent(bool _val, Item _str);
    public event ToggleSelectionEvent OnToggleSelectionChanged;

    public delegate void ToggleChoiceChanged_ApplyToSectionsEvent(string toggleName, bool toggleShouldBeOn, bool _val);
    public event ToggleChoiceChanged_ApplyToSectionsEvent ToggleChoiceChanged_ApplyToSections;

    public delegate Item UpdateItemEvent(Item _str);
    public event UpdateItemEvent GetItemFromSelectedValues;

    public delegate void AddToggleTrackableEvent(string toggleName, bool toggleShouldBeOn, ToggleAssess toggleAssess);
    public event AddToggleTrackableEvent AddToggleTrackable;

    public Dictionary<string, System.Action> togglePhrasesActions_Voice;
    public Dictionary<List<string>, List<ToggleAssess>> sectionsViewDependinOnChoice = new Dictionary<List<string>, List<ToggleAssess>>();
    //public Dictionary<List<TextStr>, List<ToggleAssess>> sectionsViewDependinOnChoice = new Dictionary<List<TextStr>, List<ToggleAssess>>();

    //public List<TextStr> trackToggleChoice = new List<TextStr>();
    public List<string> trackToggleChoice = new List<string>();
    public bool toggleShouldBeOn_SectionsViews = true;

    RectTransform title_rt;
    RectTransform description_rt;

    void Awake()
    {
        title_rt = title.gameObject.GetComponent<RectTransform>(); // Acessing the RectTransform 
        description_rt = descriptionToTitle.gameObject.GetComponent<RectTransform>(); // Acessing the RectTransform 
    }

    public virtual void SetView(Item item, Action AfterChildItemInstantiated = null)
    {
        title.text = item.title;
        title_rt.sizeDelta = new Vector2(title.preferredWidth, title.preferredHeight); // Setting the height to equal the height of text
        if (item.hideTitle == true)
            this.title.gameObject.SetActive(false);

        if (string.IsNullOrEmpty(item.descriptionToTitle)) descriptionToTitle.gameObject.SetActive(false);
        else
        {
            descriptionToTitle.gameObject.SetActive(true);
            descriptionToTitle.text = item.descriptionToTitle;
            description_rt.sizeDelta = new Vector2(descriptionToTitle.preferredWidth, descriptionToTitle.preferredHeight); // Setting the height to equal the height of text
        }
        if (string.IsNullOrEmpty(item.imageName)) image.transform.parent.gameObject.SetActive(false);
        else
        {
            image.transform.parent.gameObject.SetActive(true);
            image.sprite = LoadResources<Sprite>("Screenshots/" + item.imageName);
        }

        trackToggleChoice = item.trackToggleChoice;
        toggleShouldBeOn_SectionsViews = item.toggleShouldBeOn_SectionsViews;
        togglePhrasesActions_Voice = new Dictionary<string, Action>();

        SetTogglesAndTheirChildren(item, toggleTransform, toggleGroup, AfterChildItemInstantiated);
    }

    T LoadResources<T>(string _path) where T: UnityEngine.Object
    {
        return Resources.Load<T>(_path);
    }

    protected ToggleAssess toggleAssess_instant;

    /// <summary>
    /// Instantiate toggles and assign its selected values and do it recursivly for their children
    /// </summary>
    /// <param name="item"></param>
    /// <param name="childrenTransform"></param>
    void SetTogglesAndTheirChildren(Item item, Transform childrenTransform, ToggleGroup _toggleGroup, Action AfterChildItemInstantiated = null, bool isChild = false)
    {
        foreach (Item valueItem in item.values)
        {
            if (string.IsNullOrEmpty(valueItem.toggleImageName))
            {
                ToggleAssess_Number toggleAssess_Number = GetRightPrefabForChildrenView(valueItem.linkingTag);
                ToggleAssess_Number toggleAssess_instant_No = Instantiate(toggleAssess_Number, childrenTransform);
                toggleAssess_instant_No.Init(valueItem.title, valueItem.descriptionToTitle, valueItem.number.ToString());

                toggleAssess_instant = toggleAssess_instant_No;
            }
            else
            {
                ToggleAssess_Image toggleAssess_instant_Img = Instantiate(toggleAssess_Image, childrenTransform);
                toggleAssess_instant_Img.Init(valueItem.title, valueItem.descriptionToTitle, LoadResources<Sprite>("Sprites/History/" + valueItem.toggleImageName));

                toggleAssess_instant = toggleAssess_instant_Img;
            }

            AddToTogglePhrasesActions_Voice(valueItem.title, valueItem.number.ToString(), toggleAssess_instant.ToggleChange, isChild);

            if (item.singleSelection)
                toggleAssess_instant.SetToggleGroup(_toggleGroup, false);

            Transform child = toggleAssess_instant.childrenTransform;

            if (trackToggleChoice.Contains(toggleAssess_instant.label.text))
                AddToggleTrackable(toggleAssess_instant.label.text, toggleShouldBeOn_SectionsViews, toggleAssess_instant);

            CollectSectionsTheirViewsDependinOnChoice(item.viewDependingOn, toggleAssess_instant); //If this item has a string in viewDependingOn 

            toggleAssess_instant.AddListener(val =>
            {
                if (val == true)
                {
                    valueItem.selectedValues = GetSelectedValuesOfItem(item, valueItem);
                    SetTogglesAndTheirChildren(valueItem, child, toggleAssess_instant.ToggleGroup, AfterChildItemInstantiated, true);
                }
                else
                    RemoveChildrenGameObjects(valueItem.values, child);

                ModifyScores(valueItem.scoreWeight, val);

                //if (trackToggleChoice.Contains(valueItem.title))
                    ToggleChoiceChanged_ApplyToSections(valueItem.title, toggleShouldBeOn_SectionsViews, val); //This calls hideView functions to the sections their views depending on this toggle choice

                OnToggleSelectionChanged(val, valueItem); //Fire event to add/remove values from selected values
            });

            toggleAssess_instant.ToggleOn = (item[valueItem.title] != null) ? true : false;

            AfterChildItemInstantiated?.Invoke();
        }
    }

    public ToggleAssess_Number GetRightPrefabForChildrenView(string linkingTag)
    {
        if(Enum.TryParse(linkingTag, out TogglesViewType togglesViewType))
        {
            switch (togglesViewType)
            {
                case TogglesViewType.Horizontal:
                    return toggleAssess_Number_Horizontal;

                case TogglesViewType.Vertical:
                    return toggleAssess_Number_Vertical;
            }
        }

        return toggleAssess_Number_Grid;
    }

    private void CollectSectionsTheirViewsDependinOnChoice(List<string> toggleName, ToggleAssess toggleAssess)
    {
        if (toggleName == null || toggleName.Count == 0) return;

        List<string> toggleNames = MainPanel_Controller.DictionaryContainsList(sectionsViewDependinOnChoice, toggleName);
        if(toggleNames.Count > 0)
            sectionsViewDependinOnChoice[toggleNames].Add(toggleAssess);
        else
            sectionsViewDependinOnChoice.Add(toggleName, new List<ToggleAssess>() { toggleAssess });

    }

    public void PassScoreFunctions(System.Action<int, Action<int, int>> modifyScores, System.Action<int, int> updateGroupScoresResults)
    {
        this.modifyScores = modifyScores;
        this.updateGroupScoresResults = updateGroupScoresResults;
    }

    void ModifyScores(int _amount, bool val)
    {
        if (modifyScores == null) return;

        if (val) this.modifyScores(_amount, updateGroupScoresResults);
        else this.modifyScores(_amount * -1, updateGroupScoresResults);
    }

    void AddToTogglePhrasesActions_Voice(string phrase, string number, Action phraseAction, bool isChild)
    {
        AddPhraseToVoice(phrase, phraseAction, isChild);
        AddPhraseToVoice(number, phraseAction, isChild);
    }

    void AddPhraseToVoice(string str, Action action, bool isChild)
    {
        if (string.IsNullOrEmpty(str)) return;

        if (togglePhrasesActions_Voice.ContainsKey(str))
            togglePhrasesActions_Voice[str] = action;
        else
            togglePhrasesActions_Voice.Add(str, action);

        if (isChild)
            PanelVoiceController.Instance.UpdatePhraseAction(str, action);

    }

    /// <summary>
    /// Using itemParent to search for item then return the selected values of that item
    /// </summary>
    /// <param name="itemParent">Parent of item</param>
    /// <param name="item">The item from which selected values will be retrieved</param>
    /// <returns>List of selected values of item</returns>
    List<Item> GetSelectedValuesOfItem(Item itemParent, Item item)
    {
        itemParent = GetItemFromSelectedValues(itemParent); //Get itemParent from selectedValues search for it (nested search)
        item = itemParent[item.title];  //Get item from itemParent SelectedValues _search using its title
        if (item == null)
            return new List<Item>();
        return item.selectedValues; //return selected values of item
    }
    void RemoveChildrenGameObjects(List<Item> _values, Transform childrenTransform)
    {
        foreach (Transform child in childrenTransform)
            Destroy(child.gameObject);
    }
}
