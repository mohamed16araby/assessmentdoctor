﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class InputField_View : MonoBehaviour
{
    [SerializeField]
    Text title;

    [SerializeField]
    TMP_InputField input;

    public delegate void InputFieldChangedEvent(string _txt);
    public event InputFieldChangedEvent OnInputFieldChanged;

    public void SetView(string _title, bool hideTitle, List<string> value, string hintValue)
    //public void SetView(string _title, bool hideTitle, string value, string hintValue)
    {
        this.title.text = _title;
        if (hideTitle == true)
            this.title.gameObject.SetActive(false);

        input.placeholder.GetComponent<TextMeshProUGUI>().text = hintValue;
        if (value.Count > 0)
        //if (!string.IsNullOrEmpty(value))
        {
            input.text = value[0];
            input.textComponent.text = value[0];
            //input.text = value;
            //input.textComponent.text = value;
        }
        else
        {
            input.text = "";
            input.textComponent.text = "";
        }

        input.onValueChanged.AddListener(val => OnInputFieldChanged(val));
    }
}
