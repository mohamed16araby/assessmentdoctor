﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ImageField_View : MonoBehaviour
{
    [SerializeField]
    Text title;
    [SerializeField]
    Image image;

    public void SetView(string _title, bool hideTitle, string spriteName)
    {
        this.title.text = _title;
        if (hideTitle == true)
            this.title.gameObject.SetActive(false);

        image.sprite = Resources.Load<Sprite>("Screenshots/" + spriteName);
    }
}
