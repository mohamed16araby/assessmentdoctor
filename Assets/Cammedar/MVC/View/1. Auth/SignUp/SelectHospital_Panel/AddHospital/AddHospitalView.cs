﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Ricimi;

public class AddHospitalView : MonoBehaviour
{
    [SerializeField]
    Text headline;
    [SerializeField]
    InputField inputField;
    [SerializeField]
    BasicButton addBtn;
    [SerializeField]
    private Text errorMsgTxt;

    string identifierText;

    public string TextEntered { get => inputField.text; }

    UnityEngine.Events.UnityAction AddBtnFunctionality_IfPassed;

    public void Init(string title, string placeholder, string identifierText, UnityEngine.Events.UnityAction AddBtnFunctionality_IfPassed)
    {
        this.identifierText = identifierText;

        headline.text = title;
        inputField.placeholder.GetComponent<Text>().text = placeholder;
        this.AddBtnFunctionality_IfPassed = AddBtnFunctionality_IfPassed;
        addBtn.onClicked.AddListener(OnAddBtnClicked);

    }

    private void OnAddBtnClicked()
    {
        if(string.IsNullOrEmpty(TextEntered))
        {
            ViewError(string.Format("Please Enter {0} name!", identifierText));
        }
        else
        {
            errorMsgTxt.text = "";
            AddBtnFunctionality_IfPassed();
        }
    }

    public void ViewError(string text)
    {
        errorMsgTxt.text = text;
        errorMsgTxt.color = new Color32(212, 49, 49, 255);
    }

    public void ViewSuccessFeedback(string text)
    {
        errorMsgTxt.text = text;
        errorMsgTxt.color = new Color32(67, 144, 57, 255);
    }
}
