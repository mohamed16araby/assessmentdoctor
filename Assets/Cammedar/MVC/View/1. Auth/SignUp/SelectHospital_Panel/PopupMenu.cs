﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Beebyte.Obfuscator;
using Ricimi;

public class PopupMenu : MonoBehaviour
{
    [SerializeField]
    Text textHeader;
    [SerializeField]
    BasicButton addButton;
    [SerializeField]
    GameObject addHospital_prefab;
    [SerializeField]
    GameObject addSpecialization_prefab;

    public PopupOpener popupOpener;
    public List<string> currentList;

    int optionIndex;

    private void Start()
    {
        if (addButton != null)
            addButton.onClicked.AddListener(OnAddbuttonClicked);
    }
    [SkipRename]
    public void ChangePopupHeaderAccordingly(int optionIndex)
    {
        this.optionIndex = optionIndex;

        string text = "";
        switch (optionIndex)
        {
            case 0:
                text = "Choose Country";
                break;

            case 1:
                text = "Choose City";
                break;

            case 2:
                text = "Choose State";
                break;

            case 3:
                text = "Choose Hospital/Clinic";
                break;

            case 4:
                text = "Choose Hospital/Clinic Branch";
                break;

            case 5:
                text = "Choose Specialization";
                break;

            case 6:
                text = "Choose Sub-Specialization";
                break;
        }
        textHeader.text = text;
        AddButtonAppearence(optionIndex);
    }

    public void OnAddbuttonClicked()
    {
        string identifierText = "";

        if (optionIndex == 3 || optionIndex == 4)
        {
            identifierText = optionIndex == 3 ? "Hospital/Clinic" : "Hospital/Clinic branch";
            GameObject addHospitalViewer_GO = OpenPrompt(addHospital_prefab.gameObject);

            AddHospitalController addHospitalController = addHospitalViewer_GO.GetComponentInChildren<AddHospitalController>();
            addHospitalController.Init(optionIndex, identifierText, currentList, addHospitalViewer_GO.GetComponent<AddHospitalView>(), ClosePrompt);
        }
        else if(optionIndex == 5)
        {
            identifierText = "Speciality";
            GameObject addSpecializationViewer_GO = OpenPrompt(addSpecialization_prefab.gameObject);

            AddSpecializationController addSpecializationController = addSpecializationViewer_GO.GetComponentInChildren<AddSpecializationController>();
            addSpecializationController.Init(optionIndex, identifierText, currentList, addSpecializationViewer_GO.GetComponent<AddSpecializationView>(), ClosePrompt);

        }
    }

    private void AddButtonAppearence(int optionIndex)
    {
        if (addButton == null) return;

        addButton.gameObject.SetActive(optionIndex == 3 || optionIndex == 4 || optionIndex == 5);
    }

    public GameObject OpenPrompt(GameObject prefabView)
    {
        popupOpener.popupPrefab = prefabView;
        popupOpener.OpenPopup();

        return popupOpener.popupInstantiated;
    }

    public void ClosePrompt()
    {
        popupOpener.ClosePopup();
    }
}