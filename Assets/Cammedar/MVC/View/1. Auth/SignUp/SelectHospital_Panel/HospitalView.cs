﻿using UnityEngine;
using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;
using TMPro;
using Beebyte.Obfuscator;
using System.Collections.Generic;
using Cammedar.Network;
using URI = Cammedar.Network.URI;
using fb = Cammedar.Network.Firebase;

public class HospitalView : EnhancedScrollerCellView
{
    public TextMeshProUGUI ItemNameTxt;
    public Button mainBtn;

    SignnUp_2 signnUp_2;
    string id = "";

    private void Start()
    {
        signnUp_2 = GameObject.FindGameObjectWithTag("SignnUp_2").GetComponent<SignnUp_2>();
    }

    [SkipRename]
    public void SetData(string hospitalName)
    {
        ItemNameTxt.text = hospitalName; // update the UI text with the cell data
    }

    public void SetData(KeyValuePair<string, string> country)
    {
        ItemNameTxt.text = country.Value;
        id = country.Key;
    }

    private void ChangeSelectionColors(TextMeshProUGUI text, Image image, string currentSelectionText)
    {
        if (text.text == currentSelectionText)
        {
            text.color = Color.white;
            image.color = new Color32(3, 189, 91, 255);
        }
        else
        {
            text.color = Color.black;
            image.color = new Color32(255, 255, 255, 100);
        }
    }

    private void Update()
    {
        switch (ListOptions.Options)
        {
            case 0: // CountryList
                ChangeSelectionColors(ItemNameTxt, mainBtn.GetComponent<Image>(), UserData.country_name);
                break;

            case 1: // CityList
                ChangeSelectionColors(ItemNameTxt, mainBtn.GetComponent<Image>(), UserData.city_name);
                break;
            case 2: // StatesList
                ChangeSelectionColors(ItemNameTxt, mainBtn.GetComponent<Image>(), UserData.state_name);
                break;

            case 3: // HospitalsList
                ChangeSelectionColors(ItemNameTxt, mainBtn.GetComponent<Image>(), UserData.doctor_hospital_name);
                break;

            case 4: // Hospital Branches List
                ChangeSelectionColors(ItemNameTxt, mainBtn.GetComponent<Image>(), UserData.doctor_hospital_branch_name);
                break;

            default:
                break;
        }
    }

    void ResetCountryDependencies()
    {
        ResetState();
        ResetCity();
        ResetHospital();
        ResetHospitalBranch();
        ResetSpecialization();
        ResetSub_Specialization();
    }
    void ResetStateDependencies()
    {
        ResetCity();
        ResetHospital();
        ResetHospitalBranch();
        ResetSpecialization();
        ResetSub_Specialization();
    }
    void ResetCityDependencies()
    {
        ResetHospital();
        ResetHospitalBranch();
        ResetSpecialization();
        ResetSub_Specialization();
    }
    void ResetHospitalDependencies()
    {
        ResetHospitalBranch();
        ResetSpecialization();
        ResetSub_Specialization();
    }
    void ResetHospitalBranchDependencies()
    {
        ResetSpecialization();
        ResetSub_Specialization();
    }
    void ResetSpecializationDependencies()
    {
        ResetSub_Specialization();
    }

    [SkipRename]
    public void MainBtn_pressed()
    {
        switch (ListOptions.Options)
        {
            case 0: // CountryList
                if (ItemNameTxt.text != UserData.country_name)
                {
                    ResetCountryDependencies();
                }

                UserData.country_name = ItemNameTxt.text;
                signnUp_2.ChooseCountryBtn_txt.text = ItemNameTxt.text;

                UserData.country_id = id;
                CheckCountryState();
                break;

            case 1: // CityList
                if (ItemNameTxt.text != UserData.city_name)
                {
                    ResetCityDependencies();
                }

                UserData.city_name = ItemNameTxt.text;
                signnUp_2.ChooseCityBtn_txt.text = ItemNameTxt.text;
                signnUp_2.EnableButtons(3, true);

                UserData.city_id = id;
                break;
            case 2: // StatesList
                if (UserData.state_name != ItemNameTxt.text)
                {
                    ResetStateDependencies();
                }

                UserData.state_name = ItemNameTxt.text;
                signnUp_2.ChooseStateBtn_txt.text = ItemNameTxt.text;
                signnUp_2.EnableButtons(1, true);

                UserData.state_id = id;

                break;
            case 3: // HospitalsList
                if (UserData.doctor_hospital_name != ItemNameTxt.text)
                {
                    ResetHospitalDependencies();
                }

                UserData.doctor_hospital_name = ItemNameTxt.text;
                signnUp_2.ChooseHospitalsBtn_txt.text = ItemNameTxt.text;
                signnUp_2.EnableButtons(4, true);

                UserData.doctor_hospital_id = id;
                break;
            case 4: // Hospital Branches List
                if (UserData.doctor_hospital_branch_name != ItemNameTxt.text)
                {
                    ResetHospitalBranchDependencies();
                }

                UserData.doctor_hospital_branch_name = ItemNameTxt.text;
                signnUp_2.ChooseHospitalBranchBtn_txt.text = ItemNameTxt.text;
                signnUp_2.EnableButtons(5, true);

                UserData.doctor_hospital_branch_id = id;
                break;
            default:
                break;
        }
    }

    void ResetCity()
    {
        UserData.city_name = "";
        UserData.city_id = "";
        signnUp_2.ChooseCityBtn_txt.text = "Choose City";
        UserData.city_id = "";
        signnUp_2.EnableButtons(1, false);
    }

    void ResetState()
    {
        UserData.state_name = "";
        UserData.state_id = "";
        signnUp_2.ChooseStateBtn_txt.text = "Choose State";
        UserData.state_id = "";
        signnUp_2.EnableButtons(2, false);
    }

    void ResetHospital()
    {
        UserData.doctor_hospital_name = "";
        UserData.doctor_hospital_id = "";
        signnUp_2.ChooseHospitalsBtn_txt.text = "Choose Hospital, Clinic";
        signnUp_2.EnableButtons(3, false);
    }

    void ResetHospitalBranch()
    {
        UserData.doctor_hospital_branch_name = "";
        UserData.doctor_hospital_branch_id = "";
        signnUp_2.ChooseHospitalBranchBtn_txt.text = "Choose Hospital, Clinic branch";
        signnUp_2.EnableButtons(4, false);
    }

    void ResetSpecialization()
    {
        signnUp_2.ResetSpecialization();
    }

    void ResetSub_Specialization()
    {
        signnUp_2.ResetSub_Specialization();
    }

    // check if the country has a states like USA OR cities only like EGYPT
    private void CheckCountryState()
    {
        URI country_URI = fb.Instance.databasePath.Child("states").Child(UserData.country_id).JSON().AuthQuery(UserData.tokenID);

        System.Action<StatesParent> onSuccess = (statesParent) =>
        {
            UserData.states = new Dictionary<string, string>();

            UserData.has_state = statesParent.has_state != "0";
            if (UserData.has_state) //has states
            {
                foreach (KeyValuePair<string, string> listItem in statesParent.list)
                    UserData.states.Add(listItem.Key, listItem.Value);

                signnUp_2.EnableButtons(2, true);
            }
            else
                signnUp_2.EnableButtons(1, true);
        };

        fb.Instance.Get(country_URI, onSuccess, delegate { });
    }
}
