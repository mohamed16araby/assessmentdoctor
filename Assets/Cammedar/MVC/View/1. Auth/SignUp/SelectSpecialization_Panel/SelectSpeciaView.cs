﻿using UnityEngine;
using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using Beebyte.Obfuscator;

public class SelectSpeciaView : EnhancedScrollerCellView
{
    // Start is called before the first frame update
    public TextMeshProUGUI SpecializatioNameTxt;
    public Button mainBtn;
    SignnUp_2 signnUp_2;

    private void Start()
    {
        signnUp_2 = GameObject.FindGameObjectWithTag("SignnUp_2").GetComponent<SignnUp_2>();
    }

    private void Update()
    {
        if (SpecializatioNameTxt.text == UserData.doctor_Specialization || SpecializatioNameTxt.text == UserData.doctor_SubSpecialization)
        {
            SpecializatioNameTxt.color = Color.white;
            mainBtn.GetComponent<Image>().color = new Color32(3, 189, 91, 255);
        }
        else
        {
            SpecializatioNameTxt.color = Color.black;
            mainBtn.GetComponent<Image>().color = new Color32(255, 255, 255, 100);
        }
    }

    public void SetData(string specialization)
    {
        // update the UI text with the cell data
        SpecializatioNameTxt.text = specialization;
    }

    [SkipRename]
    public void MainBtn_pressed()
    {

        if (Options.SpecializationsList)
        {
            signnUp_2.ChooseSpecializationsBtn_txt.text = SpecializatioNameTxt.text;
            UserData.doctor_Specialization = SpecializatioNameTxt.text;

            signnUp_2.ChooseSubSpecializationsBtn_txt.text = "Choose Sub-Specialization";
            //UserData.doctor_SubSpecialization = SpecializatioNameTxt.text;

            signnUp_2.EnableButtons(6, true);

        }
        else
        {
            
            UserData.doctor_SubSpecialization = SpecializatioNameTxt.text;
            
            if (Options.Registration)
            {
                signnUp_2.ChooseSubSpecializationsBtn_txt.text = SpecializatioNameTxt.text;
                //UserData.Doctor_SubSpecialization = SpecializatioNameTxt.text;
            }else {
                if (!Options.ChangeSubSpecialityAtMenu){
                    GameObject.FindGameObjectsWithTag("ManagerSceneCanvas")[0].SetActive(false);
                    //UserData.Doctor_SubSpecialization = SpecializatioNameTxt.text;
                    Options.ChangeSubSpecialityAtMenu = true;
                    //SceneManager.LoadScene("Animations", LoadSceneMode.Single);
                    SceneManager.LoadScene("Animations");
                }
            }

        }

    }

  

}




