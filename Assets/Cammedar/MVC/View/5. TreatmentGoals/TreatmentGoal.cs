﻿using UnityEngine;
using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;
using TMPro;
using Beebyte.Obfuscator;
using System.Collections.Generic;

public class TreatmentGoal : EnhancedScrollerCellView
{
    public TextMeshProUGUI ItemNameTxt;
    public Button mainBtn;
    bool isShortGoal;
    private int number;

    // update the UI text with the cell data
    public void SetData(string TreatmentGoal, int numberItem, bool isShortGoal, System.Action<string, System.Action> AddToVoiceDictionary)
    {
        number = numberItem + 1;
        ItemNameTxt.text = number.ToString() + ". " + TreatmentGoal;
        this.isShortGoal = isShortGoal;

        AddToVoiceDictionary(TreatmentGoal, MainBtn_pressed);
        AddToVoiceDictionary(number.ToString(), MainBtn_pressed);
    }


    private void Update()
    {

        if ((TreatmentGoals.selected_ShortTerm_goals_Orig.Contains(ItemNameTxt.text) && isShortGoal)||
            (TreatmentGoals.selected_LongTerm_goals_Orig.Contains(ItemNameTxt.text) && !isShortGoal))
        {
            ItemNameTxt.color = Color.white;
            mainBtn.GetComponent<Image>().color = new Color32(3, 189, 91, 255);
        }
        else
        {
            ItemNameTxt.color = Color.black;
            mainBtn.GetComponent<Image>().color = new Color32(255, 255, 255, 100);
        }

    }

    [SkipRename]
    public void MainBtn_pressed()
    {
        List<string> selectedGoals = isShortGoal ? TreatmentGoals.selected_ShortTerm_goals_Orig : TreatmentGoals.selected_LongTerm_goals_Orig;

        if (selectedGoals.Contains(ItemNameTxt.text))
            selectedGoals.Remove(ItemNameTxt.text);
        else
            selectedGoals.Add(ItemNameTxt.text);
    }
}
