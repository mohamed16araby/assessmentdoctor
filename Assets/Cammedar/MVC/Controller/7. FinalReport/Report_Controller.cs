﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Cammedar;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.SceneManagement;

public class Report_Controller : MonoBehaviour
{
    public Report_Model report_Model;

    [HideInInspector]
    public string reportPath = "";
    string reportDirectory = "";

    [HideInInspector]
    public string ReportParentFolder = "";

    string currentSceneName = "PDFWriter";
    string nextSceneName = "PDFViewer_Asset";

    Document PatientReport = new Document(PageSize.A4, 20f, 20f, 20f, 20f);
    PdfWriter pdfWriter;
    byte[][] _logosBytes = new byte[][] { new byte[] { }, new byte[] { } };

    private void Start()
    {
        report_Model = new Report_Model();
        report_Model.ViewReport += CreateReport;
        report_Model.GetPatientData(this);
    }
    public void CreateReport()
    {
        string report_name = PatientData.patient_name + "_" + PatientData.patient_phone + ".pdf";
        reportDirectory = System.IO.Path.Combine(new string[] { Application.persistentDataPath, "PDF Reports", UserData.doctor_hospital_name });
        reportPath = System.IO.Path.Combine(new string[] { reportDirectory, report_name });

        LocalFileSystem.PrepareDirectoryForNewFile(reportDirectory, report_name);

        FileStream fs = new FileStream(reportPath, FileMode.Create, FileAccess.Write);
        pdfWriter = PdfWriter.GetInstance(PatientReport, fs);

        string[] logos_FileNames = new string[] { "Logo-Icon.png", "Logo-Text.png" };
        _logosBytes = new byte[][] { new byte[] { }, new byte[] { } };
        StartCoroutine(GetTexture(logos_FileNames[0], 0));
        StartCoroutine(GetTexture(logos_FileNames[1], 1));
    }

    private IEnumerator GetTexture(string _imageName, int _index)
    {
        // in general I would always avoid to have spaces in file-paths
        var path = Path.Combine(Application.streamingAssetsPath, "Logo", _imageName);

        using (var www = UnityEngine.Networking.UnityWebRequestTexture.GetTexture(path))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.LogErrorFormat(this, "Unable to load texture due to {0} - {1}", www.responseCode, www.error);
            }
            else
            {
                _logosBytes[_index] = ((UnityEngine.Networking.DownloadHandlerTexture)www.downloadHandler).texture.EncodeToPNG();/*.GetRawTextureData()*/;
            }

            if (_logosBytes[0].Length != 0 && _logosBytes[1].Length != 0)
            {
                PdfReportCreator.StyleReport(UserData.user_UDID, report_Model.patientData_Retrieved, report_Model.historyDatas, report_Model.bodyPartsComplaints,
                                                    report_Model.specialityPath_SessionData, _logosBytes,
                                                    ref pdfWriter, ref PatientReport);

                StartCoroutine(ScenesController.Instance.LoadYourAsyncScene(currentSceneName, nextSceneName));
            }
        }
    }
}