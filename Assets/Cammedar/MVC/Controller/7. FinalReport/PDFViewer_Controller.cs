﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Paroxe.PdfRenderer;
using UnityEngine.SceneManagement;
using Facebook.Unity;
public class PDFViewer_Controller : MonoBehaviour
{
    [SerializeField]
    PDFViewer m_PDFViewer;
    [SerializeField]
    Button backButton;
    [SerializeField]
    Button selectAnotherPatientButton;
    [SerializeField]
    Button ContinueButton;
    [SerializeField]
    Button helpbutton;
    string backSceneName = "Animations";
    string currentSceneName = "PDFViewer_Asset";
    string reportPath;
    public GRP_MultichoiceManager gRP_Multichoice;
    public GameObject HelpPopup;
    public Sprite Audioon;
    public Sprite AudioOff;
    [SerializeField]
    Button AudioButton;
    GameObject AudioController;
    void Awake()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            FB.ActivateApp();
        }
    }
    private void InitCallback()
    {
        if (FB.IsInitialized)
        {

            FB.ActivateApp();
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }
    private void Start()
    {
        FB.LogAppEvent(AppEventName.ViewedContent, null, new Dictionary<string, object>()
{{ AppEventParameterName.Description, "open Pdf Screen" }});
        string report_name = PatientData.patient_name + "_" + PatientData.patient_phone + ".pdf";
        string reportDirectory = System.IO.Path.Combine(new string[] { Application.persistentDataPath, "PDF Reports", UserData.doctor_hospital_name });

     //   string reportDirectory = System.IO.Path.Combine(new string[] { Application.streamingAssetsPath, "PDF Reports", UserData.doctor_hospital_name });
        reportPath = System.IO.Path.Combine(new string[] { reportDirectory, report_name });

        SetPathOfCurrentReport();
        backButton.onClick.AddListener(GoBack);
        selectAnotherPatientButton.onClick.AddListener(EnterSelectPatientScene);
        ContinueButton.onClick.AddListener(EnterAssessmentSceneScene);
        helpbutton.onClick.AddListener(helpButtonfunction);
        AudioButton.onClick.AddListener(FVoiceController);
        GameVoiceControl.Instance.reinitializeVoice();
        GameVoiceControl.Instance.RecognitionResult.AddListener(onReceiveRecognitionResult);

    }
    public string ToUpperEveryWord(string s)
    {
        var words = s.Split(' ');

        var t = "";
        foreach (var word in words)
        {
            t += char.ToUpper(word[0]) + word.Substring(1) + ' ';
        }
        return (t.Trim());
    }
    public void onReceiveRecognitionResult(string result)
    {
        result = ToUpperEveryWord(result);
        Debug.Log(result);
        if (result == "Previous") GoBack();
        else if (result == "Back") GoBack();
        else if (result == "Next") EnterAssessmentSceneScene();
        else if (result == "Forward") EnterAssessmentSceneScene();
        else if (result == "Previous Group") GoBack();
        else if (result == "Select Another Patient") GoBack();
        else if (result == "Continue") EnterAssessmentSceneScene();
        else if (result == "Help") helpButtonfunction();
        else if (result == "Close") helpPromptClose();
    }

    public void FVoiceController()
    {
        if (AudioButton.GetComponent<Image>().sprite == Audioon)
        {
            GameVoiceControl.Instance.onStopListening();
            AudioButton.GetComponent<Image>().sprite = AudioOff;
        }
        else
        {
            GameVoiceControl.Instance.onStartListening();
            AudioButton.GetComponent<Image>().sprite = Audioon;
        }
    }

    public void helpButtonfunction()
    {
        HelpPopup.SetActive(true);
    }
    public void helpPromptClose()
    {
        //todo
        HelpPopup.SetActive(false);
    }
    public void GoBack()
    {
        SceneManager.LoadScene("SelectPatientScene");

    }

    public void EnterSelectPatientScene()
    {
        //SceneManager.LoadScene("SelectPatientScene", LoadSceneMode.Single);
        SceneManager.LoadScene("SelectPatientScene");
    }
    public void EnterAssessmentSceneScene()
    {
        //SceneManager.LoadScene("SelectPatientScene", LoadSceneMode.Single);
        SceneManager.LoadScene("AssessmentScene");
    }

    public void SetPathOfCurrentReport()
    {
        m_PDFViewer.FileSource = PDFViewer.FileSourceType.FilePath;

        m_PDFViewer.FilePath = reportPath;
    }

    #region Voice
    void SetVoiceControl()
    {
        Dictionary<string, System.Action> phraseAction = new Dictionary<string, System.Action>()
        {
            {"Back", GoBack},
            {"Select another patient", GoBack},
            {"Next", EnterAssessmentSceneScene},
            {"Continue", EnterAssessmentSceneScene},
            {"Back Group", GoBack},
            {"help", helpButtonfunction},
            {"close", helpPromptClose},
        };

        PanelVoiceController.Instance.AddSetOfPhrases(phraseAction);
    }
    #endregion
}