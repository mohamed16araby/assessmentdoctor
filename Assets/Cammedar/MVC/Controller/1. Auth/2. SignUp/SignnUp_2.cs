﻿using UnityEngine;
using UnityEngine.UI;
using Beebyte.Obfuscator;
using Ricimi;
using System.Collections.Generic;
using Cammedar.Network;
using Facebook.Unity;
public class SignnUp_2 : MonoBehaviour
{
    public InputField FirstName_inputField;
    public Image FirstName_background_img;

    public InputField LastName_inputField;
    public Image LastName_background_img;

    public InputField PhoneNumber_inputField;
    public Image PhoneNumber_background_img;

    public Text ErrorTxt;

    public BasicButton ChooseCountryBtn;
    public Text ChooseCountryBtn_txt;
    public Image ChooseCountry_background_img;

    public BasicButton ChooseCityBtn;
    public Text ChooseCityBtn_txt;
    public Image ChooseCity_background_img;

    public BasicButton ChooseStateBtn;
    public Text ChooseStateBtn_txt;
    public Image ChooseState_background_img;

    public BasicButton ChooseHospitalsBtn;
    public Text ChooseHospitalsBtn_txt;
    public Image ChooseHospitals_background_img;

    public BasicButton ChooseHospitalBranchBtn;
    public Text ChooseHospitalBranchBtn_txt;
    public Image ChooseHospitalBranch_background_img;

    public BasicButton ChooseSpecializationsBtn;
    public Text ChooseSpecializationsBtn_txt;
    public Image ChooseSpecializations_background_img;

    public BasicButton ChooseSubSpecializationsBtn;
    public Text ChooseSubSpecializationsBtn_txt;
    public Image ChooseSubSpecializations_background_img;


    public GameObject ChooseStatePanel;
    public BasicButton closeBtn;
    public GameEvent viewSignIn;

    public GameEvent CountryBtn_pressed;
    public GameEvent CityBtn_pressed;
    public GameEvent StateBtn_pressed;
    public GameEvent HospitalsBtn_pressed;
    public GameEvent HospitalBranchesBtn_pressed;
    public GameEvent SpecializationsBtn_pressedEvent;
    public GameEvent SubSpecializationsBtn_pressedEvent;

    public static Cammedar.Network.Firebase Firebase = Cammedar.Network.Firebase.Instance;

    CanvasGroup GetCanvasGroup(BasicButton basicButton)
    {
        return basicButton.GetComponent<CanvasGroup>();
    }
    void Awake()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            FB.ActivateApp();
        }
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }
    public void EnableButtons(int index, bool enable)
    {
        CanvasGroup canvasGroup = null;
        switch (index)
        {
            case 0:
                ChooseCountryBtn.enabled = enable;
                canvasGroup = GetCanvasGroup(ChooseCountryBtn);
                break;

            case 1:
                ChooseCityBtn.enabled = enable;
                canvasGroup = GetCanvasGroup(ChooseCityBtn);
                break;

            case 2:
                ChooseStateBtn.enabled = enable;
                canvasGroup = GetCanvasGroup(ChooseStateBtn);
                break;

            case 3:
                ChooseHospitalsBtn.enabled = enable;
                canvasGroup = GetCanvasGroup(ChooseHospitalsBtn);
                break;

            case 4:
                ChooseHospitalBranchBtn.enabled = enable;
                canvasGroup = GetCanvasGroup(ChooseHospitalBranchBtn);
                break;

            case 5:
                ChooseSpecializationsBtn.enabled = enable;
                canvasGroup = GetCanvasGroup(ChooseSpecializationsBtn);
                break;

            case 6:
                ChooseSubSpecializationsBtn.enabled = enable;
                canvasGroup = GetCanvasGroup(ChooseSubSpecializationsBtn);
                break;
        }
        if (canvasGroup)
            canvasGroup.alpha = enable ? 1 : 0.54f;

    }

    void Start()
    {
        Options.Registration = true;
        closeBtn.onClicked.AddListener(delegate { viewSignIn.Raise(); });

        EnableButtons(1, false);
        EnableButtons(2, false);
        EnableButtons(3, false);
        EnableButtons(4, false);
        EnableButtons(5, false);
        EnableButtons(6, false);
    }

    bool UpdateErrorBackground(Dictionary<Image, string> txt_BackgroundError)
    {
        bool foundMissingItem = false;
        foreach (KeyValuePair<Image, string> keyValuePair in txt_BackgroundError)
        {
            keyValuePair.Key.enabled = string.IsNullOrEmpty(keyValuePair.Value);
            if(!foundMissingItem) foundMissingItem = string.IsNullOrEmpty(keyValuePair.Value);
        }

        return foundMissingItem;
    }
    [SkipRename]
    public void SignUpBtn_pressed()
    {
        Dictionary<Image, string> choicesItems_backgroundError = new Dictionary<Image, string>()
        {
            {FirstName_background_img, FirstName_inputField.text},
            {LastName_background_img, LastName_inputField.text },
            {PhoneNumber_background_img, PhoneNumber_inputField.text },
            {ChooseHospitals_background_img, UserData.doctor_hospital_name},
            {ChooseHospitalBranch_background_img, UserData.doctor_hospital_branch_id },
            {ChooseSpecializations_background_img, UserData.doctor_Specialization },
            //{ChooseSubSpecializations_background_img, UserData.doctor_SubSpecialization },
            {ChooseCity_background_img, UserData.city_name },
            {ChooseCountry_background_img, UserData.country_name },

        };

        choicesItems_backgroundError.Add(ChooseState_background_img, UserData.has_state? UserData.state_name: "N/A");

        if (UpdateErrorBackground(choicesItems_backgroundError))
        {
            ErrorTxt.color = new Color32(209, 1, 85, 255);
            ErrorTxt.text = "Please Fill all required data";
        }
        else
        {
            ErrorTxt.text = "";

            AddNewDoctor(
                new Doctor()
                {
                    authorized = true,
                    email = UserData.user_email,
                    first_name = FirstName_inputField.text,
                    last_name = LastName_inputField.text,
                    hospital_name = UserData.doctor_hospital_name,
                    hospital_id = UserData.doctor_hospital_id,
                    hospital_branch_name = UserData.doctor_hospital_branch_name,
                    hospital_branch_id = UserData.doctor_hospital_branch_id,
                    phone_number = PhoneNumber_inputField.text,
                    speciality = UserData.doctor_Specialization,
                    sub_speciality = UserData.doctor_SubSpecialization,
                    country_id = UserData.country_id,
                    city_id = UserData.city_id,
                }
             );
        }

    }

    private void AddNewDoctor(Doctor doctor)
    {
        FB.LogAppEvent(AppEventName.CompletedRegistration, null, new Dictionary<string, object>()
{{ AppEventParameterName.Description, "Add new Doctor" }});
        URI uri = Firebase.databasePath.Child("doctors").Child(UserData.user_UDID).Child("doctor_Data").JSON().AuthQuery(UserData.tokenID);

        System.Action<string> onSuccess = (mssg) =>
        {
            AddDoctorToHospitalBranch(UserData.user_UDID, FirstName_inputField.text + " " + LastName_inputField.text,
                        UserData.doctor_hospital_branch_id, UserData.doctor_hospital_id);
        };

        System.Action<string> onFail = (mssg) =>
        {
            ErrorTxt.text = "Failed to add user!";
        };

        doctor.Encrypt(UserData.user_UDID);
        Firebase.Put(uri, doctor, onSuccess, onFail);
    }

    void AddDoctorToHospitalBranch(string userId, string fullName, string hospitalBranch_id_Unencrypt, string hospital_id_Unencrypt)
    {
        FB.LogAppEvent(AppEventName.CompletedRegistration, null, new Dictionary<string, object>()
{{ AppEventParameterName.Description, "Add Doctor To Hospital Branch" }});
        URI uri = Firebase.databasePath.Child("hospitals").Child(hospital_id_Unencrypt).Child("branches")
            .Child(hospitalBranch_id_Unencrypt).Child("doctors").Child(userId).JSON().AuthQuery(UserData.tokenID);

        System.Action<string> onSuccess = (mssg) =>
        {
            viewSignIn.Raise();
        };

        System.Action<string> onFail = (mssg) =>
        {
            ErrorTxt.text = "Failed to add doctor to hospital!";
        };

        Firebase.Put(uri, fullName, onSuccess, onFail);
    }

    [SkipRename]
    public void SpecializatiosBtn_pressed()
    {
        Options.SpecializationsList = true;
        SpecializationsBtn_pressedEvent.Raise();
    }

    [SkipRename]
    public void SubSpecializatiosBtn_pressed()
    {
        Options.SpecializationsList = false;
        SubSpecializationsBtn_pressedEvent.Raise();
    }

    [SkipRename]
    public void PopupViewed(int optionIndex)
    {
        ListOptions.Options = optionIndex;

        switch (optionIndex)
        {
            case 0:
                CountryBtn_pressed.Raise();
                break;

            case 1:
                CityBtn_pressed.Raise();
                break;

            case 2:
                StateBtn_pressed.Raise();
                break;

            case 3:
                HospitalsBtn_pressed.Raise();
                break;

            case 4:
                HospitalBranchesBtn_pressed.Raise();
                break;
        }
    }

    public void ResetSpecialization()
    {
        UserData.doctor_Specialization = "";
        ChooseSpecializationsBtn_txt.text = "Choose Specialization";
        EnableButtons(5, false);
    }

    public void ResetSub_Specialization()
    {
        UserData.doctor_SubSpecialization = "";
        ChooseSubSpecializationsBtn_txt.text = "Choose Sub-Specialization";
        EnableButtons(6, false);
    }
}