﻿using System.Collections.Generic;
using UnityEngine;
using fb = Cammedar.Network.Firebase;
using URI = Cammedar.Network.URI;
using Newtonsoft.Json;
using System.Linq;
using Cammedar.Network;
using Newtonsoft.Json.Linq;

public class AddSpecializationController : MonoBehaviour
{
    [HideInInspector]
    public AddSpecializationView addSpecializationView;
    [SerializeField]
    GameEvent RefreshSpecialities;

    List<string> currentList;
    int optionIndex;
    string identifierText;

    System.Action ClosingPopupAction;

    public void Init(int optionIndex, string identifierText, List<string> currentList, AddSpecializationView addSpecializationView, System.Action ClosingPopupAction)
    {
        this.optionIndex = optionIndex;
        this.currentList = currentList;
        this.identifierText = identifierText;
        this.ClosingPopupAction = ClosingPopupAction;
        this.addSpecializationView = addSpecializationView;

        RetrieveAllSpecialities();
    }

    private void InitializeView()
    {
        string title = "Add " + identifierText;
        if (optionIndex == 5)
            title += " to " + UserData.doctor_hospital_branch_name;

        string placeholder = identifierText + " name";
        addSpecializationView.Init(title, placeholder, identifierText, allSpecializations.Keys.ToList(), AddSpecialization);
    }

    Dictionary<string, Dictionary<string, List<string>>> allSpecializations = new Dictionary<string, Dictionary<string, List<string>>>();
    private void RetrieveAllSpecialities()
    {
        URI uri = fb.Instance.databasePath.Child("supportedSpecialities").JSON().AuthQuery(UserData.tokenID);

        System.Action<Dictionary<string, JObject>> onSuccess = (specialities) =>
        {
            Debug.Log("specialities.count: "+ specialities.Count);
            allSpecializations = new Dictionary<string, Dictionary<string, List<string>>>();
            foreach (KeyValuePair<string, JObject> keyValue in specialities)
            {
                allSpecializations.Add(keyValue.Key, new Dictionary<string, List<string>>());
                if (keyValue.Value["has_subspecialities"].ToString() == "true")
                {
                    Debug.Log("1st if");

                    Dictionary<string, JObject> subspecialities = JsonConvert.DeserializeObject<Dictionary<string, JObject>>(keyValue.Value["subSpecialities"].ToString());
                    foreach(KeyValuePair<string, JObject> keyValueChild in subspecialities)
                    {
                        Debug.Log("2nd foreach");

                        allSpecializations[keyValue.Key].Add(keyValueChild.Key, new List<string>());
                        if(keyValueChild.Value["has_subspecialities"].ToString() == "true")
                        {
                            Debug.Log("2nd if");

                            Dictionary<string, string> subsubspecialities = JsonConvert.DeserializeObject<Dictionary<string, string>>(keyValueChild.Value["subsubSpecialities"].ToString());
                            allSpecializations[keyValue.Key][keyValueChild.Key] = subsubspecialities.Keys.ToList();
                        }
                    }
                }
            }
            InitializeView();
        };

        fb.Instance.Get(uri, onSuccess,
                         delegate { addSpecializationView.ViewError("Something Wrong! .. Please try again later"); });
    }
    
    public void AddSpecialization()
    {
        if (Specialization_AlreadyExists())
            addSpecializationView.ViewError(string.Format("The {0} name is already exist!", identifierText));
        else
            AddSpecializationToBranch_ToDatabase(addSpecializationView.TextEntered);
    }

    private bool Specialization_AlreadyExists()
    {
        string itemNameFound = currentList.Find(i => i.ToLower() == addSpecializationView.TextEntered);
        return !string.IsNullOrEmpty(itemNameFound);
    }

    private void AddSpecializationToBranch_ToDatabase(string name)
    {
        if (optionIndex == 5) //adding speciality
        {
            if (allSpecializations[addSpecializationView.TextEntered].Count > 0)
            {
                URI uri = fb.Instance.databasePath.Child("hospitals").Child(UserData.doctor_hospital_id).Child("branches")
                    .Child(UserData.doctor_hospital_branch_id).Child("specializations").Child(addSpecializationView.TextEntered).JSON().AuthQuery(UserData.tokenID);

                foreach (KeyValuePair<string, List<string>> keyValue in allSpecializations[addSpecializationView.TextEntered])
                {
                    SubspecialityContainer subspecialityContainer =
                    new SubspecialityContainer()
                    {
                        has_subspecialities = (keyValue.Value.Count > 0),
                        subsubSpecialities = new Dictionary<string, bool>()
                    };

                    foreach (string subsub in keyValue.Value)
                        subspecialityContainer.subsubSpecialities.Add(subsub, true);

                    fb.Instance.Put(uri, new Dictionary<string, SubspecialityContainer>() { { keyValue.Key, subspecialityContainer } }, 
                        AfterAddingSpecialityToBranch, delegate { addSpecializationView.ViewError("Something Wrong! .. Please try again later"); });
                }
            }
            else
            {
                addSpecializationView.ViewError(string.Format("{0} speciality doesn't have subspecialities", addSpecializationView.TextEntered));
            }
        }

        void AfterAddingSpecialityToBranch(string str)
        {
            addSpecializationView.ViewSuccessFeedback("Speciality has been added successfully!");
            ClosingPopupAction();
            RefreshSpecialities.Raise();
        }
    }
}