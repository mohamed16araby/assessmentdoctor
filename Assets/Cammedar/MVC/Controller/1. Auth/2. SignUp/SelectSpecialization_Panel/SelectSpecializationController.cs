﻿using System.Collections.Generic;
using UnityEngine;
using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;
using Beebyte.Obfuscator;
using fb = Cammedar.Network.Firebase;
using URI = Cammedar.Network.URI;
using Cammedar.Network;
using System.Linq;

public class SelectSpecializationController : MonoBehaviour, IEnhancedScrollerDelegate
{
    private List<string> Specializations = new List<string>();
    private List<string> Searched_specializations = new List<string>();

    public EnhancedScroller SpecializationScroller;
    public EnhancedScrollerCellView SpecializationViewPrefab;

    public InputField SearchField;
    bool SearchActive = false;

    void Start()
    {
        SpecializationScroller.Delegate = this;
        SearchField.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
        Invoke("Getdata", 0);
    }

    void Getdata()
    {
        if (Options.SpecializationsList)
            GetSpecializations();

        else if (UserData.doctor_Specialization != "")
            GetSubSpecializations();
    }

    [SkipRename]
    public void GetSpecializations()
    {
        URI uri = fb.Instance.databasePath.Child("hospitals").Child(UserData.doctor_hospital_id).Child("branches")
            .Child(UserData.doctor_hospital_branch_id).Child("specializations").JSON().AuthQuery(UserData.tokenID);

        System.Action<Dictionary<string, Dictionary<string, SubspecialityContainer>>> onSuccess = (specializations) =>
        {
            Specializations = (specializations == null) ? new List<string>() : specializations.Keys.ToList();
            SpecializationScroller.ReloadData();
        };

        fb.Instance.Get(uri, onSuccess, delegate { });
    }

    private void GetSubSpecializations()
    {
        URI uri = fb.Instance.databasePath.Child("hospitals").Child(UserData.doctor_hospital_id).Child("branches")
            .Child(UserData.doctor_hospital_branch_id).Child("specializations").Child(UserData.doctor_Specialization).JSON().AuthQuery(UserData.tokenID);

        System.Action<Dictionary<string, SubspecialityContainer>> onSuccess = (subSpecializations) =>
        {
            Specializations = subSpecializations == null ? new List<string>() : subSpecializations.Keys.ToList();
            SpecializationScroller.ReloadData();
        };

        fb.Instance.Get(uri, onSuccess, delegate { });
    }

    [SkipRename]
    public void ValueChangeCheck()
    {
        if (SearchField.text == "")
        {
            SearchActive = false;
            SpecializationScroller.ReloadData();
        }
        else
        {
            Searched_specializations.Clear();
            foreach (string p in Specializations)
            {
                if (p.ToLower().Contains(SearchField.text.ToLower()))
                {
                    Searched_specializations.Add(p);
                }
            }
            SearchActive = true;
            SpecializationScroller.ReloadData();
        }
    }

    [SkipRename]
    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return SearchActive ? Searched_specializations.Count : Specializations.Count;
    }

    [SkipRename]
    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 30f;
    }

    [SkipRename]
    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        SelectSpeciaView cellView = scroller.GetCellView(SpecializationViewPrefab) as SelectSpeciaView;
        cellView.name = "Cell " + dataIndex.ToString();
        if (SearchActive)
        {
            cellView.SetData(Searched_specializations[dataIndex]);
        }
        else
        {
            cellView.SetData(Specializations[dataIndex]);
        }

        return cellView;
    }
}