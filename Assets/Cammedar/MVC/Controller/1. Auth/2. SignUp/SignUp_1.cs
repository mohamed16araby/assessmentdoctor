﻿using UnityEngine;
using UnityEngine.UI;
using Beebyte.Obfuscator;
using Cammedar.Network;
using FAW = Cammedar.AuthenticationWrapper;

public class SignUp_1 : MonoBehaviour
{
    public InputField Email_inputField;
    public InputField Password_inputField;
    public InputField ConfirmPassword_inputField;

    public Text ErrorTxt;
    public GameObject Signup_2Prefab;

    [SerializeField]
    GameEvent signUp_FirstStep;

    public static Cammedar.Network.Firebase Firebase = Cammedar.Network.Firebase.Instance;

    [SkipRename]
    public void SignUpBtn_pressed()
    {
        ErrorTxt.text = "";
        UserData.user_email = Email_inputField.text;
        UserData.user_password = Password_inputField.text;

        if (UserData.user_email == "" || UserData.user_password == "")
            OnFail("Please Fill all required data");

        else if (UserData.user_password.Length < 6)
            OnFail("Password must be at least 6 characters or numbers");

        else if (UserData.user_password == ConfirmPassword_inputField.text)
            Signup();
        
        else
            OnFail("Passwords don't match");
    }

    public void OnFail(string message)
    {
        ErrorTxt.color = new Color32(209, 1, 85, 255);
        ErrorTxt.text = message;
    }

    public void Signup()
    {
        FAW.SignUp(UserData.user_email, UserData.user_password, str =>
        {
            UserData.user_UDID = Firebase.authInfo.localId;
            UserData.tokenID = Firebase.authInfo.idToken;
            AddCurrentUser_ToDoctors();
        },
        (mssg) =>
        {
            if (mssg == "Email exists")
            {
                FAW.SignIn(UserData.user_email, UserData.user_password,
                    delegate
                    {
                        UserData.user_UDID = Firebase.authInfo.localId;
                        UserData.tokenID = Firebase.authInfo.idToken;

                        if (FAW.doctorRetrieved != null)
                        {
                            if (FAW.doctorRetrieved.authorized)
                                OnFail("The user is already exist!");

                            else
                            {
                                signUp_FirstStep.Raise();
                                Signup_2Prefab.SetActive(true);
                            }
                        }
                        else
                            AddCurrentUser_ToDoctors();
                    }, (mssg2) =>
                    {
                        if (mssg2 == "Invalid password")
                            OnFail("This email is already in use!");
                        else
                            OnFail(mssg2);
                    });
            }
        });
    }

    private void AddCurrentUser_ToDoctors()
    {
        UserData.authorized = false;

        Doctor doctor = new Doctor(UserData.user_email, UserData.authorized);

        URI doctorID_URI = Firebase.databasePath.Child("doctors").Child(UserData.user_UDID).Child("doctor_Data").JSON().AuthQuery(UserData.tokenID);

        System.Action<string> onDone = (message) =>
        {
            signUp_FirstStep.Raise();
            Signup_2Prefab.SetActive(true);
        };

        doctor.Encrypt(UserData.user_UDID);

        Firebase.Put(doctorID_URI, doctor, onDone,
            (message) =>
            {
                ErrorTxt.color = new Color32(209, 1, 85, 255);
                ErrorTxt.text = message;
            });
    }

    public void ViewSignup_1()
    {
        Signup_2Prefab.SetActive(false);
    }
}