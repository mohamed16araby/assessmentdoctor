﻿using System.Collections.Generic;
using UnityEngine;
using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;
using Beebyte.Obfuscator;
using System.Linq;
using fb = Cammedar.Network.Firebase;
using URI = Cammedar.Network.URI;
using Cammedar.Network;

struct ListOptions
{
    /// <summary>
    /// 0: CountryList,
    /// 1: CityList,
    /// 2: StatesList,
    /// 3: HospitalsList,
    /// 4: HospitalsBranchesList
    /// </summary>
    public static int Options = 0;
}

public class HospitalsListController : MonoBehaviour, IEnhancedScrollerDelegate
{
    private Dictionary<string, string> countries = new Dictionary<string, string>();
    private Dictionary<string, string> searched_countries = new Dictionary<string, string>();

    public EnhancedScroller HospitalsScroller;
    public EnhancedScrollerCellView HospitalsViewPrefab;

    public InputField SearchField;
    public Text menuTitle;
    public PopupMenu PopupMenu;

    //DatabaseReference reference;
    string ErrorMsg = "";
    int API_request_status = 0;

    bool SearchActive = false;

    void Start()
    {
        HospitalsScroller.Delegate = this;
        SearchField.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
        //FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(FirebaseDatabaseData.DataBaseURL);
        //reference = FirebaseDatabase.DefaultInstance.RootReference;

        switch (ListOptions.Options)
        {
            case 0: // CountryList
                GetCountries();
                break;
            case 1: // CityList
                GetCities();
                break;
            case 2: // StatesList
                countries = (UserData.states == null) ? new Dictionary<string, string>() : UserData.states;
                break;
            case 3: // HospitalsList
                GetHospitals();
                break;
            case 4: // HospitalsList
                GetHospitalBranches();
                break;

            default:
                break;
        }
    }

    [SkipRename]
    public void ValueChangeCheck()
    {
        if (SearchField.text == "")
        {
            SearchActive = false;
            HospitalsScroller.ReloadData();
        }
        else
        {
            switch (ListOptions.Options)
            {
                case 0: // CountryList
                case 1: // CityList
                case 2: // StatesList
                case 3: // HospitalsList
                case 4: // HospitalBranches List
                    searched_countries.Clear();
                    foreach (KeyValuePair<string, string> c in countries)
                        if (c.Value.ToLower().Contains(SearchField.text.ToLower()))
                            searched_countries.Add(c.Key, c.Value);
                    break;

                default:
                    break;
            }

            SearchActive = true;
            HospitalsScroller.ReloadData();
        }
    }

    private void GetCountries()
    {
        URI uri = fb.Instance.databasePath.Child("countries").JSON().AuthQuery(UserData.tokenID);

        System.Action<Dictionary<string, Dictionary<string, string>>> onSuccess = (countriesRetrieved) =>
        {
            countries = new Dictionary<string, string>();
            if (countriesRetrieved != null && countriesRetrieved.Count > 0)
                foreach (KeyValuePair<string, Dictionary<string, string>> item in countriesRetrieved)
                    if (item.Value.TryGetValue("country_name", out string name))
                        countries.Add(item.Key.ToString(), name);

            HospitalsScroller.ReloadData();
        };

        fb.Instance.Get(uri, onSuccess, delegate { });
    }

    private void GetCities()
    {
        URI uri = fb.Instance.databasePath.Child("cities").Child(UserData.country_id).Child(UserData.state_id).JSON().AuthQuery(UserData.tokenID);

        System.Action<Dictionary<string, string>> onSuccess = (citiesRetrieved) =>
        {
            countries = citiesRetrieved == null ? new Dictionary<string, string>() : citiesRetrieved;
            HospitalsScroller.ReloadData();
        };

        fb.Instance.Get(uri, onSuccess, delegate { });
    }

    private void GetHospitals()
    {
        URI uri = fb.Instance.databasePath.Child("hospital_branches").Child(UserData.country_id).Child(UserData.city_id).JSON().AuthQuery(UserData.tokenID);

        System.Action<Dictionary<string, Hospital>> onSuccess = (hospitals) =>
        {
            countries = new Dictionary<string, string>();

            if (hospitals != null)
                foreach (KeyValuePair<string, Hospital> hospital in hospitals)
                    countries.Add(hospital.Key, hospital.Value.hospital_name);

            HospitalsScroller.ReloadData();
            PopupMenu.currentList = countries.Values.ToList();
        };

        fb.Instance.Get(uri, onSuccess, delegate { });
    }

    [SkipRename]
    public void GetHospitalBranches()
    {
        URI uri = fb.Instance.databasePath.Child("hospital_branches").Child(UserData.country_id).Child(UserData.city_id)
            .Child(UserData.doctor_hospital_id).Child("branches").JSON().AuthQuery(UserData.tokenID);

        System.Action<Dictionary<string, string>> onSuccess = (branches) =>
        {
            countries = (branches == null) ? new Dictionary<string, string>() : branches;
            HospitalsScroller.ReloadData();
            PopupMenu.currentList = countries.Values.ToList();
        };

        fb.Instance.Get(uri, onSuccess, delegate { });
    }

    [SkipRename]
    public void RefreshHospitals()
    {
        GetHospitals();
    }

    [SkipRename]
    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        switch (ListOptions.Options)
        {
            case 0: // CountryList
            case 1: // CityList
            case 2: // StatesList
            case 3: // HospitalsList
            case 4: // HospitalsList
                return SearchActive ? searched_countries.Count : countries.Count;

            default:
                return 0;
        }
    }

    [SkipRename]
    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 30f;
    }

    [SkipRename]
    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        HospitalView cellView = scroller.GetCellView(HospitalsViewPrefab) as HospitalView;
        cellView.name = "Cell " + dataIndex.ToString();

        switch (ListOptions.Options)
        {
            case 0: // CountryList
            case 1: // CityList
            case 2: // StatesList
            case 3: // HospitalsList
            case 4: // HospitalBranchesList
                if (SearchActive)
                    cellView.SetData(searched_countries.ElementAt(dataIndex));

                else
                    cellView.SetData(countries.ElementAt(dataIndex));
                break;

            default:
                break;
        }
        return cellView;
    }
}