﻿using System.Collections.Generic;
using UnityEngine;
using fb = Cammedar.Network.Firebase;
using URI = Cammedar.Network.URI;

public class AddHospitalController : MonoBehaviour
{
    [HideInInspector]
    public AddHospitalView addHospitalView;
    [SerializeField]
    GameEvent RefreshHospitals;
    [SerializeField]
    GameEvent RefreshBranches;

    List<string> currentList;
    int optionIndex;
    string identifierText;

    System.Action ClosingPopupAction;

    public void Init(int optionIndex, string identifierText, List<string> currentList, AddHospitalView addHospitalView, System.Action ClosingPopupAction)
    {
        this.optionIndex = optionIndex;
        this.currentList = currentList;
        this.identifierText = identifierText;
        this.ClosingPopupAction = ClosingPopupAction;
        this.addHospitalView = addHospitalView;

        string title = "Add " + identifierText;
        if(optionIndex == 4)
           title += " to " + UserData.doctor_hospital_name;

        string placeholder = identifierText + " name";
        addHospitalView.Init(title, placeholder, identifierText, AddHospitalOrBranch);
    }

    public void AddHospitalOrBranch()
    {
        if (Hospital_AlreadyExists())
            addHospitalView.ViewError(string.Format("The {0} name is already exist!", identifierText));
        else
            AddHospitalOrBranch_ToDatabase(addHospitalView.TextEntered);
    }

    private bool Hospital_AlreadyExists()
    {
        string itemNameFound = currentList.Find(i => i.ToLower() == addHospitalView.TextEntered);
        return !string.IsNullOrEmpty(itemNameFound);
    }

    private void AddHospitalOrBranch_ToDatabase(string name)
    {

        if (optionIndex == 3) //adding hospital
        {
            URI uri = fb.Instance.databasePath.Child("hospital_branches").Child(UserData.country_id).Child(UserData.state_id)
                    .Child(UserData.city_id).JSON().AuthQuery(UserData.tokenID);
            fb.Instance.Post(uri, new Dictionary<string, string>() { { "hospital_name", addHospitalView.TextEntered } },
                             delegate
                             {
                                 addHospitalView.ViewSuccessFeedback("Hospital has been added successfully!");
                                 ClosingPopupAction();
                                 RefreshHospitals.Raise();
                             },
                             delegate { addHospitalView.ViewError("Something Wrong! .. Please try again later"); });
        }
        else if (optionIndex == 4) //adding hospital branch
        {
            URI uri = fb.Instance.databasePath.Child("hospital_branches").Child(UserData.country_id).Child(UserData.state_id)
                    .Child(UserData.city_id).Child(UserData.doctor_hospital_id).Child("branches").JSON().AuthQuery(UserData.tokenID);
            fb.Instance.Post(uri, addHospitalView.TextEntered,
                             delegate
                             {
                                 addHospitalView.ViewSuccessFeedback("Hospital branch has been added successfully!");
                                 ClosingPopupAction();
                                 RefreshBranches.Raise();
                             },
                             delegate { addHospitalView.ViewError("Something Wrong! .. Please try again later"); });
        }
    }
}