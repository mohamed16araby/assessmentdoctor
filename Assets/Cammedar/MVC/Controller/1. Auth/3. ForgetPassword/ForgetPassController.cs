﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ForgetPassController : MonoBehaviour
{
    public InputField EmailField;
    protected Firebase.Auth.FirebaseAuth auth;
    public Text ErrorTxt;
    string ErrorMsg = "";
    int API_request_status = 0;

    private void Start()
    {
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
    }

    public void SendLinkBtn_pressed()
    {
        if (EmailField.text != "")
        {
            ForgetPassword(EmailField.text);
            StartCoroutine(CheckState());
        }
        else
        {
            ErrorTxt.color = new Color32(209, 1, 85, 255);
            ErrorTxt.text = "Please enter your email";
        }
    }

    void ForgetPassword(string email)
    {
        auth.SendPasswordResetEmailAsync(email).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                API_request_status = 2;
                ErrorMsg = task.Exception.InnerException.Message;

                return;
            }
            if (task.IsFaulted)
            {
                API_request_status = 2;
                ErrorMsg = task.Exception.InnerException.Message;
                return;
            }

            ErrorMsg = "Password reset email sent successfully";
            API_request_status = 1;
        });
    }

    IEnumerator CheckState()
    {
        yield return new WaitUntil(() => API_request_status > 0);
        if (API_request_status == 2)
        { // wrong
            ErrorTxt.color = new Color32(209, 1, 85, 255);
            ErrorTxt.text = ErrorMsg;
            API_request_status = 0;
        }
        else if (API_request_status == 1)
        {
            ErrorTxt.color = new Color32(3, 189, 91, 255);
            ErrorTxt.text = ErrorMsg;
            API_request_status = 0;
        }
    }
}