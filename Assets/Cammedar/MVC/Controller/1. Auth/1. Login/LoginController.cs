﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Beebyte.Obfuscator;
using Ricimi;
using Cammedar.Network;
using FAW = Cammedar.AuthenticationWrapper;
using Facebook;
using Facebook.Unity;
using System.Collections.Generic;
public class LoginController : MonoBehaviour
    {
        public InputField userNameInputField;
        public InputField PasswordInputField;
        public Text ErrorMessageTxt;

        public GameObject SelectPatientMenu;
        public GameObject SignUP_2;

        public Popup Signup1_popup;

        [Header("Popup")]
        [SerializeField]
        PopupOpener popupOpener;
        [SerializeField]
        GameObject prompt_YesNo;

        public static Cammedar.Network.Firebase Firebase_Instance = Cammedar.Network.Firebase.Instance;

        void Start()
        {


        userNameInputField.text =  "mo@gmail.com";
        PasswordInputField.text =  "123456";
            //#if UNITY_ANRDOID || UNITY_IOS
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
        ViewPopup();
#endif
        }
    void Awake()
    {
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
            FB.Mobile.SetAdvertiserTrackingEnabled(true);
        }
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            FB.Mobile.SetAdvertiserTrackingEnabled(true);
            // Continue with Facebook SDK
            // ...
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }



    public void ViewPopup()
        {
            Prompt_YesNo instanPrompt = OpenPrompt(prompt_YesNo).GetComponent<Prompt_YesNo>();
            instanPrompt.Init("This mobile app is for testing purposes only and getting feedback on the app version.\n\nFor the best experience use our desktop version!",
                delegate
                {
                    ClosePrompt();
                }, null, null,
            "Got it", "");
        }

        [SkipRename]
        public void LoginBtn_pressed()
        {
            FAW.SignIn(userNameInputField.text, PasswordInputField.text, delegate
            {
                UserData.user_UDID = Firebase_Instance.authInfo.localId;
                UserData.tokenID = Firebase_Instance.authInfo.idToken;

                Doctor doctor = FAW.doctorRetrieved;
                if (doctor == null)
                {
                    ErrorMessageTxt.text = "User not exists, Create new account!";
                    return;
                }
                doctor.Decrypt(UserData.user_UDID);

                UserData.authorized = doctor.authorized;
                UserData.approved = doctor.approved;
                UserData.user_email = doctor.email;

                if (UserData.authorized)
                {
                    if (UserData.approved)
                    {
                        Debug.Log(doctor.first_name);
                        UserData.first_name = doctor.first_name;
                        UserData.last_name = doctor.last_name;
                        UserData.phone_number = doctor.phone_number;
                        UserData.sessionsCount = new SessionsCount() { examinations = doctor.sessionsCount.examinations, followUps = doctor.sessionsCount.followUps };
                        UserData.doctor_hospital_name = doctor.hospital_name;
                        UserData.doctor_hospital_id = doctor.hospital_id;
                        UserData.doctor_hospital_branch_name = doctor.hospital_branch_name;
                        UserData.doctor_hospital_branch_id = doctor.hospital_branch_id;
                        UserData.doctor_Specialization = doctor.speciality;
                        UserData.doctor_SubSpecialization = doctor.sub_speciality;
                        UserData.city_id = doctor.city_id;
                        UserData.country_id = doctor.country_id;

                        ErrorMessageTxt.text = "";
                        FB.LogAppEvent(AppEventName.CompletedRegistration, null, new Dictionary<string, object>()
{{ AppEventParameterName.Description, "successfully logged in" }});
                        SceneManager.LoadScene("SelectPatientScene");
                    }
                    else
                    {
                        FB.LogAppEvent(AppEventName.CompletedRegistration, null, new Dictionary<string, object>()
{{ AppEventParameterName.Description, "Pending admin approval, please contact cammedar sales team!" }});
                        SceneManager.LoadScene("SelectPatientScene");
                        OnFail("Pending admin approval, please contact cammedar sales team!");
                    }
                }
                else
                {
                    FB.LogAppEvent(AppEventName.CompletedRegistration, null, new Dictionary<string, object>()
{{ AppEventParameterName.Description, "Signup Active" }});
                    SignUP_2.SetActive(true);
                }
            }, OnFail);
        }

        [SkipRename]
        public void SeePasswordBtn_pressed()
        {
            if (PasswordInputField.contentType == InputField.ContentType.Password)
            {
                PasswordInputField.contentType = InputField.ContentType.Standard;
            }
            else
            {
                PasswordInputField.contentType = InputField.ContentType.Password;
            }
            PasswordInputField.ForceLabelUpdate();
        }

        public void OnFail(string message)
        {
            ErrorMessageTxt.text = message;
        }

        [SkipRename]
        public void SignedUp_FirstStep()
        {
            userNameInputField.text = UserData.user_email;
            PasswordInputField.text = UserData.user_password;
        }

        public GameObject OpenPrompt(GameObject prefabView)
        {
            popupOpener.popupPrefab = prefabView;
            popupOpener.OpenPopup();

            return popupOpener.popupInstantiated;
        }

        public void ClosePrompt()
        {
            popupOpener.ClosePopup();
        }
    }
