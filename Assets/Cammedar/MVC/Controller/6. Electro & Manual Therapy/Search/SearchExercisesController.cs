﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;
using System.IO;
using Beebyte.Obfuscator;

public class SearchExercisesController : MonoBehaviour, IEnhancedScrollerDelegate
{
    private List<therapy_shoulder_exercise> exercises = new List<therapy_shoulder_exercise>();
    private List<therapy_shoulder_exercise> Searched_exercises = new List<therapy_shoulder_exercise>();
    private Therapy exercisesModel = new Therapy();
    public EnhancedScroller ExercisesScroller;
    public EnhancedScrollerCellView ExerciseViewPrefab;
    public InputField SearchField;
    bool SearchActive = false;

    void Start()
    {
        ExercisesScroller.Delegate = this;
        SearchField.onValueChanged.AddListener(delegate { ValueChangeCheck(); });

        exercisesModel = FileData.currentExercises;

        foreach (therapy_shoulder_exercises exGroup in exercisesModel.therapy)
        {
            foreach (therapy_shoulder_exercise ex in exGroup.exercises)
            {
                this.exercises.Add(ex);
                foreach (therapy_shoulder_exercise ex2 in ex.sub_exs)
                    this.exercises.Add(ex2);
            }
        }

        ExercisesScroller.ReloadData();
    }

    [SkipRename]
    public void ValueChangeCheck()
    {
        if (SearchField.text == "")
        {
            SearchActive = false;
            ExercisesScroller.ReloadData();
        }
        else
        {
            Searched_exercises.Clear();
            foreach (therapy_shoulder_exercise e in exercises)
            {
                if (e.name.ToLower().Contains(SearchField.text.ToLower()))
                {
                    Searched_exercises.Add(e);
                }
            }

            SearchActive = true;
            ExercisesScroller.ReloadData();
        }
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        if (SearchActive)
        {
            return Searched_exercises.Count;
        }
        else
        {
            return exercises.Count;
        }
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 60f;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        ExerciseAnimView_search cellView = scroller.GetCellView(ExerciseViewPrefab) as ExerciseAnimView_search;
        cellView.name = "Cell " + dataIndex.ToString();
        if (SearchActive)
        {
            cellView.SetData(Searched_exercises[dataIndex]);
        }
        else
        {
            cellView.SetData(exercises[dataIndex]);
        }

        return cellView;
    }
}