﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

using Ricimi;
using Beebyte.Obfuscator;

public class SavingManualTherapyAlgorithms_Controller : MonoBehaviour
{

    TMP_InputField algorithmName;

    BasicButton addAlgorithm_Btn;


    TextMeshProUGUI save_Feedback_Txt;

   
    Color succeededColor;


    Color failedColor;


    StringField algorithmName_tobeStored;


    GameEvent addAlgorithm_BtnClicked;

    private void Start()
    {
        addAlgorithm_Btn.onClicked.AddListener(AddAlgorithm);
        save_Feedback_Txt.gameObject.SetActive(false);
    }

    void AddAlgorithm()
    {
        if (string.IsNullOrEmpty(algorithmName.text)) return;

        algorithmName_tobeStored.Value = algorithmName.text;
        addAlgorithm_BtnClicked.Raise();
    }

    [SkipRename]
    public void OnSuccess()
    {
        save_Feedback_Txt.gameObject.SetActive(true);
        save_Feedback_Txt.text = "Algorithm has been added successfully";
        save_Feedback_Txt.color = succeededColor;
    }

    [SkipRename]
    public void OnFailing()
    {
        save_Feedback_Txt.gameObject.SetActive(true);
        save_Feedback_Txt.text = "Something went wrong try again later!";
        save_Feedback_Txt.color = failedColor;

    }
}
