﻿using System.Collections;
using UnityEngine;
using EnhancedUI.EnhancedScroller;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Beebyte.Obfuscator;
using Ricimi;
using Newtonsoft.Json;
using fb = Cammedar.Network.Firebase;
using URI = Cammedar.Network.URI;
using TMPro;

public class ManualTherapy_Algorithm
{
    public string algorithmName = "";
    public Dictionary<string, AssignExercise> assignedExercises = new Dictionary<string, AssignExercise>();
}

public class ManualTherapyController : MonoBehaviour, IEnhancedScrollerDelegate
{
    private Therapy exercises;
    public InputField SearchField;
    /// <summary>
    /// This is our scroller we will be a delegate for
    /// </summary>
    public EnhancedScroller ExercisesTitleScroller;
    public EnhancedScroller ExercisesScroller;
    public EnhancedScroller SubExercsisScroller;
    public EnhancedScrollerCellView ExercisesViewPrefab;
    public EnhancedScrollerCellView ExerciseViewPrefab_search;
    public GameObject SearchExercisesPanel;
    public Text subSpeciality;

    [SerializeField]
    BasicButton prev_Btn;
    [SerializeField]
    Button finish_Btn;
    [SerializeField]
    BasicButton save_Btn;


    StringField algorithmName_tobeStored;


    StringField unassignedExerciseId;


    StringField currentSelected_AlgorithmName;


    GameEvent SavingManualAlgorithm_Succeeded;

    GameEvent SavingManualAlgorithm_Failed;
    [SerializeField]
    GameEvent ResetDataWhenLooping;

    [SerializeField]
    GameObject prompt_PreformAnotherSessionPrefab;

    [SerializeField]
    TMP_InputField followUp_InputField;

    string backSceneName = "ElectroTherapy";
    string currentSceneName = "Animations";
    string nextSceneName = "PDFWriter";

    bool FirebaseProcessFinish = false;
    AnimationsManager animationsManager;

    private static string selectedGroupId = "";
    private static string selectedExerciseID = "";
    private static string selectedSubExerciseID = "";
    public Sprite Audioon;
    public Sprite AudioOff;
    [SerializeField]
    Button AudioButton;
    GameObject AudioController;
    private bool goingBack = false;
    Session session;

    void Start()
    {
        prev_Btn.onClicked.AddListener(LoadPreviousScene);
        finish_Btn.onClick.AddListener(FinishBtn_pressed);
        save_Btn.onClicked.AddListener(delegate { OpenSavePopup(); });
        AudioButton.onClick.AddListener(FVoiceController);
        AudioController = GameObject.Find("VoiceControl");
        session = Session.Instance;

        ExerciseAnimControllerStruct.selectedGroupId = "";
        ExerciseAnimControllerStruct.selectedGroupIndex = 0;

        selectedGroupId = ExerciseAnimControllerStruct.selectedGroupId;

        exercises = new Therapy();
        ExercisesTitleScroller.Delegate = this;
        SubExercsisScroller.Delegate = this;
        ExercisesScroller.Delegate = this;

        subSpeciality.text = UserData.GetLastSpeciality + " Manual Therapy";

        LoadExercisesData();
        RetrieveFollowUp();
        Retrieving_ManualTherapyAlgorithms_From_Database();

        SetVoiceControl();
    }
    public void FVoiceController()
    {
        Debug.Log("log");
        if (AudioController.activeSelf)
        {
            AudioButton.GetComponent<Image>().sprite = AudioOff;
#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
            VoiceControl.Instance.Discard();
#elif UNITY_ANDROID

        Debug.Log("android");
#endif

            AudioController.SetActive(false);
        }
        else
        {
            AudioButton.GetComponent<Image>().sprite = Audioon;
            AudioController.SetActive(true);
#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
            VoiceControl.Instance.RestartRecognizer();
#elif UNITY_ANDROID

        Debug.Log("android");
#endif
        }
    }

    private void FixedUpdate()
    {
        if (selectedGroupId != ExerciseAnimControllerStruct.selectedGroupId)
        {
            ExercisesScroller.ReloadData();
            selectedExerciseID = "";
            selectedGroupId = ExerciseAnimControllerStruct.selectedGroupId;
        }
        else if (selectedExerciseID != ExerciseAnimControllerStruct.selectedExerciseID)
        {
            selectedExerciseID = ExerciseAnimControllerStruct.selectedExerciseID;
            SubExercsisScroller.ReloadData();

            if (exercises.therapy != null && exercises.therapy.Count > ExerciseAnimControllerStruct.selectedGroupIndex)
            {
                int id = exercises.therapy[ExerciseAnimControllerStruct.selectedGroupIndex].exercises.FindIndex(exs => exs.id.Equals(ExerciseAnimControllerStruct.selectedExerciseID));
                if (id >= 0)
                    ExercisesScroller.JumpToDataIndex(id);
            }
        }
        else if (selectedSubExerciseID != ExerciseAnimControllerStruct.selectedSubExerciseID)
        {
            selectedSubExerciseID = ExerciseAnimControllerStruct.selectedSubExerciseID;

            int id = exercises.therapy[ExerciseAnimControllerStruct.selectedGroupIndex].exercises[ExerciseAnimControllerStruct.selectedExerciseIndex].sub_exs.FindIndex(exs => exs.id.Equals(ExerciseAnimControllerStruct.selectedSubExerciseID));
            if (id >= 0)
                SubExercsisScroller.JumpToDataIndex(id);
        }
    }

    #region Algorithm

    void Retrieving_ManualTherapyAlgorithms_From_Database()
    {
        URI uri = fb.Instance.databasePath.Child("doctors").Child(UserData.user_UDID).Child("algorithms_ManualTherapy");

        foreach (string str in UserData.Doctor_Speciality_Sub_Subsub_Path)
            uri = uri.Child(str);

        uri = uri.JSON().AuthQuery(UserData.tokenID);

        System.Action<Dictionary<string, Dictionary<string, AssignExercise>>> onSuccess = (algorithm) =>
        {
            ProgramGeneralData.manualTherapyAlgorithms = new List<ManualTherapy_Algorithm>();
            if (algorithm != null && algorithm.Count > 0)
                foreach (KeyValuePair<string, Dictionary<string, AssignExercise>> keyValuePair in algorithm)
                    ProgramGeneralData.manualTherapyAlgorithms.Add(new ManualTherapy_Algorithm() { algorithmName = keyValuePair.Key, assignedExercises = keyValuePair.Value });
        };

        fb.Instance.Get(uri, onSuccess, delegate { });
    }

    /// <summary>
    /// Gets called whenever a new algorithm has been added
    /// </summary>
    [SkipRename]
    public void UpdateExercisesWithAlgorithmSelected()
    {
        ManualTherapy_Algorithm manualTherapy_Algorithm = ProgramGeneralData.manualTherapyAlgorithms.Find(a => a.algorithmName == currentSelected_AlgorithmName.Value);
        if (manualTherapy_Algorithm == null)
        {
            Debug.Log("Something Not Right!!");
            return;
        }

        session.exercises = manualTherapy_Algorithm.assignedExercises;
        GetUpdatedExercises_and_turnOnAssignedFlag(session.exercises);
        ExercisesTitleScroller.ReloadData();
    }

    [SkipRename]
    public void Add_to_ManualTherapyAlgorithms()
    {
        string algorithm_Name = algorithmName_tobeStored.Value;
        if (string.IsNullOrEmpty(algorithm_Name)) return;

        URI uri = fb.Instance.databasePath.Child("doctors").Child(UserData.user_UDID).Child("algorithms_ManualTherapy").Child(UserData.doctor_Specialization)
            .Child(UserData.doctor_SubSpecialization_logIn).Child(UserData.doctor_SubSubSpecialization_logIn).Child(algorithm_Name)
            .JSON().AuthQuery(UserData.tokenID);

        fb.Instance.Put(uri, session.exercises,
            delegate
            {
                SavingManualAlgorithm_Succeeded.Raise();
                Retrieving_ManualTherapyAlgorithms_From_Database();
            },
            delegate
            {
                SavingManualAlgorithm_Failed.Raise();
            });
    }

    private void RetrieveFollowUp()
    {
        followUp_InputField.text = session.followUp;
    }

    private void SaveFollowUp()
    {
        if (string.IsNullOrEmpty(followUp_InputField.text)) return;

        URI uri = fb.Instance.databasePath.Child("sessions").Child(ProgramGeneralData.currentSessionId).Child("followUp").JSON().AuthQuery(UserData.tokenID);
        fb.Instance.Put(uri, followUp_InputField.text,
            (mssg1) =>
                {
                    session.followUp = followUp_InputField.text;
                    SaveLastVisit_FinalPatientExercises();
                },
            (mssg2) => { });
    }
    #endregion

    #region Exercises Data
    [SkipRename]
    public void LoadExercisesData()
    {
        StartCoroutine(CheckFirebaseProcessStatus());
        DownloadExercisesFile(FirebaseStoragePaths.StorageDirectory + "ManualTherapy.json");
    }

    public void SaveLastVisit_FinalPatientExercises()
    {
        session.followUp = followUp_InputField.text;
        session.Encrypt(PatientData.patient_UDID);

        URI uri = fb.Instance.databasePath.Child("sessions").Child(ProgramGeneralData.currentSessionId).JSON().AuthQuery(UserData.tokenID);

        fb.Instance.Put(uri, session, delegate
        {
            session.Decrypt(PatientData.patient_UDID);
            EnableTransitionButtons(true);
            if (goingBack)
                StartCoroutine(ScenesController.Instance.LoadYourAsyncScene(currentSceneName, backSceneName));
            else
                ViewPreformingAnotherSession_Popup();
        }, delegate { EnableTransitionButtons(true); });
    }

    /// <summary>
    /// After assigning or unassign exercise, we need to get the update the main data holder inside exercises.therapy
    /// also turning on the assigned flag
    /// then apply those changes to ui, with ReloadData
    /// </summary>
    /// <param name="exercise_id"></param>
    public void UpdateSelections(string exercise_id)
    {
        GetUpdatedExercises_and_turnOnAssignedFlag(session.exercises);

        string[] id = Operations.SplitBy(exercise_id, '_');
        int idLength = id.Length;

        if (idLength >= 5)
        {
            ExercisesTitleScroller.ReloadData();
            ExercisesScroller.ReloadData();

            if (idLength == 6)
                SubExercsisScroller.ReloadData();
        }
    }

    public void GetUpdatedExercises_and_turnOnAssignedFlag(Dictionary<string, AssignExercise> exercisesToBeApplied)
    {
        foreach (therapy_shoulder_exercises therapy_Shoulder_Exercises in exercises.therapy)
        {
            therapy_Shoulder_Exercises.hasSelectedChild = false;
            for (int i = 0; i < therapy_Shoulder_Exercises.exercises.Count; i++)
            {
                therapy_Shoulder_Exercises.exercises[i].ResetData();
                if (exercisesToBeApplied.ContainsKey(therapy_Shoulder_Exercises.exercises[i].id))
                {
                    AssignExercise exercise = exercisesToBeApplied[therapy_Shoulder_Exercises.exercises[i].id];
                    therapy_Shoulder_Exercises.exercises[i].daysPerWeek = exercise.daysPerWeek;
                    therapy_Shoulder_Exercises.exercises[i].notes = exercise.notes;
                    therapy_Shoulder_Exercises.exercises[i].grade = exercise.grade;
                    therapy_Shoulder_Exercises.exercises[i].repetitionsPerSet = exercise.repetitionsPerSet;
                    therapy_Shoulder_Exercises.exercises[i].setsPerDay = exercise.setsPerDay;
                    therapy_Shoulder_Exercises.exercises[i].isAssigned = true;
                    therapy_Shoulder_Exercises.hasSelectedChild = true;
                }

                therapy_Shoulder_Exercises.exercises[i].hasSelectedChild = false;
                for (int j = 0; j < therapy_Shoulder_Exercises.exercises[i].sub_exs.Count; j++)
                {
                    therapy_Shoulder_Exercises.exercises[i].sub_exs[j].ResetData();
                    if (exercisesToBeApplied.ContainsKey(therapy_Shoulder_Exercises.exercises[i].sub_exs[j].id))
                    {
                        AssignExercise exercise = exercisesToBeApplied[therapy_Shoulder_Exercises.exercises[i].sub_exs[j].id];
                        therapy_Shoulder_Exercises.exercises[i].sub_exs[j].daysPerWeek = exercise.daysPerWeek;
                        therapy_Shoulder_Exercises.exercises[i].sub_exs[j].notes = exercise.notes;
                        therapy_Shoulder_Exercises.exercises[i].sub_exs[j].grade = exercise.grade;
                        therapy_Shoulder_Exercises.exercises[i].sub_exs[j].repetitionsPerSet = exercise.repetitionsPerSet;
                        therapy_Shoulder_Exercises.exercises[i].sub_exs[j].setsPerDay = exercise.setsPerDay;
                        therapy_Shoulder_Exercises.exercises[i].sub_exs[j].isAssigned = true;
                        therapy_Shoulder_Exercises.exercises[i].hasSelectedChild = true;
                        if (!therapy_Shoulder_Exercises.hasSelectedChild)
                            therapy_Shoulder_Exercises.hasSelectedChild = true;
                    }
                }
            }
        }
    }

    [SkipRename]
    public void Add_TO_ExercisesSelected()
    {
        if (session.exercises.ContainsKey(AssignedExerciseData.exercise_id)) return;
        session.exercises.Add(AssignedExerciseData.exercise_id, new AssignExercise(
                                                                                    AssignedExerciseData.exercise_id, AssignedExerciseData.exercise_name,
                                                                                    AssignedExerciseData.repetitionsPerSet, AssignedExerciseData.setsPerDay,
                                                                                    AssignedExerciseData.daysPerWeek, AssignedExerciseData.notes, AssignedExerciseData.grade));
        UpdateSelections(AssignedExerciseData.exercise_id);
    }

    [SkipRename]
    public void Remove_FROM_ExercisesSelected()
    {
        if (!session.exercises.ContainsKey(unassignedExerciseId.Value)) return;
        session.exercises.Remove(unassignedExerciseId.Value);
        UpdateSelections(unassignedExerciseId.Value);
    }
    #endregion

    #region ScrollView & Setting its data & Snapping
    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        // first, we get a cell from the scroller by passing a prefab.
        // if the scroller finds one it can recycle it will do so, otherwise
        // it will create a new cell.

        // set the name of the game object to the cell's data index.
        // this is optional, but it helps up debug the objects in 
        // the scene hierarchy.

        // in this example, we just pass the data to our cell's view which will update its UI

        if (scroller == ExercisesTitleScroller)
        {
            ExerciseAnimMasterView cellView = scroller.GetCellView(ExercisesViewPrefab) as ExerciseAnimMasterView;
            cellView.name = "Cell " + dataIndex.ToString();
            cellView.SetData(exercises.therapy[dataIndex].name, exercises.therapy[dataIndex].id, exercises.therapy[dataIndex].hasSelectedChild, true);
            return cellView;
        }

        if (scroller == ExercisesScroller)
        {
            ExerciseAnimMasterView cellView = scroller.GetCellView(ExercisesViewPrefab) as ExerciseAnimMasterView;
            ExerciseAnimControllerStruct.selectedGroupIndex = exercises.therapy.FindIndex(group => group.id.Equals(ExerciseAnimControllerStruct.selectedGroupId));

            cellView.name = "Cell " + dataIndex.ToString();
            cellView.ChooseSettingData(exercises.therapy[ExerciseAnimControllerStruct.selectedGroupIndex].exercises[dataIndex], false);
            return cellView;
        }


        if (scroller == SubExercsisScroller)
        {
            if (ExerciseAnimControllerStruct.selectedExerciseID != "")
            {
                ExerciseAnimMasterView cellView = scroller.GetCellView(ExercisesViewPrefab) as ExerciseAnimMasterView;

                ExerciseAnimControllerStruct.selectedGroupIndex = exercises.therapy.FindIndex(group => group.id.Equals(ExerciseAnimControllerStruct.selectedGroupId));
                ExerciseAnimControllerStruct.selectedExerciseIndex = exercises.therapy[ExerciseAnimControllerStruct.selectedGroupIndex].exercises.FindIndex(exs => exs.id.Equals(ExerciseAnimControllerStruct.selectedExerciseID));

                cellView.name = "Cell " + dataIndex.ToString();
                cellView.ChooseSettingData(exercises.therapy[ExerciseAnimControllerStruct.selectedGroupIndex].exercises[ExerciseAnimControllerStruct.selectedExerciseIndex].sub_exs[dataIndex], false);

                return cellView;
            }
        }

        return new AnimsGroupNameView(); // return the cell to the scroller
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return (scroller != ExercisesScroller) && (scroller != SubExercsisScroller) ? 80f : 250f;
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        if (exercises != null)
        {
            if (scroller == ExercisesTitleScroller)
                return (exercises.therapy != null) ? exercises.therapy.Count : 0;

            if (scroller == ExercisesScroller)
            {
                if (exercises.therapy.Count == 0)
                    return 0;

                if (!string.IsNullOrEmpty(ExerciseAnimControllerStruct.selectedGroupId))
                {
                    ExerciseAnimControllerStruct.selectedGroupIndex = exercises.therapy.FindIndex(group => group.id.Equals(ExerciseAnimControllerStruct.selectedGroupId));
                    if (ExerciseAnimControllerStruct.selectedGroupIndex < 0) return 0;
                    return (exercises.therapy[ExerciseAnimControllerStruct.selectedGroupIndex].exercises != null) ? exercises.therapy[ExerciseAnimControllerStruct.selectedGroupIndex].exercises.Count : 0;
                }
                else return 0;
            }

            if (scroller == SubExercsisScroller)
            {
                if (exercises.therapy.Count == 0 || string.IsNullOrEmpty(ExerciseAnimControllerStruct.selectedGroupId))
                {
                    if (string.IsNullOrEmpty(ExerciseAnimControllerStruct.selectedGroupId))
                        ExerciseAnimControllerStruct.selectedExerciseID = "";
                    return 0;
                }
                else
                {
                    ExerciseAnimControllerStruct.selectedGroupIndex = exercises.therapy.FindIndex(group => group.id.Equals(ExerciseAnimControllerStruct.selectedGroupId));
                    ExerciseAnimControllerStruct.selectedExerciseIndex = exercises.therapy[ExerciseAnimControllerStruct.selectedGroupIndex].exercises.FindIndex(exercise => exercise.id.Equals(ExerciseAnimControllerStruct.selectedExerciseID));

                    if (ExerciseAnimControllerStruct.selectedExerciseIndex < 0 || ExerciseAnimControllerStruct.selectedGroupIndex < 0)
                        return 0;

                    int index = (exercises.therapy[ExerciseAnimControllerStruct.selectedGroupIndex].exercises[ExerciseAnimControllerStruct.selectedExerciseIndex].sub_exs != null) ? exercises.therapy[ExerciseAnimControllerStruct.selectedGroupIndex].exercises[ExerciseAnimControllerStruct.selectedExerciseIndex].sub_exs.Count : 0;
                    return index > 0 ? index : 0;
                }
            }
        }
        return 0;
    }

    public void SnapTo(params int[] indices)
    {
        switch (indices.Length)
        {
            case 1:
                ExercisesTitleScroller.JumpToDataIndex(indices[0]);
                break;
            case 2:
                ExercisesTitleScroller.JumpToDataIndex(indices[0]);
                ExercisesScroller.JumpToDataIndex(indices[1]);
                break;
            case 3:
                ExercisesTitleScroller.JumpToDataIndex(indices[0]);
                ExercisesScroller.JumpToDataIndex(indices[1]);
                SubExercsisScroller.JumpToDataIndex(indices[2]);
                break;
        }
    }
    #endregion

    #region Buttons Actions
    void LoadPreviousScene()
    {
        EnableTransitionButtons(false);
        goingBack = true;
        SaveLastVisit_FinalPatientExercises();
    }

    public void FinishBtn_pressed()
    {
        EnableTransitionButtons(false);
        goingBack = false;
        SaveLastVisit_FinalPatientExercises();
    }

    public void EnableTransitionButtons(bool enable)
    {
        prev_Btn.enabled = enable;
        finish_Btn.enabled = enable;
    }

    void OpenSavePopup()
    {
        save_Btn.GetComponent<PopupOpener>().OpenPopup();
    }
    #endregion

    #region Firebase Operations
    private void DownloadExercisesFile(string FirebaseFilePath)
    {
        FBStorage.Instance.Download_To_ByteArray(FirebaseFilePath,
                                        delegate (string fileContent) { 
                                            exercises = JsonUtility.FromJson<Therapy>(fileContent);
                                            FileData.currentExercises = exercises;
                                            exercises.OrderList();
                                            FirebaseProcessFinish = true;
                                        },
                                        delegate { });
    }

    IEnumerator CheckFirebaseProcessStatus()
    {
        yield return new WaitUntil(() => FirebaseProcessFinish == true);

        Options.YouTubeMode = false;

        if (!Options.YouTubeMode)
            GetUpdatedExercises_and_turnOnAssignedFlag(session.exercises);
        else
            ExerciseAnimControllerStruct.selectedGroupId = exercises.therapy[0].id;

        ExercisesTitleScroller.ReloadData();
    }
    #endregion

    public void ViewPreformingAnotherSession_Popup()
    {
        Manager.Instance.ClosePrompt();
        GameObject popupInstantiated = Manager.Instance.OpenPrompt(prompt_PreformAnotherSessionPrefab);

        Prompt_PerformAnotherSession_View prompt_View = popupInstantiated.GetComponent<Prompt_PerformAnotherSession_View>();
        if (prompt_View) prompt_View.AssignButtonsFunctions(
            delegate { Enter_ChoosingSubspeciality_Panel(); ResetDataWhenLooping.Raise(); },
            GoNextToReportScene);
    }
    public void GoNextToReportScene()
    {
        StartCoroutine(ScenesController.Instance.LoadYourAsyncScene(currentSceneName, nextSceneName));
    }

    public void Enter_ChoosingSubspeciality_Panel()
    {
        SceneManager.LoadScene("AssessmentScene");
        ChoosingSubspeciality_Panel_Manager.Instance.Enter_ChoosingSubspeciality_Panel();
    }

    #region Voice
    void SetVoiceControl()
    {
        Dictionary<string, System.Action> phraseAction = new Dictionary<string, System.Action>()
        {
            {"Next", FinishBtn_pressed},
            {"Next Group", FinishBtn_pressed},
            {"Back", LoadPreviousScene},
            {"Back Group", LoadPreviousScene},
        };

        PanelVoiceController.Instance.AddSetOfPhrases(phraseAction);
    }
    #endregion

    void SaveFollowUp(string _noOfWeeks, System.Action<string> _onSuccess, System.Action<string> _onFail)
    {
        if (string.IsNullOrEmpty(_noOfWeeks))
            _onFail("Enter number of weeks!");
        else if (!int.TryParse(_noOfWeeks, out int _no))
            _onFail("Enter number, avoid using spaces!");
        else
        {
            URI uri = fb.Instance.databasePath.Child("sessions").Child(ProgramGeneralData.currentSessionId).Child("followUp").JSON().AuthQuery(UserData.tokenID);

            fb.Instance.Put(uri, _no, _onSuccess, _onFail);
        }
    }
}