﻿using UnityEngine;
using UnityEngine.UI;
using Beebyte.Obfuscator;


public class AssignExercisesController : MonoBehaviour
{
    public InputField RepitionsPerSet;
    public InputField SetsPerDay;
    public InputField DaysPerWeek;
    public InputField Grades;
    public GameObject Grades_GO;

    public InputField Notes;
    public Text ErrorMsgTxt;
    public Text PanelTitle;
    GameEvent Add_TO_ExercisesSelected;

    string repetitionsPerSet = "10", setsPerDay = "3", daysPerWeek = "7";

    private void Start()
    {
        PanelTitle.text = "Assign " + AssignedExerciseData.exercise_name + " " + "\nto " + PatientData.patient_name;
        Grades_GO.SetActive(AssignedExerciseData.hasGrades);
        ErrorMsgTxt.text = "";
        Grades.onValueChanged.AddListener(CheckInputForGrades);
    }

    void CheckInputForGrades(string input)
    {
        if (Grades.text == "")
        {
            ErrorMsgTxt.text = "";
            return;
        }
        if (int.TryParse(input, out int result))
        {
            if (result > 10 || result > 5 || result < 1)
            {
                Grades.text = Grades.text.Remove(Grades.text.Length - 1, 1);
                ErrorMsgTxt.text = "Please, enter number between 1 and 5 inside \"Grades\" field";
            }
            else
                ErrorMsgTxt.text = "";
        }
    }

    [SkipRename]
    public void AssignBtn_pressed()
    {
        AssignedExerciseData.repetitionsPerSet = string.IsNullOrEmpty(RepitionsPerSet.text) ? repetitionsPerSet : RepitionsPerSet.text;
        AssignedExerciseData.setsPerDay = string.IsNullOrEmpty(SetsPerDay.text) ? setsPerDay : SetsPerDay.text;
        AssignedExerciseData.daysPerWeek = string.IsNullOrEmpty(DaysPerWeek.text) ? daysPerWeek : DaysPerWeek.text;
        AssignedExerciseData.grade = Grades.text;
        AssignedExerciseData.notes = Notes.text;

        Add_TO_ExercisesSelected.Raise();
        ErrorMsgTxt.color = new Color32(3, 189, 91, 255);
        ErrorMsgTxt.text = "Assessment added successfuly";
    }
}