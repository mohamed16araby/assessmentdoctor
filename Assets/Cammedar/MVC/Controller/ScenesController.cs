﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;

public class ScenesController : MonoBehaviour
{
    private static ScenesController _instance;
    public static ScenesController Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<ScenesController>();
            return _instance;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public IEnumerator LoadYourAsyncScene(string currentSceneName, string tobeLoadedScene)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(tobeLoadedScene, LoadSceneMode.Additive);

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(tobeLoadedScene));

        if (!string.IsNullOrEmpty(currentSceneName))
            SceneManager.UnloadSceneAsync(currentSceneName);
    }

    public IEnumerator RefreshScene(string sceneName)
    {
        Scene oldOne = SceneManager.GetActiveScene();
        AsyncOperation asyncUnload = SceneManager.UnloadSceneAsync(oldOne);

        while (oldOne.isLoaded)
        {
            yield return null;
        }

        StartCoroutine(LoadYourAsyncScene("", sceneName));
    }

    public GameObject FindObjectInSpecificScene(string objectTag, string sceneName)
    {
        Scene scene = SceneManager.GetSceneByName(sceneName);

        GameObject[] gameObjects = scene.GetRootGameObjects();

        GameObject found = new List<GameObject>(GameObject.FindGameObjectsWithTag(objectTag)).Find(g => gameObjects.Contains(g.transform.root.gameObject));

        return found;
    }
}
