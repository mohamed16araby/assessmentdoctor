﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Beebyte.Obfuscator;

public class HandlingDataBetweenScenes : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    [SkipRename]
    public void ResetWhenLooping()
    {
        TreatmentGoals.currentGoalsIsShort = true;
    }
}
