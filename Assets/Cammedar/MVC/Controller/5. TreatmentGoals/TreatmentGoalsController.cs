﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;
using Beebyte.Obfuscator;
using fb = Cammedar.Network.Firebase;
using URI = Cammedar.Network.URI;
using Cammedar.Network;

class TreatmentGoals : EncryptedSerialization<TreatmentGoals>
{
    public static bool currentGoalsIsShort = true;
    public static List<string> selected_ShortTerm_goals_Orig = new List<string>();
    public static List<string> selected_LongTerm_goals_Orig = new List<string>();

    public List<string> selected_ShortTerm_goals_EnOrDecrpted = new List<string>();
    public List<string> selected_LongTerm_goals_EnOrDecrpted = new List<string>();

    public override TreatmentGoals Encrypt(string id)
    {
        if (!Debugging.encryptionEnabled) return this;

        if (currentGoalsIsShort)
        {
            selected_ShortTerm_goals_EnOrDecrpted = new List<string>();

            for (int i = 0; i < selected_ShortTerm_goals_Orig.Count; i++)
                selected_ShortTerm_goals_EnOrDecrpted.Add(Encryption(selected_ShortTerm_goals_Orig[i], id, true));
        }
        else
        {
            selected_LongTerm_goals_EnOrDecrpted = new List<string>();

            for (int i = 0; i < selected_LongTerm_goals_Orig.Count; i++)
                selected_LongTerm_goals_EnOrDecrpted.Add(Encryption(selected_LongTerm_goals_Orig[i], id, true));
        }

        return this;
    }

    public override TreatmentGoals Decrypt(string id)
    {
        if (!Debugging.encryptionEnabled) return this;

        if (currentGoalsIsShort)
            for (int i = 0; i < selected_ShortTerm_goals_Orig.Count; i++)
                selected_ShortTerm_goals_Orig[i] = Decryption(selected_ShortTerm_goals_Orig[i], id, true);

        else
            for (int i = 0; i < selected_LongTerm_goals_Orig.Count; i++)
                selected_LongTerm_goals_Orig[i] = Decryption(selected_LongTerm_goals_Orig[i], id, true);

        return this;
    }

    protected override string Decryption(string text, string key, bool CanBeNull)
    {
        return DecryptString_Aes(text, key, key, CanBeNull);
    }

    protected override string Encryption(string text, string key, bool CanBeNull)
    {
        return EncryptString_Aes(text, key, key, CanBeNull);
    }
}

[System.Serializable]
public class Treatment_goals_long_term
{
    public List<string> treatment_goals_long_term = new List<string>();

    public void OrderList()
    {
        treatment_goals_long_term.Sort();
    }
}

[System.Serializable]
public class Treatment_goals_short_term
{
    public List<string> treatment_goals_short_term = new List<string>();

    public void OrderList()
    {
        treatment_goals_short_term.Sort();
    }
}

public class TreatmentGoalsController : MonoBehaviour, IEnhancedScrollerDelegate
{
    public EnhancedScroller GoalsScroller;
    public EnhancedScrollerCellView GoalsViewPrefab;
    public Text menuTitle;

    [SerializeField]
    AssessButton nextBtn;
    [SerializeField]
    AssessButton backBtn;
    [SerializeField]
    Text Subspeciality_Chosen_Txt;
    public Sprite Audioon;
    public Sprite AudioOff;
    [SerializeField]
    Button AudioButton;
    GameObject AudioController;
    //string backSceneName = "ProblemsList";
    string backSceneName = "Diagnosis_List";
    string currentSceneName = "TreatmentGoals";
    string nextSceneName = "ElectroTherapy";

    private Treatment_goals_long_term treatment_goals_long_term;
    private Treatment_goals_short_term treatment_goals_short_term;

    private Dictionary<string, System.Action> shortTerm_VoicePhrases = new Dictionary<string, System.Action>();
    private Dictionary<string, System.Action> longTerm_VoicePhrases = new Dictionary<string, System.Action>();
    bool goingBack = false;

    ///---Storage Paths---///
    string TreatmentGoals_ShortTerm_FileStoragePath
    {
        get => FirebaseStoragePaths.StorageDirectory + "TreatmentGoals_ShortTerm.json";
    }
    string TreatmentGoals_LongTerm_FileStoragePath
    {
        get => FirebaseStoragePaths.StorageDirectory + "TreatmentGoals_LongTerm.json";
    }

    Dictionary<SessionFirebaseRequests, FirebaseOperation> firebaseResponses = new Dictionary<SessionFirebaseRequests, FirebaseOperation>
    {
        {SessionFirebaseRequests.DownloadPatientTreatGoals_ShortTerm_File, new FirebaseOperation(){ operationStatus = FirebaseOperationStatus.Idle} },
        {SessionFirebaseRequests.DownloadPatientTreatGoals_LongTerm_File, new FirebaseOperation(){ operationStatus = FirebaseOperationStatus.Idle} },
    };

    private void Start()
    {
        AudioButton.onClick.AddListener(FVoiceController);
        AudioController = GameObject.Find("VoiceControl");
        Subspeciality_Chosen_Txt.text = UserData.GetLastSpeciality + " Assessment";
        Enter();
    }

    private void Enter()
    {
        EnableTransitionButtons(true);
        GoalsScroller.Delegate = this;
        LoadTreatmentGoalsData();
    }
    public void FVoiceController()
    {
        Debug.Log("log");
        if (AudioController.activeSelf)
        {
            AudioButton.GetComponent<Image>().sprite = AudioOff;
#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
            VoiceControl.Instance.Discard();
#elif UNITY_ANDROID

        Debug.Log("android");
#endif

            AudioController.SetActive(false);
        }
        else
        {
            AudioButton.GetComponent<Image>().sprite = Audioon;
            AudioController.SetActive(true);
#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
            VoiceControl.Instance.RestartRecognizer();
#elif UNITY_ANDROID

        Debug.Log("android");
#endif

        }
    }
    private void Exit()
    {
        nextBtn.OnClick.RemoveAllListeners();
        backBtn.OnClick.RemoveAllListeners();
    }

    [SkipRename]
    public void LoadTreatmentGoalsData()
    {
        if (TreatmentGoals.currentGoalsIsShort)
        {
            menuTitle.text = "Short-term Treatment Goals";
            firebaseResponses[SessionFirebaseRequests.DownloadPatientTreatGoals_ShortTerm_File].operationStatus = FirebaseOperationStatus.Inprocess;
            StartCoroutine(CheckFirebaseProcessStatus(SessionFirebaseRequests.DownloadPatientTreatGoals_ShortTerm_File));
            DownloadTreatmentGoalsFile(TreatmentGoals_ShortTerm_FileStoragePath, SessionFirebaseRequests.DownloadPatientTreatGoals_ShortTerm_File);
        }
        else
        {
            menuTitle.text = "Long-term Treatment Goals";
            firebaseResponses[SessionFirebaseRequests.DownloadPatientTreatGoals_LongTerm_File].operationStatus = FirebaseOperationStatus.Inprocess;
            StartCoroutine(CheckFirebaseProcessStatus(SessionFirebaseRequests.DownloadPatientTreatGoals_LongTerm_File));
            DownloadTreatmentGoalsFile(TreatmentGoals_LongTerm_FileStoragePath, SessionFirebaseRequests.DownloadPatientTreatGoals_LongTerm_File);
        }

    }

    private void DownloadTreatmentGoalsFile(string FirebaseFilePath, SessionFirebaseRequests firebaseRequest)
    {
        FBStorage.Instance.Download_To_ByteArray(FirebaseFilePath,
                                        delegate (string fileContent)
                                        {
                                            firebaseResponses[firebaseRequest] = new FirebaseOperation()
                                            {
                                                operationStatus = FirebaseOperationStatus.Completed,
                                                content = fileContent
                                            };
                                        },
                                        delegate { firebaseResponses[firebaseRequest].operationStatus = FirebaseOperationStatus.Failed; });
    }

    void SetFile(string _fileContent)
    {
        if (TreatmentGoals.currentGoalsIsShort)
        {
            treatment_goals_short_term = JsonUtility.FromJson<Treatment_goals_short_term>(_fileContent);
            treatment_goals_short_term.OrderList();
        }

        else
        {
            treatment_goals_long_term = JsonUtility.FromJson<Treatment_goals_long_term>(_fileContent);
            treatment_goals_long_term.OrderList();
        }
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        if (TreatmentGoals.currentGoalsIsShort)
            return (treatment_goals_short_term != null) ? treatment_goals_short_term.treatment_goals_short_term.Count : 0;

        else
            return (treatment_goals_long_term != null) ? treatment_goals_long_term.treatment_goals_long_term.Count : 0;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 30f;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        TreatmentGoal cellView = scroller.GetCellView(GoalsViewPrefab) as TreatmentGoal;
        cellView.name = "Cell " + dataIndex.ToString();
        List<string> treatmentGoals = TreatmentGoals.currentGoalsIsShort ? treatment_goals_short_term.treatment_goals_short_term :
                                                                           treatment_goals_long_term.treatment_goals_long_term;

        cellView.SetData(treatmentGoals[dataIndex], dataIndex, TreatmentGoals.currentGoalsIsShort, AddToVoiceDictionary);
        return cellView;
    }

    void SaveTreatmentGoals()
    {
        if (TreatmentGoals.currentGoalsIsShort)
            SaveTreatmentGoals_ShortTerm();
        else
            SaveTreatmentGoals_LongTerm();
    }

    void SaveTreatmentGoals_ShortTerm()
    {
        URI uri = fb.Instance.databasePath.Child("sessions").Child(ProgramGeneralData.currentSessionId).Child("TreatmentGoals_ShortTerm").JSON().AuthQuery(UserData.tokenID);
        TreatmentGoals treatmentGoals = new TreatmentGoals().Encrypt(PatientData.patient_UDID);

        fb.Instance.Put(uri, treatmentGoals.selected_ShortTerm_goals_EnOrDecrpted, delegate
        {
            if (goingBack)
                StartCoroutine(ScenesController.Instance.LoadYourAsyncScene(currentSceneName, backSceneName));
            else
            {
                Exit();
                TreatmentGoals.currentGoalsIsShort = false;
                Enter();
            }

        }, delegate
        {
            EnableTransitionButtons(true);
        });
    }

    void SaveTreatmentGoals_LongTerm()
    {
        URI uri = fb.Instance.databasePath.Child("sessions").Child(ProgramGeneralData.currentSessionId).Child("TreatmentGoals_LongTerm").JSON().AuthQuery(UserData.tokenID);
        TreatmentGoals treatmentGoals = new TreatmentGoals().Encrypt(PatientData.patient_UDID);

        foreach (string str in TreatmentGoals.selected_LongTerm_goals_Orig)
        {
            Debug.Log("str: " + str);
        }
        fb.Instance.Put(uri, treatmentGoals.selected_LongTerm_goals_EnOrDecrpted, delegate
        {
            if (goingBack)
            {
                Exit();
                TreatmentGoals.currentGoalsIsShort = true;
                Enter();
            }
            else
                StartCoroutine(ScenesController.Instance.LoadYourAsyncScene(currentSceneName, nextSceneName));

        }, delegate
        {
            EnableTransitionButtons(true);
        });
    }

    IEnumerator CheckFirebaseProcessStatus(SessionFirebaseRequests firebaseRequest)
    {
        while (!firebaseResponses[firebaseRequest].ProcessFinished())
            yield return null;
        if (firebaseResponses[firebaseRequest].IsCompleted())
        {
            switch (firebaseRequest)
            {
                case SessionFirebaseRequests.DownloadPatientTreatGoals_ShortTerm_File:

                    firebaseResponses[firebaseRequest].operationStatus = FirebaseOperationStatus.Idle;
                    SetFile(firebaseResponses[firebaseRequest].content);

                    TreatmentGoals.selected_ShortTerm_goals_Orig = Session.Instance.TreatmentGoals_ShortTerm;

                    shortTerm_VoicePhrases = new Dictionary<string, System.Action>();
                    GoalsScroller.ReloadData();
                    AddPhrasesToVoice(shortTerm_VoicePhrases);
                    break;

                case SessionFirebaseRequests.DownloadPatientTreatGoals_LongTerm_File:

                    firebaseResponses[firebaseRequest].operationStatus = FirebaseOperationStatus.Idle;
                    SetFile(firebaseResponses[firebaseRequest].content);

                    TreatmentGoals.selected_LongTerm_goals_Orig = Session.Instance.TreatmentGoals_LongTerm;

                    longTerm_VoicePhrases = new Dictionary<string, System.Action>();
                    GoalsScroller.ReloadData();
                    AddPhrasesToVoice(longTerm_VoicePhrases);
                    break;
            }
        }

        firebaseResponses[firebaseRequest] = new FirebaseOperation();

        nextBtn.AddListener(NextBtn_pressed);
        backBtn.AddListener(BackBtn_pressed);
    }

    void AddToVoiceDictionary(string phrase, System.Action action)
    {
        if (TreatmentGoals.currentGoalsIsShort)
        {
            if (shortTerm_VoicePhrases.ContainsKey(phrase))
                shortTerm_VoicePhrases[phrase] = action;
            else
                shortTerm_VoicePhrases.Add(phrase, action);
        }
        else
        {
            if (longTerm_VoicePhrases.ContainsKey(phrase))
                longTerm_VoicePhrases[phrase] = action;
            else
                longTerm_VoicePhrases.Add(phrase, action);
        }
    }

    public void NextBtn_pressed()
    {
        EnableTransitionButtons(false);
        goingBack = false;
        SaveTreatmentGoals();
    }

    public void BackBtn_pressed()
    {
        EnableTransitionButtons(false);
        goingBack = true;
        SaveTreatmentGoals();
    }

    public void EnableTransitionButtons(bool enable)
    {
        backBtn.UpdateInteractability(enable);
        nextBtn.UpdateInteractability(enable);
    }

    public void AddPhrasesToVoice(Dictionary<string, System.Action> phrasesOfPanel)
    {
        phrasesOfPanel.Add("Next", NextBtn_pressed);
        phrasesOfPanel.Add("Back", BackBtn_pressed);

        PanelVoiceController.Instance.AddSetOfPhrases(phrasesOfPanel);
    }
}