﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageField_Controller : FieldController
{
    public ImageField_Model model { get; private set; }
    public ImageField_View view { get; private set; }

    ////---Initiating References and events---//
    public ImageField_Controller(ImageField_Model model, ImageField_View view)
    {
        this.model = model;
        this.view = view;

        this.model.ImageFieldDataChanged += DataChanged_UpdateView;
    }

    /// <summary>
    /// Called when model data is changed.
    /// </summary>
    public override void DataChanged_UpdateView()
    {
        string _title = model.title;
        bool _hideTitle = model.hideTitle;
        string _imageName = model.GetImageName();

        view.SetView(_title, _hideTitle, _imageName);
    }

    public override void LoadSavedData(string _title, EntryField _model)
    {
        this.model.LoadSavedData(_title, _model);
    }
}
