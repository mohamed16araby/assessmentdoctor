﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Newtonsoft.Json;

public class TextStr
{
    [JsonProperty]
    public string name;
}

public class Item
{
    public string title;
    public string descriptionToTitle;
    public string linkingTag;
    public bool scoreTracking;
    public string path;
    public bool hideTitle;
    public string imageName;
    public string toggleImageName;
    public int number;
    public int gridColumnsCount;
    public float gridXSpace;

    public List<string> trackToggleChoice;
    public List<string> viewDependingOn;

    public bool toggleShouldBeOn_SectionsViews;
    public int scoreWeight;
    public bool singleSelection;
    public List<Item> values = new List<Item>();
    public List<Item> selectedValues = new List<Item>();

    /// <summary>
    /// Get item in selected values that has the same title.
    /// </summary>
    /// <param name="title"></param>
    /// <returns></returns>
    public Item this[string title] { get => selectedValues.Find(v => v.title == title); }

    #region Conversions between ToggleNested_Model and Item(mid class to use it with ToggleNested_View)
    public static explicit operator Item(Toggles_Model model)
    {
        Item item = new Item
        {
            title = model.title,
            descriptionToTitle = model.descriptionToTitle,
            linkingTag = model.linkingTag,
            scoreTracking = model.scoreTracking,
            hideTitle = model.hideTitle,
            path = model.path,
            imageName = model.imageName,
            toggleImageName = model.toggleImageName,
            number = model.number,
            gridColumnsCount = model.gridColumnsCount,
            gridXSpace = model.gridXSpace,
            toggleShouldBeOn_SectionsViews = model.toggleShouldBeOn_SectionsViews,
            trackToggleChoice = model.trackToggleChoice,
            viewDependingOn = model.viewDependingOn,
            scoreWeight = model.scoreWeight,
            singleSelection = model.singleSelection
        };

        if (model.values != null)
            foreach (Toggles_Model modelChild in model.values)
                item.values.Add((Item)modelChild);

        if (model.selectedValues != null)
            foreach (Toggles_Model modelChild in model.selectedValues)
                item.selectedValues.Add((Item)modelChild);

        return item;
    }
    public static explicit operator Toggles_Model(Item item)
    {
        Toggles_Model model = new Toggles_Model
        {
            title = item.title,
            descriptionToTitle = item.descriptionToTitle,
            linkingTag = item.linkingTag,
            scoreTracking = item.scoreTracking,
            hideTitle = item.hideTitle,
            path = item.path,
            imageName = item.imageName,
            toggleImageName = item.toggleImageName,
            number = item.number,
            gridColumnsCount = item.gridColumnsCount,
            gridXSpace = item.gridXSpace,
            viewDependingOn = item.viewDependingOn,
            toggleShouldBeOn_SectionsViews = item.toggleShouldBeOn_SectionsViews,
            trackToggleChoice = item.trackToggleChoice,
            scoreWeight = item.scoreWeight,
            singleSelection = item.singleSelection
        };

        if (item.values != null)
            foreach (Item itemChild in item.values)
                model.values.Add((Toggles_Model)itemChild);

        if (item.selectedValues != null)
            foreach (Item itemChild in item.selectedValues)
                model.selectedValues.Add((Toggles_Model)itemChild);

        return model;
    }
    #endregion
}

public interface IListen
{
    Dictionary<string, System.Action> GetPhrasesWithTheirActions();
    bool ReadyToListen();
}

public interface IScore
{
    void PassScoreFunctions(System.Action<int, System.Action<int, int>> modifyScore, System.Action<int, int> updateGroupScoresResults);
}

public class Toggles_Controller : FieldController, IListen, IScore
{
    public Toggles_Model model { get; private set; }
    public Toggles_View view { get; private set; }

    ////---Initiating References and events---//
    public Toggles_Controller(Toggles_Model _model, Toggles_View _view)
    {
        this.model = _model;
        this.view = _view;
        this.model.OnToggleDataChanged += DataChanged_UpdateView;
        this.view.OnToggleSelectionChanged += DataChanged_UpdateModel;
        this.view.ToggleChoiceChanged_ApplyToSections += ToggleChoiceChanged_ApplyToSections;
        //this.view.OnToggleSelectionChanged += ToggleChoiceChanged_ApplyToSections;
        this.view.GetItemFromSelectedValues += GetUpdatedItem;
        this.view.AddToggleTrackable += AddToggleTrackable;
    }

    //---Model Controlling---//
    /// <summary>
    /// Calling this will fire the ToggleDataChanged event.
    /// </summary>
    /// <param name="_title"></param>
    /// <param name="_model"></param>
    public override void LoadSavedData(string _title, EntryField _model)
    {
        this.model.LoadSavedData(_title, _model);
    }

    /// <summary>
    /// Called when there is a change in toggle selections.
    /// </summary>
    /// <param name="_item"></param>
    /// <param name="_val"></param>
    public void DataChanged_UpdateModel(bool _val, Item _item)
    {
        Toggles_Model _model = (Toggles_Model)_item;
        if (_val)
            this.model.AddValue(_model);
        else
            this.model.RemoveValue(_model);
    }

    bool viewIsDone_VoiceReady = false;

    //---View Controlling----//
    /// <summary>
    /// Called when model data is changed.
    /// </summary>
    public override void DataChanged_UpdateView() //When modelvalues and selected values get initiated
    {
        Item item = (Item)model;
        view.SetView(item);
        viewIsDone_VoiceReady = true;
    }

    [System.Reflection.Obfuscation]
    public Dictionary<string, System.Action> GetPhrasesWithTheirActions()
    {
        return view.togglePhrasesActions_Voice;
    }

    [System.Reflection.Obfuscation]
    public bool ReadyToListen()
    {
        return viewIsDone_VoiceReady;
    }

    [System.Reflection.Obfuscation]
    public void PassScoreFunctions(System.Action<int, System.Action<int, int>> modifyScores, System.Action<int, int> updateGroupScoresResults)
    {
        view.PassScoreFunctions(modifyScores, updateGroupScoresResults);
    }

    #region Sections views controlled from toggle selection
    /// <summary>
    /// Toggle responsible for sections views as key, and the sections HideView functions as value.
    /// </summary>
    public Dictionary<List<string>, List<System.Action<string, bool, bool>>> sectionsVisibilityDependingOnToggleChoice = new Dictionary<List<string>, List<System.Action<string, bool, bool>>>();

    /// <summary>
    /// Contains toggles names and their toggleAssess prefabs to track the toggle choices.
    /// </summary>
    Dictionary<string, Dictionary<bool, ToggleAssess>> trackable_ToggleAssesses = new Dictionary<string, Dictionary<bool,ToggleAssess>>();

    public KeyValuePair<string, Dictionary<bool, ToggleAssess>> GetSelectedToggle()
    {
        KeyValuePair<string, Dictionary<bool, ToggleAssess>> keyValuePair = new KeyValuePair<string, Dictionary<bool, ToggleAssess>>();

        foreach (KeyValuePair<string, Dictionary<bool, ToggleAssess>> keyValue in trackable_ToggleAssesses)
        {
            if (keyValue.Value.Values.ToList()[0].ToggleOn) return keyValue;
            keyValuePair = keyValue;
        }

        return keyValuePair;
    }

    /// <summary>
    /// Gets called after any toggle (responsible for this section ) selection change.
    /// </summary>
    /// <param name="toggleChoiceShouldBeOn_ToViewSections">Whether toggle choice should be on/off for sections to be viewed</param>
    /// <param name="_isOn"></param>
    public void HideViewSectionsDependinOnToggleChoice(string toggleName, bool toggleChoiceShouldBeOn_ToViewSections, bool _isOn)
    {
        view.gameObject.SetActive(toggleChoiceShouldBeOn_ToViewSections == _isOn);

        if (toggleChoiceShouldBeOn_ToViewSections != _isOn)
            foreach (List<ToggleAssess> toggleAssesses in view.sectionsViewDependinOnChoice.Values) //remove toggles selections from sections
                foreach (ToggleAssess toggleAssess in toggleAssesses)
                    toggleAssess.ToggleOn = false;
    }

    /// <summary>
    /// Add toggle to a dictionary to track its choice (if this toggle name match trackToggleChoice variable).
    /// </summary>
    /// <param name="toggleName"></param>
    /// <param name="toggleAssess"></param>
    void AddToggleTrackable(string toggleName, bool toggleShouldBeOn, ToggleAssess toggleAssess)
    {
        if (trackable_ToggleAssesses.ContainsKey(toggleName))
            trackable_ToggleAssesses[toggleName] = new Dictionary<bool, ToggleAssess>() { { toggleShouldBeOn, toggleAssess } };
        else
            trackable_ToggleAssesses.Add(toggleName, new Dictionary<bool, ToggleAssess>() { { toggleShouldBeOn, toggleAssess } });
    }

    /// <summary>
    /// Gets called after every toggle change (if this toggle name match trackToggleChoice variable) to call the HideView functions of each section.
    /// </summary>
    /// <param name="toggleName"></param>
    /// <param name="_isOn"></param>
    public void ToggleChoiceChanged_ApplyToSections(string toggleName, bool toggleShouldBeOn, bool _isOn)
    {
        if (!view.trackToggleChoice.Contains(toggleName)) return;

        foreach (KeyValuePair<List<string>, List<System.Action<string, bool, bool>>> keyValuePair in sectionsVisibilityDependingOnToggleChoice)
            if (keyValuePair.Key.Contains(toggleName))
                foreach (System.Action<string, bool, bool> HideViewSectionDependinOnToggleChoice in keyValuePair.Value)
                    HideViewSectionDependinOnToggleChoice(toggleName, toggleShouldBeOn, _isOn);
    }
    #endregion

    Item GetUpdatedItem(Item item) //Event respond gets called before instantiating toggle children to update selectedValues of model data
    {
        Toggles_Model updatedModel;
        if (item.title == model.title)
            updatedModel = model;
        else
        {
            Toggles_Model modelP = model.FindModelParent(model, (Toggles_Model)item);
            updatedModel = modelP.selectedValues.Find(m => m.title == item.title);
        }
        Item itemUpdated = (Item)updatedModel;
        return itemUpdated;
    }
}