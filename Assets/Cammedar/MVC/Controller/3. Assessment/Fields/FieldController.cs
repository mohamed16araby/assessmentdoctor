﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class FieldController : MonoBehaviour
{
    public string linkingTag;

    [System.Reflection.Obfuscation]
    public abstract void LoadSavedData(string _title, EntryField _model);
    public abstract void DataChanged_UpdateView();
}
