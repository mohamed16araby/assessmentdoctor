﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextField_Controller : FieldController
{
    public TextField_Model model { get; private set; }
    public TextField_View view { get; private set; }

    ////---Initiating References and events---//
    public TextField_Controller(TextField_Model model, TextField_View view)
    {
        this.model = model;
        this.view = view;

        this.model.TextFieldDataChanged += DataChanged_UpdateView;
    }

    /// <summary>
    /// Called when model data is changed.
    /// </summary>
    public override void DataChanged_UpdateView()
    {
        string _title = model.title;
        bool _hideTitle = model.hideTitle;
        string _text = model.GetText();

        view.SetView(_title, _hideTitle, _text);
    }

    public override void LoadSavedData(string _title, EntryField _model)
    {
        this.model.LoadSavedData(_title, _model);
    }
}
