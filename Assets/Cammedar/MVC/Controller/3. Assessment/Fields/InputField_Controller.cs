﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputField_Controller : FieldController
{
    public InputField_Model model { get; private set; }
    public InputField_View view { get; private set; }

    ////---Initiating References and events---//
    public InputField_Controller(InputField_Model model, InputField_View view)
    {
        this.model = model;
        this.view = view;

        this.model.InputFieldDataChanged += DataChanged_UpdateView;
        this.view.OnInputFieldChanged += DataChanged_UpdateModel;
    }

    /// <summary>
    /// Called when model data is changed.
    /// </summary>
    public override void DataChanged_UpdateView()
    {
        string _title = model.title;
        bool _hideTitle = model.hideTitle;
        List<string> _value = model.value;
        //string _value = model.value;
        string _hintValue = model.hintValue;

        view.SetView(_title, _hideTitle, _value, _hintValue);
    }

    /// <summary>
    /// Called when input field changed
    /// </summary>
    /// <param name="_val"></param>
    public void DataChanged_UpdateModel(string _val)
    {
        this.model.RemoveValue();
        this.model.AddValue(_val);
        //this.model.SetValue(_val);
    }

    // Calling this will fire the TextFieldDataChanged event 
    public override void LoadSavedData(string _title, EntryField _model)
    {
        this.model.LoadSavedData(_title, _model);
    }
}
