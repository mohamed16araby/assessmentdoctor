﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;

/// <typeparam name="T">T is the type of panel view</typeparam>
public abstract class Panel_Controller<T> where T: Panel_2MainHeaders_View
{
    public Panel_2MainHeaders_Model model { get; private set; }
    public T view { get; private set; }

    ////---Initiating References and events---//
    public Panel_Controller(Panel_2MainHeaders_Model model, T view)
    {
        this.model = model;
        this.view = view;

        this.model.OnPanelDataChanged += DataChanged_UpdateView;
    }

    /// <summary>
    /// Called when model data is changed.
    /// </summary>
    public abstract void DataChanged_UpdateView();

    // Calling this will fire the OnPanelDataChanged event 
    public void SetData(Panel _model, MonoBehaviour mono = null)
    {
        this.model.SetData(_model, mono);
    }
}