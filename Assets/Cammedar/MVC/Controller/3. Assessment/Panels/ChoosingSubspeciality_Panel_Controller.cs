﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using System.Reflection;

public class ChoosingSubspeciality_Panel_Controller : Panel_Controller<ChoosingSubspeciality_Panel_View>
{
    ChoosingSubspeciality_Panel_Model ChoosingSubspeciality_Panel_Model
    {
        get => model as ChoosingSubspeciality_Panel_Model;
    }
    public ChoosingSubspeciality_Panel_Controller(Panel_2MainHeaders_Model model, ChoosingSubspeciality_Panel_View view) : base(model, view){}

    public override void DataChanged_UpdateView()
    {
        Dictionary<string, List<string>> subspecialitiesList = ChoosingSubspeciality_Panel_Model.subspecialitiesList;
        view.SetView(model.groupName, model.panelName, model.hideTitle, subspecialitiesList, Set_Subspeciality_UnderneathChoice);
    }

    public void Set_Subspeciality_UnderneathChoice(KeyValuePair<string, string> keyValuePair, bool isOn)
    {
        if (isOn)
            ChoosingSubspeciality_Panel_Model.keyValueOfSelectedChoiceUnderneath = keyValuePair;

        else
            ChoosingSubspeciality_Panel_Model.keyValueOfSelectedChoiceUnderneath = new KeyValuePair<string, string>();


        ChoosingSubspeciality_Panel_Manager.Instance.UpdateSubspecialities();
    }

    public KeyValuePair<string, string> GetSubspecialityChoice()
    {
        return ChoosingSubspeciality_Panel_Model.keyValueOfSelectedChoiceUnderneath;
    }

    public void ViewVisibility(bool _show)
    {
        view.ViewVisibility(_show);
    }
}