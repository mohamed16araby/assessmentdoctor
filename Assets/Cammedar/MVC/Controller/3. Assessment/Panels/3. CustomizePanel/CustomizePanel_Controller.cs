﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using System.Reflection;

public class CustomizePanel_Controller : Panel_Controller<CustomizePanel_View>
{
    //public delegate void CustomizationDoneEvent(Dictionary<CustomizeItem, ITransit> _CustomGroupDictionary);
    //public event CustomizationDoneEvent OnCustomizationItemsInstantiated;
    public static ArrayList Data = new ArrayList();
    public Action<Dictionary<CustomizeItem, ITransit>> OnCustomizationItemsInstantiated;
    /// <summary>
    /// Constructor passing values to Panel_Controller constructor.
    /// </summary>
    /// <param name="model"></param>
    /// <param name="view"></param>
    public CustomizePanel_Controller(Panel_2MainHeaders_Model model, CustomizePanel_View view) : base(model, view)
    {
       // OnCustomizationItemsInstantiated += SetCustomizationDictionaryData_UpdateModel;
    }

    /// <summary>
    /// Called when model data is changed.
    /// </summary>
    public override void DataChanged_UpdateView()
    {

        view.SetView(model.groupName, model.panelName, model.hideTitle);
        Dictionary<CustomizeItem, ITransit> _CustomGroupDictionary = InstantiateCustomizationItems();

        OnCustomizationItemsInstantiated?.Invoke(_CustomGroupDictionary); //for algorithm to start
        SetCustomizationDictionaryData_UpdateModel(_CustomGroupDictionary);
        view.RaisePanel_isReadyTobeViewed();
    }

    void SetCustomizationDictionaryData_UpdateModel(Dictionary<CustomizeItem, ITransit> _CustomGroupDictionary)
    {
        (model as CustomizePanel_Model).CustomizeGroup = _CustomGroupDictionary;
    }

    private Dictionary<CustomizeItem, ITransit> InstantiateCustomizationItems()
    {
        Dictionary<CustomizeItem, ITransit> customizationItems_Appearence = new Dictionary<CustomizeItem, ITransit>();

        CustomizePanel_Model customizePanel_Model = model as CustomizePanel_Model;
        Data.Clear();
        customizationItems_Appearence = GenerateItems<Group>(customizePanel_Model.CurrentGroups.SortedView, null);

        return customizationItems_Appearence;
    }

    Dictionary<CustomizeItem, ITransit> GenerateItems<T>(List<KeyValuePair<int, List<ITransit>>> dictionary, CustomizeItem parent, bool hideTests = false)
    {
        Dictionary<CustomizeItem, ITransit> returnedDictionary = new Dictionary<CustomizeItem, ITransit>();
        bool addedOneSide = false;
        foreach (KeyValuePair<int, List<ITransit>> keyValuePair in dictionary)
        {
            CustomizeItem customChild = null;

            if (typeof(T) == typeof(Group))
            {
                Group group = keyValuePair.Value[0] as Group;
                customChild = view.AddItem(group.name, group, parent);
                if (!Data.Contains(group.name))
                    Data.Add(group.name);
                GenerateItems<Panel>(group.Panels.SortedView, customChild);
                returnedDictionary.Add(customChild, group);
            }
            else if (typeof(T) == typeof(Panel))
            {
                Panel panel = keyValuePair.Value[0] as Panel;
                customChild = view.AddItem(panel.name, panel, parent, (panel.hideTitle == true));
                    if (!Data.Contains(panel.name))
                        Data.Add(panel.name);
                GenerateItems<Section>(panel.Sections.SortedView, customChild);
            }
            else if (typeof(T) == typeof(Section))
            {
                Section section = keyValuePair.Value[0] as Section;
                customChild = view.AddItem(section.name, section, parent, section.hideTitleInCustomizationPanel);
                if (!Data.Contains(section.name))
                    Data.Add(section.name);
                GenerateItems<Side>(section.Sides.SortedView, customChild);
            }
            else if (typeof(T) == typeof(Side))
            {
                //Side side = keyValuePair.Value[0] as Side;
                //customChild = view.AddItem(side.name, side, parent, true);

                if (!addedOneSide)
                {
                    Side side = keyValuePair.Value[0] as Side;
                    customChild = view.AddItem(side.name, side, parent, true);
                    addedOneSide = true;
                    GenerateItems<EntryField>(side.Tests.SortedView, customChild);
                }
                //else
                //{
                //    GenerateItems<EntryField>(side.Tests.SortedView, customChild, true);
                //}
            }
            else if (typeof(T) == typeof(EntryField))
            {
                foreach (EntryField entry in (keyValuePair.Value as List<ITransit>))
                {
                    customChild = view.AddItem(entry.title, entry, parent, entry.hideTitleInCustomizationPanel || hideTests);
                }
            }
        }
        return returnedDictionary;
    }
}