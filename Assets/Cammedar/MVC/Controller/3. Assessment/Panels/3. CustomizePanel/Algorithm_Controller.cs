﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Algorithm_Controller
{
    public Algorithm_Model model { get; private set; }
    public Algorithm_View view { get; private set; }

    ////---Initiating References and events---//
    public Algorithm_Controller(Algorithm_Model model, Algorithm_View view)
    {
        this.model = model;
        this.view = view;

        this.model.On_LastVisitLoadedEvent += LoadPatientLastVisit;
        this.model.On_AlgorithmsLoadedEvent += DataChanged_UpdateView;
        this.model.On_Save_OperationCallBackEvent += ViewSaveCallBack;
        this.view.OnSaveAlgorithmEvent += SaveAlgorithm;
    }

    public void SetData(List<CustomizeItem> currentCustomizeItems, Dictionary<string, List<CustomizeItemSerializable>> patientVisitAlgorithm, bool hideView)
    {
        model.Init(currentCustomizeItems, patientVisitAlgorithm);

        if (!hideView)
            view.Init(LoadAlgorithm, DeleteAlgorithm);
        else
            view.HideView();
    }

    public void DataChanged_UpdateView()
    {
        Dictionary<string, List<CustomizeItemSerializable>> algorithmFilesNames = this.model.algorithmFilesNames;
        view.SetView(algorithmFilesNames.Keys.ToList());
    }

    public void SaveAlgorithm(string _filename, bool _patientLastVisit = false)
    {

        List<CustomizeItem> customizeItems = model.currentSelectedItems;

        List<CustomizeItemSerializable> customizeItemsSerializable = SerializeCustomizeItems(customizeItems);
       
        model.SaveAlgorithm_OnClick(_filename, customizeItemsSerializable, _patientLastVisit);

    }

    void ViewSaveCallBack(OperationState state, string _message)
    {
        view.ViewSaveCallBack(state, _message);
    }

    void LoadAlgorithm(string _filename, bool _patientLastVisit = false)
    {
        List<CustomizeItemSerializable> customizeItemsSerializable = model.LoadAlgorithm_OnClick(_filename, _patientLastVisit);
        SelectLoadedTests(customizeItemsSerializable, model.currentSelectedItems);
    }

    void DeleteAlgorithm(string _filename)
    {
        model.DeleteAlgorithm_OnClick(_filename);
    }

    void LoadPatientLastVisit()
    {
        LoadAlgorithm("patientVisitAlgorithm", true);
    }

    public void SelectLoadedTests(List<CustomizeItemSerializable> items, List<CustomizeItem> customItems)
    {
        foreach (CustomizeItem customItem in customItems)
        {
            if (items.Exists(i => i.name == customItem.title.text))
            {
                CustomizeItemSerializable item = items.Find(i => i.name == customItem.title.text);
                switch (item.checkType)
                {
                    case TristateCheck.FullChecked:
                        if (customItem.CheckType != TristateCheck.FullChecked)
                        {
                            if (customItem.CheckType == TristateCheck.SemiChecked)
                                customItem.OnCheckboxChange();
                            customItem.OnCheckboxChange();
                        }
                        break;

                    case TristateCheck.SemiChecked:
                        SelectLoadedTests(item.children, customItem.childrenDictionary.Keys.ToList());
                        break;

                    case TristateCheck.NoneChecked:
                        if (customItem.CheckType != TristateCheck.NoneChecked)
                            customItem.OnCheckboxChange();
                        break;
                }
            }
            else
            {
                if (customItem.CheckType != TristateCheck.NoneChecked)
                    customItem.OnCheckboxChange(); //uncheck this customItem
            }
        }
    }

    public List<CustomizeItemSerializable> SerializeCustomizeItems(List<CustomizeItem> customizeItems)
    {
        List<CustomizeItemSerializable> customizeItemsSerializable = new List<CustomizeItemSerializable>();
        foreach (CustomizeItem customizeItem in customizeItems)
            if (customizeItem.CheckType != TristateCheck.NoneChecked)
                customizeItemsSerializable.Add((CustomizeItemSerializable)customizeItem);

        return customizeItemsSerializable;
    }
}

public class CustomizeItemSerializable
{
    public string name;
    public TristateCheck checkType;
    public List<CustomizeItemSerializable> children = new List<CustomizeItemSerializable>();

    public static explicit operator CustomizeItemSerializable(CustomizeItem customizeItem)
    {
        CustomizeItemSerializable customizeItemSerializable = new CustomizeItemSerializable
        {
            name = customizeItem.title.text,
            checkType = customizeItem.CheckType,
        };

        foreach (CustomizeItem customizeItemChild in customizeItem.childrenDictionary.Keys)
            if (customizeItemChild.CheckType != TristateCheck.NoneChecked)
                customizeItemSerializable.children.Add((CustomizeItemSerializable)customizeItemChild);

        return customizeItemSerializable;
    }
}
