﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using System.Reflection;

public class SidePanel_Controller : Panel_Controller<SidePanel_View>
{
    /// <summary>
    /// Constructor passing values to Panel_Controller constructor.
    /// </summary>
    /// <param name="model"></param>
    /// <param name="view"></param>
    public SidePanel_Controller(Panel_2MainHeaders_Model model, SidePanel_View view) :base(model, view) { }

    /// <summary>
    /// Called when model data is changed.
    /// </summary>
    public override void DataChanged_UpdateView()
    {
        SidePanel_Model panel_Model = model as SidePanel_Model;

        view.SetView(panel_Model.groupName, panel_Model.panelName, panel_Model.sectionName, panel_Model.sideName, panel_Model.hideTitle, panel_Model.hideSectionTitle, GetQuestionsHeadersAppearence());
    }

    private Dictionary<string, bool> GetQuestionsHeadersAppearence()
    {
        Dictionary<string, bool> questionsHeaders_Appearence = new Dictionary<string, bool>();

        SidePanel_Model sidePanel_Model = model as SidePanel_Model;

        foreach (KeyValuePair<int, List<ITransit>> field in sidePanel_Model.Questions)
            foreach (ITransit question in field.Value)
                questionsHeaders_Appearence.Add((question as EntryField).title, (question as EntryField).hideTitle);

        return questionsHeaders_Appearence;
    }
}