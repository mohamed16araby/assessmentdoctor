﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using System.Reflection;

public class AddEdit_MedicalRecord_Panel_Controller : Panel_Controller<AddEdit_MedicalRecord_Panel_View>
{
    public AddEdit_MedicalRecord_Panel_Controller(Panel_2MainHeaders_Model model, AddEdit_MedicalRecord_Panel_View view) : base(model, view)
    {
        view.AddEdit_MedicalRecordChoiceChangedEvent += SetAddEdit_MedicalRecordChoice;
        //view.AddEdit_UnderneathChoiceChangedEvent += SetChoiceUnderneath;
    }

    public override void DataChanged_UpdateView()
    {
        AddEdit_MedicalRecord_Panel_Model addEdit_MedicalRecord_Panel_Model = model as AddEdit_MedicalRecord_Panel_Model;

        List<PerformAnotherAssessmentChoices> performAnotherAssessmentChoices = addEdit_MedicalRecord_Panel_Model.performAnotherAssessmentChoices;
        Dictionary<string, string> sessionIDwithDate = addEdit_MedicalRecord_Panel_Model.sessionIDwithDate;
        KeyValuePair<string, string> lastFollowupDate = addEdit_MedicalRecord_Panel_Model.lastFollowupDate;
        
        view.SetView(model.groupName, model.panelName, model.hideTitle, performAnotherAssessmentChoices, sessionIDwithDate, lastFollowupDate, Set_AddEdit_UnderneathChoice);
    }

    public void SetAddEdit_MedicalRecordChoice(string performAnotherAssessmentChoice, bool isOn)
    {
        if (!Enum.TryParse(performAnotherAssessmentChoice, out PerformAnotherAssessmentChoices performAnotherAssessmentChoiceChoosen)) return;

        AddEdit_MedicalRecord_Panel_Model addEdit_MedicalRecord_Panel_Model = model as AddEdit_MedicalRecord_Panel_Model;

        if (isOn)
            addEdit_MedicalRecord_Panel_Model.performAnotherAssessmentChoiceChoosen = performAnotherAssessmentChoiceChoosen;

        else
            addEdit_MedicalRecord_Panel_Model.performAnotherAssessmentChoiceChoosen = PerformAnotherAssessmentChoices.None;
    }

    public void Set_AddEdit_UnderneathChoice(KeyValuePair<string, string> keyValue, bool isOn)
    {
        AddEdit_MedicalRecord_Panel_Model addEdit_MedicalRecord_Panel_Model = model as AddEdit_MedicalRecord_Panel_Model;

        if (isOn)
            addEdit_MedicalRecord_Panel_Model.keyValueOfSelectedChoiceUnderneath = keyValue;

        else if (addEdit_MedicalRecord_Panel_Model.keyValueOfSelectedChoiceUnderneath.Key == keyValue.Key) // id
            addEdit_MedicalRecord_Panel_Model.keyValueOfSelectedChoiceUnderneath = new KeyValuePair<string, string>();
    }

    public KeyValuePair<PerformAnotherAssessmentChoices, KeyValuePair<string, string>> GetAddEdit_MedicalRecordChoice()
    {
        AddEdit_MedicalRecord_Panel_Model addEdit_MedicalRecord_Panel_Model = model as AddEdit_MedicalRecord_Panel_Model;

        KeyValuePair<PerformAnotherAssessmentChoices, KeyValuePair<string, string>> keyValuePair = 
            new KeyValuePair<PerformAnotherAssessmentChoices, KeyValuePair<string, string>>
            (addEdit_MedicalRecord_Panel_Model.performAnotherAssessmentChoiceChoosen, addEdit_MedicalRecord_Panel_Model.keyValueOfSelectedChoiceUnderneath);

        return keyValuePair;
    }

    public string GetLastSession_Id()
    {
        AddEdit_MedicalRecord_Panel_Model addEdit_MedicalRecord_Panel_Model = model as AddEdit_MedicalRecord_Panel_Model;
        return addEdit_MedicalRecord_Panel_Model.LastSession_Id;
    }
}