﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using System.Reflection;

public class ChoosingSides_Panel_Controller : Panel_Controller<ChoosingSides_Panel_View>
{
    public Dictionary<string, System.Action> phraseAction = new Dictionary<string, Action>();

    public ChoosingSides_Panel_Controller(Panel_2MainHeaders_Model model, ChoosingSides_Panel_View view) : base(model, view)
    {
       view.SideToAssessChangedEvent += SetSideToAssess;
    }

    public override void DataChanged_UpdateView()
    {
        List<string> sides_Assessment = (model as ChoosingSides_Panel_Model).sides_Assessment;
       view.SetView(model.groupName, model.panelName, model.hideTitle, sides_Assessment);

        Prepare_Voice_phraseAction();
    }

    public void SetSideToAssess(string side)
    {
        if (!Enum.TryParse(side, out Sides sideToAssess)) return;

        (model as ChoosingSides_Panel_Model).sideToAssess = sideToAssess;
    }

    public Sides GetSideToAssess()
    {
        return (model as ChoosingSides_Panel_Model).sideToAssess;
    }

    public void Prepare_Voice_phraseAction()
    {
        phraseAction = new Dictionary<string, Action>();
        foreach (KeyValuePair<string, ToggleCircled> keyValuePair in view.allTogglesSelections)
        {
            phraseAction.Add(keyValuePair.Key, keyValuePair.Value.SwitchToggleSelection);

        }
    }
}