﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using System.Reflection;
using System;

public class MainPanel_Controller : Panel_Controller<MainPanel_View>
{
    public Dictionary<string, Action> panelPhrasesActions_Voice = new Dictionary<string, Action>();
    public bool PanelIsDone_VoiceReady { set; get; }

    /// <summary>
    /// Contains sections visibility functions as value, and their controlled toggles names as key.
    /// </summary>
    Dictionary<List<string>, List<Action<string, bool, bool>>> sectionsVisibilityDependingOnToggleChoice = new Dictionary<List<string>, List<Action<string, bool, bool>>>();

    /// <summary>
    /// Stores toggles responsible names and their controllers.
    /// </summary>
    Dictionary<List<string>, Toggles_Controller> togglesResponsibleForOtherTogglesViews = new Dictionary<List<string>, Toggles_Controller>();

    /// <summary>
    /// Constructor passing values to Panel_Controller constructor.
    /// </summary>
    /// <param name="model"></param>
    /// <param name="view"></param>
    public MainPanel_Controller(Panel_2MainHeaders_Model model, MainPanel_View view) :base(model, view)
    {
        panelPhrasesActions_Voice = new Dictionary<string, Action>();
        PanelIsDone_VoiceReady = false;
    }

    /// <summary>
    /// Called when model data is changed.
    /// </summary>
    public override void DataChanged_UpdateView()
    {
        MainPanel_Model panel_Model = model as MainPanel_Model;

        view.SetView(panel_Model.groupName, panel_Model.panelName, panel_Model.sectionName, panel_Model.sideName, panel_Model.hideTitle, panel_Model.hideSectionTitle,
                     panel_Model.currentGroup.scoreTotal.ToString(), panel_Model.currentGroup.scoreResult.ToString(), panel_Model.sectionScoreTotal.ToString(), 
                     panel_Model.sectionScoreResult.ToString(), panel_Model.UpdateScoresResults, panel_Model.scoreTracking);
        SetPanel_Questions_Score_Voice();
    }

    public void SetPanel_Questions_Score_Voice()
    {
        MainPanel_Model mainPanel_Model = model as MainPanel_Model;

        List<ITransit> listOfEntryFields = mainPanel_Model.Questions.SortedView.ToList()[0].Value;
        for (int i=0;  i< listOfEntryFields.Count; i++)
        {
            EntryField entryField = listOfEntryFields[i] as EntryField;

            /// ---Invoking LoadSavedData method based on controller type which we will get based on element mvc type-- -///
            Element_MVC element_MVC = Manager.Instance.taggedMVCDict[entryField.GetLinkingTag()]; //Get mvc element of this field based on its linking tag.
            element_MVC.Initialize(view.GetContentHolderTransform()); //Instantiate mvc component and make view a child to questions holder.

            PropertyInfo controllerProp = element_MVC.GetType().GetProperty("controller");
            object controllerValue = controllerProp.GetValue(element_MVC); //return controller value of element_MVC
            Type controllerType = controllerProp.PropertyType;

            CollectTogglesAndSectionsReliableOnThem(controllerType, controllerValue, entryField);
            LoadSavedData(controllerType, controllerValue, entryField);
            PassScoreFunctions(controllerType, controllerValue, new object[] { new Action<int, Action<int, int>>( view.scores.ModifyScores),
                                                                              new Action<int, int> (mainPanel_Model.UpdateScoresResults) });

            PanelIsDone_VoiceReady = PanelIsReady_For_VoiceRecogn(controllerType, controllerValue, i, listOfEntryFields.Count);
        }

        UpdateSectionsViews();
    }

    #region Sections views controlled from toggle selection
    /// <summary>
    /// Build 2 dictionaries one for toggles responsible for other sections views, and their controllers and other, for sections with their controlled toggle name.
    /// </summary>
    /// <param name="controllerType"></param>
    /// <param name="controllerValue"></param>
    /// <param name="entryField"></param>
    void CollectTogglesAndSectionsReliableOnThem(Type controllerType, object controllerValue, EntryField entryField)
    {
        if (controllerType != typeof(Toggles_Controller)) return;

        Toggles_Controller controller = controllerValue as Toggles_Controller;
        Toggles_Model model = (Toggles_Model)entryField;
        if (model.trackToggleChoice.Count != 0)
        {
            List<string> togglesNames = DictionaryContainsList(togglesResponsibleForOtherTogglesViews, model.trackToggleChoice);
                if(togglesNames.Count > 0)
                togglesResponsibleForOtherTogglesViews[togglesNames] = controller;
            else
                togglesResponsibleForOtherTogglesViews.Add(model.trackToggleChoice, controller);
        }
        if (model.viewDependingOn.Count != 0)
        {
            List<string> togglesNames = DictionaryContainsList(sectionsVisibilityDependingOnToggleChoice, model.viewDependingOn);
            if (togglesNames.Count > 0)
                sectionsVisibilityDependingOnToggleChoice[togglesNames].Add(controller.HideViewSectionsDependinOnToggleChoice);

            else
                sectionsVisibilityDependingOnToggleChoice.Add(model.viewDependingOn, new List<Action<string, bool, bool>>() { controller.HideViewSectionsDependinOnToggleChoice });
        }

    }

    /// <summary>
    /// After loading data of toggles views, then iterating between them and check the toggle selection that is responsible for their views and behave accordingly.
    /// </summary>
    void UpdateSectionsViews()
    {
        foreach (KeyValuePair<List<string>, List<Action<string, bool, bool>>> keyValue in sectionsVisibilityDependingOnToggleChoice)
        {
            List<string> togglesNames = DictionaryContainsList(togglesResponsibleForOtherTogglesViews, keyValue.Key);
            if (togglesNames.Count >0)
            {
                Toggles_Controller toggles_Controller = togglesResponsibleForOtherTogglesViews[togglesNames];
                toggles_Controller.sectionsVisibilityDependingOnToggleChoice[keyValue.Key] = keyValue.Value;

                KeyValuePair<string, Dictionary<bool, ToggleAssess>> keyValueToggle = toggles_Controller.GetSelectedToggle();
                if(!string.IsNullOrEmpty(keyValueToggle.Key))
                    toggles_Controller.ToggleChoiceChanged_ApplyToSections(keyValueToggle.Key, keyValueToggle.Value.Keys.ToList()[0], keyValueToggle.Value.Values.ToList()[0].ToggleOn);
            }
        }
    }

    /// <summary>
    /// Search for list in dictionary keys and return it if found.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="dict"></param>
    /// <param name="list"></param>
    /// <returns></returns>
    public static List<string> DictionaryContainsList<T>(Dictionary<List<string>, T> dict, List<string> list)
    {
        if (list == null || list.Count == 0) return new List<string>();

        foreach (List<string> dictList in dict.Keys)
        {
            int foundNoOfItems = 0;
            foreach (string str in list)
                if (dictList.Contains(str)) foundNoOfItems++;

            if (foundNoOfItems == list.Count) return dictList;
        }

        return new List<string>();
    }
    #endregion

    #region LoadSavedData
    void LoadSavedData(Type objectType, object _object, EntryField entryField)
    {
        InvokeMethodWithReflection("LoadSavedData", objectType, _object, new object[] { entryField.title, entryField });//Calling LoadSavedData method from controllerValue object 
                                                                                                                        //with entryField.title, entryField as function parameters.
    }
    #endregion

    #region Score Tracking
    void PassScoreFunctions(Type objectType, object _object, params object[] parameters)
    {
        if (!typeof(IScore).IsAssignableFrom(objectType)) return;

        InvokeMethodWithReflection("PassScoreFunctions", objectType, _object, parameters);
    }
    #endregion

    #region Reflection
    object InvokeMethodWithReflection(string methodStr, Type objectType, object _object, params object[] parameters)
    {
        MethodInfo methodInfo = objectType.GetMethod(methodStr);
        return methodInfo.Invoke(_object, parameters);
    }
    #endregion

    #region Voice Recognition
    bool PanelIsReady_For_VoiceRecogn(Type objectType, object _object, int current_Index, int EntryFieldsCount)
    {
        bool isLastItem = current_Index == EntryFieldsCount - 1;
        if (typeof(IListen).IsAssignableFrom(objectType))
            LoadPanelPhrasesActionsWhenReady_VoiceRecogn(objectType, _object, isLastItem);

        return isLastItem;
    }

    void LoadPanelPhrasesActionsWhenReady_VoiceRecogn(Type controllerType, object controllerValue, bool isLastItem)
    {
        view.StartCoroutine(WaitUntilPanelIsDone_LoadPhrases(controllerType, controllerValue, isLastItem));
    }

    IEnumerator WaitUntilPanelIsDone_LoadPhrases(Type controllerType, object controllerValue, bool isLastItem)
    {
        string ReadyToListen = InvokeMethodWithReflection("ReadyToListen", controllerType, controllerValue, new object[] { }).ToString();
        yield return new WaitUntil(() => bool.Parse(ReadyToListen) == true);

        Dictionary<string, Action> phrasesWithTheirActions_Dict = InvokeMethodWithReflection("GetPhrasesWithTheirActions", controllerType, controllerValue, new object[] { }) 
                                                                    as Dictionary<string, Action>;

        foreach (KeyValuePair<string, Action> phraseAction in phrasesWithTheirActions_Dict)
        {
            if (!panelPhrasesActions_Voice.ContainsKey(phraseAction.Key))
                panelPhrasesActions_Voice.Add(phraseAction.Key, phraseAction.Value);
            else
                Debug.LogError(string.Format("key {0} already exist in current phrases listeners.", phraseAction.Key));
        }

        PanelIsDone_VoiceReady = isLastItem;
    }
    #endregion
}