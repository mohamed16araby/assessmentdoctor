﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Beebyte.Obfuscator;
using UnityEngine.UI;

public class DiagPredictionsController : MonoBehaviour
{
    [SerializeField]
    Button nextBtn;
    [SerializeField]
    Button backBtn;
    [SerializeField]
    Text Subspeciality_Chosen_Txt;

    private void Start()
    {
        nextBtn.onClick.AddListener(NextBtn_pressed);
        backBtn.onClick.AddListener(BackBtn_pressed);

        Subspeciality_Chosen_Txt.text = UserData.GetLastSpeciality + " Assessment";

        AdPhrasesToVoice();
    }

    public void NextBtn_pressed()
    {
        StartCoroutine(ScenesController.Instance.LoadYourAsyncScene("Diag&predictions","ProblemsList"));
    }

    public void BackBtn_pressed()
    {
        Manager.Instance.HideSceneContent();
        SceneManager.UnloadSceneAsync("Diag&predictions");
        Manager.Instance.ViewAssessmentSceneViewContent();
        PanelVoiceController.Instance.ContinueListeningToAssessmentContent();
    }

    public void AdPhrasesToVoice()
    {
        Dictionary<string, System.Action> phrasesOfPanel = new Dictionary<string, System.Action>()
        {
            {"Next", NextBtn_pressed},
            {"Back", BackBtn_pressed},
        };

        PanelVoiceController.Instance.AddSetOfPhrases(phrasesOfPanel);
    }

    
}
