﻿using System.Collections.Generic;
using UnityEngine;
using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;
using Beebyte.Obfuscator;
using UnityEngine.SceneManagement;
using fb = Cammedar.Network.Firebase;
using URI = Cammedar.Network.URI;
using Cammedar.Network;
using System.Linq;
using Ricimi;
using Facebook.Unity;

public class SelectPatientController : MonoBehaviour, IEnhancedScrollerDelegate
{
    public EnhancedScroller PatientsScroller;
    public EnhancedScrollerCellView PatientsViewPrefab;
    public InputField SearchField;

    public AssessButton backButton;

    public PopupOpener popupOpener;
    public AddPatientView addPatientView;
    public BasicButton addBtn;

    public AssessButton nextButton;
    public GameObject HelpPopup;


    [SerializeField]
    Button helpbutton;

    private Dictionary<string, Patient> allPatients = new Dictionary<string, Patient>();
    private Dictionary<string, Patient> hospitalPatients = new Dictionary<string, Patient>();
    private Dictionary<string, Patient> Searched_patients = new Dictionary<string, Patient>();


    bool SearchActive = false;
    public Sprite Audioon;
    public Sprite AudioOff;
    [SerializeField]
    Button  AudioButton;
    GameObject AudioController;
    void Awake()
    {
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
            FB.LogAppEvent(AppEventName.ViewedContent, null, new Dictionary<string, object>()
{{ AppEventParameterName.Description, "Select Patient Screen" }});
        }
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
            FB.LogAppEvent(AppEventName.ViewedContent, null, new Dictionary<string, object>()
{{ AppEventParameterName.Description, "Select Patient Screen" }});
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }
    void Start()
    {

        addBtn.onClicked.AddListener(delegate { OpenAddPatientPanel(); });
        SearchField.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
        nextButton.AddListener(NextBtn_pressed);
        backButton.AddListener(BackBtn_pressed);
        helpbutton.onClick.AddListener(helpButtonfunction);
        AudioButton.onClick.AddListener(FVoiceController);
        ButtonsInteractability(false);

        CheckNextButtonActivation();
        PatientsScroller.Delegate = this;
        GetPatients();
        GameVoiceControl.Instance.reinitializeVoice();
       GameVoiceControl.Instance.RecognitionResult.AddListener(onReceiveRecognitionResult);


    }
    public string ToUpperEveryWord(string s)
    {
        var words = s.Split(' ');

        var t = "";
        foreach (var word in words)
        {
            t += char.ToUpper(word[0]) + word.Substring(1) + ' ';
        }
        return (t.Trim());
    }
    [SkipRename]
    public void onReceiveRecognitionResult(string result)
    {
        result = ToUpperEveryWord(result);
        if (result == "Previous") BackBtn_pressed();
      else if (result == "Next") NextBtn_pressed();
      else if (result == "Forward") NextBtn_pressed();
      else  if (result == "Help") helpButtonfunction();
      else if (result == "Close") helpPromptClose();
    }


    [SkipRename]
    public void FVoiceController()
    {
        if(AudioButton.GetComponent<Image>().sprite== Audioon)
        {
            GameVoiceControl.Instance.onStopListening();
            AudioButton.GetComponent<Image>().sprite = AudioOff;
        }
        else
        {
            GameVoiceControl.Instance.onStartListening();
            AudioButton.GetComponent<Image>().sprite = Audioon;
        }

    }

    [SkipRename]
    public void GetPatients()
    {
        URI uri = fb.Instance.databasePath.Child("patients").JSON().AuthQuery(UserData.tokenID);
        allPatients = new Dictionary<string, Patient>();

        System.Action<Dictionary<string, Patient>> onSuccess = (patientsRetrieved) =>
        {
            if (patientsRetrieved != null)
            {
                allPatients = patientsRetrieved.Where(p => p.Value.Patient_Data.authorized == true).ToDictionary(i => i.Key, i => i.Value);
                foreach (KeyValuePair<string, Patient> patient in allPatients)
                    patient.Value.Patient_Data.Decrypt(patient.Key);
            }

            //Reload_PatientScroller_Data();
            GetPatients_FromHospital();
        };

        fb.Instance.Get(uri, onSuccess, delegate { });
    }

    private void GetPatients_FromHospital()
    {
        Debug.Log("UserData.doctor_hospital_id = " + UserData.doctor_hospital_id);
        Debug.Log("UserData.doctor_hospital_branch_id = " + UserData.doctor_hospital_branch_id);
        URI uri = fb.Instance.databasePath.Child("hospitals").Child(UserData.doctor_hospital_id).Child("branches")
            .Child(UserData.doctor_hospital_branch_id).Child("patients").JSON().AuthQuery(UserData.tokenID);
        hospitalPatients = new Dictionary<string, Patient>();

        System.Action<Dictionary<string, string>> onSuccess = (patientsRetrieved) =>
        {
            if (patientsRetrieved != null)
                hospitalPatients = allPatients.Where(p => patientsRetrieved.ContainsKey(p.Key)).ToDictionary(i => i.Key, i => i.Value);
            Reload_PatientScroller_Data();
            ButtonsInteractability(true);
        };

        fb.Instance.Get(uri, onSuccess, delegate { });
    }

    public void OpenAddPatientPanel()
    {
        FB.LogAppEvent(AppEventName.ViewedContent, null, new Dictionary<string, object>()
{{ AppEventParameterName.Description, "Open Add Patient Panel" }});
        GameObject addPatientView_GO = OpenPrompt(addPatientView.gameObject);

        AddPatientController addPatientController = addPatientView_GO.GetComponentInChildren<AddPatientController>();
        AddPatientView addPatientView_Instan = addPatientView_GO.GetComponent<AddPatientView>();

        addPatientController.Init(allPatients, hospitalPatients, addPatientView_Instan, ClosePrompt);
    }

    public GameObject OpenPrompt(GameObject prefabView)
    {
        popupOpener.popupPrefab = prefabView;
        popupOpener.OpenPopup();

        return popupOpener.popupInstantiated;
    }

    public void ClosePrompt()
    {
        popupOpener.ClosePopup();
    }

    [SkipRename]
    public void ValueChangeCheck()
    {
        if (SearchField.text == "")
        {
            SearchActive = false;
            Reload_PatientScroller_Data();
        }
        else
        {
            Searched_patients.Clear();
            foreach (KeyValuePair<string, Patient> p in hospitalPatients)
            {
                Patient_GeneralData patient_Data = p.Value.Patient_Data;
                if (patient_Data.first_name.ToLower().Contains(SearchField.text.ToLower()) || 
                    patient_Data.middle_name.ToLower().Contains(SearchField.text.ToLower()) ||
                    patient_Data.last_name.ToLower().Contains(SearchField.text.ToLower()) ||
                    patient_Data.nationalID.ToLower().Contains(SearchField.text.ToLower()) ||
                    patient_Data.email.ToLower().Contains(SearchField.text.ToLower()) ||
                    patient_Data.phoneNumber.ToLower().Contains(SearchField.text.ToLower()))
                {
                    Searched_patients.Add(p.Key, p.Value);
                }
            }
            SearchActive = true;
            Reload_PatientScroller_Data();
        }
    }

    private void Reload_PatientScroller_Data()
    {
        PatientsScroller.ReloadData();
    }

    [SkipRename]
    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return SearchActive ? Searched_patients.Count : hospitalPatients.Count;
    }

    [SkipRename]
    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 40f;
    }

    [SkipRename]
    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        SelectPatient_ItemView cellView = scroller.GetCellView(PatientsViewPrefab) as SelectPatient_ItemView;
        cellView.name = "Cell " + dataIndex.ToString();
        if (SearchActive)
        {
            cellView.SetData(Searched_patients.Values.Select(p=> p.Patient_Data).ToList()[dataIndex], 
                             Searched_patients.Keys.ToList()[dataIndex], Reload_PatientScroller_Data,
                             CheckNextButtonActivation);
        }
        else
        {
            cellView.SetData(hospitalPatients.Values.Select(p => p.Patient_Data).ToList()[dataIndex],
                             hospitalPatients.Keys.ToList()[dataIndex], Reload_PatientScroller_Data,
                             CheckNextButtonActivation);
        }

        return cellView;
    }

    public void BackBtn_pressed()
    {
        //SceneManager.LoadScene("Auth", LoadSceneMode.Single);
        SceneManager.LoadScene("Auth");
        PatientData.patient_UDID = "";
        PatientData.patient_name = "";
        PatientData.patient_phone = "";
    }

    [SkipRename]
    public void NextBtn_pressed()
    {
        if (!string.IsNullOrEmpty(PatientData.patient_UDID) == true )
        {
            PlayerPrefs.SetString("PDFWriter", "selectpatient");
            SceneManager.LoadScene("PDFWriter");
        }
    }

    public void helpButtonfunction()
    {
        HelpPopup.SetActive(true);
    }
    public void helpPromptClose()
    {
        //todo
        HelpPopup.SetActive(false);
    }

    private void CheckNextButtonActivation()
    {
        Debug.Log("rrrrrrrrrrrrr");
        nextButton.UpdateInteractability(!string.IsNullOrEmpty(PatientData.patient_UDID));
    }

    private void ButtonsInteractability(bool _interact)
    {
        addBtn.enabled = _interact;
        SearchField.interactable = _interact;
        //nextButton.UpdateInteractability(_interact);
        backButton.UpdateInteractability(_interact);
    }
    #region Voice
    void SetVoiceControl()
    {
        //Dictionary<string, System.Action> phraseAction = new Dictionary<string, System.Action>()
        //{
        //    {"Back", BackBtn_pressed},
        //    {"Next", NextBtn_pressed},
        //    {"help", helpButtonfunction},
        //    {"close", helpPromptClose},


        //};

        //PanelVoiceController.Instance.AddSetOfPhrases(phraseAction);
    }
    #endregion
}