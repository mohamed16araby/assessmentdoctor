﻿using System.Collections.Generic;
using UnityEngine;
using fb = Cammedar.Network.Firebase;
using URI = Cammedar.Network.URI;
using Cammedar.Network;
using System.Linq;

public class AddPatientController : MonoBehaviour
{
    [HideInInspector]
    public AddPatientView addPatientView;
    [SerializeField]
    GameEvent RefreshPatientList;

    Dictionary<string, Patient> allPatientsList;
    Dictionary<string, Patient> hospitalPatientsList;

    System.Action ClosingPopupAction;

    public void Init(Dictionary<string, Patient> allPatientsList, 
                     Dictionary<string, Patient> hospitalPatientsList,
                     AddPatientView addPatientView, System.Action ClosingPopupAction)
    {
        Debug.Log("init in add patient controller");
        this.allPatientsList = allPatientsList;
        this.hospitalPatientsList = hospitalPatientsList;
        this.ClosingPopupAction = ClosingPopupAction;
        this.addPatientView = addPatientView;

        addPatientView.Init(AddPatient_To_HospitalBranch);
    }

    public void AddPatient_To_HospitalBranch()
    {
        if (Patient_AlreadyExists(addPatientView.SelectedToggle))
            addPatientView.ViewError("This patient is already added!");
        else
        {
           string patientID = GetPatient_FromAllPatients(addPatientView.SelectedToggle, out Patient patient);
            if (patient == null)
                addPatientView.ViewError("Patient not found, above data doesn't match with any patient");
            else
                AddPatient_To_HospitalBranch_ToDatabase(patient, patientID);
        }
    }

    private bool Patient_AlreadyExists(AddPatient_Toggle patientData)
    {
        Patient patient = null;
        switch (patientData.choiceTag)
        {
            case ToggleTag.NationalID:
                patient = hospitalPatientsList.Values.ToList().Find(p=>p.Patient_Data.nationalID == addPatientView.SelectedToggle.FieldsEntries[0]);
                if(patient != null) return true;
                break;

            case ToggleTag.PhoneAndEmail:
                patient = hospitalPatientsList.Values.ToList().Find(p => 
                (p.Patient_Data.phoneNumber == addPatientView.SelectedToggle.FieldsEntries[0]) &&
                (p.Patient_Data.email == addPatientView.SelectedToggle.FieldsEntries[1]));
                if (patient != null) return true;
                break;

            case ToggleTag.PhoneAndSerial:
                patient = hospitalPatientsList.Values.ToList().Find(p => 
                (p.Patient_Data.phoneNumber == addPatientView.SelectedToggle.FieldsEntries[0]) &&
                (p.Patient_Data.serial_number == addPatientView.SelectedToggle.FieldsEntries[1]));
                if (patient != null) return true;
                break;
        }

        return false;
    }

    private string GetPatient_FromAllPatients(AddPatient_Toggle patientData, out Patient patient)
    {
        string patientID = "";
        patient = null;
        Dictionary<string, Patient> patientsMatch = new Dictionary<string, Patient>();

        switch (patientData.choiceTag)
        {
            case ToggleTag.NationalID:
                patientsMatch = allPatientsList.Where(p => p.Value.Patient_Data.nationalID == addPatientView.SelectedToggle.FieldsEntries[0]).ToDictionary(i => i.Key, i => i.Value);
                break;

            case ToggleTag.PhoneAndEmail:
                patientsMatch = allPatientsList
                            .Where(p => (p.Value.Patient_Data.phoneNumber == addPatientView.SelectedToggle.FieldsEntries[0]) &&
                                        (p.Value.Patient_Data.email == addPatientView.SelectedToggle.FieldsEntries[1]))
                            .ToDictionary(i => i.Key, i => i.Value);
                break;

            case ToggleTag.PhoneAndSerial:
                patientsMatch = allPatientsList
                            .Where(p => (p.Value.Patient_Data.phoneNumber == addPatientView.SelectedToggle.FieldsEntries[0]) &&
                                        (p.Value.Patient_Data.serial_number == addPatientView.SelectedToggle.FieldsEntries[1]))
                            .ToDictionary(i => i.Key, i => i.Value);
                break;
        }
        if (patientsMatch.Count > 0)
        {
            KeyValuePair<string, Patient>  id_Patient = patientsMatch.First();
            patientID = id_Patient.Key;
            patient = id_Patient.Value;
        }
        
        return patientID;
    }

    private void AddPatient_To_HospitalBranch_ToDatabase(Patient patient, string patientID)
    {
        if (hospitalPatientsList.Count > 0)
            hospitalPatientsList.Add(patientID, patient);
        else
            hospitalPatientsList = new Dictionary<string, Patient>() { { patientID, patient } };

        URI uri = fb.Instance.databasePath.Child("hospitals").Child(UserData.doctor_hospital_id).Child("branches")
                .Child(UserData.doctor_hospital_branch_id).Child("patients").JSON().AuthQuery(UserData.tokenID);
        fb.Instance.Put(uri, hospitalPatientsList.ToDictionary(p=>p.Key, p=>p.Value.Patient_Data.first_name + p.Value.Patient_Data.last_name),
                         delegate
                         {
                             addPatientView.ViewSuccessFeedback("Patient has been added successfully!");
                             ClosingPopupAction();
                             RefreshPatientList.Raise();
                         },
                         delegate { addPatientView.ViewError("Something Wrong! .. Please try again later"); });
    }
}