﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KKSpeech;
using System;
using System.Linq;
using UnityEngine.Audio;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif

public class VoiceController_Android : MonoBehaviour
{
    private static VoiceController_Android _instance;
    public static string word;
    public static VoiceController_Android Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<VoiceController_Android>();
            return _instance;
        }
    }
    private Dictionary<string, Action> keyActs = new Dictionary<string, Action>();

    void Start()
    {
        if (SpeechRecognizer.ExistsOnDevice())
        {
            SpeechRecognizerListener listener = GameObject.FindObjectOfType<SpeechRecognizerListener>();
            listener.onAuthorizationStatusFetched.AddListener(OnAuthorizationStatusFetched);
            listener.onAvailabilityChanged.AddListener(OnAvailabilityChange);
            listener.onErrorDuringRecording.AddListener(OnError);
            listener.onErrorOnStartRecording.AddListener(OnError);
            listener.onFinalResults.AddListener(OnFinalResult);
            listener.onPartialResults.AddListener(OnPartialResult);
            listener.onEndOfSpeech.AddListener(OnEndOfSpeech);
            //startRecordingButton.enabled = false;
            SpeechRecognizer.RequestAccess();
            //AudioSettings.Mobile.stopAudioOutputOnMute = true;
            SpeechRecognizer.SetSystemVolume(0.25f);
        }
        else
        {
            Debug.Log("Sorry, but this device doesn't support speech recognition");
            //startRecordingButton.enabled = false;
        }
    }

    public void OnFinalResult(string result)
    {
        Debug.Log("OnFinalResult(): " + result);
        result = keyActs.Keys.ToList().Find(s => s.ToLower() == result);

        if (result != null)
            keyActs[result].Invoke();

        RestartRecognizer();
        //StartCoroutine(WaitForSeconds_and_Function(0.5f, RestartRecognizer));
    }

    public void AddSetOfPhrases(Dictionary<string, Action> _keyActs)
    {
        keyActs = _keyActs;
        RestartRecognizer();
    }

    public void RestartRecognizer()
    {
        Debug.Log("IsRecording(): " + SpeechRecognizer.IsRecording());
        if (SpeechRecognizer.IsRecording())
            SpeechRecognizer.StopIfRecording();
        else
            SpeechRecognizer.StartRecording(true);
    }
    public void Discard()
    {
            SpeechRecognizer.StopIfRecording();    
    }

    public void UpdatePhraseAction(string key, Action action)
    {
        if (keyActs.ContainsKey(key))
            RemovePhraseFromDict(key);

        keyActs.Add(key, action);
        RestartRecognizer();
    }

    void RemovePhraseFromDict(string key)
    {
        keyActs.Remove(key);
    }

    public void OnPartialResult(string result)
    {
        //Debug.LogError("result: " + result);
    }

    public void OnAvailabilityChange(bool available)
    {
        if (!available)
        {
            Debug.Log("Speech Recognition not available");
        }
        else
        {
            Debug.Log("Say something :-)");
        }
    }

    public void OnAuthorizationStatusFetched(AuthorizationStatus status)
    {
        switch (status)
        {
            case AuthorizationStatus.Authorized:
                Debug.Log(AuthorizationStatus.Authorized);
                break;
            default:
                AskForPermissions();
                Debug.Log("Cannot use Speech Recognition, authorization status is " + status);
                break;
        }
    }

    public void OnEndOfSpeech()
    {
        Debug.Log("OnEndOfSpeech");
    }

    public void OnError(string error)
    {
        //Debug.LogError(error);
        Debug.Log("Something went wrong... Try again! \n [" + error + "]");

        RestartRecognizer();
        //StartCoroutine(WaitForSeconds_and_Function(0.5f, RestartRecognizer));
    }


    public void AskForPermissions()
    {
#if PLATFORM_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.Microphone))
        {
            Permission.RequestUserPermission(Permission.Microphone);
        }
#endif
    }

    IEnumerator WaitForSeconds_and_Function(float seconds, System.Action AfterWaitingAction)
    {
        yield return new WaitForSeconds(seconds);
        AfterWaitingAction();
    }

    //public void OnStartRecordingPressed()
    //{
    //    if (SpeechRecognizer.IsRecording())
    //    {
    //        SpeechRecognizer.StopIfRecording();
    //        startRecordingButton.GetComponentInChildren<Text>().text = "Start Recording";
    //    }
    //    else
    //    {
    //        SpeechRecognizer.StartRecording(true);
    //        startRecordingButton.GetComponentInChildren<Text>().text = "Stop Recording";
    //        resultText.text = "Say something :-)";
    //    }
    //}
}
