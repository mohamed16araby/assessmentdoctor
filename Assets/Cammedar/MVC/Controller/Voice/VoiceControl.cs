﻿#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Windows.Speech;

public class VoiceControl : MonoBehaviour
{
    private static VoiceControl _instance;
    public static string word;
    public static VoiceControl Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<VoiceControl>();
            return _instance;
        }
    }

    // Voice command vars
    private Dictionary<string, Action> keyActs = new Dictionary<string, Action>();
    private KeywordRecognizer recognizer;

    public bool SpeechRecognition_IsSupported()
    {
        return PhraseRecognitionSystem.isSupported;
    }

    public void AddSetOfPhrases(Dictionary<string, Action> _keyActs)
    {
        keyActs = _keyActs;
        RestartRecognizer();
    }

    public void UpdatePhraseAction(string key, Action action)
    {
        if (keyActs.ContainsKey(key))
        {
            RemovePhraseFromDict(key);
        }

        keyActs.Add(key, action);
        AssessPanels_Manager.ChoicesVoicePhrases.Add(key, action);
        RestartRecognizer();
    }

    void RemovePhraseFromDict(string key)
    {
        keyActs.Remove(key);
        AssessPanels_Manager.ChoicesVoicePhrases.Remove(key);
    }

   public void RestartRecognizer()
    {
        if (recognizer != null)
            recognizer.Dispose();

        if (keyActs.Keys.Count == 0)
        {

            return;
        }

        recognizer = new KeywordRecognizer(keyActs.Keys.ToArray(), ConfidenceLevel.Low);
        recognizer.OnPhraseRecognized += OnKeywordsRecognized;
        recognizer.Start();
    }

    void OnKeywordsRecognized(PhraseRecognizedEventArgs args)
    {
        Debug.Log("Command: " + args.text);
        gameObject.GetComponent<AudioSource>().Play();
        word = args.text.ToString();
        keyActs[args.text].Invoke();
        try
        {
            for (int i = 0; i < Manager.Instance.ItemsScrollList.Count; i++)
            {
                Debug.Log(word + "lololololo");
                if (word == Manager.Instance.ItemsScrollList[i].ToString())
                {
                    Manager.Instance.scrollToSpecificElement(i);

                }
            }
        }
        catch(System.Exception e)
        {
            Debug.Log("error");
        }
        
    }

    public void Discard()
    {
        if (recognizer != null)
        {
            recognizer.Stop();
            recognizer.Dispose();
        }
    }

    void OnDestroy()
    {
        Discard();
    }

    void OnApplicationFocus(bool hasFocus)
    {
        //if (!hasFocus)
        if (hasFocus)
            RestartRecognizer();
        else
        {
            try
            {

           //     CustomizePanels_Manager.Instance.MuteVoice();
            }
            catch (System.Exception e)
            {

            }
        }
    }
}
#endif