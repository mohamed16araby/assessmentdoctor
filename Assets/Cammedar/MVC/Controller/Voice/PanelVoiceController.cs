﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PanelVoiceController : MonoBehaviour
{
    private static PanelVoiceController _instance;
    public static PanelVoiceController Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<PanelVoiceController>();
            return _instance;
        }
    }

#if UNITY_ANDROID
    public VoiceController_Android voiceController_Android;
#endif
#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
    public VoiceControl voiceControl;
#endif

    Dictionary<string, System.Action> CommonVoicePhrases
    {
        get
        {
            return new Dictionary<string, Action>() {
                //{ "Finished", Manager.Instance.AddSessionToCloud },
                { "Up", Manager.Instance.SnapUp },
                { "Middle", Manager.Instance.SnapMiddle },
                { "Down", Manager.Instance.SnapDown },
                { "Top", Manager.Instance.SnapTop },
                { "Bottom", Manager.Instance.SnapBottom },
            };
        }
    }

    Dictionary<string, Action> phrasesOfLastPanelVisited = new Dictionary<string, Action>();

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void AddSetOfPhrases(Dictionary<string, Action> phrasesOfPanel)
    {
        phrasesOfLastPanelVisited = phrasesOfPanel;

        if (Manager.Instance != null)
            phrasesOfLastPanelVisited = Operations.Merging2Dictionaries(phrasesOfPanel, CommonVoicePhrases);

        AddPhrasesToVoice(phrasesOfLastPanelVisited);
    }

    public void ContinueListeningToAssessmentContent()
    {
        AddPhrasesToVoice(phrasesOfLastPanelVisited);
    }

    private void AddPhrasesToVoice(Dictionary<string, Action> phrasesOfPanel)
    {
        AssessPanels_Manager.ChoicesVoicePhrases=phrasesOfPanel;
//#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
//        if (voiceControl.SpeechRecognition_IsSupported())
//            voiceControl.AddSetOfPhrases(phrasesOfPanel);
//        Debug.Log("win and editor");
//#elif UNITY_ANDROID
//        voiceController_Android.AddSetOfPhrases(phrasesOfPanel);
//        Debug.Log("android");
//#endif

    }

    public void UpdatePhraseAction(string key, Action action)
    {
        AssessPanels_Manager.ChoicesVoicePhrases.Remove(key);
        AssessPanels_Manager.ChoicesVoicePhrases.Add(key,action);
//#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
//        if (voiceControl.SpeechRecognition_IsSupported())
//            voiceControl.UpdatePhraseAction(key, action);
//#elif UNITY_ANDROID
//        Debug.Log("VoiceControl.Instance.UpdatePhraseAction(key, action);");
//        voiceController_Android.UpdatePhraseAction(key, action);
//#endif
    }
}