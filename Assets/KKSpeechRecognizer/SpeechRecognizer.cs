﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine.Events;
using System.Text;
using System.Collections.Generic;

namespace KKSpeech {
	
	public enum AuthorizationStatus {
		Authorized,
		Denied,
		NotDetermined,
		Restricted
	}

	public struct SpeechRecognitionOptions {
		public bool shouldCollectPartialResults;
	}

	public struct LanguageOption {
		public readonly string id;
		public readonly string displayName;

		public LanguageOption(string id, string displayName) {
			this.id = id;
			this.displayName = displayName;
		}
	}

	/*
	 * check readme.pdf before using!
	 */

	public class SpeechRecognizer : System.Object {

		#pragma warning disable CS0162 
		public static bool ExistsOnDevice() {
			#if UNITY_IOS && !UNITY_EDITOR
			return iOSSpeechRecognizer._EngineExists();
			#elif UNITY_ANDROID && !UNITY_EDITOR
			return AndroidSpeechRecognizer.EngineExists();
			#endif
			return false; // sorry, no support besides Android and iOS :-(
		}

		public static void RequestAccess() {
			#if UNITY_IOS && !UNITY_EDITOR
			iOSSpeechRecognizer._RequestAccess();
			#elif UNITY_ANDROID && !UNITY_EDITOR
			AndroidSpeechRecognizer.RequestAccess();
			#endif
		}

		public static bool IsRecording() {
			#if UNITY_IOS && !UNITY_EDITOR
			return iOSSpeechRecognizer._IsRecording();
			#elif UNITY_ANDROID && !UNITY_EDITOR
			return AndroidSpeechRecognizer.IsRecording();
			#endif
			return false;
		}

		public static AuthorizationStatus GetAuthorizationStatus() {
			#if UNITY_IOS && !UNITY_EDITOR
			return (AuthorizationStatus)iOSSpeechRecognizer._AuthorizationStatus();
			#elif UNITY_ANDROID && !UNITY_EDITOR
			return (AuthorizationStatus)AndroidSpeechRecognizer.AuthorizationStatus();
			#endif
			return AuthorizationStatus.NotDetermined;
		}

		public static void StopIfRecording() {
			Debug.Log("StopRecording...");
			#if UNITY_IOS && !UNITY_EDITOR
			iOSSpeechRecognizer._StopIfRecording();
			#elif UNITY_ANDROID && !UNITY_EDITOR
			AndroidSpeechRecognizer.StopIfRecording();
			#endif
		}

		private static void StartRecording(SpeechRecognitionOptions options) {
			#if UNITY_IOS && !UNITY_EDITOR
			iOSSpeechRecognizer._StartRecording(options.shouldCollectPartialResults);
			#elif UNITY_ANDROID && !UNITY_EDITOR
			AndroidSpeechRecognizer.StartRecording(options);
			#endif
		}

		public static void StartRecording(bool shouldCollectPartialResults) {
			Debug.Log("StartRecording...");
			#if UNITY_IOS && !UNITY_EDITOR
			iOSSpeechRecognizer._StartRecording(shouldCollectPartialResults);
			#elif UNITY_ANDROID && !UNITY_EDITOR
			AndroidSpeechRecognizer.StartRecording(shouldCollectPartialResults);
			#endif
		}

		public static void GetSupportedLanguages() {
			#if UNITY_IOS && !UNITY_EDITOR
			iOSSpeechRecognizer.SupportedLanguages();
			#elif UNITY_ANDROID && !UNITY_EDITOR
			AndroidSpeechRecognizer.GetSupportedLanguages();
			#endif
		}

		public static void SetDetectionLanguage(string languageID) {
			#if UNITY_IOS && !UNITY_EDITOR
			iOSSpeechRecognizer._SetDetectionLanguage(languageID);
			#elif UNITY_ANDROID && !UNITY_EDITOR
			AndroidSpeechRecognizer.SetDetectionLanguage(languageID);
			#endif
		}

        public static void SetSystemVolume(float volume) {
            #if UNITY_ANDROID && !UNITY_EDITOR
			AndroidSpeechRecognizer.SetSystemVolume(volume);
            #endif
        }
#pragma warning restore CS0162

#if UNITY_IOS && !UNITY_EDITOR
		private class iOSSpeechRecognizer {

			[DllImport ("__Internal")]
			internal static extern void _SetDetectionLanguage(string languageID);

			[DllImport ("__Internal")]
			internal static extern string _SupportedLanguages();

			[DllImport ("__Internal")]
			internal static extern void _RequestAccess();

			[DllImport ("__Internal")]
			internal static extern bool _IsRecording();

			[DllImport ("__Internal")]
			internal static extern bool _EngineExists();

			[DllImport ("__Internal")]
			internal static extern int _AuthorizationStatus();

			[DllImport ("__Internal")]
			internal static extern void _StopIfRecording();

			[DllImport ("__Internal")]
			internal static extern void _StartRecording(bool shouldCollectPartialResults);

			public static void SupportedLanguages() {
				string formattedLangs = _SupportedLanguages();
				var listener = GameObject.FindObjectOfType<SpeechRecognizerListener>();
				if (listener != null) {
					listener.SupportedLanguagesFetched(formattedLangs);
				}
			}
		}
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
        private class AndroidSpeechRecognizer {

			private static string DETECTION_LANGUAGE = null;

			internal static void GetSupportedLanguages() {
				GetAndroidBridge().CallStatic("GetSupportedLanguages");
			}

			internal static void SetDetectionLanguage(string languageID) {
				AndroidSpeechRecognizer.DETECTION_LANGUAGE = languageID;
			}

			internal static void RequestAccess() {
				GetAndroidBridge().CallStatic("RequestAccess");
			}
				
			internal static bool IsRecording() {
				return GetAndroidBridge().CallStatic<bool>("IsRecording");
			}

			internal static bool EngineExists() {
				return GetAndroidBridge().CallStatic<bool>("EngineExists");
			}

			internal static int AuthorizationStatus() {
				return GetAndroidBridge().CallStatic<int>("AuthorizationStatus");
			}

			internal static void StopIfRecording() {
				GetAndroidBridge().CallStatic("StopIfRecording");
			}

			internal static void StartRecording(bool shouldCollectPartialResults) {
				var options = new SpeechRecognitionOptions();
				options.shouldCollectPartialResults = shouldCollectPartialResults;
				StartRecording(options);
			}

            internal static void StartRecording(SpeechRecognitionOptions options)
            {
                GetAndroidBridge().CallStatic("StartRecording", CreateJavaRecognitionOptionsFrom(options));
            }

            private static AndroidJavaObject CreateJavaRecognitionOptionsFrom(SpeechRecognitionOptions options)
            {
                var javaOptions = new AndroidJavaObject("kokosoft.unity.speechrecognition.SpeechRecognitionOptions");
                javaOptions.Set<bool>("shouldCollectPartialResults", options.shouldCollectPartialResults);
                javaOptions.Set<string>("languageID", DETECTION_LANGUAGE);
                return javaOptions;
            }

            private static AndroidJavaObject GetAndroidBridge()
            {
                var bridge = new AndroidJavaClass("kokosoft.unity.speechrecognition.SpeechRecognizerBridge");
                return bridge;
            }

            //private static AndroidJavaObject audioManager;

            //private static AndroidJavaObject deviceAudio
            //{
            //    get
            //    {
            //        if (audioManager == null)
            //        {
            //            AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            //            AndroidJavaObject context = up.GetStatic<AndroidJavaObject>("currentActivity");

            //            string audioName = context.GetStatic<string>("STREAM_MUSIC");

            //            Debug.Log("audioName: " + audioName);

            //            audioManager = context.Call<AndroidJavaObject>("getSystemService", audioName);

            //            Debug.Log("volume: " + audioManager.Call<int>("getStreamVolume", context.GetStatic<string>("STREAM_MUSIC")));

            //        }
            //        return audioManager;
            //    }
            //}

            //internal static void SetDeviceVolume(int value)
            //{
            //    int streammusic = 3;
            //    int flagshowui = 0;

            //    deviceAudio.Call("setStreamVolume", streammusic, value, flagshowui);
            //}

            static int STREAMMUSIC;
            static int FLAGSHOWUI = 1;

            private static AndroidJavaObject audioManager;

            private static AndroidJavaObject deviceAudio
            {
                get
                {
                    if (audioManager == null)
                    {
                        AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                        AndroidJavaObject currentActivity = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");
                        AndroidJavaObject context = currentActivity.Call<AndroidJavaObject>("getApplicationContext");
                        AndroidJavaClass audioManagerClass = new AndroidJavaClass("android.media.AudioManager");
                        AndroidJavaClass contextClass = new AndroidJavaClass("android.content.Context");

                        STREAMMUSIC = audioManagerClass.GetStatic<int>("STREAM_NOTIFICATION");
                        string Context_AUDIO_SERVICE = contextClass.GetStatic<string>("AUDIO_SERVICE");

                        audioManager = context.Call<AndroidJavaObject>("getSystemService", Context_AUDIO_SERVICE);

                        if (audioManager != null)
                            Debug.Log("[AndroidNativeVolumeService] Android Audio Manager successfully set up");
                        else
                            Debug.Log("[AndroidNativeVolumeService] Could not read Audio Manager");
                    }
                    return audioManager;
                }

            }

            private static int GetDeviceMaxVolume()
            {
                return deviceAudio.Call<int>("getStreamMaxVolume", STREAMMUSIC);
            }

            public static float GetSystemVolume()
            {
                int deviceVolume = deviceAudio.Call<int>("getStreamVolume", STREAMMUSIC);
                float scaledVolume = (float)(deviceVolume / (float)GetDeviceMaxVolume());

                return scaledVolume;
            }

            public static void SetSystemVolume(float volumeValue)
            {
                int scaledVolume = (int)(volumeValue * (float)GetDeviceMaxVolume());
                deviceAudio.Call("setStreamVolume", STREAMMUSIC, scaledVolume, FLAGSHOWUI);
            }
        }
#endif
    }

}



