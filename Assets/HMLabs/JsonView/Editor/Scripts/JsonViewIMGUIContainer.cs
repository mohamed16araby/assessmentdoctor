﻿using UnityEngine;
using System.Collections;
using UnityEngine.UIElements;
using UnityEditor.IMGUI.Controls;
using UnityEditor;

namespace HMLabs.Editor
{

    public class JsonViewIMGUIContainer : IMGUIContainer
    {
        public JsonViewContainer m_JsonTreeViewContainer;



        public JsonViewIMGUIContainer(string sKey)
        {
            onGUIHandler += OnGUI;
            m_JsonTreeViewContainer = new JsonViewContainer(sKey);
        }

        public void SetContext(string sJson)
        {
            m_JsonTreeViewContainer.Context = sJson;
        }

        void OnGUI()
        {
            m_JsonTreeViewContainer.OnGUI();
        }

    }
}