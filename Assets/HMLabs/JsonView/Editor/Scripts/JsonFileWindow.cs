﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Unity.Collections;
using Unity.Jobs;
using UnityEditor;
using UnityEngine;

namespace HMLabs.Editor
{
    public class JsonFileWindow : EditorWindow
    {
        public JsonViewContainer m_JsonTreeViewContainer;


        private string m_sFilePath;



        void OnEnable()
        {
            m_JsonTreeViewContainer = new JsonViewContainer("JsonFileWindow");
        }
        void OnGUI()
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                m_sFilePath = EditorGUILayout.TextField("Path", m_sFilePath);
                if (GUILayout.Button("...", GUILayout.Width(25f)))
                {
                    m_sFilePath = EditorUtility.OpenFilePanel("Open a Json file", "", "*");
                }
                if (GUILayout.Button("Open", GUILayout.Width(40f)))
                {
                    try
                    {
                        string sText = System.IO.File.ReadAllText(m_sFilePath);
                        m_JsonTreeViewContainer.Context = sText;
                    }catch(Exception e)
                    {
                        Debug.LogError(e.Message);
                    }
                }
                if (GUILayout.Button("Save", GUILayout.Width(40f)))
                {
                    try
                    {
                        System.IO.File.WriteAllText(m_sFilePath, m_JsonTreeViewContainer.Context);
                    }
                    catch (Exception e)
                    {
                        Debug.LogError(e.Message);
                    }
                }
            }
            m_JsonTreeViewContainer.OnGUI();
        }

        [MenuItem("HMLab/TEstao")]
        public static void Read()
        {
            Debug.Log("starting");
            HttpClient client = new HttpClient();
            using (Stream s = client.GetStreamAsync("http://127.0.0.1:8080/Files/simple-json.txt").Result)
            using (StreamReader sr = new StreamReader(s))
            using (JsonReader reader = new JsonTextReader(sr))
            {
                JsonSerializer serializer = new JsonSerializer();

                // read the json from a stream
                // json size doesn't matter because only a small piece is read at a time from the HTTP request
                JObject p = serializer.Deserialize<JObject>(reader);
            }
            Debug.Log("done");
        }
        [MenuItem("HMLab/asdfasss")]
        static void asdfsAsync()
        {
            Debug.Log("start");
            //string sPath = "I:/Projects/Oktagon/OktaIncubator/UIElements/tools-netmonitor/Files/Planeswalkers.bytes";
            string sPath = "I:/Projects/Oktagon/OktaIncubator/UIElements/tools-netmonitor/Files/static_data.json";
            string sText = System.IO.File.ReadAllText(sPath);
            string sContext = sText;
            var task = Task.Run(async () => await tetssfsd(sContext));
            //Debug.Log(task.Result.ToString());
            Debug.Log("OK");
        }
        static async Task<JObject> tetssfsd(string sValue)
        {
            Debug.Log("tetssfsd start");
            var list = await Task.Run(() => JObject.Parse(sValue));
            Debug.Log("tetssfsd Done");
            return list;
        }





        // Add menu named "My Window" to the Window menu
        [MenuItem("HM Labs/Json File View")]
        static void ShowWindow()
        {
            // Get existing open window or if none, make a new one:
            var window = GetWindow<JsonFileWindow>();
            window.titleContent = new GUIContent("Json File View");
            window.Show();
        }



    }
}
