﻿using UnityEngine;
using UnityEditor.IMGUI.Controls;
using UnityEditor;

namespace HMLabs.Editor
{
    public class JsonViewWindow : EditorWindow
    {
        public JsonViewContainer m_JsonTreeViewContainer;


        void OnEnable()
        {
            m_JsonTreeViewContainer = new JsonViewContainer("JsonTreeViewWindow");
        }
        void OnGUI()
        {
            m_JsonTreeViewContainer.OnGUI();
        }

        public static void ShowWindow(string sJsonValue)
        {
            // Get existing open window or if none, make a new one:
            var window = GetWindow<JsonViewWindow>();
            window.titleContent = new GUIContent("Json View");
            window.Show();
            window.m_JsonTreeViewContainer.Context = sJsonValue;
        }

        // Add menu named "My Window" to the Window menu
        [MenuItem("HM Labs/Json View")]
        static void ShowWindow()
        {
            // Get existing open window or if none, make a new one:
            var window = GetWindow<JsonViewWindow>();
            window.titleContent = new GUIContent("Json View");
            window.Show();
        }
    }
}
