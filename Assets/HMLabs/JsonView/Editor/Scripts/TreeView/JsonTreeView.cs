﻿using System.Collections.Generic;
using UnityEditor.IMGUI.Controls;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using UnityEditor;
using UnityEngine.UIElements;
using System.Data;

namespace HMLabs.Editor
{
    public class JsonTreeView : TreeView
    {
        private string m_sJsonString;
        private int iId = 0;
        private List<JsonTreeViewItem> m_JsonItems;
        private JObject m_pOriginalJsonObject;
        private JArray m_pOriginalJsonArray;
        private Action<string> m_pOnJsonChanged;
        private TimeSpan m_pParseElapsedTime;
        private long m_pMemoryUsed;
        private int m_iLength;
        private int m_iCount;
        private bool m_bReadOnly;

        public List<JsonTreeViewItem> JsonItems => m_JsonItems;
        private GUIStyle StyleIndex => Defs.JsonView_LabelIndex;
        private GUIStyle StyleKey => Defs.JsonView_LabelKey;
        private GUIStyle StyleValue => Defs.JsonView_LabelValue;

        

        public TimeSpan ParseElapsedTime => m_pParseElapsedTime;
        public long ParseMemoryUsed => m_pMemoryUsed;
        public int Length => m_iLength;
        public int Count => m_iCount;
        public bool ReadOnly { get => m_bReadOnly; set => m_bReadOnly = value; }

        public JsonTreeView(TreeViewState treeViewState, MultiColumnHeader pHeader, string sJsonString, bool bReadOnly, Action<string> pOnJsonChanged)
            : base(treeViewState, pHeader)
        {
            columnIndexForTreeFoldouts = 1;
            showAlternatingRowBackgrounds = true;
            showBorder = true;
            m_pOnJsonChanged = pOnJsonChanged;
            SetJsonText(sJsonString);
        }

        public void SetJsonText(string sJsonString)
        {
            m_sJsonString = sJsonString;
            Reload();
        }

        private void ParseJson()
        {
            if (string.IsNullOrEmpty(m_sJsonString))
                return;

            long available = GC.GetTotalMemory(false);
            System.Diagnostics.Stopwatch pStopwatch = new System.Diagnostics.Stopwatch();
            pStopwatch.Start();
            try
            {
                if (IsJsonArray(m_sJsonString))
                {
                    m_pOriginalJsonArray = JArray.Parse(m_sJsonString);
                    m_JsonItems = BuildFromJArray(m_pOriginalJsonArray, 0);
                    m_iCount = m_pOriginalJsonArray != null ? m_pOriginalJsonArray.Count : 0;
                }
                else
                {
                    m_pOriginalJsonObject = JObject.Parse(m_sJsonString);
                    m_JsonItems = BuildFromJObject(m_pOriginalJsonObject, 0);
                    m_iCount = m_pOriginalJsonObject != null ? m_pOriginalJsonObject.Count : 0;
                }
            }
            catch (Exception e) { Debug.LogError(e.Message); }
            pStopwatch.Stop();
            m_pParseElapsedTime = pStopwatch.Elapsed;
            m_pMemoryUsed = GC.GetTotalMemory(false) - available;
            m_iLength = m_sJsonString.Length;
        }

        protected override void DoubleClickedItem(int id)
        {
            base.DoubleClickedItem(id);
            SetExpandedRecursive(id, true);
        }
        private bool IsJsonArray(string sJson)
        {
            int iLength = sJson.Length;
            int i = -1;
            while (i++ < iLength)
            {
                if (sJson[i] == ' ')
                    continue;
                if (sJson[i] == '[')
                    return true;
                if (sJson[i] == '{')
                    return false;
            }
            return false;
        }

        #region Builder

        protected override TreeViewItem BuildRoot()
        {
            m_JsonItems = new List<JsonTreeViewItem>();
            var root = new TreeViewItem { id = 0, depth = -1, displayName = "Root" };
            iId = 0;
            m_pOriginalJsonObject = null;
            m_pOriginalJsonArray = null;
            List<TreeViewItem> allItems = new List<TreeViewItem>()
            {
                new TreeViewItem() { id = 1, depth = 0, displayName = "Empty" }
            };

            ParseJson();

            SetupParentsAndChildrenFromDepths(root,
                m_JsonItems.Select(p=>(TreeViewItem)p).ToList()
                );
            return root;
        }
        List<JsonTreeViewItem> BuildFromJObject(JObject pObject, int iDept)
        {
            List<JsonTreeViewItem> vList = new List<JsonTreeViewItem>();
            foreach (JProperty property in pObject.Properties())
            {
                var vlist = BuildFromToken(property, property.Value, property.Name, iDept);
                vList.AddRange(vlist);
            }
            return vList;
        }
        List<JsonTreeViewItem> BuildFromJArray(JArray pObject, int iDept)
        {
            List<JsonTreeViewItem> vList = new List<JsonTreeViewItem>();
            int iCounter = 0;
            foreach (JToken property in pObject)
            {
                var vlist = BuildFromToken(null, property, iCounter.ToString(), iDept);
                vList.AddRange(vlist);
                iCounter++;
            }
            return vList;
        }
        List<JsonTreeViewItem> BuildFromToken(JProperty pProperty, JToken pValue, string sName, int iDept)
        {
            

            List<JsonTreeViewItem> vList = new List<JsonTreeViewItem>();
            switch (pValue.Type)
            {
                case JTokenType.None:
                case JTokenType.Null:
                case JTokenType.Constructor:
                case JTokenType.String:
                case JTokenType.Guid:
                case JTokenType.Raw:
                case JTokenType.Comment:
                case JTokenType.Undefined:
                case JTokenType.Date:
                    vList.Add(JsonTreeViewItemFactory.Build(iId++, iDept, sName, pProperty, pValue.ToString()));
                    break;
                case JTokenType.Boolean:
                case JTokenType.Integer:
                case JTokenType.Float:
                    vList.Add(JsonTreeViewItemFactory.Build(iId++, iDept, sName, pProperty, pValue));
                    break;

                case JTokenType.Object:
                    vList.Add(JsonTreeViewItemFactory.Build(iId++, iDept, sName, pProperty, null));
                    JObject pObject = (JObject)pValue;
                    vList.AddRange(BuildFromJObject(pObject, iDept + 1));
                    break;
                case JTokenType.Array:
                    vList.Add(JsonTreeViewItemFactory.Build(iId++, iDept, sName, pProperty, null));
                    JArray vArray = (JArray)pValue;
                    int i = 0;
                    foreach (JToken item in vArray)
                    {
                        vList.AddRange(BuildFromToken(pProperty, item, i++.ToString(), iDept + 1));
                    }
                    break;
                case JTokenType.Property:
                case JTokenType.Bytes:
                case JTokenType.Uri:
                case JTokenType.TimeSpan:
                    vList.Add(JsonTreeViewItemFactory.Build(iId++, iDept, sName, pProperty, $"{pValue.Type.ToString()} not parsed"));
                    break;
            }
            return vList;
        }
        #endregion


        protected override bool DoesItemMatchSearch(TreeViewItem item, string search)
        {
            if (item == null)
                return false;
            return base.DoesItemMatchSearch(item, search);
        }
        protected override void RowGUI(RowGUIArgs vArgs)
        {
            if (ReadOnly)
                DrawReadOnly(vArgs);
            else
                DrawWriteable(vArgs);
        }
        private void DrawReadOnly(RowGUIArgs vArgs)
        {
            JsonTreeViewItem pItem = (JsonTreeViewItem)vArgs.item;
            float pContentIndent = GetContentIndent(pItem);
            Rect pRect = vArgs.GetCellRect(0);
            // draw index
            EditorGUI.LabelField(pRect, pItem.id.ToString(), StyleIndex);
            // body
            pRect = vArgs.GetCellRect(1);
            pRect.x = pRect.x + pContentIndent;// + pRect.width;
            Vector2 vSize1 = EditorStyles.label.CalcSize(pItem.DisplayName);
            pRect.width = vSize1.x;
            EditorGUI.LabelField(pRect, pItem.DisplayName);
        }
        private void DrawWriteable(RowGUIArgs vArgs)
        {
            JsonTreeViewItem pItem = (JsonTreeViewItem)vArgs.item;

            float pContentIndent = GetContentIndent(pItem);

            // init some vars
            Rect pRect = vArgs.GetCellRect(0);

            // draw index
            EditorGUI.LabelField(pRect, pItem.id.ToString(), StyleIndex);

            // drawing key
            pRect = vArgs.GetCellRect(0);
            pRect.x = pRect.x + pContentIndent;// + pRect.width;
            string sKey = pItem.Key;
            GUIContent pContent1 = new GUIContent(sKey);
            Vector2 vSize1 = EditorStyles.label.CalcSize(pContent1);
            pRect.x += pRect.width + Defs.DefaultSpacing;
            pRect.width = vSize1.x;
            pItem.Key = EditorGUI.TextField(pRect, sKey, StyleKey);

            // do we have value?
            if (pItem.Value == null)
                return;

            // draw the separator
            pRect.x += pRect.width;
            pRect.width = Defs.SeparatorWidth;
            EditorGUI.LabelField(pRect, ":", StyleKey);

            // so draw it
            string sValue = pItem.Value.ToString();
            GUIContent pContent2 = new GUIContent(sValue);
            Vector2 vSize2 = EditorStyles.label.CalcSize(pContent2);
            pRect.x += pRect.width + Defs.DefaultSpacing;
            pRect.width = vSize2.x + 15f;
            if (pItem.DrawValue(pRect, StyleValue))
            {
                if (m_pOnJsonChanged != null)
                {
                    if (m_pOriginalJsonObject != null)
                    {
                        string sNewJson = m_pOriginalJsonObject.ToString(Formatting.Indented, null);
                        m_pOnJsonChanged.Invoke(sNewJson);
                    }
                    if (m_pOriginalJsonArray != null)
                    {
                        string sNewJson = m_pOriginalJsonArray.ToString(Formatting.Indented, null);
                        m_pOnJsonChanged.Invoke(sNewJson);
                    }
                }
            }
        }

        public static MultiColumnHeaderState CreateDefaultMultiColumnHeaderState()
        {
            var columns = new[]
            {
                new MultiColumnHeaderState.Column
                {
                    headerContent = new GUIContent("Idx"),
                    headerTextAlignment = TextAlignment.Right,
                    sortedAscending = true,
                    sortingArrowAlignment = TextAlignment.Right,
                    width = Defs.IndexLabelWidth,
                    minWidth= Defs.IndexLabelWidth,
                    maxWidth= Defs.IndexLabelWidth * 3,
                    autoResize = false,
                    allowToggleVisibility = false,
                    canSort =false
                },
                new MultiColumnHeaderState.Column
                {
                    headerContent = new GUIContent("Content"),
                    headerTextAlignment = TextAlignment.Left,
                    sortedAscending = true,
                    sortingArrowAlignment = TextAlignment.Right,
                    autoResize = false,
                    allowToggleVisibility = true,
                    minWidth = 150f,
                    width = 150f,
                    canSort =false
                }
            };
            var state = new MultiColumnHeaderState(columns);

            return state;
        }
    }
}

