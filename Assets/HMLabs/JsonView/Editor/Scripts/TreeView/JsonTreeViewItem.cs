﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

namespace HMLabs.Editor
{
    public class JsonTreeViewItem : TreeViewItem
    {
        public string Key;
        public JToken Value;
        protected JProperty m_pProperty;
        public GUIContent DisplayName;

        public JsonTreeViewItem(int iId, int iDepth, string sKey, JProperty pProperty, JToken pObject)
        {
            id = iId;
            depth = iDepth;
            Key = sKey;
            Value = pObject;
            m_pProperty = pProperty;
            displayName = BuildDisplayName();
            DisplayName = new GUIContent(displayName);
        }
        public virtual bool DrawValue(Rect rect, GUIStyle pStyle)
        {
            return false;
        }

        protected string BuildDisplayName()
        {
            StringBuilder sValue = new StringBuilder();
            sValue.Append(Key != null ? Key : "");
            sValue.Append(Value != null ? ": " + Value.ToString() : "");
            return sValue.ToString();
        }

        protected void UpdateValue(JToken pNewValue)
        {
            if (m_pProperty != null)
            {
                if (m_pProperty.Value.Type == JTokenType.Array)
                {
                    JArray jArray = (JArray)m_pProperty.Value;
                    var vValues = jArray.Select(p => p).ToArray();
                    int iIdxChange = jArray.Select((p, i) => new { p, i }).First(p => p.p == Value).i;
                    vValues[iIdxChange] = pNewValue;
                    m_pProperty.Value = new JArray(vValues);
                    Value = m_pProperty.Value;
                }
                else
                {
                    m_pProperty.Value = pNewValue;
                    Value = m_pProperty.Value;
                }
            }
        }
    }

    public class JsonTreeViewItemString : JsonTreeViewItem
    {
        public JsonTreeViewItemString(int iId, int iDepth, string sKey, JProperty pProperty, JToken pObject) : base(iId, iDepth, sKey, pProperty, pObject)
        {

        }
        public override bool DrawValue(Rect rect, GUIStyle pStyle)
        {
            string sValue = Value.ToString();
            string sNewValue = EditorGUI.TextField(rect, sValue, pStyle);
            bool bChanged = sNewValue != sValue;
            if (bChanged)
            {
                UpdateValue(sNewValue);
            }
            return bChanged;
        }
    }
    public class JsonTreeViewItemStringHex : JsonTreeViewItemString
    {
        public JsonTreeViewItemStringHex(int iId, int iDepth, string sKey, JProperty pProperty, JToken pObject) : base(iId, iDepth, sKey, pProperty, pObject)
        {

        }
        public override bool DrawValue(Rect rect, GUIStyle pStyle)
        {
            bool bChanged = base.DrawValue(rect, pStyle);
            string sValue = Value.ToString();
            if (!IsColor(sValue))
                return bChanged;

            Color pColor = hexToColor(sValue);
            rect.x += EditorStyles.label.CalcSize(new GUIContent(sValue)).x;
            Color pNewColor = EditorGUI.ColorField(rect, pColor);
            if (pColor != pNewColor)
            {
                bChanged = true;
                UpdateValue(colorToHex(pNewColor));
            }
            return bChanged;
        }
        public static string colorToHex(Color32 color)
        {
            string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
            if (!hex.StartsWith("#"))
                hex = "#" + hex;
            return hex;
        }
        public static Color hexToColor(string hex)
        {
            hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
            hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
            byte a = 255;//assume fully visible unless specified in hex
            byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
            byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
            byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
            //Only use alpha if the string has enough characters
            if (hex.Length == 8)
            {
                a = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);
            }
            return new Color32(r, g, b, a);
        }
        public static bool IsColor(string sValue)
        {
            if (sValue.Length < 7 || !sValue.StartsWith("#"))
                return false;
            int v;
            if (Int32.TryParse(sValue.Substring(1), NumberStyles.HexNumber, CultureInfo.CurrentCulture, out v))
                return true;
            return false;
        }
    }
    public class JsonTreeViewItemInt : JsonTreeViewItem
    {
        public JsonTreeViewItemInt(int iId, int iDepth, string sKey, JProperty pProperty, JToken pObject) : base(iId, iDepth, sKey, pProperty, pObject)
        {

        }
        public override bool DrawValue(Rect rect, GUIStyle pStyle)
        {
            int iValue = (int)Value;
            int iNewValue = EditorGUI.IntField(rect, (int)Value, pStyle);
            bool bChanged = iValue != iNewValue;
            if (bChanged)
            {
                UpdateValue(iNewValue);
            }
            return bChanged;
        }
    }
    public class JsonTreeViewItemFloat : JsonTreeViewItem
    {
        public JsonTreeViewItemFloat(int iId, int iDepth, string sKey, JProperty pProperty, JToken pObject) : base(iId, iDepth, sKey, pProperty, pObject)
        {

        }
        public override bool DrawValue(Rect rect, GUIStyle pStyle)
        {
            float fValue = (float)Value;
            float fNewValue = EditorGUI.FloatField(rect, (float)Value, pStyle);
            bool bChanged = fValue != fNewValue;
            if (bChanged)
            {
                UpdateValue(fNewValue);
            }
            return bChanged;
        }
    }
    public class JsonTreeViewItemBoolean : JsonTreeViewItem
    {
        public JsonTreeViewItemBoolean(int iId, int iDepth, string sKey, JProperty pProperty, JToken pObject) : base(iId, iDepth, sKey, pProperty, pObject)
        {

        }
        public override bool DrawValue(Rect rect, GUIStyle pStyle)
        {
            bool bValue = (bool)Value;
            bool bNewValue = EditorGUI.Toggle(rect, (bool)Value);
            bool bChanged = bValue != bNewValue;
            if (bChanged)
            {
                UpdateValue(bNewValue);
            }
            return bChanged;
        }
    }
}