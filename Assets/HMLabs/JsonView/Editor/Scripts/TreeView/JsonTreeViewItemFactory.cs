﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMLabs.Editor
{
    public class JsonTreeViewItemFactory
    {
        static public JsonTreeViewItem Build(int iId, int iDept, string sName, JProperty pProperty, JToken pValue)
        {
            if(pValue == null)
                return new JsonTreeViewItem(iId, iDept, sName, pProperty, null);
            switch (pValue.Type)
            {
                case JTokenType.None:
                case JTokenType.Null:
                case JTokenType.Constructor:
                case JTokenType.String:
                case JTokenType.Guid:
                case JTokenType.Raw:
                case JTokenType.Comment:
                case JTokenType.Undefined:
                case JTokenType.Date:
                    string sValue = pValue.ToString();
                    if (JsonTreeViewItemStringHex.IsColor(sValue))
                        return new JsonTreeViewItemStringHex(iId, iDept, sName, pProperty, sValue);
                    return new JsonTreeViewItemString(iId, iDept, sName, pProperty, sValue);
                case JTokenType.Boolean:
                    return new JsonTreeViewItemBoolean(iId, iDept, sName, pProperty, pValue);
                case JTokenType.Integer:
                    return new JsonTreeViewItemInt(iId, iDept, sName, pProperty, pValue);
                case JTokenType.Float:
                    return new JsonTreeViewItemFloat(iId, iDept, sName, pProperty, pValue);
                case JTokenType.Object:
                    return new JsonTreeViewItem(iId, iDept, sName, pProperty, null);
                case JTokenType.Array:
                    return (new JsonTreeViewItem(iId, iDept, sName, pProperty, null));
                case JTokenType.Property:
                case JTokenType.Bytes:
                case JTokenType.Uri:
                case JTokenType.TimeSpan:
                    return (new JsonTreeViewItemString(iId, iDept, sName, pProperty, $"{pValue.Type.ToString()} not parsed"));
            }
            return null;
        }
    }
}
