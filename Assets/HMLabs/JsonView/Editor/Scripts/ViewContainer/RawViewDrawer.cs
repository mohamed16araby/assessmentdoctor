﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Newtonsoft.Json;

namespace HMLabs.Editor
{
    public class RawViewDrawer : IViewDrawer
    {
        private string m_sKey;
        private Vector2 m_vScrollPosition;
        public string Context
        {
            get => EditorPrefs.GetString(m_sKey, "");
            set => EditorPrefs.SetString(m_sKey, value);
        }


        public RawViewDrawer(string sKey)
        {
            m_sKey = sKey;
        }


        public void DrawBody()
        {
            m_vScrollPosition = GUILayout.BeginScrollView(m_vScrollPosition);
            string sNow = Context;
            string sNew = GUILayout.TextArea(Context);
            if(sNew != sNow)
            {
                Context = sNew;
            }
            GUILayout.EndScrollView();
        }
        public void DrawToolbar()
        {
            if (GUILayout.Button("Format", EditorStyles.toolbarButton))
            {
                Context = JsonConvert.SerializeObject(JsonConvert.DeserializeObject(Context), Formatting.Indented);
            }
            if (GUILayout.Button("Shrink", EditorStyles.toolbarButton))
            {
                Context = JsonConvert.SerializeObject(JsonConvert.DeserializeObject(Context), Formatting.None);
            }
        }
        public void DrawInfo()
        {
        }
    }
}