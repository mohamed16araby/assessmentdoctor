﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using System.Linq;

namespace HMLabs.Editor
{
    public class JsonViewDrawer : IViewDrawer
    {
        private TreeViewState m_TreeViewState;
        private JsonTreeView m_TreeView;
        private MultiColumnHeaderState m_MultiColumnHeaderState;
        private SearchField m_SearchField;
        private bool m_bShowJsonView = true;
        private string m_sKey;
        private string m_sStats;

        protected bool ReadOnly
        {
            get => EditorPrefs.GetBool("ReadOnly", true);
            set { EditorPrefs.SetBool("ReadOnly", value); m_TreeView.ReadOnly = value; }
        }

        public string Context
        {
            get => EditorPrefs.GetString(m_sKey, "");
            set {
                EditorPrefs.SetString(m_sKey, value);
                m_TreeView.SetJsonText(value);
            }
        }

        
        public JsonViewDrawer(string sKey)
        {
            m_sKey = sKey;
            if (m_TreeViewState == null)
                m_TreeViewState = new TreeViewState();
            
            bool firstInit = m_MultiColumnHeaderState == null;
            var headerState = JsonTreeView.CreateDefaultMultiColumnHeaderState();
            if (MultiColumnHeaderState.CanOverwriteSerializedFields(m_MultiColumnHeaderState, headerState))
                MultiColumnHeaderState.OverwriteSerializedFields(m_MultiColumnHeaderState, headerState);
            m_MultiColumnHeaderState = headerState;

            var multiColumnHeader = new MyMultiColumnHeader(headerState);
            if (firstInit)
                multiColumnHeader.ResizeToFit();

            m_TreeView = new JsonTreeView(m_TreeViewState, multiColumnHeader, Context, ReadOnly, OnJsonTextChanged);
            m_SearchField = new SearchField();
            m_SearchField.downOrUpArrowKeyPressed += m_TreeView.SetFocusAndEnsureSelectedItem;

            string sLoadTime = "";
            if (m_TreeView.ParseElapsedTime.TotalMinutes > .02f)
                sLoadTime = m_TreeView.ParseElapsedTime.TotalMinutes.ToString() + "min";
            else if (m_TreeView.ParseElapsedTime.TotalSeconds > .02f)
                sLoadTime = m_TreeView.ParseElapsedTime.TotalMinutes.ToString() + "sec";
            else
                sLoadTime = m_TreeView.ParseElapsedTime.TotalMilliseconds.ToString() + "ms";
            m_sStats = $"Stats: " +
                $"Parse Time[{sLoadTime}] " +
                $"Parse Memory[{m_TreeView.ParseMemoryUsed}] " +
                $"Len[{m_TreeView.Length}] " +
                $"Count[{m_TreeView.Count}]"
                ;
        }

        private void OnJsonTextChanged(string sNewJson)
        {
            Context = sNewJson;
        }
        public void DrawToolbar()
        {
            ReadOnly = GUILayout.Toggle(ReadOnly, "Read Only", EditorStyles.toolbarButton);
            if (GUILayout.Button("Expand All", EditorStyles.toolbarButton))
            {
                m_TreeView.JsonItems.ForEach(p => m_TreeView.SetExpanded(p.id, true));
            }
            if (GUILayout.Button("Collapse All", EditorStyles.toolbarButton))
            {
                m_TreeView.JsonItems.ForEach(p => m_TreeView.SetExpanded(p.id, false));
            }
            if (GUILayout.Button("Expand Selected", EditorStyles.toolbarButton))
            {
                m_TreeView.SetExpandedRecursive(m_TreeView.GetSelection()[0], true);
            }
            GUILayout.Space(100);
            GUILayout.FlexibleSpace();
            m_TreeView.searchString = m_SearchField.OnToolbarGUI(m_TreeView.searchString);
        }


        public void DrawBody()
        {
            Rect rect = GUILayoutUtility.GetRect(0, 100000, 0, 100000);
            m_TreeView.OnGUI(rect);
        }

        public void DrawInfo()
        {
            EditorGUILayout.LabelField(m_sStats);
        }
    }



    internal class MyMultiColumnHeader : MultiColumnHeader
    {

        public MyMultiColumnHeader(MultiColumnHeaderState state)
            : base(state)
        {
            height = 20f;
            
        }
        protected override void ColumnHeaderGUI(MultiColumnHeaderState.Column column, Rect headerRect, int columnIndex)
        {
            // Default column header gui
            base.ColumnHeaderGUI(column, headerRect, columnIndex);
        }
    }


}