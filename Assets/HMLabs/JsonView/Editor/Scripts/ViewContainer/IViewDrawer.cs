﻿using UnityEngine;
using System.Collections;


namespace HMLabs.Editor
{
    public interface IViewDrawer
    {
        string Context { get; set; }
        void DrawToolbar();
        void DrawBody();
        void DrawInfo();
    }
}