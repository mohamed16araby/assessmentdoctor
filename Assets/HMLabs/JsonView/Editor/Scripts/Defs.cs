﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace HMLabs.Editor
{
    public static class Defs
    {

        public static float IndexLabelWidth = 25f;
        public static float SeparatorWidth = 12f;
        public static float DefaultSpacing = 2f;


        static GUISkin m_pSkin;
        static GUIStyle m_pStyleValue;
        static public GUISkin Skin
        {
            get
            {
                if (m_pSkin == null)
                    m_pSkin = (GUISkin)Resources.Load("JsonView/GUISkin");
                return m_pSkin;
            }
        }


        static public GUIStyle JsonView_LabelValue
        {
            get
            {
                
                return EditorGUIUtility.isProSkin? Skin.GetStyle("JsonView_LabelValuePro") : Skin.GetStyle("JsonView_LabelValue");
            }
        }
        static public GUIStyle JsonView_LabelKey
        {
            get
            {
                return EditorGUIUtility.isProSkin ? Skin.GetStyle("JsonView_LabelKeyPro") : Skin.GetStyle("JsonView_LabelKey");
            }
        }
        static public GUIStyle JsonView_LabelIndex
        {
            get
            {
                return EditorGUIUtility.isProSkin ? Skin.GetStyle("JsonView_LabelIndexPro") : Skin.GetStyle("JsonView_LabelIndex");
            }
        }

    }

}