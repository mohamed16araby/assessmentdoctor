﻿using UnityEngine;
using System.Collections;
using HMLabs.Editor;
using UnityEditor.IMGUI.Controls;
using UnityEditor;
using System;
using System.Linq;

namespace HMLabs.Editor
{
    public class JsonViewContainer
    {
        protected enum Option { Raw, Json }

        private Option m_eOption;
        private string[] m_vAllOptions;
        private IViewDrawer m_pViewContainer;
        private string m_sKey;
        private string m_sKeyOption;


        #region properties

        protected Option PrefOption
        {
            get => (Option)EditorPrefs.GetInt(m_sKeyOption, 0);
            set => EditorPrefs.SetInt(m_sKeyOption, (int)value);
        }
        private string[] AllOptions
        {
            get { if (m_vAllOptions == null) m_vAllOptions = Enum.GetNames(typeof(Option)); return m_vAllOptions; }
        }
        #endregion

        public JsonViewContainer(string sKey)
        {
            m_sKey = sKey;
            m_sKeyOption = $"{sKey}_Option";
            SetOption(PrefOption);
        }

        public string Context
        {
            get { return m_pViewContainer.Context; }
            set { m_pViewContainer.Context = value; }
        }

        public void OnGUI()
        {
            DrawToolbar();
            DrawBody();
            DrawInfo();
        }

        void SetOption(Option eOption)
        {
            PrefOption = eOption;
            m_pViewContainer = null;
            switch (PrefOption)
            {
                case Option.Raw:
                    m_pViewContainer = new RawViewDrawer(m_sKey);
                    break;
                case Option.Json:
                    m_pViewContainer = new JsonViewDrawer(m_sKey);
                    break;
            }
        }

        void DrawToolbar()
        {
            using(new EditorGUILayout.HorizontalScope(EditorStyles.toolbar))
            {
                if (GUILayout.Button("Clear", EditorStyles.toolbarButton))
                {
                    Context = "";
                }

                Option eOptNow = PrefOption;
                int iOption = GUILayout.Toolbar((int)eOptNow, AllOptions, EditorStyles.toolbarButton);
                Option eOptNew = (Option)(iOption);
                if(eOptNow != eOptNew)
                {
                    SetOption(eOptNew);
                }

                GUILayout.Space(10);
                if (GUILayout.Button("Copy", EditorStyles.toolbarButton))
                {
                    EditorGUIUtility.systemCopyBuffer = m_pViewContainer.Context;
                }
                if (GUILayout.Button("Paste", EditorStyles.toolbarButton))
                {
                    m_pViewContainer.Context = EditorGUIUtility.systemCopyBuffer;
                }

                GUILayout.FlexibleSpace();
                m_pViewContainer?.DrawToolbar();
            }
        }

        void DrawBody()
        {
            m_pViewContainer?.DrawBody();
        }

        void DrawInfo()
        {
            m_pViewContainer?.DrawInfo();
        }
    }

}