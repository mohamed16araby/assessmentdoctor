// Copyright (C) 2015-2017 ricimi - All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement.
// A Copy of the Asset Store EULA is available at http://unity3d.com/company/legal/as_terms.

using UnityEngine;
using UnityEngine.SceneManagement;
using Beebyte.Obfuscator;

namespace Ricimi
{
    // This class is responsible for creating and opening a popup of the given prefab and add
    // it to the UI canvas of the current scene.
    public class PopupOpener : MonoBehaviour
    {
        public GameObject popupPrefab;
        [HideInInspector]
        public GameObject popupInstantiated;

        //protected Canvas m_canvas;

        protected void Start()
        {
            //m_canvas = GameObject.FindGameObjectWithTag("View_Canvas").GetComponent<Canvas>();
            
        }

        [SkipRename]
        public virtual void OpenPopup()
        {
            var popup = Instantiate(popupPrefab) as GameObject;
            popup.SetActive(true);
            popup.transform.localScale = Vector3.zero;
            Canvas m_canvas = ScenesController.Instance.FindObjectInSpecificScene("View_Canvas", SceneManager.GetActiveScene().name).GetComponent<Canvas>();
            popup.transform.SetParent(m_canvas.transform, false);

            popupInstantiated = popup;
            popupInstantiated.GetComponent<Popup>().Open();
        }

        public virtual void ClosePopup()
        {
            if (!popupInstantiated) return;

            popupInstantiated.GetComponent<Popup>().Close();
        }
    }
}
