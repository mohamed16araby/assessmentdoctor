var class_local_file_system =
[
    [ "Delete", "class_local_file_system.html#a793333640a22bba03e14fd4dab95a21b", null ],
    [ "GettingSavedFiles", "class_local_file_system.html#a53b3ae88edfa7678951eb0fbe028afb0", null ],
    [ "Load", "class_local_file_system.html#a61748ede50c3b19b6ae8ad3f9db5fd6f", null ],
    [ "PrepareDirectoryForNewFile", "class_local_file_system.html#affe44e2a7a4830d45c3066d4bd24b822", null ],
    [ "ReadFile", "class_local_file_system.html#a77d5e87fdad1fa1b62980c826bb5f28b", null ],
    [ "Save", "class_local_file_system.html#a88100ad3ad9803e5c983d4b1804ece5a", null ]
];