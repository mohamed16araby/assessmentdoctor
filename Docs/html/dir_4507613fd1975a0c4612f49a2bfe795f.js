var dir_4507613fd1975a0c4612f49a2bfe795f =
[
    [ "CustomTools", "dir_7099277aeb482920cd847e5525c2784f.html", "dir_7099277aeb482920cd847e5525c2784f" ],
    [ "UnityPackages", "dir_e0339c4531221ac2fad11017c3f2fa72.html", "dir_e0339c4531221ac2fad11017c3f2fa72" ],
    [ "VideoController", "dir_25508fba7e4852cb9a1e4757440f18de.html", "dir_25508fba7e4852cb9a1e4757440f18de" ],
    [ "ReactingLights.cs", "_reacting_lights_8cs.html", [
      [ "ReactingLights", "class_reacting_lights.html", "class_reacting_lights" ]
    ] ],
    [ "SimpleJSON.cs", "_simple_j_s_o_n_8cs.html", "_simple_j_s_o_n_8cs" ],
    [ "UIExtensions.cs", "_u_i_extensions_8cs.html", [
      [ "UISetExtensions", "class_u_i_set_extensions.html", "class_u_i_set_extensions" ]
    ] ],
    [ "YoutubeLogo.cs", "_youtube_logo_8cs.html", [
      [ "YoutubeLogo", "class_youtube_logo.html", "class_youtube_logo" ]
    ] ],
    [ "YoutubePlayer.cs", "_youtube_player_8cs.html", [
      [ "YoutubePlayer", "class_youtube_player.html", "class_youtube_player" ],
      [ "YoutubeResultIds", "class_youtube_player_1_1_youtube_result_ids.html", "class_youtube_player_1_1_youtube_result_ids" ],
      [ "Html5PlayerResult", "class_youtube_player_1_1_html5_player_result.html", "class_youtube_player_1_1_html5_player_result" ],
      [ "Extensions", "class_extensions.html", "class_extensions" ]
    ] ],
    [ "YoutubePlayerLivestream.cs", "_youtube_player_livestream_8cs.html", [
      [ "YoutubePlayerLivestream", "class_youtube_player_livestream.html", "class_youtube_player_livestream" ]
    ] ]
];