var dir_c9d53fd1189196d9fe9f16a042751fac =
[
    [ "Platform", "dir_8c8a55d482262847fc4386628044b911.html", "dir_8c8a55d482262847fc4386628044b911" ],
    [ "DetectOrientationChange.cs", "_detect_orientation_change_8cs.html", [
      [ "DetectOrientationChange", "class_almost_engine_1_1_preview_1_1_detect_orientation_change.html", "class_almost_engine_1_1_preview_1_1_detect_orientation_change" ]
    ] ],
    [ "DeviceCustomBorder.cs", "_device_custom_border_8cs.html", [
      [ "DeviceCustomBorder", "class_almost_engine_1_1_examples_1_1_preview_1_1_device_custom_border.html", "class_almost_engine_1_1_examples_1_1_preview_1_1_device_custom_border" ]
    ] ],
    [ "DeviceInfo.cs", "_device_info_8cs.html", [
      [ "DeviceInfo", "class_almost_engine_1_1_preview_1_1_device_info.html", "class_almost_engine_1_1_preview_1_1_device_info" ]
    ] ],
    [ "MaskRenderer.cs", "_mask_renderer_8cs.html", [
      [ "MaskRenderer", "class_almost_engine_1_1_preview_1_1_mask_renderer.html", "class_almost_engine_1_1_preview_1_1_mask_renderer" ]
    ] ],
    [ "SafeArea.cs", "_safe_area_8cs.html", [
      [ "SafeArea", "class_almost_engine_1_1_preview_1_1_safe_area.html", "class_almost_engine_1_1_preview_1_1_safe_area" ]
    ] ],
    [ "UniversalDevicePreview.cs", "_universal_device_preview_8cs.html", [
      [ "UniversalDevicePreview", "class_almost_engine_1_1_preview_1_1_universal_device_preview.html", "class_almost_engine_1_1_preview_1_1_universal_device_preview" ]
    ] ]
];