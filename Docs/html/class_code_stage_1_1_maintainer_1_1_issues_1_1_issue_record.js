var class_code_stage_1_1_maintainer_1_1_issues_1_1_issue_record =
[
    [ "IssueRecord", "class_code_stage_1_1_maintainer_1_1_issues_1_1_issue_record.html#a0709763013a573a7cd6366434abea280", null ],
    [ "ConstructCompactLine", "class_code_stage_1_1_maintainer_1_1_issues_1_1_issue_record.html#acead248ab23a8585b1f4706a1ab78d45", null ],
    [ "ConstructHeader", "class_code_stage_1_1_maintainer_1_1_issues_1_1_issue_record.html#a392cab9fe109d00ec846a53e2dd93d53", null ],
    [ "Fix", "class_code_stage_1_1_maintainer_1_1_issues_1_1_issue_record.html#a674162bdffbc3e20c55dead463caadf8", null ],
    [ "IsFixable", "class_code_stage_1_1_maintainer_1_1_issues_1_1_issue_record.html#a233da1ede71e2b14d53d306ed2c1e38d", null ],
    [ "Kind", "class_code_stage_1_1_maintainer_1_1_issues_1_1_issue_record.html#a9536b706858b6314031ac617a2bc5986", null ],
    [ "Severity", "class_code_stage_1_1_maintainer_1_1_issues_1_1_issue_record.html#a5f154bfef206527de0934b2d1661ced2", null ]
];