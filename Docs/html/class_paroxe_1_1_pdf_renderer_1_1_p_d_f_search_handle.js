var class_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_handle =
[
    [ "MatchOption", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_handle.html#af83de9ad68f8382f55f58b62e4b0f28d", [
      [ "NONE", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_handle.html#af83de9ad68f8382f55f58b62e4b0f28dab50339a10e1de285ac99d4c3990b8693", null ],
      [ "MATCH_CASE", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_handle.html#af83de9ad68f8382f55f58b62e4b0f28dae5467ab895e769369f1e5af4766257b4", null ],
      [ "MATCH_WHOLE_WORD", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_handle.html#af83de9ad68f8382f55f58b62e4b0f28dafd2572df7309090a6b725bbf8df5554d", null ],
      [ "MATCH_CASE_AND_WHOLE_WORD", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_handle.html#af83de9ad68f8382f55f58b62e4b0f28da41ab3962e439eb99d0056f342a2fbf85", null ]
    ] ],
    [ "PDFSearchHandle", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_handle.html#a7abf793b27fc88316381a81a8bfc39fa", null ],
    [ "Dispose", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_handle.html#a80f5b72dc001b4e18335dbb4b9bf43fd", null ],
    [ "Dispose", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_handle.html#a13f51cc0c078501e04c9a39107e29b37", null ],
    [ "EnumerateSearchResults", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_handle.html#a837191e61598ab3ed13f8379203a66a0", null ],
    [ "FindNext", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_handle.html#ad3153b6f125049fa1aa9e7ae52752c8b", null ],
    [ "FindPrevious", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_handle.html#a46cde644053f8d5c9aa19a1f4a6b949a", null ],
    [ "GetResults", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_handle.html#a0766945fcdbbb6fdf57a6d037b46d14f", null ],
    [ "NativePointer", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_handle.html#a37efa328579e4d748a7266261d759dae", null ]
];