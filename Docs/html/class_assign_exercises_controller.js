var class_assign_exercises_controller =
[
    [ "AssignBtn_pressed", "class_assign_exercises_controller.html#a33d36ef88398ec885600dfef5ce7f09a", null ],
    [ "DaysPerWeek", "class_assign_exercises_controller.html#a45154d6251ec54966e540ca7c4ab41ab", null ],
    [ "ErrorMsgTxt", "class_assign_exercises_controller.html#ab5bf6cfbb00ae46e72dad065f96c0745", null ],
    [ "Grades", "class_assign_exercises_controller.html#a14ae3ef5062a8bb448a9fe0dd6b8b188", null ],
    [ "Grades_GO", "class_assign_exercises_controller.html#af4651f603a0e4f66116748fd354b5e3d", null ],
    [ "Notes", "class_assign_exercises_controller.html#ac7ab9850aafd599e56cd8857430eb8f3", null ],
    [ "PanelTitle", "class_assign_exercises_controller.html#a2d4f66310a73aada6501d8d74e15fedc", null ],
    [ "RepitionsPerSet", "class_assign_exercises_controller.html#ad3e4ba10d51f89728137413b851c4d89", null ],
    [ "SetsPerDay", "class_assign_exercises_controller.html#a4fe3201a367562dfc11c58d4b8c6ab85", null ]
];