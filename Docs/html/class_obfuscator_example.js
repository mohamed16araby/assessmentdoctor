var class_obfuscator_example =
[
    [ "EquivalentMethod", "class_obfuscator_example.html#a02bd777c84fd4852e355a4309ae90c89", null ],
    [ "ObfuscatedButtonMethod", "class_obfuscator_example.html#a5a4779205ee27d74c19d8c299c2fc4de", null ],
    [ "ObfuscatedMethod", "class_obfuscator_example.html#a5ecb9139b33f9581d1a86554c900f4d5", null ],
    [ "ObfuscatedMethod", "class_obfuscator_example.html#a1601f9bd77099cb4cd7b484646216652", null ],
    [ "OldName", "class_obfuscator_example.html#ad58e99f024f0cc6c34c9ad38d3de2683", null ],
    [ "OnAnimationEvent", "class_obfuscator_example.html#a8b543dd4c55466b7c35426b35de2e146", null ],
    [ "OnButtonClick", "class_obfuscator_example.html#aa2d5117f14c785f812f918d0309332f4", null ],
    [ "SkipMethod", "class_obfuscator_example.html#a2cbdf616c074c2adce9977540e93f903", null ],
    [ "SkipRenameMethod", "class_obfuscator_example.html#ac72d60581ec86273709bfa5f9e421be8", null ],
    [ "OldPublicField", "class_obfuscator_example.html#a6e1f7b0be6f10744a6d6b56eee54cc14", null ],
    [ "publicObfuscatedField", "class_obfuscator_example.html#ab480bc7301dfead5100451e24561a882", null ]
];