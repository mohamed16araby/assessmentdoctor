var dir_0ab6b3f211d6bf0b572c410e02cb9ebf =
[
    [ "ChannelSearchDemo.cs", "_channel_search_demo_8cs.html", [
      [ "ChannelSearchDemo", "class_channel_search_demo.html", "class_channel_search_demo" ]
    ] ],
    [ "CommentsDemo.cs", "_comments_demo_8cs.html", [
      [ "CommentsDemo", "class_comments_demo.html", "class_comments_demo" ]
    ] ],
    [ "IndividualVideoDataDemo.cs", "_individual_video_data_demo_8cs.html", [
      [ "IndividualVideoDataDemo", "class_individual_video_data_demo.html", "class_individual_video_data_demo" ]
    ] ],
    [ "PlaylistDemo.cs", "_playlist_demo_8cs.html", [
      [ "PlaylistDemo", "class_playlist_demo.html", "class_playlist_demo" ]
    ] ],
    [ "VideoSearchDemo.cs", "_video_search_demo_8cs.html", [
      [ "VideoSearchDemo", "class_video_search_demo.html", "class_video_search_demo" ]
    ] ],
    [ "YoutubeApiGetUnlimitedVideos.cs", "_youtube_api_get_unlimited_videos_8cs.html", [
      [ "YoutubeApiGetUnlimitedVideos", "class_youtube_api_get_unlimited_videos.html", "class_youtube_api_get_unlimited_videos" ]
    ] ],
    [ "YoutubeChannelUI.cs", "_youtube_channel_u_i_8cs.html", [
      [ "YoutubeChannelUI", "class_youtube_channel_u_i.html", "class_youtube_channel_u_i" ]
    ] ],
    [ "YoutubeVideoUi.cs", "_youtube_video_ui_8cs.html", [
      [ "YoutubeVideoUi", "class_youtube_video_ui.html", "class_youtube_video_ui" ]
    ] ]
];