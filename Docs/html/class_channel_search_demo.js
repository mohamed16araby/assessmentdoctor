var class_channel_search_demo =
[
    [ "LoadChannelResult", "class_channel_search_demo.html#a3e24eb2c37a4b43cb3c4907ae4e433b3", null ],
    [ "SearchChannel", "class_channel_search_demo.html#afe068f18eae8dd8aa46f22c07f28a03e", null ],
    [ "channelListUI", "class_channel_search_demo.html#a1b87eeb3f719daf201532557f65cb70e", null ],
    [ "channelUIResult", "class_channel_search_demo.html#a041128e5155358a06e546bdd236f85a4", null ],
    [ "mainFilters", "class_channel_search_demo.html#a220a7ec4f16bc25b565ec3673599ca83", null ],
    [ "mainUI", "class_channel_search_demo.html#a6fde2176c740b52ad9f1775cb4fd3972", null ],
    [ "searchField", "class_channel_search_demo.html#a9297322e3b645e392b93cfe21b80c815", null ],
    [ "videoListUI", "class_channel_search_demo.html#a6912267db7a14f16791709b99ae6bf59", null ],
    [ "videoUIResult", "class_channel_search_demo.html#a3a9e805bdfdf6a8e6b021b27205ec262", null ]
];