var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz",
  1: "abcdefghijlmnoprstuvwy",
  2: "abceghkmnprsty",
  3: "abcdefghijklmnoprstuvwy",
  4: "_abcdefghijlmnopqrstuvwxyz",
  5: "_abcdefghijklmnopqrstuvwxyz",
  6: "abdfotu",
  7: "abcdefghijlmoprstvwy",
  8: "abcdefghijklmnopqrstuvwz",
  9: "abcdefghijklmnopqrstuvwyz",
  10: "adgiostv",
  11: "u"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "properties",
  10: "events",
  11: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Properties",
  10: "Events",
  11: "Macros"
};

