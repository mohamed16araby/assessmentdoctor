var dir_a4c8a5e96427ac87f38302878724b818 =
[
    [ "AddEdit_MedicalRecord_Panel_Model.cs", "_add_edit___medical_record___panel___model_8cs.html", "_add_edit___medical_record___panel___model_8cs" ],
    [ "Algorithm_Model.cs", "_algorithm___model_8cs.html", "_algorithm___model_8cs" ],
    [ "ChoosingSides_Panel_Model.cs", "_choosing_sides___panel___model_8cs.html", "_choosing_sides___panel___model_8cs" ],
    [ "ChoosingSubspeciality_Panel_Model.cs", "_choosing_subspeciality___panel___model_8cs.html", "_choosing_subspeciality___panel___model_8cs" ],
    [ "CustomizePanel_Model.cs", "_customize_panel___model_8cs.html", [
      [ "CustomizePanel_Model", "class_customize_panel___model.html", "class_customize_panel___model" ]
    ] ],
    [ "MainPanel_Model.cs", "_main_panel___model_8cs.html", [
      [ "MainPanel_Model", "class_main_panel___model.html", "class_main_panel___model" ]
    ] ],
    [ "Panel_Model.cs", "_panel___model_8cs.html", [
      [ "Panel_2MainHeaders_Model", "class_panel__2_main_headers___model.html", "class_panel__2_main_headers___model" ],
      [ "Panel_3MainHeaders_Model", "class_panel__3_main_headers___model.html", "class_panel__3_main_headers___model" ],
      [ "Panel_4MainHeaders_Model", "class_panel__4_main_headers___model.html", "class_panel__4_main_headers___model" ]
    ] ],
    [ "SidePanel_Model.cs", "_side_panel___model_8cs.html", [
      [ "SidePanel_Model", "class_side_panel___model.html", "class_side_panel___model" ]
    ] ]
];