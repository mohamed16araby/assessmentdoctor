var _add_edit___medical_record___panel___model_8cs =
[
    [ "AddEdit_MedicalRecord_Panel_Model", "class_add_edit___medical_record___panel___model.html", "class_add_edit___medical_record___panel___model" ],
    [ "fb", "_add_edit___medical_record___panel___model_8cs.html#a6aed44ddf0f5a44a7aa92423292006ae", null ],
    [ "URI", "_add_edit___medical_record___panel___model_8cs.html#a1930caaef3335addfb398a6b5f22e559", null ],
    [ "FollowupChoices", "_add_edit___medical_record___panel___model_8cs.html#af2607fd161ff6be8502af6bf28b0297b", [
      [ "EditLastFollowup", "_add_edit___medical_record___panel___model_8cs.html#af2607fd161ff6be8502af6bf28b0297ba163c78a9070073a14ffda23bc7a9faa8", null ],
      [ "CreateFollowupFromScratch", "_add_edit___medical_record___panel___model_8cs.html#af2607fd161ff6be8502af6bf28b0297bad04a1fbfae080013fdc97ad9583bda23", null ]
    ] ],
    [ "PerformAnotherAssessmentChoices", "_add_edit___medical_record___panel___model_8cs.html#a7e3c5c82930d395df494f82823c96d62", [
      [ "None", "_add_edit___medical_record___panel___model_8cs.html#a7e3c5c82930d395df494f82823c96d62a6adf97f83acf6453d4a6a4b1070f3754", null ],
      [ "EditOldAssessment", "_add_edit___medical_record___panel___model_8cs.html#a7e3c5c82930d395df494f82823c96d62a5326c8c60cd634235b8f59a727c2b71b", null ],
      [ "ReevaluateOldAssessment", "_add_edit___medical_record___panel___model_8cs.html#a7e3c5c82930d395df494f82823c96d62a38f11b36d40a5f4bac6b63d1a1385e5b", null ],
      [ "NewAssessmentFromScratch", "_add_edit___medical_record___panel___model_8cs.html#a7e3c5c82930d395df494f82823c96d62a5bff17c85f1b36c9ade8fc553ca6e10d", null ],
      [ "Followup", "_add_edit___medical_record___panel___model_8cs.html#a7e3c5c82930d395df494f82823c96d62a313b56e67b3771e0ab2f44c062854590", null ]
    ] ]
];