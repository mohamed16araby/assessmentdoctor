var class_diagnosis_list___controller =
[
    [ "AddPhrasesToVoice", "class_diagnosis_list___controller.html#a5f82bb5fa7eca4677364a4f37ff211b1", null ],
    [ "BackBtn_pressed", "class_diagnosis_list___controller.html#a5a4d961e4d9d149ec93ca9c604a85847", null ],
    [ "EnableTransitionButtons", "class_diagnosis_list___controller.html#aabab8ad1d46aefef2d83addf8c3f71c2", null ],
    [ "GetCellView", "class_diagnosis_list___controller.html#a60040e5797d74da595126bbfd394cc30", null ],
    [ "GetCellViewSize", "class_diagnosis_list___controller.html#a329aae08e70072dbdd3468fe55701231", null ],
    [ "GetNumberOfCells", "class_diagnosis_list___controller.html#abb0a4e2626925de239666cc6a18ca57b", null ],
    [ "GoBack", "class_diagnosis_list___controller.html#a17def4e0d168233e3fef8446702a1f7c", null ],
    [ "LoadDiagnosisListData", "class_diagnosis_list___controller.html#adebba3b8d3180058eef07348310b56e1", null ],
    [ "NextBtn_pressed", "class_diagnosis_list___controller.html#a0e944809a025a87f1d6f9b773e61e8df", null ],
    [ "DiagnosisScroller", "class_diagnosis_list___controller.html#a7d5af9411d5e378cb3da1ba9d2600f4f", null ],
    [ "diagnosisViewPrefab", "class_diagnosis_list___controller.html#a0f69bbc10cb49af57daa624ded17e675", null ]
];