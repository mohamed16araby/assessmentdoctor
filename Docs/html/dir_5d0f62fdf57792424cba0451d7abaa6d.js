var dir_5d0f62fdf57792424cba0451d7abaa6d =
[
    [ "YoutubeAPIManager.cs", "_youtube_a_p_i_manager_8cs.html", [
      [ "YoutubeAPIManager", "class_youtube_a_p_i_manager.html", "class_youtube_a_p_i_manager" ],
      [ "YoutubeData", "class_youtube_data.html", "class_youtube_data" ],
      [ "YoutubeComments", "class_youtube_comments.html", "class_youtube_comments" ],
      [ "YoutubePlaylistItems", "class_youtube_playlist_items.html", "class_youtube_playlist_items" ],
      [ "YoutubeChannel", "class_youtube_channel.html", "class_youtube_channel" ]
    ] ],
    [ "YoutubeContentDetails.cs", "_youtube_content_details_8cs.html", [
      [ "YoutubeContentDetails", "class_youtube_content_details.html", "class_youtube_content_details" ]
    ] ],
    [ "YoutubeSnippet.cs", "_youtube_snippet_8cs.html", [
      [ "YoutubeSnippet", "class_youtube_snippet.html", "class_youtube_snippet" ],
      [ "YoutubeTumbnails", "class_youtube_tumbnails.html", "class_youtube_tumbnails" ],
      [ "YoutubeThumbnailData", "class_youtube_thumbnail_data.html", "class_youtube_thumbnail_data" ]
    ] ],
    [ "YoutubeStatistics.cs", "_youtube_statistics_8cs.html", [
      [ "YoutubeStatistics", "class_youtube_statistics.html", "class_youtube_statistics" ]
    ] ]
];