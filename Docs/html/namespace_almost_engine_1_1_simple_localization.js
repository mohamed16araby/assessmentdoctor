var namespace_almost_engine_1_1_simple_localization =
[
    [ "ISimpleLocalizer", "class_almost_engine_1_1_simple_localization_1_1_i_simple_localizer.html", "class_almost_engine_1_1_simple_localization_1_1_i_simple_localizer" ],
    [ "LanguagesDrawer", "class_almost_engine_1_1_simple_localization_1_1_languages_drawer.html", "class_almost_engine_1_1_simple_localization_1_1_languages_drawer" ],
    [ "LanguageSwitcher", "class_almost_engine_1_1_simple_localization_1_1_language_switcher.html", "class_almost_engine_1_1_simple_localization_1_1_language_switcher" ],
    [ "SimpleImageLocalizer", "class_almost_engine_1_1_simple_localization_1_1_simple_image_localizer.html", "class_almost_engine_1_1_simple_localization_1_1_simple_image_localizer" ],
    [ "SimpleImageLocalizerInspector", "class_almost_engine_1_1_simple_localization_1_1_simple_image_localizer_inspector.html", "class_almost_engine_1_1_simple_localization_1_1_simple_image_localizer_inspector" ],
    [ "SimpleLocalizationLanguagesAsset", "class_almost_engine_1_1_simple_localization_1_1_simple_localization_languages_asset.html", "class_almost_engine_1_1_simple_localization_1_1_simple_localization_languages_asset" ],
    [ "SimpleTextLocalizer", "class_almost_engine_1_1_simple_localization_1_1_simple_text_localizer.html", "class_almost_engine_1_1_simple_localization_1_1_simple_text_localizer" ],
    [ "SimpleTextLocalizerInspector", "class_almost_engine_1_1_simple_localization_1_1_simple_text_localizer_inspector.html", "class_almost_engine_1_1_simple_localization_1_1_simple_text_localizer_inspector" ]
];