var _algorithm___model_8cs =
[
    [ "Algorithm_Model", "class_algorithm___model.html", "class_algorithm___model" ],
    [ "fb", "_algorithm___model_8cs.html#a6aed44ddf0f5a44a7aa92423292006ae", null ],
    [ "URI", "_algorithm___model_8cs.html#a1930caaef3335addfb398a6b5f22e559", null ],
    [ "AgorithmFirebaseRequests", "_algorithm___model_8cs.html#a4372051125edc6daa2df2cd512cbb9a1", [
      [ "Save", "_algorithm___model_8cs.html#a4372051125edc6daa2df2cd512cbb9a1ac9cc8cce247e49bae79f15173ce97354", null ],
      [ "Delete", "_algorithm___model_8cs.html#a4372051125edc6daa2df2cd512cbb9a1af2a6c498fb90ee345d997f888fce3b18", null ]
    ] ],
    [ "OperationState", "_algorithm___model_8cs.html#ad634f347acbb7db0a858f667a945f07b", [
      [ "SUCCESS", "_algorithm___model_8cs.html#ad634f347acbb7db0a858f667a945f07bad0749aaba8b833466dfcbb0428e4f89c", null ],
      [ "FAILED", "_algorithm___model_8cs.html#ad634f347acbb7db0a858f667a945f07bab9e14d9b2886bcff408b85aefa780419", null ]
    ] ]
];