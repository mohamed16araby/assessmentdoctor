var class_reacting_lights =
[
    [ "VideoSide", "class_reacting_lights.html#a3b844b8ec151a227593979c6ccb6af0e", [
      [ "up", "class_reacting_lights.html#a3b844b8ec151a227593979c6ccb6af0ea46c48bec0d282018b9d167eef7711b2c", null ],
      [ "left", "class_reacting_lights.html#a3b844b8ec151a227593979c6ccb6af0ea811882fecd5c7618d7099ebbd39ea254", null ],
      [ "right", "class_reacting_lights.html#a3b844b8ec151a227593979c6ccb6af0ea7c4f29407893c334a6cb7a87bf045c0d", null ],
      [ "down", "class_reacting_lights.html#a3b844b8ec151a227593979c6ccb6af0ea74e8333ad11685ff3bdae589c8f6e34d", null ],
      [ "center", "class_reacting_lights.html#a3b844b8ec151a227593979c6ccb6af0eaadb115059e28d960fa8badfac5516667", null ]
    ] ],
    [ "averageColor", "class_reacting_lights.html#ad2c9a40a3e08d253392641a0aa775bbb", null ],
    [ "lights", "class_reacting_lights.html#a841a4311901090ebc4965b897227458d", null ],
    [ "videoSide", "class_reacting_lights.html#ac9c584a4c3cb662988d6d7ee794246b5", null ],
    [ "videoSource", "class_reacting_lights.html#a3341717148f4c9b7c0a5a29e8fd32824", null ]
];