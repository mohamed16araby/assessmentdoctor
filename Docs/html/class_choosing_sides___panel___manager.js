var class_choosing_sides___panel___manager =
[
    [ "AdjustButtons", "class_choosing_sides___panel___manager.html#acea70665892211e4191f83b545b84732", null ],
    [ "CanGoToAssessmentPanels", "class_choosing_sides___panel___manager.html#abe3a6dfd2becfa487ede1b2bfa6c0c95", null ],
    [ "EnableChoosingSides_Panel_View", "class_choosing_sides___panel___manager.html#a4fb20be2bf50770863bad6d816b280f1", null ],
    [ "Enter_ChoosingSides_Panel", "class_choosing_sides___panel___manager.html#a95f9cebf067f8f78f934ee982f02f3f3", null ],
    [ "GetSideToAssess", "class_choosing_sides___panel___manager.html#a58ea3e6d3df1b7074d1e9098c1c85c7c", null ],
    [ "GoBack", "class_choosing_sides___panel___manager.html#adf39321f6e0b86c263df5c7c950a0894", null ],
    [ "GoNext", "class_choosing_sides___panel___manager.html#a210e352753c45b584ac7833d102873a5", null ],
    [ "IsThereSidesToChooseFrom", "class_choosing_sides___panel___manager.html#a39d65a94e8bdabf30612df81305bff77", null ],
    [ "choosingSides_Panel_MVC", "class_choosing_sides___panel___manager.html#afee38b2f272282488b92b37e958b0088", null ],
    [ "Instance", "class_choosing_sides___panel___manager.html#a6b755b8f82f02cacb22810a8dd07dea8", null ]
];