var namespace_code_stage_1_1_maintainer =
[
    [ "Cleaner", "namespace_code_stage_1_1_maintainer_1_1_cleaner.html", "namespace_code_stage_1_1_maintainer_1_1_cleaner" ],
    [ "Core", "namespace_code_stage_1_1_maintainer_1_1_core.html", "namespace_code_stage_1_1_maintainer_1_1_core" ],
    [ "Issues", "namespace_code_stage_1_1_maintainer_1_1_issues.html", "namespace_code_stage_1_1_maintainer_1_1_issues" ],
    [ "References", "namespace_code_stage_1_1_maintainer_1_1_references.html", "namespace_code_stage_1_1_maintainer_1_1_references" ],
    [ "Settings", "namespace_code_stage_1_1_maintainer_1_1_settings.html", "namespace_code_stage_1_1_maintainer_1_1_settings" ],
    [ "UI", "namespace_code_stage_1_1_maintainer_1_1_u_i.html", null ],
    [ "Maintainer", "class_code_stage_1_1_maintainer_1_1_maintainer.html", "class_code_stage_1_1_maintainer_1_1_maintainer" ],
    [ "MaintainerMarker", "class_code_stage_1_1_maintainer_1_1_maintainer_marker.html", "class_code_stage_1_1_maintainer_1_1_maintainer_marker" ],
    [ "RecordBase", "class_code_stage_1_1_maintainer_1_1_record_base.html", "class_code_stage_1_1_maintainer_1_1_record_base" ],
    [ "ReportsBuilder", "class_code_stage_1_1_maintainer_1_1_reports_builder.html", "class_code_stage_1_1_maintainer_1_1_reports_builder" ]
];