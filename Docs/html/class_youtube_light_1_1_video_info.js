var class_youtube_light_1_1_video_info =
[
    [ "ToString", "class_youtube_light_1_1_video_info.html#afdd630f6bb732a7119e8b57c04ebefed", null ],
    [ "AdaptiveType", "class_youtube_light_1_1_video_info.html#a4a76e0f4f3f278915010d9a13538b8b0", null ],
    [ "AudioBitrate", "class_youtube_light_1_1_video_info.html#a86d3f56c5c47078d260eb691f6c1787f", null ],
    [ "AudioExtension", "class_youtube_light_1_1_video_info.html#a573c48b68608712db1bce3c4e642317a", null ],
    [ "AudioType", "class_youtube_light_1_1_video_info.html#aaef500409f0c135d510343c81ac1f520", null ],
    [ "CanExtractAudio", "class_youtube_light_1_1_video_info.html#a040ba5e0523da38f525d23977f9c6044", null ],
    [ "DownloadUrl", "class_youtube_light_1_1_video_info.html#aab6147d41693d05ff61547e13c7c3d9b", null ],
    [ "FormatCode", "class_youtube_light_1_1_video_info.html#a4151c795917ce8a0324a291fd8850ce6", null ],
    [ "HDR", "class_youtube_light_1_1_video_info.html#aeda29ce3dca6c96944ac29fd51b82711", null ],
    [ "HtmlPlayerVersion", "class_youtube_light_1_1_video_info.html#a9e5a198de7ce627c044e6565ca864b70", null ],
    [ "HtmlscriptName", "class_youtube_light_1_1_video_info.html#a7b5195e9ba98fd8da0341112aafd3221", null ],
    [ "Is3D", "class_youtube_light_1_1_video_info.html#a13473602ddb0c5bef2bf25da0fdc8f8e", null ],
    [ "RequiresDecryption", "class_youtube_light_1_1_video_info.html#a5a425c3305126020af93a80b90352f73", null ],
    [ "Resolution", "class_youtube_light_1_1_video_info.html#a065d41b513af1969de3323ce9159eba4", null ],
    [ "Title", "class_youtube_light_1_1_video_info.html#a957cd660c678dc24f7efa215416cc055", null ],
    [ "VideoExtension", "class_youtube_light_1_1_video_info.html#afca88b591a764bf037a37f27a423c735", null ],
    [ "VideoType", "class_youtube_light_1_1_video_info.html#a6b948a97b39466db85968cd65bf32806", null ]
];