var class_treatment_goals =
[
    [ "Decrypt", "class_treatment_goals.html#a6fb3b6afba853f277fbba420eb608abb", null ],
    [ "Decryption", "class_treatment_goals.html#a1f0a884bfb70d8f8598dcda3fbb5230b", null ],
    [ "Encrypt", "class_treatment_goals.html#a05b413fe9615bb532a126dd2a2487cc0", null ],
    [ "Encryption", "class_treatment_goals.html#a1da6fc7129a61ae1d1a67df431aa6c92", null ],
    [ "currentGoalsIsShort", "class_treatment_goals.html#a79044637332a7211315fd77b2a874bf9", null ],
    [ "selected_LongTerm_goals_EnOrDecrpted", "class_treatment_goals.html#a10ed81ef132258a9914ac000c73fcd3b", null ],
    [ "selected_LongTerm_goals_Orig", "class_treatment_goals.html#adae5e636ed8df7b5d0f3c4cba23ea04c", null ],
    [ "selected_ShortTerm_goals_EnOrDecrpted", "class_treatment_goals.html#ab567d622c31230096ce39beb9cb38afe", null ],
    [ "selected_ShortTerm_goals_Orig", "class_treatment_goals.html#a1c640dbea2adf35d8ce511d537cf30f4", null ]
];