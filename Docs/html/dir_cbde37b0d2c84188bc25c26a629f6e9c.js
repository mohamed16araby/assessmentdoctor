var dir_cbde37b0d2c84188bc25c26a629f6e9c =
[
    [ "Authentication", "dir_42cc01795d1c5d080e7bc2cc5c364c06.html", "dir_42cc01795d1c5d080e7bc2cc5c364c06" ],
    [ "Storage", "dir_618e1bb8cfef920fd1f1ed775aa75abf.html", "dir_618e1bb8cfef920fd1f1ed775aa75abf" ],
    [ "APIConfig.cs", "_a_p_i_config_8cs.html", null ],
    [ "DatabaseClasses.cs", "_database_classes_8cs.html", "_database_classes_8cs" ],
    [ "Firebase.cs", "_firebase_8cs.html", [
      [ "Firebase", "class_cammedar_1_1_network_1_1_firebase.html", "class_cammedar_1_1_network_1_1_firebase" ]
    ] ],
    [ "FireBaseAPI.cs", "_fire_base_a_p_i_8cs.html", [
      [ "FirebaseAPI", "class_firebase_a_p_i.html", "class_firebase_a_p_i" ],
      [ "FirebaseDatabaseData", "struct_firebase_database_data.html", "struct_firebase_database_data" ]
    ] ],
    [ "FirebaseOperation.cs", "_firebase_operation_8cs.html", "_firebase_operation_8cs" ],
    [ "URI.cs", "_u_r_i_8cs.html", [
      [ "URI", "class_cammedar_1_1_network_1_1_u_r_i.html", "class_cammedar_1_1_network_1_1_u_r_i" ]
    ] ]
];