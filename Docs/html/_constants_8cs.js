var _constants_8cs =
[
    [ "Debugging", "struct_debugging.html", "struct_debugging" ],
    [ "PatientData", "struct_patient_data.html", "struct_patient_data" ],
    [ "ProgramGeneralData", "struct_program_general_data.html", "struct_program_general_data" ],
    [ "UserData", "struct_user_data.html", "struct_user_data" ],
    [ "FirebaseStoragePaths", "struct_firebase_storage_paths.html", "struct_firebase_storage_paths" ],
    [ "Operations", "class_operations.html", "class_operations" ],
    [ "SessionFirebaseRequests", "_constants_8cs.html#a0f534d0a19aa3f3f43ea1bd273bee807", [
      [ "None", "_constants_8cs.html#a0f534d0a19aa3f3f43ea1bd273bee807a6adf97f83acf6453d4a6a4b1070f3754", null ],
      [ "AddSessionData", "_constants_8cs.html#a0f534d0a19aa3f3f43ea1bd273bee807aacdb8717d1e6aa5bd92036303354dcbd", null ],
      [ "EditSessionData", "_constants_8cs.html#a0f534d0a19aa3f3f43ea1bd273bee807acf5b988290aaa75a45f47357b8e7fe93", null ],
      [ "DownloadAssessmentFileFromStorage", "_constants_8cs.html#a0f534d0a19aa3f3f43ea1bd273bee807a810d6cbb3565cd4784c3a6356bfd71b2", null ],
      [ "DownloadPatientTreatGoals_ShortTerm_File", "_constants_8cs.html#a0f534d0a19aa3f3f43ea1bd273bee807a5a070da774081b7a84cd9161ae8c2ca5", null ],
      [ "DownloadPatientTreatGoals_LongTerm_File", "_constants_8cs.html#a0f534d0a19aa3f3f43ea1bd273bee807a39826d0dd30daf922600685ed401fc5f", null ]
    ] ]
];