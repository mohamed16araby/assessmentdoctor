var class_youtube_light_1_1_downloader =
[
    [ "Downloader", "class_youtube_light_1_1_downloader.html#ac51d55ecf556ef2f189f10b63e176d6e", null ],
    [ "Execute", "class_youtube_light_1_1_downloader.html#a1b1edf40d96acc717d6c8ab6cf80b29d", null ],
    [ "OnDownloadFinished", "class_youtube_light_1_1_downloader.html#aec71bba15d672adcb04bd759345ae1e1", null ],
    [ "OnDownloadStarted", "class_youtube_light_1_1_downloader.html#a42f619bf5722d6defde730c6f249b0e9", null ],
    [ "BytesToDownload", "class_youtube_light_1_1_downloader.html#af66d40664ab2def5cc72136cddaf35d3", null ],
    [ "SavePath", "class_youtube_light_1_1_downloader.html#aacaf28eaf4dfea0dc2caf26ad1cc25e0", null ],
    [ "Video", "class_youtube_light_1_1_downloader.html#a57512b644f6559595f86f9b6d7dcd28a", null ],
    [ "DownloadFinished", "class_youtube_light_1_1_downloader.html#aa09a7ead79735736a9ea46b348306f5f", null ],
    [ "DownloadStarted", "class_youtube_light_1_1_downloader.html#a1f097e7179e37b3a05ebe6a88d299b89", null ]
];