var class_video_search_demo =
[
    [ "GetTrendingVideos", "class_video_search_demo.html#a752352a2df8737870e1eae322b6e48b2", null ],
    [ "Search", "class_video_search_demo.html#af5bfeed0fd815b28de42f86318b62f1c", null ],
    [ "SearchByLocation", "class_video_search_demo.html#a6c96b9f2f655917f01c39b85c96ef1b6", null ],
    [ "categoryField", "class_video_search_demo.html#a820dc382b0d64821c963e6fae3c9b24d", null ],
    [ "categoryFilter", "class_video_search_demo.html#a118b013d69892f2bb539d6fa01de4e6f", null ],
    [ "customFilter", "class_video_search_demo.html#a24e5847c38c5d5067bfa72e2a016d9e8", null ],
    [ "mainFilters", "class_video_search_demo.html#a66143fea6f2e8be90782e3961e194ba4", null ],
    [ "mainUI", "class_video_search_demo.html#a1b973872560cacf465e8601a8325be5f", null ],
    [ "regionCode", "class_video_search_demo.html#a85a174d5b87e4f0a6e2e9a58b7d8a7d1", null ],
    [ "searchField", "class_video_search_demo.html#aeaa6f80908ece0a3e64dd3aa1753f566", null ],
    [ "videoListUI", "class_video_search_demo.html#a09b438d95da7bc9feaa218f823da5feb", null ],
    [ "videoUIResult", "class_video_search_demo.html#a85e1f0b3e1caca5a2fcd0949c324c451", null ]
];