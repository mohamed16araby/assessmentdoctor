var class_customize_panels___manager =
[
    [ "ApproveGoingBackPrompt", "class_customize_panels___manager.html#a6460b3875c792f74c0603ce76a198538", null ],
    [ "DisablePanel", "class_customize_panels___manager.html#a8b35a98440fdf14c873028c5115e341f", null ],
    [ "EnterBackPanel", "class_customize_panels___manager.html#add43987c4a52714b41ea8d52647d5e7c", null ],
    [ "EnterCustomizePanel", "class_customize_panels___manager.html#a6cc38cd8117ce65c735f5c2bf8829ae9", null ],
    [ "GoBack", "class_customize_panels___manager.html#a0f4f97cb2782933aeef75a0b623ca4c8", null ],
    [ "GoNext", "class_customize_panels___manager.html#a792767857e3085014f15ee64d50a597e", null ],
    [ "LoadGroupsWhenReady_questionLoaded", "class_customize_panels___manager.html#a0381ca2dba45ea9f78e4c69a86d2d286", null ],
    [ "OnCustomizationItemsInstantiated", "class_customize_panels___manager.html#a30aea6e52d0be13434e17d0187f71621", null ],
    [ "PrepareCustomizationPanel", "class_customize_panels___manager.html#a455b81fe8ca38fefda67a20cea8b3daf", null ],
    [ "RemovePanelContent", "class_customize_panels___manager.html#a7e3b6b39b65905ff82183181dcb77727", null ],
    [ "RemovePanelsContents", "class_customize_panels___manager.html#a864b75146a942396ccb69cef276e0fd4", null ],
    [ "SavePatientVisitAlgorithm", "class_customize_panels___manager.html#af66cd72388a619cfc027c753596b04b4", null ],
    [ "algorithm_MVC", "class_customize_panels___manager.html#a503d52d0f021294f53aba38eea182804", null ],
    [ "customizePanel_MVC", "class_customize_panels___manager.html#afbcba71571ffa6c88d5fbd62543a0619", null ],
    [ "CurrentPanelIsCustomizePanel", "class_customize_panels___manager.html#a596d687665a1c60213bfdfe6a2310237", null ],
    [ "Instance", "class_customize_panels___manager.html#a06ac2d53d5656dcbefc59341c7297fc2", null ]
];