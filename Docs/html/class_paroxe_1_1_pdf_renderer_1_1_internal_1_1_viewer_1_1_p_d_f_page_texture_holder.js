var class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_page_texture_holder =
[
    [ "RefreshTexture", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_page_texture_holder.html#afd676e36b41565e59c9bd31bb57bae83", null ],
    [ "m_AspectRatioFitter", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_page_texture_holder.html#a85de52059e6b0f504cfaa05aa5e80c36", null ],
    [ "m_Page", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_page_texture_holder.html#a934af0bf797dd024d609c8578177f8a4", null ],
    [ "m_PageIndex", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_page_texture_holder.html#a5f169e9953d1d33440824ad68e3b7db9", null ],
    [ "m_PDFViewer", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_page_texture_holder.html#a50c1accb9c9e3c071e0ff6a624f1bbec", null ],
    [ "Texture", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_page_texture_holder.html#a3b83cd132baed0e181236924504202da", null ]
];