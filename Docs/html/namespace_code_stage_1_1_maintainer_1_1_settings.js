var namespace_code_stage_1_1_maintainer_1_1_settings =
[
    [ "IssuesFinderPersonalSettings", "class_code_stage_1_1_maintainer_1_1_settings_1_1_issues_finder_personal_settings.html", "class_code_stage_1_1_maintainer_1_1_settings_1_1_issues_finder_personal_settings" ],
    [ "IssuesFinderSettings", "class_code_stage_1_1_maintainer_1_1_settings_1_1_issues_finder_settings.html", "class_code_stage_1_1_maintainer_1_1_settings_1_1_issues_finder_settings" ],
    [ "ProjectCleanerPersonalSettings", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_personal_settings.html", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_personal_settings" ],
    [ "ProjectCleanerSettings", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings.html", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings" ],
    [ "ProjectSettings", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_settings.html", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_settings" ],
    [ "ReferencesFinderPersonalSettings", "class_code_stage_1_1_maintainer_1_1_settings_1_1_references_finder_personal_settings.html", "class_code_stage_1_1_maintainer_1_1_settings_1_1_references_finder_personal_settings" ],
    [ "ReferencesFinderSettings", "class_code_stage_1_1_maintainer_1_1_settings_1_1_references_finder_settings.html", "class_code_stage_1_1_maintainer_1_1_settings_1_1_references_finder_settings" ],
    [ "UserSettings", "class_code_stage_1_1_maintainer_1_1_settings_1_1_user_settings.html", "class_code_stage_1_1_maintainer_1_1_settings_1_1_user_settings" ]
];