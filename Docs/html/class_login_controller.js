var class_login_controller =
[
    [ "ClosePrompt", "class_login_controller.html#a8ae419f5260762efcb054644f6606d5f", null ],
    [ "LoginBtn_pressed", "class_login_controller.html#a5d29b7e61f58e688ac5af104a25fa384", null ],
    [ "LoginProcess", "class_login_controller.html#a7884eceafebb0e8b4a222f0933b0328f", null ],
    [ "OnFail", "class_login_controller.html#afc9e30929d09b34f3b2285c0227381b7", null ],
    [ "OpenPrompt", "class_login_controller.html#a1906eceae3aba2aedc233fb7ddc27457", null ],
    [ "SeePasswordBtn_pressed", "class_login_controller.html#a6acee0e992870e9804ae85a89773f40d", null ],
    [ "SignedUp_FirstStep", "class_login_controller.html#ac8c5e6e20361d70e582b2b1cc4f283db", null ],
    [ "ViewPopup", "class_login_controller.html#a9ad440c28ae6be8be387bdee24171f0c", null ],
    [ "auth", "class_login_controller.html#aa87e22d0a755c2662332684c01f6ef63", null ],
    [ "ErrorMessageTxt", "class_login_controller.html#a657b5f6c8e01c484155658a4759dfaa6", null ],
    [ "newUser", "class_login_controller.html#a30093ed498100120e54d686cf82c6eed", null ],
    [ "PasswordInputField", "class_login_controller.html#ad1e07b16481e0a25a6afdf65bf64b505", null ],
    [ "SelectPatientMenu", "class_login_controller.html#a6c31d0a2b9b5f628229fb65bcf7b31d6", null ],
    [ "Signup1_popup", "class_login_controller.html#a3e3017bcc244d88d309955ad1aef152d", null ],
    [ "SignUP_2", "class_login_controller.html#a13286ff407b4f109bd7ff4e3341e1b95", null ],
    [ "userNameInputField", "class_login_controller.html#a3abad4d1d38f0aa834b1b6af029ecd66", null ]
];