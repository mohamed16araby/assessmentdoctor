var class_almost_engine_1_1_screenshot_1_1_screenshot_overlay =
[
    [ "Settings", "class_almost_engine_1_1_screenshot_1_1_screenshot_overlay_1_1_settings.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_overlay_1_1_settings" ],
    [ "ScreenshotOverlay", "class_almost_engine_1_1_screenshot_1_1_screenshot_overlay.html#ad2c2d500e6535d2b88275f42315df0cf", null ],
    [ "ScreenshotOverlay", "class_almost_engine_1_1_screenshot_1_1_screenshot_overlay.html#a4d439befb174a9056c9fe3ff22247366", null ],
    [ "ApplySettings", "class_almost_engine_1_1_screenshot_1_1_screenshot_overlay.html#ada265baf451be0a68ca57aa782d44394", null ],
    [ "Disable", "class_almost_engine_1_1_screenshot_1_1_screenshot_overlay.html#a8b46a0c7e0c64a922fdca208a717489f", null ],
    [ "RestoreSettings", "class_almost_engine_1_1_screenshot_1_1_screenshot_overlay.html#adbd78cf397c402a25580d070999c2365", null ],
    [ "m_Active", "class_almost_engine_1_1_screenshot_1_1_screenshot_overlay.html#af01511f4671d50a31c01972426eb3561", null ],
    [ "m_Canvas", "class_almost_engine_1_1_screenshot_1_1_screenshot_overlay.html#a3a0020e854ef6aac0846536f95ac17f5", null ],
    [ "m_SettingStack", "class_almost_engine_1_1_screenshot_1_1_screenshot_overlay.html#ad8f814e0f8da4b8e5d2da2ff470f8871", null ]
];