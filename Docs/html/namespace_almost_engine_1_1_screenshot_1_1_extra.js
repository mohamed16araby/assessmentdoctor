var namespace_almost_engine_1_1_screenshot_1_1_extra =
[
    [ "ChangeLanguageProcess", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_change_language_process.html", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_change_language_process" ],
    [ "GreyboxCanvas", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_greybox_canvas.html", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_greybox_canvas" ],
    [ "GridGalleryCanvas", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_grid_gallery_canvas.html", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_grid_gallery_canvas" ],
    [ "HideOnCapture", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_hide_on_capture.html", null ],
    [ "MessageCanvas", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_message_canvas.html", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_message_canvas" ],
    [ "RequestAuthAtStartup", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_request_auth_at_startup.html", null ],
    [ "RotateScreenshot", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_rotate_screenshot.html", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_rotate_screenshot" ],
    [ "ScreenshotCutter", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_screenshot_cutter.html", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_screenshot_cutter" ],
    [ "ScreenshotGallery", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_screenshot_gallery.html", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_screenshot_gallery" ],
    [ "SetScreenshotGalleryFolderPath", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_set_screenshot_gallery_folder_path.html", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_set_screenshot_gallery_folder_path" ],
    [ "ShowScreenshotThumbnail", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_show_screenshot_thumbnail.html", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_show_screenshot_thumbnail" ],
    [ "TakeScreenshotButton", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_take_screenshot_button.html", null ],
    [ "ValidationCanvas", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_validation_canvas.html", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_validation_canvas" ]
];