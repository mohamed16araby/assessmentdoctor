var class_k_k_speech_1_1_speech_recognizer =
[
    [ "ExistsOnDevice", "class_k_k_speech_1_1_speech_recognizer.html#aeb7b656e995d32ac02b228a25be1cb95", null ],
    [ "GetAuthorizationStatus", "class_k_k_speech_1_1_speech_recognizer.html#a08b1d1d6113c3f67f416332f4c03daf2", null ],
    [ "GetSupportedLanguages", "class_k_k_speech_1_1_speech_recognizer.html#af60bb6b770e324371ff36dd51377ae78", null ],
    [ "IsRecording", "class_k_k_speech_1_1_speech_recognizer.html#a616cb46da7c0f435962c22f1f9ec96f3", null ],
    [ "RequestAccess", "class_k_k_speech_1_1_speech_recognizer.html#afbefd25d03c37d79065019917277978e", null ],
    [ "SetDetectionLanguage", "class_k_k_speech_1_1_speech_recognizer.html#a98e216816d7b43bc5a49fca83bf7b42b", null ],
    [ "SetSystemVolume", "class_k_k_speech_1_1_speech_recognizer.html#a028803b12ad725b9be8ada4c9c18a9c9", null ],
    [ "StartRecording", "class_k_k_speech_1_1_speech_recognizer.html#a2865cee6ff454e3620dcdf0502fcd169", null ],
    [ "StopIfRecording", "class_k_k_speech_1_1_speech_recognizer.html#a37398ebad7c6b73fa23c72611e04be0a", null ]
];