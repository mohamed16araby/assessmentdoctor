var class_paroxe_1_1_pdf_renderer_1_1_p_d_f_text_page =
[
    [ "PDFTextPage", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_text_page.html#a2bf7706c831a9825656bda78657bd887", null ],
    [ "CountChars", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_text_page.html#aec7fdb75504e134640749fdc4c75489f", null ],
    [ "CountRects", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_text_page.html#a90eec169f682474f98d76238d54d6cac", null ],
    [ "Dispose", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_text_page.html#a477d4f33d86150b0fc204d6202c16541", null ],
    [ "Dispose", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_text_page.html#abdce9bc383c54b1fc5d9ef1bc245d4c0", null ],
    [ "Equals", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_text_page.html#aa18af92051d260545897ff7459283f54", null ],
    [ "GetBoundedText", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_text_page.html#ada7eacb9a73b733fcd5ac28bf3e86638", null ],
    [ "GetChar", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_text_page.html#a932c0de84b98525c6e5165145209648a", null ],
    [ "GetCharBox", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_text_page.html#a6f59d7b5ee22a4079b2c989274e5091a", null ],
    [ "GetCharIndexAtPos", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_text_page.html#aaefbe3a67218c82d955f60bea1a1a95e", null ],
    [ "GetFontSize", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_text_page.html#ac6cb5aa2d15ca63f27a4f008d2badd64", null ],
    [ "GetRect", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_text_page.html#a2ef867ddbe4b15a24a6650292db07ada", null ],
    [ "GetText", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_text_page.html#aef1358a269e48a1c058d7e0c984699b4", null ],
    [ "Search", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_text_page.html#a2545c02d900f39d01f466c721ac5d0f0", null ],
    [ "Search", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_text_page.html#a9f0833778f1ca326c4f7b3f31ca99a10", null ],
    [ "NativePointer", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_text_page.html#ae7f47b26b9cbc43526cb02195b278113", null ],
    [ "Page", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_text_page.html#a8565f140f7e917662c727cb605bd712a", null ],
    [ "PageIndex", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_text_page.html#a167059882d2160bb745cb67069a412c7", null ]
];