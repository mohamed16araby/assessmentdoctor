var class_json_net_sample_1_1_movie =
[
    [ "Classification", "class_json_net_sample_1_1_movie.html#ac2cb0abe52086ac4e8937dd99254748d", null ],
    [ "Description", "class_json_net_sample_1_1_movie.html#a8da86cb021963b3b779c163adff0ff3b", null ],
    [ "Name", "class_json_net_sample_1_1_movie.html#af486c14e2367df27800b27e52e8ff2af", null ],
    [ "ReleaseCountries", "class_json_net_sample_1_1_movie.html#a3df0b56da81befa9f78b012387e3a2f0", null ],
    [ "ReleaseDate", "class_json_net_sample_1_1_movie.html#affafcd3b1391d0495c9ece62247a16ea", null ],
    [ "Studio", "class_json_net_sample_1_1_movie.html#ae02fdc96075261f8acbc53a5d88b3236", null ]
];