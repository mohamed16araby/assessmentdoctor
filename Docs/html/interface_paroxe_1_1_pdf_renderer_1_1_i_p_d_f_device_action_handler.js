var interface_paroxe_1_1_pdf_renderer_1_1_i_p_d_f_device_action_handler =
[
    [ "HandleGotoAction", "interface_paroxe_1_1_pdf_renderer_1_1_i_p_d_f_device_action_handler.html#a0050bb11ad72be53e9be7cdcaedfaf02", null ],
    [ "HandleLaunchAction", "interface_paroxe_1_1_pdf_renderer_1_1_i_p_d_f_device_action_handler.html#a9efb97929796970297f38c89bb9b755f", null ],
    [ "HandleRemoteGotoActionPasswordResolving", "interface_paroxe_1_1_pdf_renderer_1_1_i_p_d_f_device_action_handler.html#a567665ffa12d3ea8749b780a62fc5f40", null ],
    [ "HandleRemoteGotoActionPathResolving", "interface_paroxe_1_1_pdf_renderer_1_1_i_p_d_f_device_action_handler.html#aa69c057aa38c7b2f494f252292219b1f", null ],
    [ "HandleRemoteGotoActionResolved", "interface_paroxe_1_1_pdf_renderer_1_1_i_p_d_f_device_action_handler.html#a309a24ca9e1586d408533a3b0a9b3185", null ],
    [ "HandleRemoteGotoActionUnresolved", "interface_paroxe_1_1_pdf_renderer_1_1_i_p_d_f_device_action_handler.html#a1d589ffcd3e75f149f1322cb418698d0", null ],
    [ "HandleUnsuportedAction", "interface_paroxe_1_1_pdf_renderer_1_1_i_p_d_f_device_action_handler.html#a244535bd34a3690fd23660b9df423461", null ],
    [ "HandleUriAction", "interface_paroxe_1_1_pdf_renderer_1_1_i_p_d_f_device_action_handler.html#acbe236f434a5a1f2f535322108a52a59", null ]
];