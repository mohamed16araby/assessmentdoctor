var class_main_panel___model =
[
    [ "MainPanel_Model", "class_main_panel___model.html#adf0d71650dad5ce39b6145e5d1fb883c", null ],
    [ "SetData", "class_main_panel___model.html#a5fcaab88f5b756e2c9a3c58802d8f7ec", null ],
    [ "UpdateScoresResults", "class_main_panel___model.html#aa8453d41c16282a03dcb5196c043f412", null ],
    [ "currentGroup", "class_main_panel___model.html#a0218e9db8917f5511598c81e73d2ade7", null ],
    [ "currentPanel", "class_main_panel___model.html#a3061aebb88b4198c5f41c33a02f0c82b", null ],
    [ "scoreTracking", "class_main_panel___model.html#a5021df56bc65a5164701908646b8d0f4", null ],
    [ "OnPanelDataChanged", "class_main_panel___model.html#a68e1ccb2b8ba1f8e8cffbed78fe6d5b9", null ]
];