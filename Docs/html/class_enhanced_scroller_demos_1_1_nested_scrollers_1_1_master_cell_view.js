var class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_master_cell_view =
[
    [ "GetCellView", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_master_cell_view.html#ae390c2c8f432e30e4aa1c12c51da2cf4", null ],
    [ "GetCellViewSize", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_master_cell_view.html#a253d7c4c3737fed8441eb57fe34e3ba6", null ],
    [ "GetNumberOfCells", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_master_cell_view.html#a3471e2e6e8221782657ca976898d30ba", null ],
    [ "SetData", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_master_cell_view.html#aaec8a9b07121f3a5b11bbca5c224e808", null ],
    [ "detailCellViewPrefab", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_master_cell_view.html#a2b3245aae3999727e791940ea6b224c8", null ],
    [ "detailScroller", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_master_cell_view.html#a239591ec03455c214fc63ba495da16f8", null ]
];