var class_enhanced_scroller_demos_1_1_nested_linked_scrollers_1_1_controller =
[
    [ "GetCellView", "class_enhanced_scroller_demos_1_1_nested_linked_scrollers_1_1_controller.html#a359815c6de30a8937109229d01c6d9a1", null ],
    [ "GetCellViewSize", "class_enhanced_scroller_demos_1_1_nested_linked_scrollers_1_1_controller.html#a843250a42829acdc2e577cc7fcf1972e", null ],
    [ "GetNumberOfCells", "class_enhanced_scroller_demos_1_1_nested_linked_scrollers_1_1_controller.html#a67e45021cfc4fb104a63c516700a097b", null ],
    [ "HScrollbarOnValueChanged", "class_enhanced_scroller_demos_1_1_nested_linked_scrollers_1_1_controller.html#a96a8ff688fdf1c273d927d836e30f969", null ],
    [ "HScrollbar", "class_enhanced_scroller_demos_1_1_nested_linked_scrollers_1_1_controller.html#a1e706b5019d3861d7a461118fa691295", null ],
    [ "masterCellViewPrefab", "class_enhanced_scroller_demos_1_1_nested_linked_scrollers_1_1_controller.html#a1e7da238384452a8301f60f986212669", null ],
    [ "masterScroller", "class_enhanced_scroller_demos_1_1_nested_linked_scrollers_1_1_controller.html#a25457956b04385dd89b4f209ce160f64", null ]
];