var class_almost_engine_1_1_preview_1_1_preview_config_asset =
[
    [ "AutoRefreshMode", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a07c2cb57725ccec4fc4bb30dc74b039e", [
      [ "ONLY_IN_PLAY_MODE", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a07c2cb57725ccec4fc4bb30dc74b039ea2108cdd2aaaad28f71e5b6cb11098146", null ],
      [ "ONLY_IN_EDIT_MODE", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a07c2cb57725ccec4fc4bb30dc74b039ea9bd9f48c58f0d1833d183d8309a3be6e", null ],
      [ "ALWAYS", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a07c2cb57725ccec4fc4bb30dc74b039eaf3fc827ade4b968e50406496907ef962", null ]
    ] ],
    [ "DrawingMode", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a95811f2c49fe69ac6a5a5f8f8c9c390a", [
      [ "TEXTURE_ONLY", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a95811f2c49fe69ac6a5a5f8f8c9c390aa0bbc8e8692204b158d8f3b482e29d3dc", null ],
      [ "SCREEN_MASK", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a95811f2c49fe69ac6a5a5f8f8c9c390aa7d6b430cb7b0fd344805a621a867ce2e", null ],
      [ "FULL_DEVICE", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a95811f2c49fe69ac6a5a5f8f8c9c390aac0e6c0c10689afc967215dc062be6a9d", null ]
    ] ],
    [ "GalleryDisplayMode", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#aa484ee04b3eb7e7ba49a63305cb37b68", [
      [ "RATIOS", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#aa484ee04b3eb7e7ba49a63305cb37b68a30194740d81802dcf373eff85bf542d4", null ],
      [ "PIXELS", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#aa484ee04b3eb7e7ba49a63305cb37b68a02c1922d900c1b47ff041388e102e225", null ],
      [ "PPI", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#aa484ee04b3eb7e7ba49a63305cb37b68ada388f5a4dd69f102143b581d2b1b4d8", null ]
    ] ],
    [ "m_AutoGenerateEmptyPreview", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#aea0b7e17fe91b779d2859063e772a757", null ],
    [ "m_AutoRefresh", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a7bcf69c65523973510c94b6ad78f27d7", null ],
    [ "m_BackupPreviewToDisk", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a14abe8a7e700f9c0d8d6c9c5fd1225c9", null ],
    [ "m_Config", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a94b6fdfb8b082f6c6be6bcbf26c22e51", null ],
    [ "m_DefaultDeviceCanvas", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a2085d82b179d1723a207164fd3afdcf0", null ],
    [ "m_DeviceRendererCamera", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#ae842e8f87c6ae2a9c251f7b134b92302", null ],
    [ "m_DrawingMode", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#ad4997ca9f2a94910240023c16a548763", null ],
    [ "m_DrawSafeArea", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a3dba6f9053c12d8debbb10f21b223139", null ],
    [ "m_ExpandDevices", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a6eb4e7932197349fc6194db6760d6a6c", null ],
    [ "m_GalleryBorderSize", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a11d3824d0a31b1c1f47042c80a852cf2", null ],
    [ "m_GalleryDisplayMode", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a1ee68e05d3d534d56dab3eb3b0fd0800", null ],
    [ "m_GalleryPaddingVertical", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a73d352eac69503a3504de8df01ffa361", null ],
    [ "m_GalleryTextHeight", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a06eed665df9273e9ad140a762484f8e1", null ],
    [ "m_MarginHorizontal", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a84d07ab53921e2f383ad4ce44c88b3f2", null ],
    [ "m_MarginVertical", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a794241c5525b47c7a565fdbfcc2fb7ba", null ],
    [ "m_PreviewDisplayMode", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a4371bdd7bec6ef2cbebebf67e7c3fd8b", null ],
    [ "m_PreviewGalleryZoom", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a96838d902d68d2b345079f1ac8eb159a", null ],
    [ "m_PreviewZoom", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a374a9fdb1f7a4be427094d5fe3b5ae60", null ],
    [ "m_RefreshDelay", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#ac7fee528452142a5de22eee983cf21aa", null ],
    [ "m_RefreshMode", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#ad89d1c5ba8ffdc893f43a44500f8cf3b", null ],
    [ "m_SafeAreaCanvas", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#aca4e386b0fcfc15b81ab61ed0c806d05", null ],
    [ "m_ScreenPPI", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a6471efaf50d712a321b8a1975821ff58", null ],
    [ "m_Selected", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#ad86c27e922b40db3a2c76b7430a09f00", null ],
    [ "m_ShowGallery", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a754ac55f2916386c5e6eb17cd0fbab67", null ],
    [ "m_ShowPPI", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a9eac1c971c1f2124f9d9f7fcda264664", null ],
    [ "m_ShowRatio", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a3a63ab7fae80ec1a66cc9f72195df6c1", null ],
    [ "m_ShowResolution", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#ad161df4a5fa9bc3fb4ac6da2100d66e6", null ],
    [ "m_TransparentDeviceBackground", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#a13ad7d8e19eb3eae767f998a55f295a1", null ],
    [ "m_ZoomScrollSpeed", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html#ae70d84670c280c12f7363d0ec039966f", null ]
];