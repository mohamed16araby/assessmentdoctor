var class_treatment_goals_controller =
[
    [ "AddPhrasesToVoice", "class_treatment_goals_controller.html#a7391be6d009059da4f9970019f084f08", null ],
    [ "BackBtn_pressed", "class_treatment_goals_controller.html#a6835f3c9ef1c85fdc100db6875229a85", null ],
    [ "EnableTransitionButtons", "class_treatment_goals_controller.html#a06bc9e55f3f3d07b823a67d44a03e876", null ],
    [ "GetCellView", "class_treatment_goals_controller.html#a2857be3137277416f7e7499b4555b6eb", null ],
    [ "GetCellViewSize", "class_treatment_goals_controller.html#a7560d750727f7c9119080e7103ae0b53", null ],
    [ "GetNumberOfCells", "class_treatment_goals_controller.html#ad319954175a44977691bd05626f9fda3", null ],
    [ "LoadTreatmentGoalsData", "class_treatment_goals_controller.html#a7f8682bb3af2689749eccf968e0e7803", null ],
    [ "NextBtn_pressed", "class_treatment_goals_controller.html#a717f314910611473c6eaae1ff221298c", null ],
    [ "GoalsScroller", "class_treatment_goals_controller.html#ac33c2e8378018a4a2f23918a14ef0e7a", null ],
    [ "GoalsViewPrefab", "class_treatment_goals_controller.html#aae2556c56560b2c0d08b2eaf69c6bc55", null ],
    [ "menuTitle", "class_treatment_goals_controller.html#a0e8695987e4905a6a141324e761d8a2b", null ],
    [ "TreatmentGoals_LongTerm_FileStoragePath", "class_treatment_goals_controller.html#ab01632c079a5e6155732605e97bd4d67", null ],
    [ "TreatmentGoals_ShortTerm_FileStoragePath", "class_treatment_goals_controller.html#a48037b93d9c866c746b6f593c905b553", null ]
];