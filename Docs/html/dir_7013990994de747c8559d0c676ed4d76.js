var dir_7013990994de747c8559d0c676ed4d76 =
[
    [ "Abstraction", "dir_6febe8eee0cba2e2dc0cb0c8b1c17b5d.html", "dir_6febe8eee0cba2e2dc0cb0c8b1c17b5d" ],
    [ "GameObjectIssueRecord.cs", "_game_object_issue_record_8cs.html", [
      [ "GameObjectIssueRecord", "class_code_stage_1_1_maintainer_1_1_issues_1_1_game_object_issue_record.html", "class_code_stage_1_1_maintainer_1_1_issues_1_1_game_object_issue_record" ]
    ] ],
    [ "SceneSettingsIssueRecord.cs", "_scene_settings_issue_record_8cs.html", "_scene_settings_issue_record_8cs" ],
    [ "ScriptableObjectIssueRecord.cs", "_scriptable_object_issue_record_8cs.html", [
      [ "ScriptableObjectIssueRecord", "class_code_stage_1_1_maintainer_1_1_issues_1_1_scriptable_object_issue_record.html", "class_code_stage_1_1_maintainer_1_1_issues_1_1_scriptable_object_issue_record" ]
    ] ],
    [ "SettingsIssueRecord.cs", "_settings_issue_record_8cs.html", [
      [ "SettingsIssueRecord", "class_code_stage_1_1_maintainer_1_1_issues_1_1_settings_issue_record.html", "class_code_stage_1_1_maintainer_1_1_issues_1_1_settings_issue_record" ]
    ] ],
    [ "ShaderIssueRecord.cs", "_shader_issue_record_8cs.html", null ]
];