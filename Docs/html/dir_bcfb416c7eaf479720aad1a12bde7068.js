var dir_bcfb416c7eaf479720aad1a12bde7068 =
[
    [ "ISimpleLocalizer.cs", "_i_simple_localizer_8cs.html", [
      [ "ISimpleLocalizer", "class_almost_engine_1_1_simple_localization_1_1_i_simple_localizer.html", "class_almost_engine_1_1_simple_localization_1_1_i_simple_localizer" ]
    ] ],
    [ "LanguageSwitcher.cs", "_language_switcher_8cs.html", [
      [ "LanguageSwitcher", "class_almost_engine_1_1_simple_localization_1_1_language_switcher.html", "class_almost_engine_1_1_simple_localization_1_1_language_switcher" ]
    ] ],
    [ "SimpleImageLocalizer.cs", "_simple_image_localizer_8cs.html", [
      [ "SimpleImageLocalizer", "class_almost_engine_1_1_simple_localization_1_1_simple_image_localizer.html", "class_almost_engine_1_1_simple_localization_1_1_simple_image_localizer" ],
      [ "Localization", "class_almost_engine_1_1_simple_localization_1_1_simple_image_localizer_1_1_localization.html", null ]
    ] ],
    [ "SimpleLocalizationLanguagesAsset.cs", "_simple_localization_languages_asset_8cs.html", [
      [ "SimpleLocalizationLanguagesAsset", "class_almost_engine_1_1_simple_localization_1_1_simple_localization_languages_asset.html", "class_almost_engine_1_1_simple_localization_1_1_simple_localization_languages_asset" ]
    ] ],
    [ "SimpleTextLocalizer.cs", "_simple_text_localizer_8cs.html", [
      [ "SimpleTextLocalizer", "class_almost_engine_1_1_simple_localization_1_1_simple_text_localizer.html", "class_almost_engine_1_1_simple_localization_1_1_simple_text_localizer" ],
      [ "Localization", "class_almost_engine_1_1_simple_localization_1_1_simple_text_localizer_1_1_localization.html", null ]
    ] ]
];