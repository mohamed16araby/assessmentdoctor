var class_hospitals_list_controller =
[
    [ "GetCellView", "class_hospitals_list_controller.html#a8d08c916320e6c97c2ced3c63480e1e9", null ],
    [ "GetCellViewSize", "class_hospitals_list_controller.html#a62a5f8cec693e9a9dd5b54b3fcbbb937", null ],
    [ "GetHospitalBranches", "class_hospitals_list_controller.html#a1ad969d4501a09f50da1efbfc4863e3d", null ],
    [ "GetNumberOfCells", "class_hospitals_list_controller.html#af1124bcda4b5543e9ad34e129eab4632", null ],
    [ "RefreshHospitals", "class_hospitals_list_controller.html#a394c602e348be1c0fc0e54583b25d30e", null ],
    [ "ValueChangeCheck", "class_hospitals_list_controller.html#ae2e474cbb76e19f7ffd1cbfd02ad9865", null ],
    [ "HospitalsScroller", "class_hospitals_list_controller.html#a85e0f971fbf043ad5ac269fd418a71d5", null ],
    [ "HospitalsViewPrefab", "class_hospitals_list_controller.html#a3882b1f05d8a49fbc510eb95fe8454fb", null ],
    [ "menuTitle", "class_hospitals_list_controller.html#a298bb796ccd0bb955554dbc90df93e83", null ],
    [ "PopupMenu", "class_hospitals_list_controller.html#a1956f67bf029bf7c77b63a0ebb9f72e1", null ],
    [ "SearchField", "class_hospitals_list_controller.html#a36ae6e2d3bb8cdb93999b58961ae290f", null ]
];