var dir_7d1b3e06671446daf857d177ca6e1ea9 =
[
    [ "Core", "dir_e2295254bb2ad39357d4f219453a20ba.html", "dir_e2295254bb2ad39357d4f219453a20ba" ],
    [ "Modules", "dir_79dfee08161726122bdf0089259dd7be.html", "dir_79dfee08161726122bdf0089259dd7be" ],
    [ "Settings", "dir_0a21e032dac8105a36306ca5d98f889a.html", "dir_0a21e032dac8105a36306ca5d98f889a" ],
    [ "Tools", "dir_c0bfc35b05e558f7c51338bd2d399fd1.html", "dir_c0bfc35b05e558f7c51338bd2d399fd1" ],
    [ "UI", "dir_a0457784a6e3b260630bab6888007e04.html", "dir_a0457784a6e3b260630bab6888007e04" ],
    [ "Maintainer.cs", "_maintainer_8cs.html", [
      [ "Maintainer", "class_code_stage_1_1_maintainer_1_1_maintainer.html", "class_code_stage_1_1_maintainer_1_1_maintainer" ]
    ] ],
    [ "MaintainerMarker.cs", "_maintainer_marker_8cs.html", [
      [ "MaintainerMarker", "class_code_stage_1_1_maintainer_1_1_maintainer_marker.html", "class_code_stage_1_1_maintainer_1_1_maintainer_marker" ]
    ] ],
    [ "SearchResultsStorage.cs", "_search_results_storage_8cs.html", "_search_results_storage_8cs" ]
];