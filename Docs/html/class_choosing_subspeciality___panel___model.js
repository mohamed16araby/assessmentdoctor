var class_choosing_subspeciality___panel___model =
[
    [ "ChoosingSubspeciality_Panel_Model", "class_choosing_subspeciality___panel___model.html#ac180b3adfbb4f44c5656e4f009f30d0a", null ],
    [ "GetSpecialitiesFromFirebase", "class_choosing_subspeciality___panel___model.html#a37707f1eeb061a5ebfd43bd87a10e5c6", null ],
    [ "SetData", "class_choosing_subspeciality___panel___model.html#a74be28f4044311a0ec1b2688cb4f9e3f", null ],
    [ "keyValueOfSelectedChoiceUnderneath", "class_choosing_subspeciality___panel___model.html#a571024ba45c8ff5c1dabdd280ab1c794", null ],
    [ "subspecialitiesList", "class_choosing_subspeciality___panel___model.html#a4bb3853d615f7f9ead658f77fdf13244", null ],
    [ "OnPanelDataChanged", "class_choosing_subspeciality___panel___model.html#a64cc9d1d144d8ba94870f6980c9febb3", null ]
];