var class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_controller =
[
    [ "GetCellView", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_controller.html#abec37c4fcb79d1f8b3ffb72620af0e18", null ],
    [ "GetCellViewSize", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_controller.html#a225b93fa8315e58b58bbe5ea5b0fed86", null ],
    [ "GetNumberOfCells", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_controller.html#a0eb8072e86c145697a41a1acce273e00", null ],
    [ "masterCellViewPrefab", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_controller.html#aff308dd024df6784b030a0ba7109e147", null ],
    [ "masterScroller", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_controller.html#a769cc580f230b124d25716f50d131820", null ]
];