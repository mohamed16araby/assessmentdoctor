var class_algorithm___view =
[
    [ "GetCellView", "class_algorithm___view.html#ae280f371ae4d1df131b6b4c652b20543", null ],
    [ "GetCellViewSize", "class_algorithm___view.html#a7db36218ac96a64172b40e0742af8211", null ],
    [ "GetNumberOfCells", "class_algorithm___view.html#a85c0988ed6e87b4f7fa190e7951bb7f5", null ],
    [ "HideView", "class_algorithm___view.html#ad7d4955e46794fbaf442d3afd0e1d4f3", null ],
    [ "Init", "class_algorithm___view.html#a6016454c62d9ae295e6ef18a9bc7fdc6", null ],
    [ "SaveAlgorithmEvent", "class_algorithm___view.html#aa895310960332776ca9f23ade4ba7a38", null ],
    [ "SetView", "class_algorithm___view.html#ac56fe1481e973d4077cc7220d081dc56", null ],
    [ "ViewSaveCallBack", "class_algorithm___view.html#addd2b2d076c6bba08807a0f2304d2eea", null ],
    [ "algorithmScroller", "class_algorithm___view.html#a3588387aff548854ec00e15766d815ef", null ],
    [ "failing_Color", "class_algorithm___view.html#a8b5e9f27d4beae91caba23cc695945cc", null ],
    [ "optionsHolder", "class_algorithm___view.html#a2e55bbb4abed07ecfe58f2066a569c02", null ],
    [ "success_Color", "class_algorithm___view.html#aa5fd70eaff0f4af10a5ea3e97be90635", null ],
    [ "OnSaveAlgorithmEvent", "class_algorithm___view.html#a7ffcb7d25856f5dcf36882cd60652bf6", null ]
];