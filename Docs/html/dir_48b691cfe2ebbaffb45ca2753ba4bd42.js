var dir_48b691cfe2ebbaffb45ca2753ba4bd42 =
[
    [ "0. MainPanel", "dir_3583b26311e95a8c40513d2589ab8628.html", "dir_3583b26311e95a8c40513d2589ab8628" ],
    [ "0. SidePanel", "dir_ead9e018fc026f6b1ebb35e90af4c639.html", "dir_ead9e018fc026f6b1ebb35e90af4c639" ],
    [ "1. ChoosingSubspeciality_Panel", "dir_a8fa173f8234bd4f0110dc80dcc5b463.html", "dir_a8fa173f8234bd4f0110dc80dcc5b463" ],
    [ "2. AddEdit_MedicalRecord_Panel", "dir_32f226d44e1436556e7e67f142870c1e.html", "dir_32f226d44e1436556e7e67f142870c1e" ],
    [ "3. CustomizePanel", "dir_17293bb896cc4c13ed26c0f34a3e76e2.html", "dir_17293bb896cc4c13ed26c0f34a3e76e2" ],
    [ "4. ChoosingSides_Panel", "dir_f3380142be151aa0f0d30635b091fe2e.html", "dir_f3380142be151aa0f0d30635b091fe2e" ],
    [ "Panel_View.cs", "_panel___view_8cs.html", [
      [ "Panel_2MainHeaders_View", "class_panel__2_main_headers___view.html", "class_panel__2_main_headers___view" ],
      [ "Panel_3MainHeaders_View", "class_panel__3_main_headers___view.html", "class_panel__3_main_headers___view" ],
      [ "Panel_4MainHeaders_View", "class_panel__4_main_headers___view.html", "class_panel__4_main_headers___view" ]
    ] ]
];