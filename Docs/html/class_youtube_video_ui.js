var class_youtube_video_ui =
[
    [ "LoadThumbnail", "class_youtube_video_ui.html#af0ffead6800298b4fd6e3f38d19cf38e", null ],
    [ "PlayVideo", "class_youtube_video_ui.html#a6ed2e519a04d7a2f54528371f9502a86", null ],
    [ "PlayYoutubeVideo", "class_youtube_video_ui.html#ab9bc83c1795323df46d28ee9c61e6b95", null ],
    [ "VideoFinished", "class_youtube_video_ui.html#ae651427804c079f86cba09f04e9047db", null ],
    [ "videoId", "class_youtube_video_ui.html#a081bafdfe5ba2ced6331256d88968054", null ],
    [ "videoName", "class_youtube_video_ui.html#ab7519c3a6e8ff22d93327d76b9e4fd74", null ],
    [ "videoThumb", "class_youtube_video_ui.html#a9f769cc6f2a7fbf3ce2ce6866b476410", null ]
];