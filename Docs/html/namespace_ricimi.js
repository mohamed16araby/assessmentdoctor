var namespace_ricimi =
[
    [ "BasicButton", "class_ricimi_1_1_basic_button.html", "class_ricimi_1_1_basic_button" ],
    [ "Popup", "class_ricimi_1_1_popup.html", "class_ricimi_1_1_popup" ],
    [ "PopupOpener", "class_ricimi_1_1_popup_opener.html", "class_ricimi_1_1_popup_opener" ],
    [ "SceneTransition", "class_ricimi_1_1_scene_transition.html", "class_ricimi_1_1_scene_transition" ],
    [ "SpriteSwapper", "class_ricimi_1_1_sprite_swapper.html", "class_ricimi_1_1_sprite_swapper" ],
    [ "Tooltip", "class_ricimi_1_1_tooltip.html", "class_ricimi_1_1_tooltip" ],
    [ "Transition", "class_ricimi_1_1_transition.html", "class_ricimi_1_1_transition" ],
    [ "Utils", "class_ricimi_1_1_utils.html", "class_ricimi_1_1_utils" ]
];