var _types_8cs =
[
    [ "AdaptiveType", "_types_8cs.html#ab670a7501001635a94acca2ea1b3218b", [
      [ "None", "_types_8cs.html#ab670a7501001635a94acca2ea1b3218ba6adf97f83acf6453d4a6a4b1070f3754", null ],
      [ "Audio", "_types_8cs.html#ab670a7501001635a94acca2ea1b3218bab22f0418e8ac915eb66f829d262d14a2", null ],
      [ "Video", "_types_8cs.html#ab670a7501001635a94acca2ea1b3218ba34e2d1989a1dbf75cd631596133ee5ee", null ],
      [ "Audio_Video", "_types_8cs.html#ab670a7501001635a94acca2ea1b3218badd334ae72c6ad9280f86652c36a3e7ae", null ]
    ] ],
    [ "AudioType", "_types_8cs.html#ad8e35e82a2500583bee38b327d5e20cd", [
      [ "Aac", "_types_8cs.html#ad8e35e82a2500583bee38b327d5e20cda74787479717e3e2fa0e72ec5f96d47ff", null ],
      [ "Mp3", "_types_8cs.html#ad8e35e82a2500583bee38b327d5e20cdad7d4845656ae6671b059af6f9093886a", null ],
      [ "Opus", "_types_8cs.html#ad8e35e82a2500583bee38b327d5e20cdaf132f54e911e7ae32712890f4e448030", null ],
      [ "Vorbis", "_types_8cs.html#ad8e35e82a2500583bee38b327d5e20cda193f52b6a96ab8ea777d9613884cea04", null ],
      [ "Unknown", "_types_8cs.html#ad8e35e82a2500583bee38b327d5e20cda88183b946cc5f0e8c96b2e66e1c74a7e", null ]
    ] ],
    [ "VideoType", "_types_8cs.html#a383fad256a33dab100b9ab8c30d60e64", [
      [ "Mobile_3gp", "_types_8cs.html#a383fad256a33dab100b9ab8c30d60e64a3e0c9e84e8c8aebf5bfc974c81db0a9c", null ],
      [ "Flv", "_types_8cs.html#a383fad256a33dab100b9ab8c30d60e64ac69586e75ef1459948272eea4b4af901", null ],
      [ "Hls", "_types_8cs.html#a383fad256a33dab100b9ab8c30d60e64a6f50aa7ab28a92c1ed4811dc7276e302", null ],
      [ "Mp4", "_types_8cs.html#a383fad256a33dab100b9ab8c30d60e64a7d761542d4b5ea168dc156e85a9a2949", null ],
      [ "WebM", "_types_8cs.html#a383fad256a33dab100b9ab8c30d60e64a84f150cd3bd226ebb3708b998d164d4c", null ],
      [ "Unknown", "_types_8cs.html#a383fad256a33dab100b9ab8c30d60e64a88183b946cc5f0e8c96b2e66e1c74a7e", null ]
    ] ]
];