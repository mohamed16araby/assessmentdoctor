var class_add_edit___medical_record___panel___manager =
[
    [ "AdjustButtons", "class_add_edit___medical_record___panel___manager.html#a6a38ee49f841593be31551429be03a5b", null ],
    [ "DisablePanel", "class_add_edit___medical_record___panel___manager.html#a53332f12995591a4e17c0ca49aca3fed", null ],
    [ "Enter_AddEdit_MedicalRecord_Panel", "class_add_edit___medical_record___panel___manager.html#abc7d4e114ab2bd08c0df273c68636963", null ],
    [ "GetLastSession_Id", "class_add_edit___medical_record___panel___manager.html#af40cdc84629c24be96f75b6775a9b260", null ],
    [ "GoBack", "class_add_edit___medical_record___panel___manager.html#aecc04f1c7e66717f9a7a07b835fe317f", null ],
    [ "GoNext", "class_add_edit___medical_record___panel___manager.html#af6054966a02ae972084373b42cda8a50", null ],
    [ "PrepareAssessment", "class_add_edit___medical_record___panel___manager.html#ae7b5bb6305a8cef0218fcf842be758cc", null ],
    [ "RefreshContents", "class_add_edit___medical_record___panel___manager.html#a278a0f2c6b4734cd85592e52d44ff598", null ],
    [ "addEdit_MedicalRecord_Panel_MVC", "class_add_edit___medical_record___panel___manager.html#ab36d307f63a1917fcbdcc3332f5f2d5e", null ],
    [ "isFollowupSession", "class_add_edit___medical_record___panel___manager.html#a87b61e1462d8f4537cc999679d97c786", null ],
    [ "Instance", "class_add_edit___medical_record___panel___manager.html#a0bb7bde146c27e948cd26791fa4e144b", null ]
];