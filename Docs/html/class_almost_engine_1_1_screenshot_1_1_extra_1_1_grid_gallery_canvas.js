var class_almost_engine_1_1_screenshot_1_1_extra_1_1_grid_gallery_canvas =
[
    [ "CloseCallback", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_grid_gallery_canvas.html#aca4f7e9a5cd465ead2c9dd34b9ad01b1", null ],
    [ "DoGalleryUpdate", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_grid_gallery_canvas.html#a6a00e3f04cb12895ecc3d7acf8b3b877", null ],
    [ "ImagesPerPage", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_grid_gallery_canvas.html#a5b992cc68ee647644ba592d7eb6a708e", null ],
    [ "MaxPages", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_grid_gallery_canvas.html#aeb6a2c360fd057b8813b5e3e695d9c28", null ],
    [ "NextPageCallback", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_grid_gallery_canvas.html#abaaa583180f3d138cb6419b735bb5fed", null ],
    [ "PreviousPageCallback", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_grid_gallery_canvas.html#a173758ac330c64d781a32c1dd02f69a5", null ],
    [ "m_CurrentPage", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_grid_gallery_canvas.html#ab454b58922aa8bf23da3ea91a9a1fb91", null ],
    [ "m_Grid", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_grid_gallery_canvas.html#aee0ea2edb017d1ef9d0c52a9b16fad75", null ],
    [ "m_NextButton", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_grid_gallery_canvas.html#a15f88338f32c73f932a60601776b46de", null ],
    [ "m_PageText", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_grid_gallery_canvas.html#a10f7df6ca59696f5499e882493870011", null ],
    [ "m_PreviousButton", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_grid_gallery_canvas.html#a6e892497add08222bf0a3e9285d56751", null ]
];