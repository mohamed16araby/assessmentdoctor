var dir_a832e184f659417ab4d7ce95637a34c8 =
[
    [ "1. Auth", "dir_ebddd72639c789c70905afe34e3434b0.html", "dir_ebddd72639c789c70905afe34e3434b0" ],
    [ "2. SelectPatient", "dir_11269223d3469cf6e0b6886e7ba8ddf6.html", "dir_11269223d3469cf6e0b6886e7ba8ddf6" ],
    [ "3. Assessment", "dir_5b93fa6a38be13c8ff15922b8d34a742.html", "dir_5b93fa6a38be13c8ff15922b8d34a742" ],
    [ "4. DiagnosisList", "dir_2c29c11d05077c523f6c4cb82f21e444.html", "dir_2c29c11d05077c523f6c4cb82f21e444" ],
    [ "5. TreatmentGoals", "dir_1f8778aaa5a99d2ee10a79b99206b02b.html", "dir_1f8778aaa5a99d2ee10a79b99206b02b" ],
    [ "6. Electro & Manual Therapy", "dir_3ac71f8219d38713f0981769eecc6a88.html", "dir_3ac71f8219d38713f0981769eecc6a88" ],
    [ "7. FinalReport", "dir_ed15f4f90d0a79e6a195b23de508eb02.html", "dir_ed15f4f90d0a79e6a195b23de508eb02" ],
    [ "Voice", "dir_2779cde8184849f452c7681698b1a9f2.html", "dir_2779cde8184849f452c7681698b1a9f2" ],
    [ "HandlingDataBetweenScenes.cs", "_handling_data_between_scenes_8cs.html", [
      [ "HandlingDataBetweenScenes", "class_handling_data_between_scenes.html", "class_handling_data_between_scenes" ]
    ] ],
    [ "ScenesController.cs", "_scenes_controller_8cs.html", [
      [ "ScenesController", "class_scenes_controller.html", "class_scenes_controller" ]
    ] ]
];