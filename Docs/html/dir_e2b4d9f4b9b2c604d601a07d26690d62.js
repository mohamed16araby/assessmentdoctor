var dir_e2b4d9f4b9b2c604d601a07d26690d62 =
[
    [ "Utils", "dir_ac623f6f5ccc2ffaaf3411e06b45fdb2.html", "dir_ac623f6f5ccc2ffaaf3411e06b45fdb2" ],
    [ "CameraDrawer.cs", "_camera_drawer_8cs.html", [
      [ "CameraDrawer", "class_almost_engine_1_1_screenshot_1_1_camera_drawer.html", "class_almost_engine_1_1_screenshot_1_1_camera_drawer" ]
    ] ],
    [ "CaptureScreenshotMenuItem.cs", "_capture_screenshot_menu_item_8cs.html", null ],
    [ "DeviceSelector.cs", "_device_selector_8cs.html", [
      [ "DeviceSelector", "class_almost_engine_1_1_screenshot_1_1_device_selector.html", "class_almost_engine_1_1_screenshot_1_1_device_selector" ]
    ] ],
    [ "OverlayDrawer.cs", "_overlay_drawer_8cs.html", [
      [ "OverlayDrawer", "class_almost_engine_1_1_screenshot_1_1_overlay_drawer.html", "class_almost_engine_1_1_screenshot_1_1_overlay_drawer" ]
    ] ],
    [ "PopularityPresetAsset.cs", "_popularity_preset_asset_8cs.html", [
      [ "PopularityPresetAsset", "class_almost_engine_1_1_screenshot_1_1_popularity_preset_asset.html", "class_almost_engine_1_1_screenshot_1_1_popularity_preset_asset" ],
      [ "Stat", "class_almost_engine_1_1_screenshot_1_1_popularity_preset_asset_1_1_stat.html", "class_almost_engine_1_1_screenshot_1_1_popularity_preset_asset_1_1_stat" ]
    ] ],
    [ "PopularityPresetAssetInspector.cs", "_popularity_preset_asset_inspector_8cs.html", [
      [ "PopularityPresetAssetInspector", "class_almost_engine_1_1_screenshot_1_1_popularity_preset_asset_inspector.html", "class_almost_engine_1_1_screenshot_1_1_popularity_preset_asset_inspector" ]
    ] ],
    [ "PresetCollectionAsset.cs", "_preset_collection_asset_8cs.html", [
      [ "PresetCollectionAsset", "class_almost_engine_1_1_screenshot_1_1_preset_collection_asset.html", "class_almost_engine_1_1_screenshot_1_1_preset_collection_asset" ]
    ] ],
    [ "ResolutionDrawer.cs", "_resolution_drawer_8cs.html", [
      [ "ResolutionDrawer", "class_almost_engine_1_1_screenshot_1_1_resolution_drawer.html", "class_almost_engine_1_1_screenshot_1_1_resolution_drawer" ]
    ] ],
    [ "SceenshotNamePresets.cs", "_sceenshot_name_presets_8cs.html", [
      [ "ScreenshotNamePresets", "class_almost_engine_1_1_screenshot_1_1_screenshot_name_presets.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_name_presets" ],
      [ "NamePreset", "class_almost_engine_1_1_screenshot_1_1_screenshot_name_presets_1_1_name_preset.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_name_presets_1_1_name_preset" ]
    ] ],
    [ "ScreenshotConfigAsset.cs", "_screenshot_config_asset_8cs.html", [
      [ "ScreenshotConfigAsset", "class_almost_engine_1_1_screenshot_1_1_screenshot_config_asset.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_config_asset" ]
    ] ],
    [ "ScreenshotConfigAssetInspector.cs", "_screenshot_config_asset_inspector_8cs.html", [
      [ "ScreenshotConfigAssetInspector", "class_almost_engine_1_1_screenshot_1_1_screenshot_config_asset_inspector.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_config_asset_inspector" ]
    ] ],
    [ "ScreenshotConfigDrawer.cs", "_screenshot_config_drawer_8cs.html", [
      [ "ScreenshotConfigDrawer", "class_almost_engine_1_1_screenshot_1_1_screenshot_config_drawer.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_config_drawer" ]
    ] ],
    [ "ScreenshotManagerInspector.cs", "_screenshot_manager_inspector_8cs.html", [
      [ "ScreenshotManagerInspector", "class_almost_engine_1_1_screenshot_1_1_screenshot_manager_inspector.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_manager_inspector" ]
    ] ],
    [ "ScreenshotResolutionAsset.cs", "_screenshot_resolution_asset_8cs.html", [
      [ "ScreenshotResolutionAsset", "class_almost_engine_1_1_screenshot_1_1_screenshot_resolution_asset.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_resolution_asset" ]
    ] ],
    [ "ScreenshotResolutionAssetInspector.cs", "_screenshot_resolution_asset_inspector_8cs.html", [
      [ "ScreenshotResolutionAssetInspectorInspector", "class_almost_engine_1_1_screenshot_1_1_screenshot_resolution_asset_inspector_inspector.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_resolution_asset_inspector_inspector" ]
    ] ],
    [ "ScreenshotResolutionPresets.cs", "_screenshot_resolution_presets_8cs.html", [
      [ "ScreenshotResolutionPresets", "class_almost_engine_1_1_screenshot_1_1_screenshot_resolution_presets.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_resolution_presets" ]
    ] ],
    [ "ScreenshotWindow.cs", "_screenshot_window_8cs.html", [
      [ "ScreenshotWindow", "class_almost_engine_1_1_screenshot_1_1_screenshot_window.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_window" ]
    ] ],
    [ "TagDatabaseAsset.cs", "_tag_database_asset_8cs.html", [
      [ "TagDatabaseAsset", "class_almost_engine_1_1_screenshot_1_1_tag_database_asset.html", "class_almost_engine_1_1_screenshot_1_1_tag_database_asset" ],
      [ "TagData", "class_almost_engine_1_1_screenshot_1_1_tag_database_asset_1_1_tag_data.html", "class_almost_engine_1_1_screenshot_1_1_tag_database_asset_1_1_tag_data" ],
      [ "TagDatabaseData", "class_almost_engine_1_1_screenshot_1_1_tag_database_asset_1_1_tag_database_data.html", null ]
    ] ]
];