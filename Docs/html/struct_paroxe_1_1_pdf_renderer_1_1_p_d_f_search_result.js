var struct_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_result =
[
    [ "PDFSearchResult", "struct_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_result.html#a8d89ae78bb9c46725c12d6281603d00b", null ],
    [ "Count", "struct_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_result.html#adf211e96d3efc5023661fcc9ca90f2b8", null ],
    [ "IsValid", "struct_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_result.html#a56ff043c1a1248655e7a66e1a7d2c28d", null ],
    [ "PageIndex", "struct_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_result.html#afa933cad57bac0d5a2c1f3a762874d0e", null ],
    [ "StartIndex", "struct_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_result.html#a2454baf9f6556be418ae23f5518c56a4", null ]
];