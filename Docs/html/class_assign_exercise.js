var class_assign_exercise =
[
    [ "AssignExercise", "class_assign_exercise.html#abf04d57764f6cb757667c71e32c5da5e", null ],
    [ "Decrypt", "class_assign_exercise.html#a08521e4bbd0c988523c42156f4112fdc", null ],
    [ "Decryption", "class_assign_exercise.html#a1faba22b4908a72825805f55f2f7a9f3", null ],
    [ "Encrypt", "class_assign_exercise.html#a4cc0aa193cc17dad5ad018f22b3726a0", null ],
    [ "Encryption", "class_assign_exercise.html#a1abea309dea72ee188527b9e64f06a78", null ],
    [ "daysPerWeek", "class_assign_exercise.html#a5371f1301f2f883be1ff811c313e3a00", null ],
    [ "exercise_name", "class_assign_exercise.html#a24482452d978cfa95669a0d3fc7c43b9", null ],
    [ "grade", "class_assign_exercise.html#a9815c0083c8269c91b75696c6d93460b", null ],
    [ "id", "class_assign_exercise.html#ad94aae10e5a82f72a6cf847d4d509b30", null ],
    [ "notes", "class_assign_exercise.html#aa179a4fc3fd9c1727a6749bf73ada685", null ],
    [ "repetitionsPerSet", "class_assign_exercise.html#a517fcaea71029fa9ff1fc0bbbcfa7dd9", null ],
    [ "setsPerDay", "class_assign_exercise.html#aa8536b47219f4930118e460b1c0794d2", null ]
];