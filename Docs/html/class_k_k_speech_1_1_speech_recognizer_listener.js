var class_k_k_speech_1_1_speech_recognizer_listener =
[
    [ "AuthorizationCallback", "class_k_k_speech_1_1_speech_recognizer_listener_1_1_authorization_callback.html", null ],
    [ "AvailabilityCallback", "class_k_k_speech_1_1_speech_recognizer_listener_1_1_availability_callback.html", null ],
    [ "ErrorCallback", "class_k_k_speech_1_1_speech_recognizer_listener_1_1_error_callback.html", null ],
    [ "ResultCallback", "class_k_k_speech_1_1_speech_recognizer_listener_1_1_result_callback.html", null ],
    [ "SupportedLanguagesCallback", "class_k_k_speech_1_1_speech_recognizer_listener_1_1_supported_languages_callback.html", null ],
    [ "SupportedLanguagesFetched", "class_k_k_speech_1_1_speech_recognizer_listener.html#a0efd2f573b17c3e87e29b8d4c2ad9225", null ],
    [ "onAuthorizationStatusFetched", "class_k_k_speech_1_1_speech_recognizer_listener.html#ac2d850337dca3325f0c82bab8436005e", null ],
    [ "onAvailabilityChanged", "class_k_k_speech_1_1_speech_recognizer_listener.html#ae7a38fb5c5af942f8e1f970da00c32a6", null ],
    [ "onEndOfSpeech", "class_k_k_speech_1_1_speech_recognizer_listener.html#af80d7aad384e371e28217b7fc13d3473", null ],
    [ "onErrorDuringRecording", "class_k_k_speech_1_1_speech_recognizer_listener.html#a65aabf5fb1caa422147d2fe59635a0c3", null ],
    [ "onErrorOnStartRecording", "class_k_k_speech_1_1_speech_recognizer_listener.html#a9e94c65c5d15ed0df4c7a9b335823e28", null ],
    [ "onFinalResults", "class_k_k_speech_1_1_speech_recognizer_listener.html#aa4e7ab0901084ea8329b41dd5f376bfa", null ],
    [ "onPartialResults", "class_k_k_speech_1_1_speech_recognizer_listener.html#a59948bb9c1ccccdf6c06bb327c930bcc", null ],
    [ "onReadyForSpeech", "class_k_k_speech_1_1_speech_recognizer_listener.html#a57541762229b4e3da61a18ce4bec0abe", null ],
    [ "onSupportedLanguagesFetched", "class_k_k_speech_1_1_speech_recognizer_listener.html#a5994d669133a845f8b35fa74a9e71e33", null ]
];