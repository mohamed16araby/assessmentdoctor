var class_almost_engine_1_1_preview_1_1_device_info =
[
    [ "GetDPI", "class_almost_engine_1_1_preview_1_1_device_info.html#a392da56abd1a6ef289e02dcd9323db06", null ],
    [ "GetName", "class_almost_engine_1_1_preview_1_1_device_info.html#a0e42f4c82c5d2c3799dcd6e75c180663", null ],
    [ "GetResolution", "class_almost_engine_1_1_preview_1_1_device_info.html#a72cac20a83e40d5712b6f37d124b73e8", null ],
    [ "GetSafeArea", "class_almost_engine_1_1_preview_1_1_device_info.html#abf868ceb7659606d90ebd395078b74ee", null ],
    [ "IsAndroid", "class_almost_engine_1_1_preview_1_1_device_info.html#a8be8074dbea2d513f9fff5cd910792a8", null ],
    [ "IsIOS", "class_almost_engine_1_1_preview_1_1_device_info.html#a3e60cc75b2f8f3cd28451055af710094", null ],
    [ "IsPortrait", "class_almost_engine_1_1_preview_1_1_device_info.html#aecbe7a2f1a375ba89a7d1b81f9148240", null ],
    [ "IsStandalone", "class_almost_engine_1_1_preview_1_1_device_info.html#a29a7404395a78fa98f15479130de6207", null ]
];