var class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings =
[
    [ "ProjectCleanerSettings", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings.html#a2ba254fa0474244630e37ae162b8f568", null ],
    [ "AddDefaultFilters", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings.html#ad8e06ac33aa3ffb5ec1d69242a812bd3", null ],
    [ "GetDefaultFilters", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings.html#a34dee69ce210d633cc62165075360d9a", null ],
    [ "GetFiltersCount", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings.html#a538ed575fc853ea75718df6cf42cb30e", null ],
    [ "SetDefaultFilters", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings.html#ab6384fee368670cb53067ffe558355b6", null ],
    [ "findEmptyFolders", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings.html#a5e85ef487fcaa40f4225e462b1df853d", null ],
    [ "findEmptyFoldersAutomatically", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings.html#a3a7afd095f30dba30cebbfbd1b48bbf5", null ],
    [ "findUnreferencedAssets", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings.html#a8b106e902659a436a5311cd01ad59e27", null ],
    [ "findUnreferencedScripts", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings.html#a2e9bc518bacda702fbe17c5a33571f6f", null ],
    [ "ignoreOnlyEnabledScenesInBuild", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings.html#a3abddef4e9d03bd9b16c6ac411908223", null ],
    [ "ignoreScenesInBuild", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings.html#adc89adbc46964cc7e86ab5dd13427d0c", null ],
    [ "pathIgnores", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings.html#a08007dfd00bb96dfa528d97aaafa5c81", null ],
    [ "pathIgnoresFilters", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings.html#a1c399f368f9ae145e7980393fd087009", null ],
    [ "rescanAfterContextIgnore", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings.html#a6a4e41e49f8a387a5b7835eae36f5e01", null ],
    [ "sceneIgnores", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings.html#a8385328c8cbb52a5c99321f54f1e694a", null ],
    [ "sceneIgnoresFilters", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings.html#ae015967ca2ddf4fcb561420453b2e9c4", null ],
    [ "useTrashBin", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings.html#a13668d279c80db04e425decc532b72c9", null ],
    [ "MandatoryFilters", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings.html#aa2aba752a4b7b066dc926a9fae1a3d05", null ]
];