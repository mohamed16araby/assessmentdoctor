var class_almost_engine_1_1_screenshot_1_1_screenshot_camera_1_1_settings =
[
    [ "Settings", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera_1_1_settings.html#a866aca56ef8e88c6e661cd5e77646e03", null ],
    [ "m_BackgroundColor", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera_1_1_settings.html#ad50c12cad4ab54b4ed30b0df6ff52956", null ],
    [ "m_ClearFlags", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera_1_1_settings.html#a8cad98fe9abf30991a07bd729d41b346", null ],
    [ "m_CullingMask", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera_1_1_settings.html#a919f4868983700d78a19f6f29b9f0e8b", null ],
    [ "m_Enabled", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera_1_1_settings.html#a0f816532a5ddd1cfea1d612f4306f610", null ],
    [ "m_FOV", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera_1_1_settings.html#af0b9461134b8eed8d0a74b9f7a80c941", null ],
    [ "m_GameObjectEnabled", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera_1_1_settings.html#ad74520275a94e63bd29a0152108a6a1f", null ]
];