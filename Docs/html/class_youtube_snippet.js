var class_youtube_snippet =
[
    [ "categoryId", "class_youtube_snippet.html#ad345985acbd5c0ee943a2eae1964e73e", null ],
    [ "channelId", "class_youtube_snippet.html#acf8eefc0b8e02e3236bf128b527f54db", null ],
    [ "channelTitle", "class_youtube_snippet.html#a8b7f2e9b892e143bc4a45f7bf6cca96c", null ],
    [ "description", "class_youtube_snippet.html#af156a65a95f180178d533a0f8c712122", null ],
    [ "publishedAt", "class_youtube_snippet.html#a3a4a5c33e58c64cde208e45c910867a5", null ],
    [ "tags", "class_youtube_snippet.html#a8cfc74262e7d88bbb3f03cf9dbb1169e", null ],
    [ "thumbnails", "class_youtube_snippet.html#a10802f6799262e1ee2e65a4bf9bbc2b0", null ],
    [ "title", "class_youtube_snippet.html#aaf70ba43482c6ce4d646f51e30e48a7e", null ]
];