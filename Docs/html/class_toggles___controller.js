var class_toggles___controller =
[
    [ "Toggles_Controller", "class_toggles___controller.html#a95a95b4ba81bdb51a8bcf5142a3dfbd3", null ],
    [ "DataChanged_UpdateModel", "class_toggles___controller.html#a873020b0bbb0c56d4d13200989879a2c", null ],
    [ "DataChanged_UpdateView", "class_toggles___controller.html#aa7eb0e80a7b440f2754f145e6ea39925", null ],
    [ "GetPhrasesWithTheirActions", "class_toggles___controller.html#a27e068a0bc10d4128580ba9f82e061ca", null ],
    [ "GetSelectedToggle", "class_toggles___controller.html#a3710f6e6f49d02f5b1eaf76ed2334188", null ],
    [ "HideViewSectionsDependinOnToggleChoice", "class_toggles___controller.html#a1a613d883ab9783a7d1616116b03d7c7", null ],
    [ "LoadSavedData", "class_toggles___controller.html#a96ec1405524084c18ea5a0d746fdf4b0", null ],
    [ "PassScoreFunctions", "class_toggles___controller.html#a750ac02497d0e5519284cdbc92591630", null ],
    [ "ReadyToListen", "class_toggles___controller.html#a378095a836fece3fec46666f232371b8", null ],
    [ "ToggleChoiceChanged_ApplyToSections", "class_toggles___controller.html#a54471da05987859c6cc741e81c46cc41", null ],
    [ "sectionsVisibilityDependingOnToggleChoice", "class_toggles___controller.html#a477296ca161b600d93ab33dd9b343918", null ],
    [ "model", "class_toggles___controller.html#a313b5e43dd07e9fb978455498847bac4", null ],
    [ "view", "class_toggles___controller.html#a527a5f1f531f8d21bd0510ca738cd983", null ]
];