var dir_8a594ba937eab2a21345da9f22352c89 =
[
    [ "Exceptions.cs", "_exceptions_8cs.html", [
      [ "VideoNotAvailableException", "class_youtube_light_1_1_video_not_available_exception.html", "class_youtube_light_1_1_video_not_available_exception" ],
      [ "YoutubeParseException", "class_youtube_light_1_1_youtube_parse_exception.html", "class_youtube_light_1_1_youtube_parse_exception" ]
    ] ],
    [ "HTTPHelperYoutube.cs", "_h_t_t_p_helper_youtube_8cs.html", null ],
    [ "MagicHands.cs", "_magic_hands_8cs.html", null ],
    [ "Types.cs", "_types_8cs.html", "_types_8cs" ],
    [ "VideoInfo.cs", "_video_info_8cs.html", [
      [ "VideoInfo", "class_youtube_light_1_1_video_info.html", "class_youtube_light_1_1_video_info" ]
    ] ],
    [ "VideoRequest.cs", "_video_request_8cs.html", [
      [ "Downloader", "class_youtube_light_1_1_downloader.html", "class_youtube_light_1_1_downloader" ]
    ] ]
];