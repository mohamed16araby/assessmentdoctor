var class_almost_engine_1_1_screenshot_1_1_screenshot_camera =
[
    [ "Settings", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera_1_1_settings.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera_1_1_settings" ],
    [ "CustomSettings", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera.html#af4658b62482efe2878f1262e5b53e959", [
      [ "KEEP_CAMERA_SETTINGS", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera.html#af4658b62482efe2878f1262e5b53e959a71ec99f31e292a1a668d90b0d8e3558c", null ],
      [ "CUSTOM", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera.html#af4658b62482efe2878f1262e5b53e959a72baef04098f035e8a320b03ad197818", null ]
    ] ],
    [ "ScreenshotCamera", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera.html#a78110847da0924c6f93826aeadfad74e", null ],
    [ "ScreenshotCamera", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera.html#ade93c0fd36623e96be028391ffa2f8ba", null ],
    [ "ApplySettings", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera.html#a4bbb24a9cf3232b488c0c157acc4f295", null ],
    [ "Disable", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera.html#a9260203e5fe1e9e0e7618b9bec4fccb1", null ],
    [ "RestoreSettings", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera.html#a9058ffd0c4dd21a1c052e39ec5bf1e65", null ],
    [ "m_Active", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera.html#a943c92897fde990c32ae047ee9727bd6", null ],
    [ "m_BackgroundColor", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera.html#a0e7f057aa09a61a16b71bb52b28dbc3d", null ],
    [ "m_Camera", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera.html#a3a7f61f445884746796b2ffcc9a33f6d", null ],
    [ "m_ClearFlags", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera.html#a2b60974be488149bcd97535c262d4e6d", null ],
    [ "m_ClearSettings", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera.html#a43a806788df5a6f51dd27e3ea20e0c3c", null ],
    [ "m_CullingMask", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera.html#a5456fa306b877a14b5d9504827a63413", null ],
    [ "m_CullingSettings", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera.html#a7ee898b2365b091d0309d5c4ee5db58e", null ],
    [ "m_FOV", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera.html#a2fd55908f28b7f5371cb91d869893482", null ],
    [ "m_FOVSettings", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera.html#a2d6dde6046c632510feade92ae22b80b", null ],
    [ "m_SettingStack", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera.html#a8627ee2eba653b644611eb2d5fdc5243", null ]
];