var class_side =
[
    [ "Side", "class_side.html#a188cb21c71e1333a8a342034fc58b894", null ],
    [ "Side", "class_side.html#a7b906ef3c3baffe3202ac770a600387c", null ],
    [ "Clone", "class_side.html#ad48713d8d0e247e9518730965b43d7a6", null ],
    [ "Merge", "class_side.html#a4503db8604a285b67ca81e72652309d1", null ],
    [ "displayOrder", "class_side.html#a6320b3c1f1c8038d4fd40e68bc82d3a5", null ],
    [ "name", "class_side.html#ae40fcf125a54b2d577b758176957355e", null ],
    [ "childrenDictionary", "class_side.html#a4944a3bb65c68edd9f38d5b5d9840e2a", null ],
    [ "Tests", "class_side.html#abe2458f64a87cc060636fb4514797ae2", null ]
];