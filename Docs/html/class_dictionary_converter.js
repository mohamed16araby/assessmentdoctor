var class_dictionary_converter =
[
    [ "DictionaryConverter", "class_dictionary_converter.html#a1a535967e4445877e8afe56935f12f47", null ],
    [ "CanConvert", "class_dictionary_converter.html#a5f4e645a91bbf76014b2535dced2daa0", null ],
    [ "ReadJson", "class_dictionary_converter.html#a91980891d92a79d4d06075a24ba89b9b", null ],
    [ "WriteJson", "class_dictionary_converter.html#ac05a1dd235fbef21ac0d322000269ac2", null ],
    [ "CanWrite", "class_dictionary_converter.html#ac2e072a0e311310677673f0821ddce75", null ]
];