var class_paroxe_1_1_pdf_renderer_1_1_p_d_f_renderer_1_1_render_settings =
[
    [ "ComputeRenderingFlags", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_renderer_1_1_render_settings.html#ab44c24e42639ed8d8a636a16fffe9b59", null ],
    [ "disableSmoothImage", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_renderer_1_1_render_settings.html#ac9c3c532630043db2805c12c05b12cf8", null ],
    [ "disableSmoothPath", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_renderer_1_1_render_settings.html#a855c46fac87e3e46ce3429206cabe803", null ],
    [ "disableSmoothText", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_renderer_1_1_render_settings.html#a3502c3b08a80b0d50ac72d1a121a302e", null ],
    [ "grayscale", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_renderer_1_1_render_settings.html#a1f18d0ab681467ef73084899cfa88966", null ],
    [ "optimizeTextForLCDDisplay", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_renderer_1_1_render_settings.html#a6f5a2b8b9f93cdc8b855e1a218007ffd", null ],
    [ "renderAnnotations", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_renderer_1_1_render_settings.html#ad2261290620e2d8b8dcc1a955a263051", null ],
    [ "renderForPrinting", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_renderer_1_1_render_settings.html#ae5affbfbdbd0f1f324d3c56a26364d39", null ],
    [ "transparentBackground", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_renderer_1_1_render_settings.html#a4dbad9b4a0ad50dff63b7a0c6b575abd", null ],
    [ "defaultRenderSettings", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_renderer_1_1_render_settings.html#a28b7023ee18870e928b4294a3e43b71c", null ]
];