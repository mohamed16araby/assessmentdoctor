var class_firebase_operation =
[
    [ "FirebaseOperation", "class_firebase_operation.html#a28503cbcbe39bb4bf7fa2b3248fa7bcf", null ],
    [ "IsCompleted", "class_firebase_operation.html#ae66ff1b2f21b4bcc26d09ea3a68db3f2", null ],
    [ "IsFailed", "class_firebase_operation.html#a0383ec5fe9b04271ce86aa21f9375292", null ],
    [ "IsIdle", "class_firebase_operation.html#a13a743fca49c91ec26530d2c08ebd7e7", null ],
    [ "IsInProcess", "class_firebase_operation.html#ad70ac53a429d86e290db908270ac0b45", null ],
    [ "ProcessFinished", "class_firebase_operation.html#a041d7baceb0365146dd682d33032e555", null ],
    [ "content", "class_firebase_operation.html#af04a46a27ebd4bb7019b2c2da439df45", null ],
    [ "errorMssg", "class_firebase_operation.html#af073df63996220ca074e3c7fc8698926", null ],
    [ "operationID", "class_firebase_operation.html#ace531fbc0105bda21151200c1427b4e1", null ],
    [ "operationStatus", "class_firebase_operation.html#aaa8dd3d88a981a083fc4f7d490393ef1", null ]
];