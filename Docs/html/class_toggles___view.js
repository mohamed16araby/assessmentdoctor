var class_toggles___view =
[
    [ "AddToggleTrackableEvent", "class_toggles___view.html#a3c97780e17dfdfbd502867483026458c", null ],
    [ "GetRightPrefabForChildrenView", "class_toggles___view.html#a15f944b9d53886564c7df712ebd74bfc", null ],
    [ "PassScoreFunctions", "class_toggles___view.html#a0732e95e8c687c33befeeed0f1694ce4", null ],
    [ "SetView", "class_toggles___view.html#af1f48288522a83076802329fe87da8f3", null ],
    [ "ToggleChoiceChanged_ApplyToSectionsEvent", "class_toggles___view.html#a981f48d13af608baf2cdbbbf1789830d", null ],
    [ "ToggleSelectionEvent", "class_toggles___view.html#abfb3a5dcacff5106fd82488d80bf68c0", null ],
    [ "UpdateItemEvent", "class_toggles___view.html#a48147782bc50a14a6848e68b7ffee147", null ],
    [ "modifyScores", "class_toggles___view.html#a54cbbb5156dfa7df8d1f7f17d5ea4303", null ],
    [ "sectionsViewDependinOnChoice", "class_toggles___view.html#a43dc80be564d4edd1e3387c166c9d4c7", null ],
    [ "toggleAssess_instant", "class_toggles___view.html#ae249bb031a0137c99314c486b6ad8e73", null ],
    [ "togglePhrasesActions_Voice", "class_toggles___view.html#afc230bc0f2a5c70f9b23e217032b0d7b", null ],
    [ "toggleShouldBeOn_SectionsViews", "class_toggles___view.html#ae4eaeb6b6b06673a161fe42297f94c41", null ],
    [ "toggleTransform", "class_toggles___view.html#af806b996bc999b2dc92f161d14b45137", null ],
    [ "trackToggleChoice", "class_toggles___view.html#a1079091d5131a936e28467d6b56a65e1", null ],
    [ "updateGroupScoresResults", "class_toggles___view.html#a53defc6178ad5ac8608bac0d0b19fca0", null ],
    [ "AddToggleTrackable", "class_toggles___view.html#afa0138679436367fff5685fc3c04f856", null ],
    [ "GetItemFromSelectedValues", "class_toggles___view.html#a92cda55898cea00ee8a3262a232f44c3", null ],
    [ "OnToggleSelectionChanged", "class_toggles___view.html#ae7364e91f7501cd3d5953c7381e66326", null ],
    [ "ToggleChoiceChanged_ApplyToSections", "class_toggles___view.html#a2502e3606934ac0a6e8e8f82da752502", null ]
];