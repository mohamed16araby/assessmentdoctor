var dir_3bedd4218b3462e0c73ac5b05bbce2a9 =
[
    [ "Controller.cs", "12_01_nested_01_scrollers_2_controller_8cs.html", [
      [ "Controller", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_controller.html", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_controller" ]
    ] ],
    [ "DetailCellView.cs", "12_01_nested_01_scrollers_2_detail_cell_view_8cs.html", [
      [ "DetailCellView", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_detail_cell_view.html", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_detail_cell_view" ]
    ] ],
    [ "DetailData.cs", "12_01_nested_01_scrollers_2_detail_data_8cs.html", [
      [ "DetailData", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_detail_data.html", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_detail_data" ]
    ] ],
    [ "MasterCellView.cs", "12_01_nested_01_scrollers_2_master_cell_view_8cs.html", [
      [ "MasterCellView", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_master_cell_view.html", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_master_cell_view" ]
    ] ],
    [ "MasterData.cs", "12_01_nested_01_scrollers_2_master_data_8cs.html", [
      [ "MasterData", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_master_data.html", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_master_data" ]
    ] ],
    [ "ScrollRectEx.cs", "12_01_nested_01_scrollers_2_scroll_rect_ex_8cs.html", [
      [ "ScrollRectEx", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_scroll_rect_ex.html", "class_enhanced_scroller_demos_1_1_nested_scrollers_1_1_scroll_rect_ex" ]
    ] ]
];