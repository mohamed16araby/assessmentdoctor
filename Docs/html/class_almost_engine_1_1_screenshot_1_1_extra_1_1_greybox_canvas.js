var class_almost_engine_1_1_screenshot_1_1_extra_1_1_greybox_canvas =
[
    [ "CloseCallback", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_greybox_canvas.html#ad53c6e447db6fe2ca1b1b0097f76d864", null ],
    [ "DelayedUpdate", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_greybox_canvas.html#ae1b218714b23d71aae8701d8a8b511b0", null ],
    [ "DoUpdate", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_greybox_canvas.html#a07f312535578516a4c04271822089141", null ],
    [ "NextPageCallback", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_greybox_canvas.html#a7c35af285f71329927c99c626bc334ae", null ],
    [ "PreviousPageCallback", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_greybox_canvas.html#a44a41405fa8f56dc0f18beac16e83b8a", null ],
    [ "RemoveCallback", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_greybox_canvas.html#ae12bf0411052cea141102dd0119f7a76", null ],
    [ "SetImage", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_greybox_canvas.html#aa90586ab68f5914bdef761a85426ff71", null ],
    [ "UpdateUI", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_greybox_canvas.html#ad826290b2937fa3ef7f42af6e450e748", null ],
    [ "m_CloseButton", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_greybox_canvas.html#ab6ce0aa0975e0d6ca016da09d79b6cf3", null ],
    [ "m_FileName", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_greybox_canvas.html#a53caff2a535caba036aeb716931fb263", null ],
    [ "m_ImageContainer", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_greybox_canvas.html#a5ce63f0b5ed584849fc08a46ff92e9d3", null ],
    [ "m_NextButton", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_greybox_canvas.html#a5f238f26a6a37128293f0659e5907e44", null ],
    [ "m_PageText", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_greybox_canvas.html#a4a8942362199f4214c40d6f3dbbeb66c", null ],
    [ "m_PreviousButton", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_greybox_canvas.html#a8de0caa5e772e0897da988ba50c47ba6", null ]
];