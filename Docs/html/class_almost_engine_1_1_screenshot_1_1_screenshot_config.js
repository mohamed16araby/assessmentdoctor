var class_almost_engine_1_1_screenshot_1_1_screenshot_config =
[
    [ "AntiAliasing", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a4f79fa9324088c06bf192428ed6030f7", [
      [ "NONE", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a4f79fa9324088c06bf192428ed6030f7ab50339a10e1de285ac99d4c3990b8693", null ],
      [ "TWO", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a4f79fa9324088c06bf192428ed6030f7a0f82d86afa0f5dc965c5c15aca58dcfb", null ],
      [ "FOUR", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a4f79fa9324088c06bf192428ed6030f7a341fee9692a2ed8f09906d40d23fb1f9", null ],
      [ "EIGHT", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a4f79fa9324088c06bf192428ed6030f7a960df6d77e65cd185ca4f3501db634eb", null ]
    ] ],
    [ "BatchMode", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#aa1834685b1d8298e91b90497636a57ed", [
      [ "SIMPLE_CAPTURE", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#aa1834685b1d8298e91b90497636a57eda50314a77e8219d2299be42738df9420b", null ],
      [ "BATCHES", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#aa1834685b1d8298e91b90497636a57eda628bcf7e10fc1c2a984f379a1ec3393a", null ]
    ] ],
    [ "CamerasMode", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a100abcd08c53643ab211b12f5b97681e", [
      [ "GAME_VIEW", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a100abcd08c53643ab211b12f5b97681ea82ce2d4bcc6502ffa524849cbc5154fe", null ],
      [ "CUSTOM_CAMERAS", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a100abcd08c53643ab211b12f5b97681eac9f41b4779e437fa389c8fc54d0a1dd9", null ]
    ] ],
    [ "CompositionMode", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#aadf4e90713818e15ea029dd4218e9b3e", [
      [ "SIMPLE_CAPTURE", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#aadf4e90713818e15ea029dd4218e9b3ea50314a77e8219d2299be42738df9420b", null ],
      [ "COMPOSITION", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#aadf4e90713818e15ea029dd4218e9b3ea2bde5bae24deed3107442637c643b3ba", null ]
    ] ],
    [ "ResolutionMode", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a79b74730c5ac90b5f248c99e0f0e3fa0", [
      [ "GAME_VIEW", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a79b74730c5ac90b5f248c99e0f0e3fa0a82ce2d4bcc6502ffa524849cbc5154fe", null ],
      [ "CUSTOM_RESOLUTIONS", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a79b74730c5ac90b5f248c99e0f0e3fa0aa460f46948cf1e67ebc9bf1a94e06a0d", null ]
    ] ],
    [ "ShotMode", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#aa11954e3d346d30fc0426aeda7dc6ecc", [
      [ "ONE_SHOT", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#aa11954e3d346d30fc0426aeda7dc6ecca5bc2964599423c796ecf0aaecdc5be9d", null ],
      [ "BURST", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#aa11954e3d346d30fc0426aeda7dc6ecca9d2b6b239fcfd0ec88ac1ee2d87eb108", null ]
    ] ],
    [ "ScreenshotConfig", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a73577971882e5b2fcd745d4d2455aa38", null ],
    [ "AlignToView", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a42eee318e8f298ed2c32127a1f986944", null ],
    [ "ClearAllResolutions", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a646f0da8bf573d75f766f29ec51841d5", null ],
    [ "ClearCache", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#ad3ac94aca957bbcc87b28cd3d5c78a27", null ],
    [ "ExportAllToFiles", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a1e6a1cb117c22233d0bd09ac7d9fb59d", null ],
    [ "ExportToFiles", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a6247ca603b711fb0d59db654b5244418", null ],
    [ "GetActiveBatches", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#aee7d5d3256c7746cdcb398ba4e5ee0a7", null ],
    [ "GetActiveCameras", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a87041f87fc6b65764af5328f42b6f674", null ],
    [ "GetActiveCompositions", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a40675a3a04fad67259b97ffb39cb7b72", null ],
    [ "GetActiveResolutions", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#af4bb7e9e7ffd98311426ab2f9967e47d", null ],
    [ "GetFirstActiveResolution", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#ab9b358fb3d371d71825cb78186b1d823", null ],
    [ "GetPath", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a2bd91338a42f97a2b6819506a6d79587", null ],
    [ "InitGameViewResolution", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a12a75ff6f652d259205b3e99ee04c9af", null ],
    [ "ParseFileName", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a2dabef0043d33fc827150ec743b5a313", null ],
    [ "RemoveAllResolutions", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#affa6f892f0256d3d9d45a49531f218b2", null ],
    [ "SelectAllResolutions", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#aad53152248ad582c0e0731a086bb768d", null ],
    [ "SetAllLandscape", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#ae998bef035bb5544ecc4c856788506b7", null ],
    [ "SetAllLandscapeRight", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a4d9ed1dbfa3393179f4557690bb181ca", null ],
    [ "SetAllPortait", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a4b382408261ab2f53569b4b9f38a9d98", null ],
    [ "SetTime", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#aa80f222be9a1e9ac2c1129a422852279", null ],
    [ "TogglePause", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#aa2dec7362decbedc5c4d153b0e03530b", null ],
    [ "UpdatBatchesData", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a702628d48007fca83215ad1bbfac91ce", null ],
    [ "UpdateFileName", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a9729d754e7349d59fe8e8c4b1d10271d", null ],
    [ "UpdateGameviewResolution", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#ac9fb6a2d7f2e9712782b338f67eb2860", null ],
    [ "UpdateRatios", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#ab06f71d0a2fcae6affd1c0ea37618bb7", null ],
    [ "UpdateResolutionFilenames", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a4fb6cf6d9bb10dc9ead2fb1da918add7", null ],
    [ "m_AlignHotkey", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a442a7a0c2875ccfc2bbff59d71776890", null ],
    [ "m_Batches", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a1c64578b3c4f7578cee1310c0427c1ac", null ],
    [ "m_BatchMode", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a624c47e2c2a039643373fa2045fbc65b", null ],
    [ "m_CameraMode", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a32c2f149b03dc8def576ae4cb8443ff1", null ],
    [ "m_Cameras", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a803d9c3933820281aa3603322ba0a812", null ],
    [ "m_CaptureActiveUICanvas", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a430353130ee14b51cfe5021bec866fdc", null ],
    [ "m_CaptureHotkey", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a3cf915990fbc8ecc0e85dd1ae6da48bc", null ],
    [ "m_CaptureMode", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a974581f682bfdb551bb1b7c348d7fc93", null ],
    [ "m_ColorFormat", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#ae90edb7e896ff3bb5369d652c2b9ef9e", null ],
    [ "m_CompositionMode", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#ab70a8780e29b6ac54fa3e89ac587900a", null ],
    [ "m_Compositions", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#ab46bdded5b7879fb43d38d0107a23601", null ],
    [ "m_DestinationFolder", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#ae8f5fcc3d40848d8d55fd40672c4e2b7", null ],
    [ "m_DontDestroyOnLoad", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a6bec6f70cd990eb8d9c03b8df97de67f", null ],
    [ "m_ExportToDifferentLayers", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a2a8869d871fa7f6731cb4933c54394f7", null ],
    [ "m_FileFormat", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#aa9643388b74e45ad88f67400502c9c12", null ],
    [ "m_FileName", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a83dde3bef816d54fd4c57bba53146fff", null ],
    [ "m_GameViewResizingWaitingMode", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a68b3442285ebe49e9776e69b759a880d", null ],
    [ "m_GameViewResolution", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#adc8f9c64e96eb06d725fa75e65524333", null ],
    [ "m_GuideCanvas", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#af50021a6d0c0b9740bb2b908f8e973ce", null ],
    [ "m_GuidesColor", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#af8f994c052fcbea9117b335e536c1372", null ],
    [ "m_ItemsPerPage", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#ad54a4ec3c85cf20f3ab4f9cd6e7051f9", null ],
    [ "m_JPGQuality", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a1c97c069d9fb4346b67f8046ea1bd70b", null ],
    [ "m_MaxBurstShotsNumber", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a0c28a173d346af60874f9eb1de995560", null ],
    [ "m_MultisamplingAntiAliasing", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#ad56d048774ea1f0f4c29416425576744", null ],
    [ "m_Overlays", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a70f9a1cd3dd5c73528eec66be7a742bd", null ],
    [ "m_OverrideFiles", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a880d6640a835d2d8bfad8423898b32e1", null ],
    [ "m_PauseHotkey", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a03ccfbc44ed7c6f6ac22c71f910ed124", null ],
    [ "m_PlaySoundOnCapture", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a5a570f3ca439baa059e12526153d1cd7", null ],
    [ "m_PreviewInGameViewWhilePlaying", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a02a1bb4d466182616042673cc5a497ae", null ],
    [ "m_PreviewSize", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a21f9c7c1f73d8c4bee8ef98f037822ee", null ],
    [ "m_RecomputeAlphaLayer", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a934c64813985bf3667bc4f3ec4e329e7", null ],
    [ "m_RelativePath", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a1b683fd38d7326326587bc6f377209e4", null ],
    [ "m_ResizingWaitingFrames", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a704af057ac8cb471f19916f5c9247490", null ],
    [ "m_ResizingWaitingTime", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#ac780be400c78908c06e749c626f075c0", null ],
    [ "m_ResolutionCaptureMode", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a343e7df80915d6d59510a82ad14e21ad", null ],
    [ "m_Resolutions", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a644fc6e0a57c60edf33f9104525d6dc1", null ],
    [ "m_RootedPath", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a576ee9f351cd68d4d0151f48af1fb739", null ],
    [ "m_ShotMode", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a7570c57250afdd66d6c7fb1ac9f82dce", null ],
    [ "m_ShotSound", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a2133444cf58e96bea911d29a92ad38aa", null ],
    [ "m_ShotTimeStep", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a85945ee2c22cea7b98aeba2650691010", null ],
    [ "m_ShowBatches", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#acd8459a09d69e91e64d588d3bb2a7ae2", null ],
    [ "m_ShowCameras", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#ade05f1cd8d5f1e0696706cabee66d29b", null ],
    [ "m_ShowCanvas", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a212c051a320979957c00d663a7fc3ecc", null ],
    [ "m_ShowCapture", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#ab6580fb2d80029beb50ede397d32f0b4", null ],
    [ "m_ShowCaptureMode", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#ac3557eaa94f4634231571a49fb0779d5", null ],
    [ "m_ShowComposition", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#ac495dedf822835571394172a8e4e28f3", null ],
    [ "m_ShowDelay", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a991575ec7ae088a23ea49c53c16e7533", null ],
    [ "m_ShowDestination", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a9ec02a8cfeb1c47fd45eaac863491d4a", null ],
    [ "m_ShowGallery", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#ab99f029741162828368944f6f57716fa", null ],
    [ "m_ShowGuidesInPreview", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#aef0f5c041b3acbbd99510f0b8e7b8887", null ],
    [ "m_ShowHotkeys", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a45b6eebd9c33b1b9ae1960940a7e86a0", null ],
    [ "m_ShowName", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a7fabe32f4dfcf69b1fbf215765b57488", null ],
    [ "m_ShowPreview", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a997fb59aa562f6d90f973949a04c8a57", null ],
    [ "m_ShowPreviewGUI", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a86e6b9651735ac2d9d11ca03ab918809", null ],
    [ "m_ShowResolutions", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#af8800a5a3ad83571fdf862aea4b0bdb9", null ],
    [ "m_ShowUsage", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#ab32b5675660923ad037accfa42de38f9", null ],
    [ "m_ShowUtils", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a570d90dc1485b27234dc5787d828ba68", null ],
    [ "m_StopTimeOnCapture", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a84e283280ffd1b16f965639ea4dd67ef", null ],
    [ "m_Time", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a9523c15b40e084c1fa81effeb716653d", null ],
    [ "m_UpdatePreviewHotkey", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#a6606c9ba83a384c1d66869a125393b2b", null ]
];