var class_almost_engine_1_1_screenshot_1_1_device_selector =
[
    [ "Init", "class_almost_engine_1_1_screenshot_1_1_device_selector.html#ae963151268f27970f38d48325e9abf5a", null ],
    [ "InitPresets", "class_almost_engine_1_1_screenshot_1_1_device_selector.html#aaf662f179b59cb2a5d46e2cd43003399", null ],
    [ "OnGUI", "class_almost_engine_1_1_screenshot_1_1_device_selector.html#a6928027690feff1f4e99a81167ec5074", null ],
    [ "m_Collections", "class_almost_engine_1_1_screenshot_1_1_device_selector.html#a4d2378e6079fb7b8c871462384eb0f96", null ],
    [ "m_Config", "class_almost_engine_1_1_screenshot_1_1_device_selector.html#a98a825be383bef433c1a678165652b84", null ],
    [ "m_FilteredPresets", "class_almost_engine_1_1_screenshot_1_1_device_selector.html#a98573f36da5efe91cb8017c072825e92", null ],
    [ "m_Popularities", "class_almost_engine_1_1_screenshot_1_1_device_selector.html#a8aa1b2de9dbd9e6fadc9083c221b386b", null ],
    [ "m_PPIFilter", "class_almost_engine_1_1_screenshot_1_1_device_selector.html#a9a3790848e97561e88f3b6e7343e8b4d", null ],
    [ "m_Presets", "class_almost_engine_1_1_screenshot_1_1_device_selector.html#ad9a9e9ed4a05b82cc8e01a6a85660b75", null ],
    [ "m_ResolutionFilter", "class_almost_engine_1_1_screenshot_1_1_device_selector.html#aff7d7064f1d2720c45f331f8f8278be4", null ],
    [ "m_ShowDetailedDevice", "class_almost_engine_1_1_screenshot_1_1_device_selector.html#addc16b33dd7674ea7c1cb5647f418f95", null ],
    [ "m_TextFilter", "class_almost_engine_1_1_screenshot_1_1_device_selector.html#a671326218f5f888bf5505f7656ffd05c", null ]
];