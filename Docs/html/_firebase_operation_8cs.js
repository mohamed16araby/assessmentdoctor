var _firebase_operation_8cs =
[
    [ "FirebaseOperation", "class_firebase_operation.html", "class_firebase_operation" ],
    [ "FirebaseOperationStatus", "_firebase_operation_8cs.html#ac140cf86d64ec3eb02f1873d45307366", [
      [ "Idle", "_firebase_operation_8cs.html#ac140cf86d64ec3eb02f1873d45307366ae599161956d626eda4cb0a5ffb85271c", null ],
      [ "Inprocess", "_firebase_operation_8cs.html#ac140cf86d64ec3eb02f1873d45307366a101db4e0ae25a2434e85d7613adb39ea", null ],
      [ "Completed", "_firebase_operation_8cs.html#ac140cf86d64ec3eb02f1873d45307366a07ca5050e697392c9ed47e6453f1453f", null ],
      [ "Failed", "_firebase_operation_8cs.html#ac140cf86d64ec3eb02f1873d45307366ad7c8c85bf79bbe1b7188497c32c3b0ca", null ]
    ] ]
];