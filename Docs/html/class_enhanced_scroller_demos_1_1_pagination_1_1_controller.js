var class_enhanced_scroller_demos_1_1_pagination_1_1_controller =
[
    [ "GetCellView", "class_enhanced_scroller_demos_1_1_pagination_1_1_controller.html#aaae8a9da9255faf34e44c8ae2cbfc178", null ],
    [ "GetCellViewSize", "class_enhanced_scroller_demos_1_1_pagination_1_1_controller.html#a5038f744f061c528df6562a0c2655436", null ],
    [ "GetNumberOfCells", "class_enhanced_scroller_demos_1_1_pagination_1_1_controller.html#a655fe14fa1f1f6a05885420eb86c708d", null ],
    [ "cellHeight", "class_enhanced_scroller_demos_1_1_pagination_1_1_controller.html#acd2aca2aa88ae57eb4bee39d4acafe3e", null ],
    [ "cellViewPrefab", "class_enhanced_scroller_demos_1_1_pagination_1_1_controller.html#af8a2cd89f0212bace536fcc49aa96799", null ],
    [ "loadingCellViewPrefab", "class_enhanced_scroller_demos_1_1_pagination_1_1_controller.html#a906f3f7f8e7d989838bdebac0f7ee771", null ],
    [ "pageCount", "class_enhanced_scroller_demos_1_1_pagination_1_1_controller.html#a945d95b5dfe8eeb2516fb2567e8da5e9", null ],
    [ "scroller", "class_enhanced_scroller_demos_1_1_pagination_1_1_controller.html#a852aedb9c9daee2eafb3dacb7d49dea9", null ]
];