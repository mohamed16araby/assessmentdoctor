var class_session_report =
[
    [ "Decrypt", "class_session_report.html#acb6303866a418c522cf484f76dfa3036", null ],
    [ "Decryption", "class_session_report.html#ad2a4c5d38468fe1fdd9fe17c2421c97b", null ],
    [ "Encrypt", "class_session_report.html#a84653bd926fc1bef1045a11e9d967728", null ],
    [ "Encryption", "class_session_report.html#a8466ac4623f1b69bf959a79d585250b4", null ],
    [ "IsEmpty", "class_session_report.html#a6878bb045f804a7c148e23b7050763e4", null ],
    [ "Assessment_Info", "class_session_report.html#a19e05cbd63b0c546d3c30c8aeb495da9", null ],
    [ "diagnosisList", "class_session_report.html#a918b89955e7914cacecbe2bfd1af8470", null ],
    [ "doctor_ID", "class_session_report.html#aa156f6615fa5c85d314d302d14313ca6", null ],
    [ "ElectroTherapy", "class_session_report.html#a28225be0af4b8c3bcefb2ececa512b38", null ],
    [ "exercises", "class_session_report.html#a2be70b4ed685134d22b29a490e5a0a6b", null ],
    [ "hospital_branch_name", "class_session_report.html#aaf912e94b654da25bf6daa20bd2b37a0", null ],
    [ "patient_ID", "class_session_report.html#aedfc8f80c36b6fd3921f0b4b16983fbc", null ],
    [ "patientVisitAlgorithm", "class_session_report.html#aadf3f02ab1086a319facd209a469e550", null ],
    [ "problemList", "class_session_report.html#a0464b35eca4e20fc85eaa54f73a42911", null ],
    [ "sideOfAssessment", "class_session_report.html#aaa9ea184c9631241a3f07943e7539ffa", null ],
    [ "TreatmentGoals_LongTerm", "class_session_report.html#a6d1050314cb1a466811f5c46947898af", null ],
    [ "TreatmentGoals_ShortTerm", "class_session_report.html#aad4b074ede41f1aad8dd205321f40142", null ]
];