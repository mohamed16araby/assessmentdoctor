var class_enhanced_scroller_demos_1_1_grid_simulation_1_1_controller =
[
    [ "GetCellView", "class_enhanced_scroller_demos_1_1_grid_simulation_1_1_controller.html#a34d4ba1af68351079925531558c896ef", null ],
    [ "GetCellViewSize", "class_enhanced_scroller_demos_1_1_grid_simulation_1_1_controller.html#ad75a92f9e16ad0c14ac74d0a8201d4b6", null ],
    [ "GetNumberOfCells", "class_enhanced_scroller_demos_1_1_grid_simulation_1_1_controller.html#a0be494e106b44709fedae1a20c7e6355", null ],
    [ "cellViewPrefab", "class_enhanced_scroller_demos_1_1_grid_simulation_1_1_controller.html#ae7172089aac318653bc133bc3fe6a274", null ],
    [ "numberOfCellsPerRow", "class_enhanced_scroller_demos_1_1_grid_simulation_1_1_controller.html#ac97272042f32dad2a5fc5518ebc5be46", null ],
    [ "scroller", "class_enhanced_scroller_demos_1_1_grid_simulation_1_1_controller.html#aaa3a4114442d957cd9caf7a9dcd2d6ab", null ]
];