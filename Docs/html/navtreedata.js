/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Cammi Health", "index.html", [
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Properties", "functions_prop.html", "functions_prop" ],
        [ "Events", "functions_evnt.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"01_01_simple_01_demo_2_cell_view_8cs.html",
"_device_name_presets_8cs.html",
"_p_d_f_action_handler_helper_8cs.html",
"_simple_j_s_o_n_8cs.html#a62af00acc7925cc0284e634fb10626f5a6adf97f83acf6453d4a6a4b1070f3754",
"class_algorithm___controller.html#a0d078db11ea6c05e12cc1c3abf962690",
"class_almost_engine_1_1_screenshot_1_1_extra_1_1_greybox_canvas.html#aa90586ab68f5914bdef761a85426ff71",
"class_almost_engine_1_1_screenshot_1_1_screenshot_config.html#ade05f1cd8d5f1e0696706cabee66d29b",
"class_almost_engine_1_1_simple_localization_1_1_simple_image_localizer.html#a27296171c93783eeb50a40c89885eea7",
"class_cammedar_1_1_network_1_1_selected_parts.html#afc6aaab6f44a1ad03fa926d77aba20f3",
"class_code_stage_1_1_maintainer_1_1_record_base.html#aee75d4f5b41ac7947b9151adf9c40d33",
"class_doxygen_window.html#ad1f6043062e30f52cb634b72294a5676a8f7f4c1ce7a4f933663d10543562b096",
"class_enhanced_scroller_demos_1_1_pull_down_refresh_1_1_controller.html#a5f73dd65df2d09d17f0682511de749ec",
"class_enhanced_u_i_1_1_enhanced_scroller_1_1_enhanced_scroller_cell_view.html",
"class_h_m_labs_1_1_editor_1_1_json_view_i_m_g_u_i_container.html#a73e4371d247f09f60bde5b4199a35457",
"class_manager.html#a578803df7cafd157b4b19c287e3f117f",
"class_panel___m_v_c.html#a04c470e4cff1ffc85eb476356e911636",
"class_paroxe_1_1_pdf_renderer_1_1_p_d_f_action.html#afec789c3ec513493fe7f885f9fbf9cc1",
"class_paroxe_1_1_pdf_renderer_1_1_p_d_f_viewer.html#a7f1af06ffa9d4ef3593b13abace36ab3a2fb403e71d8ade2ba79af3f6c4695d09",
"class_search_exercises_controller.html#a0e9bcd4930a07cc22456e23843c07957",
"class_simple_j_s_o_n_1_1_j_s_o_n_node.html#a4d2bd5645578fcb66e5d0d78df8efc8c",
"class_t_m_pro_1_1_t_m_p___text_event_handler_1_1_sprite_selection_event.html",
"class_video_search_demo.html#a820dc382b0d64821c963e6fae3c9b24d",
"class_youtube_statistics.html#aeecf9660925717e95f9e8e6f9c9f9016",
"dir_b813607bc087248a362a02a421025390.html",
"interface_paroxe_1_1_pdf_renderer_1_1_i_p_d_f_device.html#a8f4f234acf612bc8a786576a8b713842",
"struct_paroxe_1_1_pdf_renderer_1_1_p_d_f_renderer_1_1_p_d_f_matrix.html#aa59a1a06bf60064f86ec81f49638d79e"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';