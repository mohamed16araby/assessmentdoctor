var namespace_almost_engine_1_1_preview =
[
    [ "DetectOrientationChange", "class_almost_engine_1_1_preview_1_1_detect_orientation_change.html", "class_almost_engine_1_1_preview_1_1_detect_orientation_change" ],
    [ "DeviceInfo", "class_almost_engine_1_1_preview_1_1_device_info.html", "class_almost_engine_1_1_preview_1_1_device_info" ],
    [ "ExportDevicePreviewMenuItem", "class_almost_engine_1_1_preview_1_1_export_device_preview_menu_item.html", null ],
    [ "MaskRenderer", "class_almost_engine_1_1_preview_1_1_mask_renderer.html", "class_almost_engine_1_1_preview_1_1_mask_renderer" ],
    [ "PPISimulation", "class_almost_engine_1_1_preview_1_1_p_p_i_simulation.html", null ],
    [ "PreviewConfigAsset", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html", "class_almost_engine_1_1_preview_1_1_preview_config_asset" ],
    [ "PreviewConfigAssetInspector", "class_almost_engine_1_1_preview_1_1_preview_config_asset_inspector.html", "class_almost_engine_1_1_preview_1_1_preview_config_asset_inspector" ],
    [ "ResolutionSettingsWindow", "class_almost_engine_1_1_preview_1_1_resolution_settings_window.html", "class_almost_engine_1_1_preview_1_1_resolution_settings_window" ],
    [ "SafeArea", "class_almost_engine_1_1_preview_1_1_safe_area.html", "class_almost_engine_1_1_preview_1_1_safe_area" ],
    [ "SafeAreaInspector", "class_almost_engine_1_1_preview_1_1_safe_area_inspector.html", "class_almost_engine_1_1_preview_1_1_safe_area_inspector" ],
    [ "UniversalDevicePreview", "class_almost_engine_1_1_preview_1_1_universal_device_preview.html", "class_almost_engine_1_1_preview_1_1_universal_device_preview" ],
    [ "UpdateDevicePreviewMenuItem", "class_almost_engine_1_1_preview_1_1_update_device_preview_menu_item.html", null ]
];