var class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_p_d_f_bitmap =
[
    [ "BitmapFormat", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_p_d_f_bitmap.html#af49665a4b6c5670280c440abf6b5df68", [
      [ "Unknown", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_p_d_f_bitmap.html#af49665a4b6c5670280c440abf6b5df68a88183b946cc5f0e8c96b2e66e1c74a7e", null ],
      [ "Gray", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_p_d_f_bitmap.html#af49665a4b6c5670280c440abf6b5df68a994ae1d9731cebe455aff211bcb25b93", null ],
      [ "BGR", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_p_d_f_bitmap.html#af49665a4b6c5670280c440abf6b5df68a2ad5640ebdec72fc79531d1778c6c2dc", null ],
      [ "BGRx", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_p_d_f_bitmap.html#af49665a4b6c5670280c440abf6b5df68ae73e03d3572482b6e32ef51a401a6494", null ],
      [ "BGRA", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_p_d_f_bitmap.html#af49665a4b6c5670280c440abf6b5df68a5a1fe3c61c9e2fd6dbbc823589f6e697", null ]
    ] ],
    [ "PDFBitmap", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_p_d_f_bitmap.html#a7d13b91792d1ba5ddc1db883cbd94218", null ],
    [ "Dispose", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_p_d_f_bitmap.html#aa8b2fa898d5c38c9a1a901dea3a004c6", null ],
    [ "Dispose", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_p_d_f_bitmap.html#a8f1b72f7b5683432c392d59de1eee623", null ],
    [ "FillRect", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_p_d_f_bitmap.html#a2cdc3bfdc07037a373ae0e0260cb7eb9", null ],
    [ "GetBuffer", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_p_d_f_bitmap.html#aca484d4942235e95a8ac44bea096119e", null ],
    [ "GetStride", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_p_d_f_bitmap.html#a01ad1f9a3619df755f9f2e145db27453", null ],
    [ "HasSameSize", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_p_d_f_bitmap.html#ae57016ca110072654ec5455626eb49a6", null ],
    [ "HasSameSize", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_p_d_f_bitmap.html#ae7871f8563e8ec094c34483f0a3ad883", null ],
    [ "Format", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_p_d_f_bitmap.html#aa7175149e4a262a658ce3920be32aefe", null ],
    [ "Height", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_p_d_f_bitmap.html#ac3284d4c70e7898c1e836558f6733c6e", null ],
    [ "NativePointer", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_p_d_f_bitmap.html#ad6d89936ecc75fba98f3f74525e3be8b", null ],
    [ "UseAlphaChannel", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_p_d_f_bitmap.html#a1d50122f39e308868acf027645e1533d", null ],
    [ "Width", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_p_d_f_bitmap.html#a694a8a0ce98b1df74133e0d200dbb50b", null ]
];