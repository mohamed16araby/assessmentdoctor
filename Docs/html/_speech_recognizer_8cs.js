var _speech_recognizer_8cs =
[
    [ "SpeechRecognitionOptions", "struct_k_k_speech_1_1_speech_recognition_options.html", "struct_k_k_speech_1_1_speech_recognition_options" ],
    [ "LanguageOption", "struct_k_k_speech_1_1_language_option.html", "struct_k_k_speech_1_1_language_option" ],
    [ "SpeechRecognizer", "class_k_k_speech_1_1_speech_recognizer.html", "class_k_k_speech_1_1_speech_recognizer" ],
    [ "AuthorizationStatus", "_speech_recognizer_8cs.html#a02e6fdf753478b924c2a243d2eea1c36", [
      [ "Authorized", "_speech_recognizer_8cs.html#a02e6fdf753478b924c2a243d2eea1c36aa206428462686af481cb072b8db11784", null ],
      [ "Denied", "_speech_recognizer_8cs.html#a02e6fdf753478b924c2a243d2eea1c36a58d036b9b7f0e7eb38cfb90f1cc70a73", null ],
      [ "NotDetermined", "_speech_recognizer_8cs.html#a02e6fdf753478b924c2a243d2eea1c36abf6dbc731ea21f21b9bf8ed1cc3e7a1a", null ],
      [ "Restricted", "_speech_recognizer_8cs.html#a02e6fdf753478b924c2a243d2eea1c36aecdee5de15af6876cfab0801bb36f445", null ]
    ] ]
];