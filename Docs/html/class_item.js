var class_item =
[
    [ "operator Item", "class_item.html#a24028ffad6adfbcbc1384ff0b9c5afa2", null ],
    [ "operator Toggles_Model", "class_item.html#a9805b7c5e39802dfc3f1d581807bb143", null ],
    [ "descriptionToTitle", "class_item.html#a02d8c2a6b1d09d03fcd16d67845577d6", null ],
    [ "gridColumnsCount", "class_item.html#aadd4660b85a2c3a6b3b8867d0c459749", null ],
    [ "gridXSpace", "class_item.html#a027daf351ea5dde8e9d450e1513f84ba", null ],
    [ "hideTitle", "class_item.html#a745f2d911d0238bc26a74130708ecc8a", null ],
    [ "imageName", "class_item.html#a1d5006c315f6d667996a94cd9a922f3d", null ],
    [ "linkingTag", "class_item.html#a3fa521e998d236ed39a31eee22466dcb", null ],
    [ "number", "class_item.html#a60db06a488a269ec77663b4ec7a41404", null ],
    [ "path", "class_item.html#a220b3dd38d168daa6892c73502ad7ede", null ],
    [ "scoreTracking", "class_item.html#a7d6d0d183ddb17cd98040f0c0f462c7c", null ],
    [ "scoreWeight", "class_item.html#af2ba967205c723a62276883b9cb2be96", null ],
    [ "selectedValues", "class_item.html#a435a0be5860a8b13e1a9dfc7e905f47c", null ],
    [ "singleSelection", "class_item.html#aca14d46f27ee26318a670f93eb77fa28", null ],
    [ "title", "class_item.html#a63516ad9bfb0a0cc8dd1dd0ba05bb9a5", null ],
    [ "toggleImageName", "class_item.html#aef42f3d86175f124f8c3a3901993b100", null ],
    [ "toggleShouldBeOn_SectionsViews", "class_item.html#a3323f047ac932391fde54c47acebea2f", null ],
    [ "trackToggleChoice", "class_item.html#a928dfd7c96807dd25931a7c1fa5c0721", null ],
    [ "values", "class_item.html#a50754cb025edc17ec58a413e99349a74", null ],
    [ "viewDependingOn", "class_item.html#a3cae6b02176e58b7c6e488057e919951", null ],
    [ "this[string title]", "class_item.html#aeebbec2eba1ff7dbbfd9237e2b1db110", null ]
];