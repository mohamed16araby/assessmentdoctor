var dir_3baf61167cc5258dcaa43736a51891c0 =
[
    [ "Detectors", "dir_a186edeb0231a79106b996252d97faae.html", "dir_a186edeb0231a79106b996252d97faae" ],
    [ "Records", "dir_7013990994de747c8559d0c676ed4d76.html", "dir_7013990994de747c8559d0c676ed4d76" ],
    [ "Routines", "dir_cc5ebcf6918a5a034c9ab22f68d03180.html", "dir_cc5ebcf6918a5a034c9ab22f68d03180" ],
    [ "IssueKind.cs", "_issue_kind_8cs.html", "_issue_kind_8cs" ],
    [ "IssuesDetector.cs", "_issues_detector_8cs.html", "_issues_detector_8cs" ],
    [ "IssuesFinder.cs", "_issues_finder_8cs.html", "_issues_finder_8cs" ],
    [ "IssuesSortingType.cs", "_issues_sorting_type_8cs.html", "_issues_sorting_type_8cs" ],
    [ "RecordSeverity.cs", "_record_severity_8cs.html", "_record_severity_8cs" ]
];