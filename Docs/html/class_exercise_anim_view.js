var class_exercise_anim_view =
[
    [ "AssignBtn_pressed", "class_exercise_anim_view.html#a3dc4af178d37b3d4e86864d3b797aac3", null ],
    [ "PlayBtn_pressed", "class_exercise_anim_view.html#aacbe5501ff817780927b64fb862e9776", null ],
    [ "SetData", "class_exercise_anim_view.html#a347cc73f41ab863142e1b89cb0adf072", null ],
    [ "UnassignBtn_pressed", "class_exercise_anim_view.html#af64a1d7939b0c16d67f9c6694bbe612c", null ],
    [ "AssignPanel", "class_exercise_anim_view.html#ae434e492700b3ac44d9487dbe1bea5c1", null ],
    [ "DataPanel", "class_exercise_anim_view.html#a5f81e16ed909d4e1843d73242b162019", null ],
    [ "daysPerWeek", "class_exercise_anim_view.html#a37f77b9693efbadf7fb6ff8fdd26afce", null ],
    [ "ExerciseNameTxt", "class_exercise_anim_view.html#ac820df7ba18e23f2ce11b8eeb858f838", null ],
    [ "grade", "class_exercise_anim_view.html#a45635d8c0630e1d2a1ea8dd980adfe0d", null ],
    [ "gradeGO", "class_exercise_anim_view.html#ae7c958fdff0681c61f985a4be734c7b0", null ],
    [ "infoPanel_View", "class_exercise_anim_view.html#a7399b779376607d06697b718630bd9bb", null ],
    [ "notes", "class_exercise_anim_view.html#ae9ef1606e13cb1542d3ba21684a5c36a", null ],
    [ "playBtn", "class_exercise_anim_view.html#a0e201c7440b888ca808ab40498683fa8", null ],
    [ "repetitionsPerSet", "class_exercise_anim_view.html#a0b4023c8b17064c93e087ed030a917b0", null ],
    [ "setsPerDay", "class_exercise_anim_view.html#a988420836e75c975f7879938b310abde", null ]
];