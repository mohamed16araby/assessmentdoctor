var dir_0c26dd0bc4c1415a706938aa4d10aa74 =
[
    [ "3. CustomizePanel", "dir_479f2fab9fa8e050f8ba49462dc6285f.html", "dir_479f2fab9fa8e050f8ba49462dc6285f" ],
    [ "AddEdit_MedicalRecord_Panel_Controller.cs", "_add_edit___medical_record___panel___controller_8cs.html", [
      [ "AddEdit_MedicalRecord_Panel_Controller", "class_add_edit___medical_record___panel___controller.html", "class_add_edit___medical_record___panel___controller" ]
    ] ],
    [ "ChoosingSides_Panel_Controller.cs", "_choosing_sides___panel___controller_8cs.html", [
      [ "ChoosingSides_Panel_Controller", "class_choosing_sides___panel___controller.html", "class_choosing_sides___panel___controller" ]
    ] ],
    [ "ChoosingSubspeciality_Panel_Controller.cs", "_choosing_subspeciality___panel___controller_8cs.html", [
      [ "ChoosingSubspeciality_Panel_Controller", "class_choosing_subspeciality___panel___controller.html", "class_choosing_subspeciality___panel___controller" ]
    ] ],
    [ "MainPanel_Controller.cs", "_main_panel___controller_8cs.html", [
      [ "MainPanel_Controller", "class_main_panel___controller.html", "class_main_panel___controller" ]
    ] ],
    [ "Panel_Controller.cs", "_panel___controller_8cs.html", [
      [ "Panel_Controller", "class_panel___controller.html", "class_panel___controller" ]
    ] ],
    [ "SidePanel_Controller.cs", "_side_panel___controller_8cs.html", [
      [ "SidePanel_Controller", "class_side_panel___controller.html", "class_side_panel___controller" ]
    ] ]
];