var class_youtube_comments =
[
    [ "authorChannelId", "class_youtube_comments.html#a29dfea3c227ccc3d9a2e5129bb968ac7", null ],
    [ "authorChannelUrl", "class_youtube_comments.html#a847ed0105b97b3849c5dc6ad0067e5ca", null ],
    [ "authorDisplayName", "class_youtube_comments.html#a720779b152aa8e66f92a0797ff558d4e", null ],
    [ "authorProfileImageUrl", "class_youtube_comments.html#a705ac036c384cce37a5a96f4e0f361d5", null ],
    [ "canRate", "class_youtube_comments.html#ae102e789a5851ebb1d14b97c2b58bde8", null ],
    [ "likeCount", "class_youtube_comments.html#abafe9d7df099de46b0d0ecb5598995bc", null ],
    [ "publishedAt", "class_youtube_comments.html#a97592c86922961fc73bba0e77a5575ee", null ],
    [ "textDisplay", "class_youtube_comments.html#a97545fcd0b1a17957231f75c82026fca", null ],
    [ "textOriginal", "class_youtube_comments.html#a6c18055da7e6ab03a4f07354bfcd53fa", null ],
    [ "updatedAt", "class_youtube_comments.html#a1af28b08f23f41e307bc65a777bfa32c", null ],
    [ "videoId", "class_youtube_comments.html#a55f6887bba4e62bc86ad9f3732f7faa1", null ],
    [ "viewerRating", "class_youtube_comments.html#a614bef91a8cc691486ddcd5d1e1ba807", null ]
];