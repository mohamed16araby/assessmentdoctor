var class_almost_engine_1_1_screenshot_1_1_screenshot_batch =
[
    [ "ActiveItem", "class_almost_engine_1_1_screenshot_1_1_screenshot_batch_1_1_active_item.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_batch_1_1_active_item" ],
    [ "m_Active", "class_almost_engine_1_1_screenshot_1_1_screenshot_batch.html#a6385e608e7242110c849df93b88442a9", null ],
    [ "m_ActiveCompositions", "class_almost_engine_1_1_screenshot_1_1_screenshot_batch.html#abc30ddc60d56be57e02c739f3ee58528", null ],
    [ "m_ActiveResolutions", "class_almost_engine_1_1_screenshot_1_1_screenshot_batch.html#a9dd1cfe2cfb541de3fc4050b2c2ab9c5", null ],
    [ "m_Name", "class_almost_engine_1_1_screenshot_1_1_screenshot_batch.html#a96b8986675b1bd4190c68ece9de73e83", null ],
    [ "m_OverrideActiveComposer", "class_almost_engine_1_1_screenshot_1_1_screenshot_batch.html#adb2e13b9da4d9eb869d2b66a1e6dca29", null ],
    [ "m_OverrideActiveResolutions", "class_almost_engine_1_1_screenshot_1_1_screenshot_batch.html#aa4020273587a9844a204bb0e6e2b4f82", null ],
    [ "m_PostProcess", "class_almost_engine_1_1_screenshot_1_1_screenshot_batch.html#ae035cada700028e4d23a2fbb952acccb", null ],
    [ "m_PreProcess", "class_almost_engine_1_1_screenshot_1_1_screenshot_batch.html#a047db7fcd75e1f03dea95615a44db005", null ]
];