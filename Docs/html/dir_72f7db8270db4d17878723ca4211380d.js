var dir_72f7db8270db4d17878723ca4211380d =
[
    [ "DoNotFakeAttribute.cs", "_do_not_fake_attribute_8cs.html", [
      [ "DoNotFakeAttribute", "class_beebyte_1_1_obfuscator_1_1_do_not_fake_attribute.html", "class_beebyte_1_1_obfuscator_1_1_do_not_fake_attribute" ]
    ] ],
    [ "ObfuscateLiterals.cs", "_obfuscate_literals_8cs.html", [
      [ "ObfuscateLiteralsAttribute", "class_beebyte_1_1_obfuscator_1_1_obfuscate_literals_attribute.html", "class_beebyte_1_1_obfuscator_1_1_obfuscate_literals_attribute" ]
    ] ],
    [ "RenameAttribute.cs", "_rename_attribute_8cs.html", [
      [ "RenameAttribute", "class_beebyte_1_1_obfuscator_1_1_rename_attribute.html", "class_beebyte_1_1_obfuscator_1_1_rename_attribute" ]
    ] ],
    [ "ReplaceLiteralsWithName.cs", "_replace_literals_with_name_8cs.html", [
      [ "ReplaceLiteralsWithNameAttribute", "class_beebyte_1_1_obfuscator_1_1_replace_literals_with_name_attribute.html", "class_beebyte_1_1_obfuscator_1_1_replace_literals_with_name_attribute" ]
    ] ],
    [ "SkipAttribute.cs", "_skip_attribute_8cs.html", [
      [ "SkipAttribute", "class_beebyte_1_1_obfuscator_1_1_skip_attribute.html", "class_beebyte_1_1_obfuscator_1_1_skip_attribute" ]
    ] ],
    [ "SkipRenameAttribute.cs", "_skip_rename_attribute_8cs.html", [
      [ "SkipRenameAttribute", "class_beebyte_1_1_obfuscator_1_1_skip_rename_attribute.html", "class_beebyte_1_1_obfuscator_1_1_skip_rename_attribute" ]
    ] ]
];