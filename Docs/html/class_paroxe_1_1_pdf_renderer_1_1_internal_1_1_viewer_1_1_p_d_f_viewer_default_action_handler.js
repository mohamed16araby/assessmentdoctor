var class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_viewer_default_action_handler =
[
    [ "HandleGotoAction", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_viewer_default_action_handler.html#a0c72b2c424f6a6c83c80b1294e04035c", null ],
    [ "HandleLaunchAction", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_viewer_default_action_handler.html#a4b8d61e1503c77e8448927be1d5b6ace", null ],
    [ "HandleRemoteGotoActionPasswordResolving", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_viewer_default_action_handler.html#a2b20065a2a75b3221755c6d5e96b3fe8", null ],
    [ "HandleRemoteGotoActionPathResolving", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_viewer_default_action_handler.html#a1bc2cb3975f0cf723f03be89da857615", null ],
    [ "HandleRemoteGotoActionResolved", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_viewer_default_action_handler.html#ab3f3d0c5ae0c6f7f1cc940cf3f088f0d", null ],
    [ "HandleRemoteGotoActionUnresolved", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_viewer_default_action_handler.html#ac52ed02931c34971b832218af3c70b9a", null ],
    [ "HandleUnsuportedAction", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_viewer_default_action_handler.html#acf1e5d6c877657bb42c7cf241831e98c", null ],
    [ "HandleUriAction", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_viewer_default_action_handler.html#af915a9dab7d761b2e2adf09cd2606bac", null ]
];