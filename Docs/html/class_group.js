var class_group =
[
    [ "Group", "class_group.html#a2cf2831ba943ab07f90f361d2122fa9d", null ],
    [ "Group", "class_group.html#ae453b0a4e71f88dff99a68b55164d6b0", null ],
    [ "CanGoBackInPanels", "class_group.html#a71d1e60a4e8e6646071dd5e85fcb7b0c", null ],
    [ "CanGoNextInPanels", "class_group.html#a8c60cb0722ed1768f47a6b9ba4d7e0c4", null ],
    [ "Clone", "class_group.html#a578f6800132471250337eb008fe3d612", null ],
    [ "Merge", "class_group.html#a70be2f3a5451b1b1695bb8ab9b9c5fa9", null ],
    [ "displayOrder", "class_group.html#a307e177a5e92bdb861a5de9986a11d67", null ],
    [ "lastPanelVisited_index", "class_group.html#aa54cb8d3f4e0ada7a75e0d5f3d69bc5d", null ],
    [ "name", "class_group.html#ac67ce61165b8e9a8668efa58ea2d9a99", null ],
    [ "scoreResult", "class_group.html#ad7d3084d24f060159e4f8b822b9a6f52", null ],
    [ "scoreTotal", "class_group.html#aeeba1818fdac23366262c300dccf38af", null ],
    [ "scoreTracking", "class_group.html#a788bd549ff0a5066c1d3b4a411d94e2b", null ],
    [ "childrenDictionary", "class_group.html#a030edc401137c7d91fbb61f8ed3cc4cb", null ],
    [ "Panels", "class_group.html#adb53cac97872c0f81d824f41a02e743a", null ]
];