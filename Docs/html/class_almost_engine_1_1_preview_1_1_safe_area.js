var class_almost_engine_1_1_preview_1_1_safe_area =
[
    [ "Constraint", "class_almost_engine_1_1_preview_1_1_safe_area.html#a090a248292e5e044750a6e1d6acebfe1", [
      [ "NONE", "class_almost_engine_1_1_preview_1_1_safe_area.html#a090a248292e5e044750a6e1d6acebfe1ab50339a10e1de285ac99d4c3990b8693", null ],
      [ "SNAP", "class_almost_engine_1_1_preview_1_1_safe_area.html#a090a248292e5e044750a6e1d6acebfe1a9bd645c2ef58737f53ff32e6c927049a", null ],
      [ "PUSH", "class_almost_engine_1_1_preview_1_1_safe_area.html#a090a248292e5e044750a6e1d6acebfe1a73dabe4437725eedc05a1824a2c31550", null ],
      [ "ENLARGE", "class_almost_engine_1_1_preview_1_1_safe_area.html#a090a248292e5e044750a6e1d6acebfe1a0ee2ddbd689998f41c3a7ff39c274faa", null ]
    ] ],
    [ "HorizontalConstraint", "class_almost_engine_1_1_preview_1_1_safe_area.html#a1afcfb71c098069660dca975cbfae623", [
      [ "LEFT", "class_almost_engine_1_1_preview_1_1_safe_area.html#a1afcfb71c098069660dca975cbfae623a684d325a7303f52e64011467ff5c5758", null ],
      [ "RIGHT", "class_almost_engine_1_1_preview_1_1_safe_area.html#a1afcfb71c098069660dca975cbfae623a21507b40c80068eda19865706fdc2403", null ],
      [ "LEFT_AND_RIGHT", "class_almost_engine_1_1_preview_1_1_safe_area.html#a1afcfb71c098069660dca975cbfae623abe4e1597f0737d20c0f7d37f3da9494f", null ]
    ] ],
    [ "VerticalConstraint", "class_almost_engine_1_1_preview_1_1_safe_area.html#a0aa7daf25a6873d616360b5dcb4d20b5", [
      [ "UP", "class_almost_engine_1_1_preview_1_1_safe_area.html#a0aa7daf25a6873d616360b5dcb4d20b5afbaedde498cdead4f2780217646e9ba1", null ],
      [ "DOWN", "class_almost_engine_1_1_preview_1_1_safe_area.html#a0aa7daf25a6873d616360b5dcb4d20b5ac4e0e4e3118472beeb2ae75827450f1f", null ],
      [ "UP_AND_DOWN", "class_almost_engine_1_1_preview_1_1_safe_area.html#a0aa7daf25a6873d616360b5dcb4d20b5ad4015bf50ef42b0a6d6488c8899f3d0f", null ]
    ] ],
    [ "ApplySafeArea", "class_almost_engine_1_1_preview_1_1_safe_area.html#a74c1b92a970262df0de10fc7942ef3de", null ],
    [ "Restore", "class_almost_engine_1_1_preview_1_1_safe_area.html#aa0a2c3d8a93be494bda46159eee34d79", null ],
    [ "m_DefaultAnchorMax", "class_almost_engine_1_1_preview_1_1_safe_area.html#af3232dc2958a58da0b212142353d1e9f", null ],
    [ "m_DefaultAnchorMin", "class_almost_engine_1_1_preview_1_1_safe_area.html#aedde1243d21285fc027e2dc06be6d3c2", null ],
    [ "m_HorizontalConstraint", "class_almost_engine_1_1_preview_1_1_safe_area.html#a5343e0bfc2321f76bb7cc927cad14fd1", null ],
    [ "m_HorizontalConstraintType", "class_almost_engine_1_1_preview_1_1_safe_area.html#aae13848e66530f5031555cbf35332d8c", null ],
    [ "m_Panel", "class_almost_engine_1_1_preview_1_1_safe_area.html#a9941bd8c39a82e77a97668751476d450", null ],
    [ "m_VerticalConstraint", "class_almost_engine_1_1_preview_1_1_safe_area.html#a61e89fd9df02b8932f1144e2100aa27c", null ],
    [ "m_VerticalConstraintType", "class_almost_engine_1_1_preview_1_1_safe_area.html#ad7d82aeea527e93cf5a85ef4e7773418", null ]
];