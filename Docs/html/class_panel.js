var class_panel =
[
    [ "Panel", "class_panel.html#a2bb0095860962aa666d96b4a95f8d2e0", null ],
    [ "Panel", "class_panel.html#a91633bfad7b6d90aa644421fa905de63", null ],
    [ "CanGoBackInSections", "class_panel.html#af7937a442779cf032be9aba4727a1313", null ],
    [ "CanGoNextInSections", "class_panel.html#a99e42ba1004262f18aad45ba5e8c53fb", null ],
    [ "Clone", "class_panel.html#a6b909284ca2f9234099b3038508aa2ae", null ],
    [ "Merge", "class_panel.html#a1da6649ae609f5a6752847e697433eff", null ],
    [ "displayOrder", "class_panel.html#aa1aa7c6324b37acaac93cd79cfe7a167", null ],
    [ "hideTitle", "class_panel.html#a4fac95962d7b5d4c0f381c30e9b3c082", null ],
    [ "lastSectionVisited_index", "class_panel.html#a8ecc06030d88a299a3debdc25edea973", null ],
    [ "name", "class_panel.html#a6b8b27d145dee78b331b507efc405542", null ],
    [ "sectionScoreResult", "class_panel.html#acd0a497e442680ab5b10d8c2c1019a5c", null ],
    [ "sectionScoreTotal", "class_panel.html#a1a76bd185be6e707aec3cb8752416572", null ],
    [ "childrenDictionary", "class_panel.html#ab9375e45deebd28d875f5a3742d9312d", null ],
    [ "Sections", "class_panel.html#ae03de5e740437addd9e398ea5db79b76", null ]
];