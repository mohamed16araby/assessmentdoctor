var class_electro_therapy___algorithms___view =
[
    [ "GetCellView", "class_electro_therapy___algorithms___view.html#ad4c2e06d35268fbd37858634eef834b4", null ],
    [ "GetCellViewSize", "class_electro_therapy___algorithms___view.html#af4c5592b5d9051656e7959c8661f6cf6", null ],
    [ "GetNumberOfCells", "class_electro_therapy___algorithms___view.html#ae35b7e9560ad953e87c90357a1c0c619", null ],
    [ "ValueChangeCheck", "class_electro_therapy___algorithms___view.html#aa1d5999c5cc959ed3ebcdbfba499de84", null ],
    [ "currentSelected_AlgorithmName", "class_electro_therapy___algorithms___view.html#a531271521e0ccc37bddb4fac1d4f91b2", null ],
    [ "manualTherapy_Item_ViewPrefab", "class_electro_therapy___algorithms___view.html#a4b0cb5cbbec471f0a5e8f3e7dac66784", null ],
    [ "scroller", "class_electro_therapy___algorithms___view.html#a331e604e0c6fac184cebda1c51ca8cd5", null ],
    [ "SearchField", "class_electro_therapy___algorithms___view.html#ac0e7c80555aafac68f89ecc335da0754", null ],
    [ "UpdateExercisesWithAlgorithmSelected", "class_electro_therapy___algorithms___view.html#a7ac50876af62c708c8e1be9f11d5f12a", null ]
];