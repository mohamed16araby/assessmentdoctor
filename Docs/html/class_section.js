var class_section =
[
    [ "Section", "class_section.html#a41d661fe2eb7d9178f05cfc9cdd97c1b", null ],
    [ "Section", "class_section.html#a1c0b1df8803fd19611f2d972ab6d67fd", null ],
    [ "CanGoBackInSides", "class_section.html#a41501f10f68f68055fece2e27c6b225c", null ],
    [ "CanGoNextInSides", "class_section.html#a28aa5a9dd97c3eb9f56e907bd95a59a9", null ],
    [ "Clone", "class_section.html#ac6f6a22cf453d9dc4cb9cc17d01676cb", null ],
    [ "Merge", "class_section.html#a13885689c59470faee10078c724ced83", null ],
    [ "displayOrder", "class_section.html#a53f71ae5269bc4c3ec57b82e87ed28e3", null ],
    [ "hideTitle", "class_section.html#a9ff079c41c18836968399b1891eb246f", null ],
    [ "hideTitleInCustomizationPanel", "class_section.html#ac97e656da573fd59ff5d0604d316f49f", null ],
    [ "lastSideVisited_index", "class_section.html#a65afda3bfd501336c8e968f562c10145", null ],
    [ "name", "class_section.html#a6134b0af899288ea97f91e82c58be057", null ],
    [ "childrenDictionary", "class_section.html#a3874834ca68fda6c097e52403d6aba9d", null ],
    [ "Sides", "class_section.html#a9ddc8d08dae3020355943a3a458fdb44", null ]
];