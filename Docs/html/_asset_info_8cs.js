var _asset_info_8cs =
[
    [ "AssetKind", "_asset_info_8cs.html#a5afb925dd436726e1d7f38654bad3848", [
      [ "Regular", "_asset_info_8cs.html#a5afb925dd436726e1d7f38654bad3848ad2203cb1237cb6460cbad94564e39345", null ],
      [ "Settings", "_asset_info_8cs.html#a5afb925dd436726e1d7f38654bad3848af4f70727dc34561dfde1a3c529b6205c", null ],
      [ "FromPackage", "_asset_info_8cs.html#a5afb925dd436726e1d7f38654bad3848aaacf5de11da41fba90f85bda8064572f", null ],
      [ "Unsupported", "_asset_info_8cs.html#a5afb925dd436726e1d7f38654bad3848ab4080bdf74febf04d578ff105cce9d3f", null ]
    ] ],
    [ "AssetSettingsKind", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acd", [
      [ "NotSettings", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acda854890a1d5f6b9dd220e64fb604c1f73", null ],
      [ "AudioManager", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acda0748fe3df831c76264c2cd2e95b8a7c9", null ],
      [ "ClusterInputManager", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acda443027a5e271ea98f85f70bc69dc6284", null ],
      [ "DynamicsManager", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acda5af2b8817ffc734ad475514e763a0608", null ],
      [ "EditorBuildSettings", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acdadf2665a2e678ef09692bc54015a29bc6", null ],
      [ "EditorSettings", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acda1a6aec6afdc50d087284f4fde56ad6be", null ],
      [ "GraphicsSettings", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acdaa74242e5f13b3da7af1893f74deb2d3f", null ],
      [ "InputManager", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acda6a54d3dfcca57870ca73192b4a75c7a4", null ],
      [ "NavMeshAreas", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acda433d067415451baa293bd3549a18b864", null ],
      [ "NavMeshLayers", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acdaf18fd3de662131a66c95dc64d8f24254", null ],
      [ "NavMeshProjectSettings", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acdac8c7d7c83441b2651496cd572f5ba30b", null ],
      [ "NetworkManager", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acdaf844a4c804cb8404e0b00daaebc093c8", null ],
      [ "Physics2DSettings", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acda951c2176ccf369c353b84ead4fccaa0f", null ],
      [ "ProjectSettings", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acdafe88d09ae00638383cf7ae01424b39c0", null ],
      [ "PresetManager", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acdaa2c4b90829da7cb742489d906adf9631", null ],
      [ "QualitySettings", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acda4f7f082338af641c925adf57ac68bc8e", null ],
      [ "TagManager", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acdafaa93052f134b808c9775b27aa1668bd", null ],
      [ "TimeManager", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acdafe289f6d626c599f44c7d2ae7585d600", null ],
      [ "UnityAdsSettings", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acdad08f94ee59f347c872d63ee556827ed1", null ],
      [ "UnityConnectSettings", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acdac416e41e0771ac8a91f98a92d40d1d5c", null ],
      [ "VFXManager", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acda788e133076932ffbdc1c5383b569937c", null ],
      [ "Unknown", "_asset_info_8cs.html#a35f9eeca5a5831dbcd7aa84af1b83acda88183b946cc5f0e8c96b2e66e1c74a7e", null ]
    ] ]
];