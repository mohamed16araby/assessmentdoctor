var namespace_k_k_speech =
[
    [ "AddSpeechFrameworkOniOS", "class_k_k_speech_1_1_add_speech_framework_oni_o_s.html", "class_k_k_speech_1_1_add_speech_framework_oni_o_s" ],
    [ "LanguageOption", "struct_k_k_speech_1_1_language_option.html", "struct_k_k_speech_1_1_language_option" ],
    [ "SetSpeechRecognitionPermissionsOniOS", "class_k_k_speech_1_1_set_speech_recognition_permissions_oni_o_s.html", "class_k_k_speech_1_1_set_speech_recognition_permissions_oni_o_s" ],
    [ "SpeechRecognitionLanguageDropdown", "class_k_k_speech_1_1_speech_recognition_language_dropdown.html", "class_k_k_speech_1_1_speech_recognition_language_dropdown" ],
    [ "SpeechRecognitionOptions", "struct_k_k_speech_1_1_speech_recognition_options.html", "struct_k_k_speech_1_1_speech_recognition_options" ],
    [ "SpeechRecognizer", "class_k_k_speech_1_1_speech_recognizer.html", "class_k_k_speech_1_1_speech_recognizer" ],
    [ "SpeechRecognizerListener", "class_k_k_speech_1_1_speech_recognizer_listener.html", "class_k_k_speech_1_1_speech_recognizer_listener" ]
];