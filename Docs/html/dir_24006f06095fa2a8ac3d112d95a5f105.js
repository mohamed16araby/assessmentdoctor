var dir_24006f06095fa2a8ac3d112d95a5f105 =
[
    [ "IssuesFinderPersonalSettings.cs", "_issues_finder_personal_settings_8cs.html", [
      [ "IssuesFinderPersonalSettings", "class_code_stage_1_1_maintainer_1_1_settings_1_1_issues_finder_personal_settings.html", "class_code_stage_1_1_maintainer_1_1_settings_1_1_issues_finder_personal_settings" ]
    ] ],
    [ "ProjectCleanerPersonalSettings.cs", "_project_cleaner_personal_settings_8cs.html", [
      [ "ProjectCleanerPersonalSettings", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_personal_settings.html", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_personal_settings" ]
    ] ],
    [ "ReferencesFinderPersonalSettings.cs", "_references_finder_personal_settings_8cs.html", [
      [ "ReferencesFinderPersonalSettings", "class_code_stage_1_1_maintainer_1_1_settings_1_1_references_finder_personal_settings.html", "class_code_stage_1_1_maintainer_1_1_settings_1_1_references_finder_personal_settings" ]
    ] ],
    [ "UserSettings.cs", "_user_settings_8cs.html", [
      [ "UserSettings", "class_code_stage_1_1_maintainer_1_1_settings_1_1_user_settings.html", "class_code_stage_1_1_maintainer_1_1_settings_1_1_user_settings" ]
    ] ]
];