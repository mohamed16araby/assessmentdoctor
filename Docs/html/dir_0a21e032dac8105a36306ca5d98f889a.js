var dir_0a21e032dac8105a36306ca5d98f889a =
[
    [ "User", "dir_24006f06095fa2a8ac3d112d95a5f105.html", "dir_24006f06095fa2a8ac3d112d95a5f105" ],
    [ "IssuesFinderSettings.cs", "_issues_finder_settings_8cs.html", [
      [ "IssuesFinderSettings", "class_code_stage_1_1_maintainer_1_1_settings_1_1_issues_finder_settings.html", "class_code_stage_1_1_maintainer_1_1_settings_1_1_issues_finder_settings" ]
    ] ],
    [ "ProjectCleanerSettings.cs", "_project_cleaner_settings_8cs.html", [
      [ "ProjectCleanerSettings", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings.html", "class_code_stage_1_1_maintainer_1_1_settings_1_1_project_cleaner_settings" ]
    ] ],
    [ "ProjectSettings.cs", "_project_settings_8cs.html", "_project_settings_8cs" ],
    [ "ReferencesFinderSettings.cs", "_references_finder_settings_8cs.html", [
      [ "ReferencesFinderSettings", "class_code_stage_1_1_maintainer_1_1_settings_1_1_references_finder_settings.html", "class_code_stage_1_1_maintainer_1_1_settings_1_1_references_finder_settings" ]
    ] ]
];