var class_recording_canvas =
[
    [ "OnAuthorizationStatusFetched", "class_recording_canvas.html#a2ca66641d7e64d7bc0ae31134a07d6db", null ],
    [ "OnAvailabilityChange", "class_recording_canvas.html#abb132d425304c825f8e07acc1786e4f1", null ],
    [ "OnEndOfSpeech", "class_recording_canvas.html#a90ef924c743f05579c6b8cd79615b04f", null ],
    [ "OnError", "class_recording_canvas.html#afa05fe7e9e95f6a6cd7b5d5e60bf154d", null ],
    [ "OnFinalResult", "class_recording_canvas.html#aa4e17b20cced926d266848980c0bd5a3", null ],
    [ "OnPartialResult", "class_recording_canvas.html#ae0139ad319ceb35ff82eb79b8c3876cf", null ],
    [ "OnStartRecordingPressed", "class_recording_canvas.html#a0c19b0f7682fd16424348570e6ef6831", null ],
    [ "resultText", "class_recording_canvas.html#aa56dd2ff43259f67af5d83aeaea508ab", null ],
    [ "startRecordingButton", "class_recording_canvas.html#a5c9b859340aa2f334e3208c4856d98db", null ]
];