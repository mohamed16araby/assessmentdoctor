var struct_assigned_exercise_data =
[
    [ "daysPerWeek", "struct_assigned_exercise_data.html#a9e05aa4875fa00de393a0d66108b1ffd", null ],
    [ "exercise_id", "struct_assigned_exercise_data.html#a415717e8ecc439ba7af9daf85e38db5b", null ],
    [ "exercise_name", "struct_assigned_exercise_data.html#a28631450f5e6db2378e23469deb96d2c", null ],
    [ "grade", "struct_assigned_exercise_data.html#a89011077c9f2b11628159c254c11681e", null ],
    [ "hasGrades", "struct_assigned_exercise_data.html#ab8022c1dd49dcb06aa672a04d95b5a12", null ],
    [ "notes", "struct_assigned_exercise_data.html#afb07ddd9c26174fa46c9d3545cf983b5", null ],
    [ "repetitionsPerSet", "struct_assigned_exercise_data.html#afdf6c8d77a44feedb81ca66c0f48b51e", null ],
    [ "setsPerDay", "struct_assigned_exercise_data.html#a453d4b4baa6964e73de05da14dc6f7eb", null ]
];