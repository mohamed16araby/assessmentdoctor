var dir_ec9aaf4633e16761472269036133e5d4 =
[
    [ "Gallery", "dir_8dfb080db872a5533820b2810ba62e38.html", "dir_8dfb080db872a5533820b2810ba62e38" ],
    [ "ChangeLanguageProcess.cs", "_change_language_process_8cs.html", [
      [ "ChangeLanguageProcess", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_change_language_process.html", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_change_language_process" ]
    ] ],
    [ "HideOnCapture.cs", "_hide_on_capture_8cs.html", [
      [ "HideOnCapture", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_hide_on_capture.html", null ]
    ] ],
    [ "MessageCanvas.cs", "_message_canvas_8cs.html", [
      [ "MessageCanvas", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_message_canvas.html", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_message_canvas" ]
    ] ],
    [ "RequestAuthAtStartup.cs", "_request_auth_at_startup_8cs.html", [
      [ "RequestAuthAtStartup", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_request_auth_at_startup.html", null ]
    ] ],
    [ "RotateScreenshot.cs", "_rotate_screenshot_8cs.html", [
      [ "RotateScreenshot", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_rotate_screenshot.html", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_rotate_screenshot" ]
    ] ],
    [ "ScreenshotCutter.cs", "_screenshot_cutter_8cs.html", [
      [ "ScreenshotCutter", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_screenshot_cutter.html", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_screenshot_cutter" ]
    ] ],
    [ "ShowScreenshotThumbnail.cs", "_show_screenshot_thumbnail_8cs.html", [
      [ "ShowScreenshotThumbnail", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_show_screenshot_thumbnail.html", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_show_screenshot_thumbnail" ]
    ] ],
    [ "TakeScreenshotButton.cs", "_take_screenshot_button_8cs.html", [
      [ "TakeScreenshotButton", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_take_screenshot_button.html", null ]
    ] ],
    [ "ValidationCanvas.cs", "_validation_canvas_8cs.html", [
      [ "ValidationCanvas", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_validation_canvas.html", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_validation_canvas" ]
    ] ]
];