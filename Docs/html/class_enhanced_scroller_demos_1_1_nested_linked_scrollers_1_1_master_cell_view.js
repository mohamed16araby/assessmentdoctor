var class_enhanced_scroller_demos_1_1_nested_linked_scrollers_1_1_master_cell_view =
[
    [ "GetCellView", "class_enhanced_scroller_demos_1_1_nested_linked_scrollers_1_1_master_cell_view.html#afee65b3aea84ee2810b8a4e64f154718", null ],
    [ "GetCellViewSize", "class_enhanced_scroller_demos_1_1_nested_linked_scrollers_1_1_master_cell_view.html#a8c13be8dd38aecd7209fac8694048b27", null ],
    [ "GetNumberOfCells", "class_enhanced_scroller_demos_1_1_nested_linked_scrollers_1_1_master_cell_view.html#ab95eeb7db368d742882c313bb048c67e", null ],
    [ "RefreshCellView", "class_enhanced_scroller_demos_1_1_nested_linked_scrollers_1_1_master_cell_view.html#aa125717bb91b968baf888a4994ae691e", null ],
    [ "SetData", "class_enhanced_scroller_demos_1_1_nested_linked_scrollers_1_1_master_cell_view.html#a12597ef92bb85c595300c9cfe18ae119", null ],
    [ "detailCellViewPrefab", "class_enhanced_scroller_demos_1_1_nested_linked_scrollers_1_1_master_cell_view.html#a997cf363bf32783345027c1d35663d28", null ],
    [ "detailScroller", "class_enhanced_scroller_demos_1_1_nested_linked_scrollers_1_1_master_cell_view.html#a4596e29b0d3464dc990779b0d3cbbde6", null ],
    [ "detailScrollerScrolledDelegate", "class_enhanced_scroller_demos_1_1_nested_linked_scrollers_1_1_master_cell_view.html#ad6bcfeaadeee204cb0a6744b8ef09bbe", null ]
];