var dir_ac3cbc888c1264fda6504870bd1d44db =
[
    [ "AddEdit_MedicalRecord_Panel_Manager.cs", "_add_edit___medical_record___panel___manager_8cs.html", [
      [ "AddEdit_MedicalRecord_Panel_Manager", "class_add_edit___medical_record___panel___manager.html", "class_add_edit___medical_record___panel___manager" ]
    ] ],
    [ "AssessPanels_Manager.cs", "_assess_panels___manager_8cs.html", "_assess_panels___manager_8cs" ],
    [ "ChoosingSides_Panel_Manager.cs", "_choosing_sides___panel___manager_8cs.html", [
      [ "ChoosingSides_Panel_Manager", "class_choosing_sides___panel___manager.html", "class_choosing_sides___panel___manager" ]
    ] ],
    [ "ChoosingSubspeciality_Panel_Manager.cs", "_choosing_subspeciality___panel___manager_8cs.html", [
      [ "ChoosingSubspeciality_Panel_Manager", "class_choosing_subspeciality___panel___manager.html", "class_choosing_subspeciality___panel___manager" ]
    ] ],
    [ "CustomizePanels_Manager.cs", "_customize_panels___manager_8cs.html", [
      [ "CustomizePanels_Manager", "class_customize_panels___manager.html", "class_customize_panels___manager" ]
    ] ],
    [ "Manager.cs", "_manager_8cs.html", "_manager_8cs" ]
];