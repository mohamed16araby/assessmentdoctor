var class_paroxe_1_1_pdf_renderer_1_1_p_d_f_action =
[
    [ "ActionType", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_action.html#a778cd6351aa0bcc576e76ca6716b44c3", [
      [ "Unsupported", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_action.html#a778cd6351aa0bcc576e76ca6716b44c3ab4080bdf74febf04d578ff105cce9d3f", null ],
      [ "GoTo", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_action.html#a778cd6351aa0bcc576e76ca6716b44c3a4b4078b45fb374165cb88d1e9f57953d", null ],
      [ "RemoteGoTo", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_action.html#a778cd6351aa0bcc576e76ca6716b44c3a88a00730e5b06bb8187d159171ee371e", null ],
      [ "Uri", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_action.html#a778cd6351aa0bcc576e76ca6716b44c3a3840cd8f73026713059f0ed0562c5493", null ],
      [ "Launch", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_action.html#a778cd6351aa0bcc576e76ca6716b44c3a9506f0fd0f7f1b07960b15b4c9e68d1a", null ],
      [ "Unknown", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_action.html#a778cd6351aa0bcc576e76ca6716b44c3a88183b946cc5f0e8c96b2e66e1c74a7e", null ]
    ] ],
    [ "PDFAction", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_action.html#a2ba1953e6b2c347a3ded4d93b8f7a1a7", null ],
    [ "PDFAction", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_action.html#a9215665ceec97b596628749553498db2", null ],
    [ "Dispose", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_action.html#a29aae5ec01352bf4240ea7ccc01be8a5", null ],
    [ "Dispose", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_action.html#a8513f97a0050e9b20c8db812b33d5868", null ],
    [ "GetActionType", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_action.html#a2f967485ab885c856c3c9e9d15ec061a", null ],
    [ "GetDest", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_action.html#aaf4586e770ae7630d4b28d55bc4ea271", null ],
    [ "GetFilePath", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_action.html#a1c89d236eebe15c063ebbcbc586ffa4e", null ],
    [ "GetURIPath", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_action.html#aa25ede673e755644c960d6ade3bc8c92", null ],
    [ "Document", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_action.html#a407c8aa1fa392d89840a4fbacfdc9c52", null ],
    [ "NativePointer", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_action.html#afec789c3ec513493fe7f885f9fbf9cc1", null ],
    [ "Source", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_action.html#a0eae4c34a85acab92fe429a9ce055e9d", null ]
];