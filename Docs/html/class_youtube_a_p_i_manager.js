var class_youtube_a_p_i_manager =
[
    [ "YoutubeSafeSearchFilter", "class_youtube_a_p_i_manager.html#adfbbbbd5cdb3b8a2bf7f1a9fb8c3ce4a", [
      [ "none", "class_youtube_a_p_i_manager.html#adfbbbbd5cdb3b8a2bf7f1a9fb8c3ce4aa334c4a4c42fdb79d7ebc3e73b517e6f8", null ],
      [ "moderate", "class_youtube_a_p_i_manager.html#adfbbbbd5cdb3b8a2bf7f1a9fb8c3ce4aa7f0217cdcdd58ba86aae84d9d3d79f81", null ],
      [ "strict", "class_youtube_a_p_i_manager.html#adfbbbbd5cdb3b8a2bf7f1a9fb8c3ce4aa2133fd717402a7966ee88d06f9e0b792", null ]
    ] ],
    [ "YoutubeSearchOrderFilter", "class_youtube_a_p_i_manager.html#aedee78a3838e0289ee67ab3222514ee3", [
      [ "none", "class_youtube_a_p_i_manager.html#aedee78a3838e0289ee67ab3222514ee3a334c4a4c42fdb79d7ebc3e73b517e6f8", null ],
      [ "date", "class_youtube_a_p_i_manager.html#aedee78a3838e0289ee67ab3222514ee3a5fc732311905cb27e82d67f4f6511f7f", null ],
      [ "rating", "class_youtube_a_p_i_manager.html#aedee78a3838e0289ee67ab3222514ee3a2c5504ab9a86164db22a92dc8793843d", null ],
      [ "relevance", "class_youtube_a_p_i_manager.html#aedee78a3838e0289ee67ab3222514ee3a54e03c3ed1f796105f0062b9f689449f", null ],
      [ "title", "class_youtube_a_p_i_manager.html#aedee78a3838e0289ee67ab3222514ee3ad5d3db1765287eef77d7927cc956f50a", null ],
      [ "videoCount", "class_youtube_a_p_i_manager.html#aedee78a3838e0289ee67ab3222514ee3a1af16c5ceaa7b9ef58f9316c14241167", null ],
      [ "viewCount", "class_youtube_a_p_i_manager.html#aedee78a3838e0289ee67ab3222514ee3a997f68f3783661f6699c366794b428c9", null ]
    ] ],
    [ "GetChannelVideos", "class_youtube_a_p_i_manager.html#aae14f8a30b223ce197be6fc85f7bde43", null ],
    [ "GetComments", "class_youtube_a_p_i_manager.html#a5786aa56b91b3546fbdf8668548d6dda", null ],
    [ "GetPlaylistItems", "class_youtube_a_p_i_manager.html#ad77fa6358f138bba7c44e52ca795d8aa", null ],
    [ "GetVideoData", "class_youtube_a_p_i_manager.html#a1b8f651466b899b113568dd95a7b250b", null ],
    [ "Search", "class_youtube_a_p_i_manager.html#abaebdcbe16855fe2974f01889a570cbc", null ],
    [ "SearchByCategory", "class_youtube_a_p_i_manager.html#a7ca14837e5018e283ee500e57e548baa", null ],
    [ "SearchByLocation", "class_youtube_a_p_i_manager.html#aad6774234dbb544b952440d233354e27", null ],
    [ "SearchForChannels", "class_youtube_a_p_i_manager.html#abf319705eb929d10676f6170dd0ff0bd", null ],
    [ "TrendingVideos", "class_youtube_a_p_i_manager.html#a6e32d81663e1fff1fe5675176c1d30e1", null ]
];