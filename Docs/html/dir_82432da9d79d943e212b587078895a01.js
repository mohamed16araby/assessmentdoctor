var dir_82432da9d79d943e212b587078895a01 =
[
    [ "PDFProgressiveSearch.cs", "_p_d_f_progressive_search_8cs.html", [
      [ "PDFProgressiveSearch", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_progressive_search.html", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_progressive_search" ]
    ] ],
    [ "PDFSearchHandle.cs", "_p_d_f_search_handle_8cs.html", [
      [ "PDFSearchHandle", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_handle.html", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_handle" ]
    ] ],
    [ "PDFSearchResult.cs", "_p_d_f_search_result_8cs.html", [
      [ "PDFSearchResult", "struct_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_result.html", "struct_paroxe_1_1_pdf_renderer_1_1_p_d_f_search_result" ]
    ] ],
    [ "PDFTextPage.cs", "_p_d_f_text_page_8cs.html", [
      [ "PDFTextPage", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_text_page.html", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_text_page" ]
    ] ]
];