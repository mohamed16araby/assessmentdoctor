var class_algorithm___model =
[
    [ "AlgorithmLoaded", "class_algorithm___model.html#a317c1f73b226e0d4fa9abd4996e7f270", null ],
    [ "DeleteAlgorithm_OnClick", "class_algorithm___model.html#ae1c69318b6376dcfb3e6a806cfd1b689", null ],
    [ "Init", "class_algorithm___model.html#abc84b1546197709a8d8d11153b9f49d2", null ],
    [ "LoadAlgorithm_OnClick", "class_algorithm___model.html#a7558f1d293cdbd71b95f4ef143c91dd2", null ],
    [ "LoadAlgorithmsFromDatabase", "class_algorithm___model.html#ad803b4987045a98810a5904c47930021", null ],
    [ "ModelInitializationEvent", "class_algorithm___model.html#a434e058a5c454bd0e778b6644a7da3d9", null ],
    [ "OperationCallBack", "class_algorithm___model.html#ab624615b5fe751c4ee0c05fafebde1c2", null ],
    [ "SaveAlgorithm_OnClick", "class_algorithm___model.html#a958b223a7cd5755198cdd4c434d2f6f2", null ],
    [ "algorithmFilesNames", "class_algorithm___model.html#a4bc9ebfc2a482cbd4abfbab33440d403", null ],
    [ "currentSelectedItems", "class_algorithm___model.html#aed8b80d9175fdeac339af01fe867b302", null ],
    [ "patientLastVisit", "class_algorithm___model.html#ab84bf9ef267b8f6cd4f106807d9c3415", null ],
    [ "On_AlgorithmsLoadedEvent", "class_algorithm___model.html#ace33aa5922b795671bc62c16b9edb0de", null ],
    [ "On_LastVisitLoadedEvent", "class_algorithm___model.html#a9a0b4a7bf67cca97e45d26333784b57a", null ],
    [ "On_Save_OperationCallBackEvent", "class_algorithm___model.html#aa74f0870d7fdbd1c8949fba5de7d8f0d", null ]
];