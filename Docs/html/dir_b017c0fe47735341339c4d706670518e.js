var dir_b017c0fe47735341339c4d706670518e =
[
    [ "Category.cs", "_category_8cs.html", [
      [ "Category", "class_category.html", "class_category" ]
    ] ],
    [ "Disease.cs", "_disease_8cs.html", [
      [ "Disease", "class_disease.html", "class_disease" ]
    ] ],
    [ "EntryField.cs", "_entry_field_8cs.html", [
      [ "EntryField", "class_entry_field.html", "class_entry_field" ]
    ] ],
    [ "Group.cs", "_group_8cs.html", [
      [ "ITransit", "interface_i_transit.html", null ],
      [ "Group", "class_group.html", "class_group" ]
    ] ],
    [ "JsonHelper.cs", "_json_helper_8cs.html", [
      [ "JsonHelper", "class_json_helper.html", "class_json_helper" ]
    ] ],
    [ "Panel.cs", "_panel_8cs.html", [
      [ "Panel", "class_panel.html", "class_panel" ]
    ] ],
    [ "Section.cs", "_section_8cs.html", [
      [ "Section", "class_section.html", "class_section" ]
    ] ],
    [ "SerializableField.cs", "_serializable_field_8cs.html", [
      [ "SerializableField", "class_serializable_field.html", "class_serializable_field" ]
    ] ],
    [ "Side.cs", "_side_8cs.html", [
      [ "Side", "class_side.html", "class_side" ]
    ] ],
    [ "Specialization.cs", "_specialization_8cs.html", [
      [ "Specialization", "class_specialization.html", "class_specialization" ]
    ] ]
];