var _issue_kind_8cs =
[
    [ "IssueKind", "_issue_kind_8cs.html#ac1a138cc40807aa968f7fa0b94091cd6", [
      [ "MissingComponent", "_issue_kind_8cs.html#ac1a138cc40807aa968f7fa0b94091cd6a660a6f66d056fbc7524cfe13c9bbdd82", null ],
      [ "DuplicateComponent", "_issue_kind_8cs.html#ac1a138cc40807aa968f7fa0b94091cd6ae42addb3ff31ce7288f5ff5e03ef6db5", null ],
      [ "MissingReference", "_issue_kind_8cs.html#ac1a138cc40807aa968f7fa0b94091cd6a38cd352b226b9b49eaea0826dfbc8c2a", null ],
      [ "MissingPrefab", "_issue_kind_8cs.html#ac1a138cc40807aa968f7fa0b94091cd6abede4bcd7b8684e62808129df5fb413a", null ],
      [ "UnnamedLayer", "_issue_kind_8cs.html#ac1a138cc40807aa968f7fa0b94091cd6a2ad8325b8d8d1549c7a969941c3bea78", null ],
      [ "HugePosition", "_issue_kind_8cs.html#ac1a138cc40807aa968f7fa0b94091cd6ac9a5714ebc9d7eb412a1f8b61c105845", null ],
      [ "InconsistentTerrainData", "_issue_kind_8cs.html#ac1a138cc40807aa968f7fa0b94091cd6ace2384f7ace045c708239d56a426e378", null ],
      [ "DuplicateLayers", "_issue_kind_8cs.html#ac1a138cc40807aa968f7fa0b94091cd6a8c01f67bef241854cb1588844f718f26", null ],
      [ "Error", "_issue_kind_8cs.html#ac1a138cc40807aa968f7fa0b94091cd6a902b0d55fddef6f8d651fe1035b7d4bd", null ],
      [ "Other", "_issue_kind_8cs.html#ac1a138cc40807aa968f7fa0b94091cd6a6311ae17c1ee52b36e68aaf4ad066387", null ]
    ] ]
];