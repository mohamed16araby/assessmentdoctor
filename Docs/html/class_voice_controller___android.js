var class_voice_controller___android =
[
    [ "AddSetOfPhrases", "class_voice_controller___android.html#a5c821822509d7b2e92310528f4dec0ef", null ],
    [ "AskForPermissions", "class_voice_controller___android.html#a2a06784b763e05778b48a284ff9ab8f5", null ],
    [ "OnAuthorizationStatusFetched", "class_voice_controller___android.html#ac6ec5094a047833714323260ddfb2f75", null ],
    [ "OnAvailabilityChange", "class_voice_controller___android.html#af6c37a7e0082b30da73ef093b8b008a1", null ],
    [ "OnEndOfSpeech", "class_voice_controller___android.html#a4f5db8b8ffebd72dcbf26258bec4758e", null ],
    [ "OnError", "class_voice_controller___android.html#a5d070afddfda5d10d2cd5e86eac678dc", null ],
    [ "OnFinalResult", "class_voice_controller___android.html#a824ffc469dd1726f2ddc032916a0b2e5", null ],
    [ "OnPartialResult", "class_voice_controller___android.html#a7f232ec4ffe5477fa55ee6610db8a87a", null ],
    [ "RestartRecognizer", "class_voice_controller___android.html#a30dd70467f7edec61beb6b6bbefd941c", null ],
    [ "UpdatePhraseAction", "class_voice_controller___android.html#aa930822f4edf15bde61241a4015daf10", null ]
];