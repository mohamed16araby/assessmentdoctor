var class_youtube_subtitles_reader =
[
    [ "YoutubeSubtitlesReader", "class_youtube_subtitles_reader.html#af9647f4052c58d4f9734c562e94c8bca", null ],
    [ "LoadSubtitle", "class_youtube_subtitles_reader.html#ae06dd34dbdc04d2638e5d27d7b3c0625", null ],
    [ "ParseStream", "class_youtube_subtitles_reader.html#ab2ad24898064a7aaaa92617819402bdb", null ],
    [ "currentTextLine", "class_youtube_subtitles_reader.html#a9f1ab71e290099dddc6a59c67f14e638", null ],
    [ "langCode", "class_youtube_subtitles_reader.html#ab9774376e389920301082393bc025f55", null ],
    [ "uiSubtitle", "class_youtube_subtitles_reader.html#a35e7fdba4b96303a6450431aef86ce65", null ],
    [ "videoID", "class_youtube_subtitles_reader.html#a266a7d93df91523884faebce90c69947", null ],
    [ "videoPlayer", "class_youtube_subtitles_reader.html#a31e90953005535c9129184a2dcdb93b3", null ]
];