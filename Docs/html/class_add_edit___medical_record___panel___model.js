var class_add_edit___medical_record___panel___model =
[
    [ "AddEdit_MedicalRecord_Panel_Model", "class_add_edit___medical_record___panel___model.html#afdb9abd840ed69c993a9022aaa024d81", null ],
    [ "GetSessionsDatesFromFirebase", "class_add_edit___medical_record___panel___model.html#a78ab1dc46b18ac464de6a6480967dc1a", null ],
    [ "SetData", "class_add_edit___medical_record___panel___model.html#afab0acde13fd3187e5559cde655180f6", null ],
    [ "keyValueOfSelectedChoiceUnderneath", "class_add_edit___medical_record___panel___model.html#a6ae2c8faf46c74c55388e9f0769241de", null ],
    [ "lastFollowupDate", "class_add_edit___medical_record___panel___model.html#a37dd24b6cfce75ae016d38dad58720b2", null ],
    [ "performAnotherAssessmentChoiceChoosen", "class_add_edit___medical_record___panel___model.html#a5798401ad90f9af0ffae7c7130834dd8", null ],
    [ "performAnotherAssessmentChoices", "class_add_edit___medical_record___panel___model.html#a281226450cb22d6bafcacd20629816be", null ],
    [ "sessionIDwithDate", "class_add_edit___medical_record___panel___model.html#a2c8a77fa8881e6c664b00f37a95feb61", null ],
    [ "LastSession_Id", "class_add_edit___medical_record___panel___model.html#ae6260466218c780a5db14e9e01af53fe", null ],
    [ "OnPanelDataChanged", "class_add_edit___medical_record___panel___model.html#a2047aa03fdb126391b1ac33d1640b119", null ]
];