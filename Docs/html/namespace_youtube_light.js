var namespace_youtube_light =
[
    [ "Downloader", "class_youtube_light_1_1_downloader.html", "class_youtube_light_1_1_downloader" ],
    [ "VideoInfo", "class_youtube_light_1_1_video_info.html", "class_youtube_light_1_1_video_info" ],
    [ "VideoNotAvailableException", "class_youtube_light_1_1_video_not_available_exception.html", "class_youtube_light_1_1_video_not_available_exception" ],
    [ "YoutubeParseException", "class_youtube_light_1_1_youtube_parse_exception.html", "class_youtube_light_1_1_youtube_parse_exception" ]
];