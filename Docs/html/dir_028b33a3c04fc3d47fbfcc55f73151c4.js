var dir_028b33a3c04fc3d47fbfcc55f73151c4 =
[
    [ "ExtraFeatures", "dir_ec9aaf4633e16761472269036133e5d4.html", "dir_ec9aaf4633e16761472269036133e5d4" ],
    [ "Utils", "dir_5115ff5e2abe5c978274b63bee9a496b.html", "dir_5115ff5e2abe5c978274b63bee9a496b" ],
    [ "MultiDisplayCameraCapture.cs", "_multi_display_camera_capture_8cs.html", [
      [ "MultiDisplayCameraCapture", "class_almost_engine_1_1_screenshot_1_1_multi_display_camera_capture.html", "class_almost_engine_1_1_screenshot_1_1_multi_display_camera_capture" ]
    ] ],
    [ "ScreenshotBatch.cs", "_screenshot_batch_8cs.html", [
      [ "ScreenshotBatch", "class_almost_engine_1_1_screenshot_1_1_screenshot_batch.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_batch" ],
      [ "ActiveItem", "class_almost_engine_1_1_screenshot_1_1_screenshot_batch_1_1_active_item.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_batch_1_1_active_item" ]
    ] ],
    [ "ScreenshotCamera.cs", "_screenshot_camera_8cs.html", [
      [ "ScreenshotCamera", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera" ],
      [ "Settings", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera_1_1_settings.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_camera_1_1_settings" ]
    ] ],
    [ "ScreenshotComposer.cs", "_screenshot_composer_8cs.html", [
      [ "ScreenshotComposer", "class_almost_engine_1_1_screenshot_1_1_screenshot_composer.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_composer" ]
    ] ],
    [ "ScreenshotComposition.cs", "_screenshot_composition_8cs.html", [
      [ "ScreenshotComposition", "class_almost_engine_1_1_screenshot_1_1_screenshot_composition.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_composition" ]
    ] ],
    [ "ScreenshotConfig.cs", "_screenshot_config_8cs.html", [
      [ "ScreenshotConfig", "class_almost_engine_1_1_screenshot_1_1_screenshot_config.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_config" ]
    ] ],
    [ "ScreenshotManager.cs", "_screenshot_manager_8cs.html", [
      [ "ScreenshotManager", "class_almost_engine_1_1_screenshot_1_1_screenshot_manager.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_manager" ]
    ] ],
    [ "ScreenshotNameParser.cs", "_screenshot_name_parser_8cs.html", [
      [ "ScreenshotNameParser", "class_almost_engine_1_1_screenshot_1_1_screenshot_name_parser.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_name_parser" ]
    ] ],
    [ "ScreenshotOverlay.cs", "_screenshot_overlay_8cs.html", [
      [ "ScreenshotOverlay", "class_almost_engine_1_1_screenshot_1_1_screenshot_overlay.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_overlay" ],
      [ "Settings", "class_almost_engine_1_1_screenshot_1_1_screenshot_overlay_1_1_settings.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_overlay_1_1_settings" ]
    ] ],
    [ "ScreenshotProcess.cs", "_screenshot_process_8cs.html", [
      [ "ScreenshotProcess", "class_almost_engine_1_1_screenshot_1_1_screenshot_process.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_process" ]
    ] ],
    [ "ScreenshotResolution.cs", "_screenshot_resolution_8cs.html", [
      [ "ScreenshotResolution", "class_almost_engine_1_1_screenshot_1_1_screenshot_resolution.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_resolution" ]
    ] ],
    [ "ScreenshotTaker.cs", "_screenshot_taker_8cs.html", [
      [ "ScreenshotTaker", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker" ]
    ] ],
    [ "SimpleScreenshotCapture.cs", "_simple_screenshot_capture_8cs.html", [
      [ "SimpleScreenshotCapture", "class_almost_engine_1_1_screenshot_1_1_simple_screenshot_capture.html", "class_almost_engine_1_1_screenshot_1_1_simple_screenshot_capture" ]
    ] ],
    [ "UltimateScreenshotCreator.cs", "_ultimate_screenshot_creator_8cs.html", [
      [ "UltimateScreenshotCreator", "class_almost_engine_1_1_screenshot_1_1_ultimate_screenshot_creator.html", "class_almost_engine_1_1_screenshot_1_1_ultimate_screenshot_creator" ]
    ] ]
];