var class_select_patient_controller =
[
    [ "BackBtn_pressed", "class_select_patient_controller.html#a5029d3c0e994ac97438e6bac1b14e892", null ],
    [ "ClosePrompt", "class_select_patient_controller.html#a6bffc4a4b58be386eccee82fc3e59d81", null ],
    [ "GetCellView", "class_select_patient_controller.html#a6b5140de044cde85a595560be05df8b1", null ],
    [ "GetCellViewSize", "class_select_patient_controller.html#ab1f9c3d441d5c2d6703bb75815ebd588", null ],
    [ "GetNumberOfCells", "class_select_patient_controller.html#a57b63b97345fbae401a6f315b0438319", null ],
    [ "GetPatients", "class_select_patient_controller.html#a46adcbfedf583752721ca02615b6a9ca", null ],
    [ "NextBtn_pressed", "class_select_patient_controller.html#aad32b62cc0d0c5489f9f12e877766d3d", null ],
    [ "OpenAddPatientPanel", "class_select_patient_controller.html#ac3f4e43fa39c650082f20c12a6e1183a", null ],
    [ "OpenPrompt", "class_select_patient_controller.html#a3d4fe9109c1cd82ae460f4b80675643e", null ],
    [ "ValueChangeCheck", "class_select_patient_controller.html#a77cc6e2ecc977e0178c109c58ca74c3a", null ],
    [ "addBtn", "class_select_patient_controller.html#a79e77958cc1216adfb2584d817e76730", null ],
    [ "addPatientView", "class_select_patient_controller.html#ac7c7be4b5e5d13ffe8205c5754b1e723", null ],
    [ "backButton", "class_select_patient_controller.html#ab8051e25af6ea9c0874a1cc3f7f02335", null ],
    [ "nextButton", "class_select_patient_controller.html#af1a3989e7fe61222d5b96e99df211823", null ],
    [ "PatientsScroller", "class_select_patient_controller.html#a8c455ca3a0654cad2da1a5817aa6be53", null ],
    [ "PatientsViewPrefab", "class_select_patient_controller.html#ab48ea14b15435f196d1fe1931c0daac1", null ],
    [ "popupOpener", "class_select_patient_controller.html#aac6177fb8f60837ee13791b6a36cb83f", null ],
    [ "SearchField", "class_select_patient_controller.html#a2f0cf2f443c6e7e125c1528c4aa9102b", null ]
];