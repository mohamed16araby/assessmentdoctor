var class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_page_range =
[
    [ "ContainsPage", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_page_range.html#ab2f212333ca3e96fb8bc7b7f77e9117c", null ],
    [ "Equals", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_page_range.html#ac3e6c224c6e2711d7651ecdfd12b7542", null ],
    [ "GetHashCode", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_page_range.html#aa8a648e4a50351b14ae56304784a48a9", null ],
    [ "GetPagesToload", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_page_range.html#ae0ac1463f20cd278bbd5c107b290d965", null ],
    [ "GetPagesToUnload", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_page_range.html#a601e1cf29ace5775e79e6f3bd77c3552", null ],
    [ "UpdatePageAgainstRanges", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_page_range.html#a3ff70f82805937ae279b4fcbe328c67f", null ],
    [ "m_From", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_page_range.html#ab8f50a8cefb78ff1e1f57ddf15e8be37", null ],
    [ "m_To", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_page_range.html#a02d5917ebd63844c4c366490324e7598", null ],
    [ "IsValid", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_page_range.html#a15b9f13cfeeb60e9f7affcb73740eed7", null ],
    [ "RangeLength", "class_paroxe_1_1_pdf_renderer_1_1_internal_1_1_viewer_1_1_p_d_f_page_range.html#a5533f12dbaf1555c46c83358664e973f", null ]
];