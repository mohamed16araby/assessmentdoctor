var class_youtube_player_1_1_youtube_result_ids =
[
    [ "audioUrl", "class_youtube_player_1_1_youtube_result_ids.html#aa68c819936cfe23da45064b2ba037c96", null ],
    [ "bestFormatWithAudioIncluded", "class_youtube_player_1_1_youtube_result_ids.html#a44ac3827061e503accd6f687d066b8d5", null ],
    [ "fullHdQuality", "class_youtube_player_1_1_youtube_result_ids.html#aef69bd16565196856750dcf122a09a07", null ],
    [ "hdQuality", "class_youtube_player_1_1_youtube_result_ids.html#a4e87db17c3f7bbfcf4280132380b982b", null ],
    [ "lowQuality", "class_youtube_player_1_1_youtube_result_ids.html#a1ffdae3bd7aab2ce30681922d96fa526", null ],
    [ "mediumQuality", "class_youtube_player_1_1_youtube_result_ids.html#a8fbd3183f05771d85e2a24f0fb99f822", null ],
    [ "standardQuality", "class_youtube_player_1_1_youtube_result_ids.html#a82770a9612649fd182395f80f770bc88", null ],
    [ "ultraHdQuality", "class_youtube_player_1_1_youtube_result_ids.html#a42f336af8dd3c53603dbf0e791bd22a2", null ]
];