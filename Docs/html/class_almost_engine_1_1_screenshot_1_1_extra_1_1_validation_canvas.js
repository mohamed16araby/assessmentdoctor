var class_almost_engine_1_1_screenshot_1_1_extra_1_1_validation_canvas =
[
    [ "Capture", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_validation_canvas.html#ae4f4707689bf130d0ffa5d182d14e3ec", null ],
    [ "OnCaptureEndDelegate", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_validation_canvas.html#a707e6bba662bab8af304171b2dce17f9", null ],
    [ "OnDiscardCallback", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_validation_canvas.html#ad270f27f249a314324e87008a68c8ddd", null ],
    [ "OnSaveCallback", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_validation_canvas.html#a860b05217b5abe86a98e3ac0e19ffcaa", null ],
    [ "m_Canvas", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_validation_canvas.html#ae9d2c5eda5123c5598a7821fef44ca25", null ],
    [ "m_ImageContainer", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_validation_canvas.html#a756bdcc836e6a2601220fab667e3799f", null ],
    [ "m_ScreenshotManager", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_validation_canvas.html#ae471d276c517535d116e9f0002673d94", null ],
    [ "m_Texture", "class_almost_engine_1_1_screenshot_1_1_extra_1_1_validation_canvas.html#a8ca781228f3b47ced5c3aa51b08ead1c", null ]
];