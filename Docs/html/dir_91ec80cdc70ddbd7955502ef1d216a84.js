var dir_91ec80cdc70ddbd7955502ef1d216a84 =
[
    [ "JsonHierarchy", "dir_b017c0fe47735341339c4d706670518e.html", "dir_b017c0fe47735341339c4d706670518e" ],
    [ "DictionaryConverter.cs", "_dictionary_converter_8cs.html", [
      [ "DictionaryConverter", "class_dictionary_converter.html", "class_dictionary_converter" ]
    ] ],
    [ "EntryFieldJSONConverter.cs", "_entry_field_j_s_o_n_converter_8cs.html", [
      [ "EntryFieldJSONConverter", "class_entry_field_j_s_o_n_converter.html", "class_entry_field_j_s_o_n_converter" ]
    ] ],
    [ "IgnoredAttribute.cs", "_ignored_attribute_8cs.html", [
      [ "IgnoredAttribute", "class_ignored_attribute.html", null ]
    ] ],
    [ "IgnoreForDiseaseAttribute.cs", "_ignore_for_disease_attribute_8cs.html", [
      [ "IgnoreForDiseaseAttribute", "class_ignore_for_disease_attribute.html", null ]
    ] ],
    [ "IgnoreForPatientAttribute.cs", "_ignore_for_patient_attribute_8cs.html", [
      [ "IgnoreForPatientAttribute", "class_ignore_for_patient_attribute.html", null ]
    ] ],
    [ "PropertyIgnoreSerializerContractResolver.cs", "_property_ignore_serializer_contract_resolver_8cs.html", "_property_ignore_serializer_contract_resolver_8cs" ]
];