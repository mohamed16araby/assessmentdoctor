var class_json_net_sample_1_1_product =
[
    [ "Equals", "class_json_net_sample_1_1_product.html#a04dfe3be874c5fb86525df8339491850", null ],
    [ "GetHashCode", "class_json_net_sample_1_1_product.html#acea1314ce6b9761ec98997a5a7fd2c0a", null ],
    [ "ExpiryDate", "class_json_net_sample_1_1_product.html#a56fa060e7736456e02475c37a13baf2c", null ],
    [ "Name", "class_json_net_sample_1_1_product.html#ab94ac096568a4f5da662a3267f701f95", null ],
    [ "Price", "class_json_net_sample_1_1_product.html#ad73f1ac10548f102f5ef8c9dc9ab943c", null ],
    [ "Sizes", "class_json_net_sample_1_1_product.html#ad63153a9885e99aafba5fa0b5b787d00", null ]
];