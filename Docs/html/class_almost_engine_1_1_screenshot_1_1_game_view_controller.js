var class_almost_engine_1_1_screenshot_1_1_game_view_controller =
[
    [ "GetCurrentGameViewSize", "class_almost_engine_1_1_screenshot_1_1_game_view_controller.html#a35dae30fa44d4dd8c84bf8ffa2b4beb0", null ],
    [ "RestoreGameViewSize", "class_almost_engine_1_1_screenshot_1_1_game_view_controller.html#a2d93af9fed505bb433b8a5e4bea57be2", null ],
    [ "SaveCurrentGameViewSize", "class_almost_engine_1_1_screenshot_1_1_game_view_controller.html#a01013e2fe6156213c92a83bdb6f89ce9", null ],
    [ "SetGameViewSize", "class_almost_engine_1_1_screenshot_1_1_game_view_controller.html#af07e6c308616bda7319ec98726dd725c", null ]
];