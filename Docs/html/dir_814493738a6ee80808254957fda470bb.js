var dir_814493738a6ee80808254957fda470bb =
[
    [ "Example", "dir_aca1c2d031f630293f4fdab8cd791764.html", "dir_aca1c2d031f630293f4fdab8cd791764" ],
    [ "SpeechRecognizer.cs", "_speech_recognizer_8cs.html", "_speech_recognizer_8cs" ],
    [ "SpeechRecognizerListener.cs", "_speech_recognizer_listener_8cs.html", [
      [ "SpeechRecognizerListener", "class_k_k_speech_1_1_speech_recognizer_listener.html", "class_k_k_speech_1_1_speech_recognizer_listener" ],
      [ "AuthorizationCallback", "class_k_k_speech_1_1_speech_recognizer_listener_1_1_authorization_callback.html", null ],
      [ "ResultCallback", "class_k_k_speech_1_1_speech_recognizer_listener_1_1_result_callback.html", null ],
      [ "AvailabilityCallback", "class_k_k_speech_1_1_speech_recognizer_listener_1_1_availability_callback.html", null ],
      [ "ErrorCallback", "class_k_k_speech_1_1_speech_recognizer_listener_1_1_error_callback.html", null ],
      [ "SupportedLanguagesCallback", "class_k_k_speech_1_1_speech_recognizer_listener_1_1_supported_languages_callback.html", null ]
    ] ]
];