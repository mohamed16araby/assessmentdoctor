var dir_82978eb5ffe81c3c6693060268a93c9d =
[
    [ "Assembly", "dir_73ca4254294cd0bcda4a12b252249e6d.html", "dir_73ca4254294cd0bcda4a12b252249e6d" ],
    [ "FileBackup.cs", "_file_backup_8cs.html", [
      [ "FileBackup", "class_beebyte_1_1_obfuscator_1_1_file_backup.html", "class_beebyte_1_1_obfuscator_1_1_file_backup" ]
    ] ],
    [ "ObfuscatorExample.cs", "_obfuscator_example_8cs.html", [
      [ "ObfuscatorExample", "class_obfuscator_example.html", "class_obfuscator_example" ],
      [ "MyClass", "class_my_namespace_1_1_my_class.html", null ],
      [ "MyOtherClass", "class_my_namespace_1_1_my_other_class.html", null ]
    ] ],
    [ "ObfuscatorMenuExample.cs", "_obfuscator_menu_example_8cs.html", null ],
    [ "OptionsManager.cs", "_options_manager_8cs.html", [
      [ "OptionsManager", "class_beebyte_1_1_obfuscator_1_1_options_manager.html", "class_beebyte_1_1_obfuscator_1_1_options_manager" ]
    ] ],
    [ "PipelineHook.cs", "_pipeline_hook_8cs.html", [
      [ "PipelineHook", "class_beebyte_1_1_obfuscator_1_1_pipeline_hook.html", "class_beebyte_1_1_obfuscator_1_1_pipeline_hook" ],
      [ "RestorationStatic", "class_beebyte_1_1_obfuscator_1_1_restoration_static.html", null ]
    ] ],
    [ "Postbuild.cs", "_postbuild_8cs.html", [
      [ "Postbuild", "class_beebyte_1_1_obfuscator_1_1_postbuild.html", null ]
    ] ],
    [ "Project.cs", "_project_8cs.html", [
      [ "Project", "class_beebyte_1_1_obfuscator_1_1_project.html", "class_beebyte_1_1_obfuscator_1_1_project" ]
    ] ],
    [ "Restore.cs", "_restore_8cs.html", [
      [ "RestoreUtils", "class_beebyte_1_1_obfuscator_1_1_restore_utils.html", "class_beebyte_1_1_obfuscator_1_1_restore_utils" ]
    ] ]
];