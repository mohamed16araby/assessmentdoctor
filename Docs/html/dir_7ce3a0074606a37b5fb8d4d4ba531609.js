var dir_7ce3a0074606a37b5fb8d4d4ba531609 =
[
    [ "MenuItem", "dir_2ec77f5fe63b8e59dd623b920cb9ee37.html", "dir_2ec77f5fe63b8e59dd623b920cb9ee37" ],
    [ "Presets", "dir_9fa4b6c5922009960dea529566a7a431.html", "dir_9fa4b6c5922009960dea529566a7a431" ],
    [ "DevicePreviewAssetInspector.cs", "_device_preview_asset_inspector_8cs.html", [
      [ "PreviewConfigAssetInspector", "class_almost_engine_1_1_preview_1_1_preview_config_asset_inspector.html", "class_almost_engine_1_1_preview_1_1_preview_config_asset_inspector" ]
    ] ],
    [ "PPISimulation.cs", "_p_p_i_simulation_8cs.html", [
      [ "PPISimulation", "class_almost_engine_1_1_preview_1_1_p_p_i_simulation.html", null ]
    ] ],
    [ "PreviewConfigAsset.cs", "_preview_config_asset_8cs.html", [
      [ "PreviewConfigAsset", "class_almost_engine_1_1_preview_1_1_preview_config_asset.html", "class_almost_engine_1_1_preview_1_1_preview_config_asset" ]
    ] ],
    [ "ResolutionGalleryWindow.cs", "_resolution_gallery_window_8cs.html", null ],
    [ "ResolutionPreviewWindow.cs", "_resolution_preview_window_8cs.html", null ],
    [ "ResolutionSettingsWindow.cs", "_resolution_settings_window_8cs.html", [
      [ "ResolutionSettingsWindow", "class_almost_engine_1_1_preview_1_1_resolution_settings_window.html", "class_almost_engine_1_1_preview_1_1_resolution_settings_window" ]
    ] ],
    [ "ResolutionWindowBase.cs", "_resolution_window_base_8cs.html", null ],
    [ "SafeAreaInspector.cs", "_safe_area_inspector_8cs.html", [
      [ "SafeAreaInspector", "class_almost_engine_1_1_preview_1_1_safe_area_inspector.html", "class_almost_engine_1_1_preview_1_1_safe_area_inspector" ]
    ] ]
];