var class_youtube_content_details =
[
    [ "ageRestrict", "class_youtube_content_details.html#a8e46511e4fda9f4a88f8d217ef6a8bd5", null ],
    [ "caption", "class_youtube_content_details.html#a10234ed073f4a9db3537c6b28c863f7f", null ],
    [ "definition", "class_youtube_content_details.html#a82c167844ea0873bcd49457e8e4d5752", null ],
    [ "dimension", "class_youtube_content_details.html#a3da1e4871ec79a5ff396b36cca20f96a", null ],
    [ "duration", "class_youtube_content_details.html#a3405e74cecfe31964ea84c56ba76faee", null ],
    [ "licensedContent", "class_youtube_content_details.html#a201982e9796b436dd988d5a85051eb6e", null ],
    [ "projection", "class_youtube_content_details.html#aa41bcf4c3ff73386ddadc42ff4801e6d", null ]
];