var class_almost_engine_1_1_screenshot_1_1_screenshot_taker =
[
    [ "CaptureMode", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a400e8bc619ad4c3d93ccf386aeadd964", [
      [ "GAMEVIEW_RESIZING", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a400e8bc619ad4c3d93ccf386aeadd964a38f01f00e60a8f189495e779a93fe97d", null ],
      [ "RENDER_TO_TEXTURE", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a400e8bc619ad4c3d93ccf386aeadd964ae1d095243543fbc5c64b6b58e1bface1", null ],
      [ "FIXED_GAMEVIEW", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a400e8bc619ad4c3d93ccf386aeadd964acf26385bd747abb701d1816b02c81489", null ]
    ] ],
    [ "ColorFormat", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a34ba9ddc3bd5263a3b8ce748bc8f1f48", [
      [ "RGB", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a34ba9ddc3bd5263a3b8ce748bc8f1f48a889574aebacda6bfd3e534e2b49b8028", null ],
      [ "RGBA", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a34ba9ddc3bd5263a3b8ce748bc8f1f48aea3495a278957dc58165e48a8945469f", null ]
    ] ],
    [ "GameViewResizingWaitingMode", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a5c26f35b9b0c799d783d213a9c0d2aa5", [
      [ "FRAMES", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a5c26f35b9b0c799d783d213a9c0d2aa5a036429f7a46a17439207d188e46ca840", null ],
      [ "TIME", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a5c26f35b9b0c799d783d213a9c0d2aa5a346ff32eaa3c09983fb2ec057816d352", null ]
    ] ],
    [ "ApplySettings", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a482407920fd7a3092eda714340e7f91b", null ],
    [ "CaptureAllCoroutine", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#ab08339275809900c25cc8f0a2deece87", null ],
    [ "CaptureCamerasToTextureCoroutine", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a27b93c3ee75b9e5868ffc9cacb13dca6", null ],
    [ "CaptureResolutionCoroutine", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a8bcd647acb3402b5ff18d95dbfe40ee4", null ],
    [ "CaptureScreenToTextureCoroutine", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#af5c4ea8b02a326aee5e822fc8d08cd80", null ],
    [ "CaptureToTextureCoroutine", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a09a092dfe49e70e6daa1aa31365b3eac", null ],
    [ "ClearCache", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#ae002fdcb092b132e70931119974b0121", null ],
    [ "CopyScreenToTexture", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a7070ddd10e7d7830ca12c44d00fe2da7", null ],
    [ "RenderCamerasToTexture", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a85056cc02e90982a3686ebfdd001a491", null ],
    [ "Reset", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a760e1dd67968124ed8a742c0ced46d4c", null ],
    [ "RestoreCameraSettings", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a3f887380b5c78a32e531f00ae60f92f6", null ],
    [ "RestoreOverlaySettings", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#abe72ee24426ce67a47a1d1e2aeef37a1", null ],
    [ "m_GameViewResizingWaitingFrames", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a5565e4a17c4453fe599bd41cf6e84652", null ],
    [ "m_GameViewResizingWaitingMode", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#ab1aad31cf995d1f05d5d81accc7e9d9c", null ],
    [ "m_GameViewResizingWaitingTime", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#af48ab934397511b2a8d5c8e795df96e8", null ],
    [ "m_IsRunning", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a70ab07920d651fe1b3575e3629768d42", null ],
    [ "m_Texture", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#adb7f3bd10d6eb94b00a09248e6e156e3", null ],
    [ "onResolutionScreenResizedDelegate", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a70daaffc31679a30dcb4f72d2ac2f02e", null ],
    [ "onResolutionUpdateEndDelegate", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a556996e87cbaa18c5c78a2d898c739df", null ],
    [ "onResolutionUpdateStartDelegate", "class_almost_engine_1_1_screenshot_1_1_screenshot_taker.html#a0ce8154b2395ee947e84f3fd4566f7e5", null ]
];