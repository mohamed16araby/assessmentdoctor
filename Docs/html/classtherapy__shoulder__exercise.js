var classtherapy__shoulder__exercise =
[
    [ "OrderList", "classtherapy__shoulder__exercise.html#a1a35e795b51591d48f2caacc63ffaa0f", null ],
    [ "ResetData", "classtherapy__shoulder__exercise.html#a85490cabc554b5320729ddc1fc14b773", null ],
    [ "daysPerWeek", "classtherapy__shoulder__exercise.html#ab79ab7df3049c7e4af0080b5942e58a2", null ],
    [ "grade", "classtherapy__shoulder__exercise.html#a0b8fb44b8ea1c1190dbe9b949f096668", null ],
    [ "hasGrades", "classtherapy__shoulder__exercise.html#a82b9a3169a8138ba8053561ac2dd61dc", null ],
    [ "hasSelectedChild", "classtherapy__shoulder__exercise.html#af61bd6eb7a0c075297e9067ea0a815e0", null ],
    [ "id", "classtherapy__shoulder__exercise.html#a477be14011662868b0b594fa51e0dfc4", null ],
    [ "info", "classtherapy__shoulder__exercise.html#a66fa932a5a59aead4158901ef765a298", null ],
    [ "isAssigned", "classtherapy__shoulder__exercise.html#a5a9ceb702c81ba1629ed62f0c3a16776", null ],
    [ "link", "classtherapy__shoulder__exercise.html#a9eefa5912cb0b2abe199ce4c252d7912", null ],
    [ "name", "classtherapy__shoulder__exercise.html#a29817f34ce30a7a0225487c947edc921", null ],
    [ "notes", "classtherapy__shoulder__exercise.html#aae5e01a87cf09d5632a74604e9b75701", null ],
    [ "repetitionsPerSet", "classtherapy__shoulder__exercise.html#aa0630623e9802a4ef20b703268c3a367", null ],
    [ "setsPerDay", "classtherapy__shoulder__exercise.html#a18c0a7eb3ddbaedecf594e052af2e98d", null ],
    [ "sub_exs", "classtherapy__shoulder__exercise.html#af455f136c7272b214b63bd9bebbdb655", null ]
];