var class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page =
[
    [ "PageRotation", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page.html#a8c78d1c7462445f4a36b4534c59812c8", [
      [ "Normal", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page.html#a8c78d1c7462445f4a36b4534c59812c8a960b44c579bc2f6818d2daaf9e4c16f0", null ],
      [ "Rotate90", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page.html#a8c78d1c7462445f4a36b4534c59812c8a1d20b3969ea74725dd1a5b7669d60a98", null ],
      [ "Rotate180", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page.html#a8c78d1c7462445f4a36b4534c59812c8a371980c5d153a94cf022d6b4daa4d34c", null ],
      [ "Rotate270", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page.html#a8c78d1c7462445f4a36b4534c59812c8a59c609399b2fb3956ecac7df34a76c2f", null ]
    ] ],
    [ "PDFPage", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page.html#a5fac2b6ab7d16654a398090de92828cf", null ],
    [ "ConvertPagePositionToUnityUIDevicePosition", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page.html#ad93c1a81c9b198ad0e91075203be1863", null ],
    [ "ConvertPageRectToDeviceRect", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page.html#ac82eb38ed07713e7f7c9375ae844c947", null ],
    [ "ConvertUnityUIDevicePositionToPagePosition", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page.html#a12fa48c321568a41520c6536ddd321fc", null ],
    [ "DeviceToPage", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page.html#adb05b853859fda24f6f746c2d2d6d0d8", null ],
    [ "Dispose", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page.html#acddffc930144d4a71b6c766719cf32be", null ],
    [ "Dispose", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page.html#a3d304c4cc2b10fb537e8a0c5c71a1313", null ],
    [ "Equals", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page.html#ae5b9c18dc9369faf60818f0cc195e6f2", null ],
    [ "GetLinkAtPoint", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page.html#ab7e9916f688bf40a33df41e2c7cfb80b", null ],
    [ "GetLinkAtPoint", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page.html#a5e4ab50d8d464bab3f8ccdcba0ad655e", null ],
    [ "GetPageSize", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page.html#a059938b64fd12669b07bac48a247d9ec", null ],
    [ "GetTextPage", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page.html#ad314cd071d22ea81e9e316f81e3036d0", null ],
    [ "LoadPageAsync", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page.html#a0c16318de97d00e86138b7e7bbde213b", null ],
    [ "PageToDevice", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page.html#a67425c99c3fb040da57108f93eede317", null ],
    [ "Document", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page.html#a64c3b6ae300e791f0a45ae91a8e8081e", null ],
    [ "NativePointer", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page.html#a5de9adb18e8a7b869b6a4a4d58bc9025", null ],
    [ "PageIndex", "class_paroxe_1_1_pdf_renderer_1_1_p_d_f_page.html#a01567b42da8de9faf6ff51b299201bd0", null ]
];