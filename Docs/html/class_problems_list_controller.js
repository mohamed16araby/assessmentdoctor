var class_problems_list_controller =
[
    [ "AddPhrasesToVoice", "class_problems_list_controller.html#a6e3b9694e8db3f9c4ad3f2f0e7a1e86f", null ],
    [ "BackBtn_pressed", "class_problems_list_controller.html#af6840a092640a0b638e3b7437142c77d", null ],
    [ "EnableTransitionButtons", "class_problems_list_controller.html#aa1de5bff0447cffcd98fd8b05020ca9c", null ],
    [ "GetCellView", "class_problems_list_controller.html#a5b56d0996dba7dca4d356d33379016b7", null ],
    [ "GetCellViewSize", "class_problems_list_controller.html#a1c2e93733eeb7c98188777e0afffb1f6", null ],
    [ "GetNumberOfCells", "class_problems_list_controller.html#afa1d39512ab0fc3552f97253b67a0ab8", null ],
    [ "LoadProblemsListData", "class_problems_list_controller.html#af8c021bdaff41166a497e54bd3e76eab", null ],
    [ "NextBtn_pressed", "class_problems_list_controller.html#a007f3aba91e7230cc4206a8421486ba9", null ],
    [ "ProblemsScroller", "class_problems_list_controller.html#a6bf523a8bba1168b267eeb85a56b956c", null ],
    [ "ProblemsViewPrefab", "class_problems_list_controller.html#ae2e70b656801e5340fac5c23e183229b", null ]
];